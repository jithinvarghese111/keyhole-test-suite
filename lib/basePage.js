const { Builder, By, Key, until } = require('selenium-webdriver');
const { describe, it, after, before } = require('mocha');
const config = require('../utils/config');
const util = require('util');
const fs = require('fs');
const writeFile = util.promisify(fs.writeFile);
const chrome = require('selenium-webdriver/chrome');
const edge = require('selenium-webdriver/edge');
const edgedriver = require('edgedriver'); 
var http = require('http');
var url = require('url') ;

let driver, page;

let o = new chrome.Options();
o.addArguments('start-fullscreen');
o.addArguments('disable-infobars');
o.addArguments('ignore-certificate-errors')
o.addArguments('ignore-ssl-errors')
// o.addArguments('headless'); // running test on visual chrome browser
o.setUserPreferences({ credential_enable_service: false });
// let options = new edge.Options();
// let service = new edge.ServiceBuilder(edgedriver.path);
var Page = function () {
    this.driver = new Builder()
        .setChromeOptions(o)
        .forBrowser(config.browser)
        .build();


// var Page = function () {    
//     this.driver = new Builder()
//                 // .forBrowser('MicrosoftEdge')
//                 .setEdgeService(service)
//                 .forBrowser(config.browser)
//                 .build();    

    // visit a webpage
    this.visit = async function (theUrl) {
        return await this.driver.get(theUrl);
    };

    // quit current session
    this.quit = async function () {
        return await this.driver.quit();
    };

    // wait and find a specific element with it's id
    this.findById = async function (id) {
        await this.driver.wait(until.elementLocated(By.id(id)), 50000, 'Looking for element');
        return await this.driver.findElement(By.id(id));
    };

    // wait and find a specific element with it's xpath
    this.findByXPath = async function (xpath) {
        await this.driver.wait(until.elementLocated(By.xpath(xpath)), 50000, 'Looking for element');
        return await this.driver.findElement(By.xpath(xpath));
    };

    // wait and find a specific element with it's name
    this.findByName = async function (name) {
        await this.driver.wait(until.elementLocated(By.name(name)), 50000, 'Looking for element');
        return await this.driver.findElement(By.name(name));
    };

    // wait and find a specific element with it's link text
    this.findByLinkText = async function (name) {
        await this.driver.wait(until.elementLocated(By.linkText(name)), 50000, 'Looking for element');
        return await this.driver.findElement(By.linkText(name));
    };

    // wait and find a specific element with it's css
    this.findByCSS = async function (css) {
        await this.driver.wait(until.elementLocated(By.css(css)), 50000, 'Looking for element');
        return await this.driver.findElement(By.css(css));
    };

    // wait and find a specific elements with it's css
    this.findElementsByCSS = async function (css) {
        await this.driver.wait(until.elementLocated(By.css(css)), 15000, 'Looking for element');
        return await this.driver.findElements(By.css(css));
    };

    // wait and find a specific elements with it's tagName
    this.findByTagName = async function (tagName) {
        await this.driver.wait(until.elementLocated(By.tagName(tagName)), 15000, 'Looking for element');
        return await this.driver.findElements(By.tagName(tagName));
    };
    
    // Clear editable fields
    this.clearFields = async function (type, element) {
        switch (type) {
            case 'css':
                return this.driver.findElement(By.css(element)).clear();
                break;
            default:
                return this.driver.findElement(By.css(element)).clear();
        }
    }

    // fill input web elements
    this.write = async function (el, txt) {
        return await el.sendKeys(txt);
    }

    //scroll to elements
    this.scrollPage = async function (selector) {
        var scrollTo = await this.findByXPath(selector);
        await this.driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
    }

    //move to elements
    this.movetoEle = async function (selector) {
        const actions = this.driver.actions({bridge: true}); 
        let elem = await this.findByCSS(selector); 
        await actions.move({duration:5000,origin:elem,x:0,y:0}).perform();
    }

    //hover on elements
    this.hoveronEle = async function (selector) {
        const actions = this.driver.actions({bridge: true}); 
        let elem = await this.findByXPath(selector); 
        await actions.move({duration:5000,origin:elem,x:0,y:0}).perform();
    }

    //scroll with pixel
    this.scrollPixel = async function (pixel) {
        await this.driver.executeScript("window.scrollBy(0,"+ pixel +")");
    }


    //Check element is present or not
    this.checkElement = async function (selector) {
        var existed = await this.driver.findElement(By.id(selector)).then(function() {
            return true; //it was found
        }, function(err) {
            return false; //element did not exist
        });

        return existed;
    }

    //screenshot
    this.screenshot = async function takeScreenshot(driver, file){
      let image = await this.driver.takeScreenshot()
      await writeFile(file, image, 'base64')
    }

    //Check element is present or not
    this.checkElementByCSS = async function (selector) {
        var existed = await this.driver.findElement(By.css(selector)).then(function() {
            return true; //it was found
        }, function(err) {
            return false; //element did not exist
        });

        return existed;
    }

    //keyboard Enter
    this.keyboardClick = async function (selector) {
        await this.driver.findElement(By.css(selector)).sendKeys(Key.ENTER); 
    }

    //Change format yyyy-mm-dd to dd-mm-yyyy
    this.ChangeFormateDate = async function (oldDate) {
       return oldDate.toString().split("-").reverse().join("-");
    }

    //Change format yyyy-mm-dd to mm-dd-yyyy
    this.ChangeDate = async function (oldDate) {
        var yourdate = oldDate.split("-").reverse();
        var tmp = yourdate[0];
        yourdate[0] = yourdate[1];
        yourdate[1] = tmp;
        yourdate = yourdate.join("-");

        return yourdate;
    }
    // change format Thu 09 2019 into yyyy-mm-dd format
    this.changeformat = async function convert(str) {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
        newdate = [date.getFullYear(), mnth, day].join("-");
      return newdate;
    }

    //get Timestamp
    this.getTimeStamp = async function getTimeStamp(input) {
        var parts = input.trim().split(' ');
        var date = parts[0].split('-');
        var time = (parts[1] ? parts[1] : '00:00:00').split(':');

        // NOTE:: Month: 0 = January - 11 = December.
        var d = new Date(date[0],date[1]-1,date[2],time[0],time[1],time[2]);
        return d.getTime() / 1000;
    }
};

Page.prototype.gotoLanding = async function () {
    await this.driver.manage().window().fullscreen();
    await this.visit(config.base_url);
};

Page.prototype.gotoLogin = async function () {
    await this.gotoLanding();
    await this.driver.sleep(1000);
    var loginBtn = await this.findByLinkText('Log in');
    await loginBtn.click();
};

Page.prototype.gotoRegister = async function () {
    await this.gotoLanding();
    await this.driver.sleep(1000);
    var loginBtn = await this.findByCSS('#menu-item-19229 > a');
    await loginBtn.click();
};

Page.prototype.gotohussainLogin = async function () {
    await this.gotoLanding();
    await this.driver.sleep(3000);
    var signinBtn = await this.findByXPath('//*[@id="sign-in-anchor"]');
    await signinBtn.click();
};

Page.prototype.redirectToDashboard = async function () {
    let email = await this.findByCSS('#login-container > form > label:nth-child(1) > input[type=text]');
    await this.write(email, config.userEmail);

    let password = await this.findByCSS('#login-container > form > label:nth-child(2) > input[type=password]');
    await this.write(password, config.userPass);

    let loginBtn = await this.findById('submit');
    await loginBtn.click();

    let checkEle = await this.checkElementByCSS('#extra-information-form > div.modal-heading > h1');
    if (checkEle) {
        let closeButton = await this.findByCSS('#registration-information__modal-step1 > div > a');
        await closeButton.click();
        await this.driver.sleep(2000);
    }

    let checkElem = await this.checkElementByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__heading > h3');
    if (checkElem) {
        let closeButton = await this.findByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__footer > button.kc-button.kc-button--outline.kc-button--medium.kc-button--textGray2.kc-button--caps.keyjs-closeButton');
        await closeButton.click();
        await this.driver.sleep(3000);
    }

};

Page.prototype.redirectToHTDashboard = async function () {

    // this.driver.manage().setTimeouts({ implicit: 10000, pageLoad: 3000 });
    
    let email = await this.findByCSS('#login-container > form > label:nth-child(1) > input[type=text]');
    await this.write(email, config.userEmail);

    let password = await this.findByCSS('#login-container > form > label:nth-child(2) > input[type=password]');
    await this.write(password, config.userPass);

    let loginBtn = await this.findById('submit');
    await loginBtn.click();
    await this.driver.sleep(2000);
    
    let checkEle = await this.checkElementByCSS('#extra-information-form > div.modal-heading > h1');
    if (checkEle) {
        let closeButton = await this.findByCSS('#registration-information__modal-step1 > div > a');
        await closeButton.click();
        await this.driver.sleep(2000);
    }

    let checkElem = await this.checkElementByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__heading > h3');
    if (checkElem) {
        let closeButton = await this.findByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__footer > button.kc-button.kc-button--outline.kc-button--medium.kc-button--textGray2.kc-button--caps.keyjs-closeButton');
        await closeButton.click();
        await this.driver.sleep(2000);
    }

    let htDashboard = await this.findByXPath('//*[@id="items--restructured"]/a[1]');
    await htDashboard.click();
    await this.driver.sleep(3000);
};

Page.prototype.redirectToAccDashboard = async function () {
    let email = await this.findByCSS('#login-container > form > label:nth-child(1) > input[type=text]');
    await this.write(email, config.userEmail);

    let password = await this.findByCSS('#login-container > form > label:nth-child(2) > input[type=password]');
    await this.write(password, config.userPass);

    let loginBtn = await this.findById('submit');
    await loginBtn.click();
    await this.driver.sleep(3000);

    let checkEle = await this.checkElementByCSS('#extra-information-form > div.modal-heading > h1');
    if (checkEle) {
        let closeButton = await this.findByCSS('#registration-information__modal-step1 > div > a');
        await closeButton.click();
        await this.driver.sleep(2000);
    }
    let checkElem = await this.checkElementByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__heading > h3');
    if (checkElem) {
        let closeButton = await this.findByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__footer > button.kc-button.kc-button--outline.kc-button--medium.kc-button--textGray2.kc-button--caps.keyjs-closeButton');
        await closeButton.click();
        await this.driver.sleep(3000);
    }

    let atDashboard = await this.findByXPath('//*[@id="items--restructured"]/a[2]');
    await atDashboard.click();
    await this.driver.sleep(5000);

};

Page.prototype.redirectToIMDashboard = async function () {
    let email = await this.findByCSS('#login-container > form > label:nth-child(1) > input[type=text]');
    await this.write(email, config.userEmail);

    let password = await this.findByCSS('#login-container > form > label:nth-child(2) > input[type=password]');
    await this.write(password, config.userPass);

    let loginBtn = await this.findById('submit');
    await loginBtn.click();

    let checkElem = await this.checkElementByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__heading > h3');
    if (checkElem) {
        let closeButton = await this.findByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__footer > button.kc-button.kc-button--outline.kc-button--medium.kc-button--textGray2.kc-button--caps.keyjs-closeButton');
        await closeButton.click();
        await this.driver.sleep(3000);
    }

    let imDashboard = await this.findByXPath('//*[@id="items--restructured"]/a[3]');
    await imDashboard.click();
    await this.driver.sleep(6000);
};

Page.prototype.redirectToQTDashboard = async function () {
    let email = await this.findByCSS('#login-container > form > label:nth-child(1) > input[type=text]');
    await this.write(email, config.userEmail);

    let password = await this.findByCSS('#login-container > form > label:nth-child(2) > input[type=password]');
    await this.write(password, config.userPass);

    let loginBtn = await this.findById('submit');
    await loginBtn.click();

    let checkElem = await this.checkElementByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__heading > h3');
    if (checkElem) {
        let closeButton = await this.findByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__footer > button.kc-button.kc-button--outline.kc-button--medium.kc-button--textGray2.kc-button--caps.keyjs-closeButton');
        await closeButton.click();
        await this.driver.sleep(3000);
    }
    let qtDashboard = await this.findByXPath('//*[@id="items--restructured"]/a[4]');
    await qtDashboard.click();
    await this.driver.sleep(5000);
};

Page.prototype.getCurrentURL = async function() {
    var url = '';

    http.createServer(function (req, res) {
        var hostname = req.headers.host;
        var pathname = url.parse(req.url).pathname;

        url = 'https://' + hostname + pathname;

        res.writeHead(200);
        res.end();
    });

    return url;
}

module.exports = Page;