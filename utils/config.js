
const defaultConfig = {
  browser: 'chrome', // chrome or firefox
  environment: 'prod',
  userEmail: 'qa@wdstech.com',
  inviteEmail: 'qa+139@wdstech.com',
  usedEmail: 'qa+02@wdstech.com',
  userPass: 'micr@s@ft123',
  trackerSearch: 'cola',
  atTrackerSearch: 'BMW',
  atInfluencerSearch: 'Wilhelm',
  accountCompany: 'keyhole',
  api_token: 'cfebb9046bf05212',
  accountFirstname: 'qa two',
  usernewPass: 'Micr@s@ft123',
  downloadFolder: 'C:\/Users\/admin\/Downloads'
}

function getDomain(environment) {
  switch (environment) {
    case 'local':
      return 'https://localdev.keyhole.com'
    case 'release':
      return 'https://release.keyhole.com'
    case 'staging':
      return 'https://staging.keyhole.com'
    case 'ui':
      return 'https://ui.keyhole.co'
    case 'prod':
      return 'https://keyhole.co'
    case 'hussain':
      return 'https://hussain.keyhole.com/'
    default:
      return 'https://staging.keyhole.com'
  }
}

module.exports = {
  browser: defaultConfig.browser,
	base_url: getDomain(defaultConfig.environment),
  userEmail: defaultConfig.userEmail,
  userPass: defaultConfig.userPass,
  userType: defaultConfig.userType,
  trackerSearch: defaultConfig.trackerSearch,
  atTrackerSearch: defaultConfig.atTrackerSearch,
  atInfluencerSearch: defaultConfig.atInfluencerSearch,
  downloadPath: defaultConfig.downloadFolder,
  inviteEmail: defaultConfig.inviteEmail,
  usernewPass: defaultConfig.usernewPass,
  usedEmail: defaultConfig.usedEmail,
  accountCompany: defaultConfig.accountCompany,
  accountFirstname: defaultConfig.accountFirstname,
  api_token: defaultConfig.api_token

};