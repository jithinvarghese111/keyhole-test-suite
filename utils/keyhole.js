const Page = require('../lib/basePage');
let plans = ''

module.exports = function() {
    this.goToHashtagTrack = async function(currentUrl) {
        var hash = currentUrl.substring(currentUrl.indexOf('#'));

        if (hash === 'account') {
            let link = await page.findByCSS('a[href$="#hashtag"]');
            await link.click();

            plans = 'Corporate'
        }
        else if (hash === 'hashtag') {
            plans = 'Normal'
        }
        else {
            plans = 'Corporate'
            
            let link = await page.findByCSS('a[href$="#hashtag"]');
            await link.click();
        }
    };

    this.setDateRangeNew = async function(days) {
        // TODO: allow for custom dates
        var allowedDays = [1, 7, 14, 30, 90, 365];

        if (days && allowedDays.indexOf(days) > -1) {
            let dateFocus = await page.findByCSS('.key-trackerHeader__wrapper > div > div > span > div > input');
            await dateFocus.click();
            await driver.sleep(2000);

            let selectRange = await page.findByCSS('.el-picker-panel__sidebar > button:nth-child(' + (allowedDays.indexOf(days) + 1) + ')');
            await selectRange.click();
            await driver.sleep(5000);

            return true;
        }
        else {
            return false;
        }
    };

    this.setDateRange = async function(days) {
        // TODO: allow for custom dates
        var allowedDays = [1, 7, 14, 30, 90, 365];

        if (days && allowedDays.indexOf(days) > -1) {
            let dateFocus = await page.findByCSS('.key-modal__body > span > div > input');
            await dateFocus.click();
            await driver.sleep(2000);

            let selectRange = await page.findByCSS('.el-picker-panel__sidebar > button:nth-child(' + (allowedDays.indexOf(days) + 1) + ')');
            await selectRange.click();
            await driver.sleep(5000);

            return true;
        }
        else {
            return false;
        }
    };

    this.setDateRangeAccount = async function(days) {
        // TODO: allow for custom dates
        var allowedDays = [30, 90, 365];

        if (days && allowedDays.indexOf(days) > -1) {
            let dateFocus = await page.findByCSS('.key-trackerHeader__wrapper > div > div > span > div > input');
            await dateFocus.click();
            await driver.sleep(1000);
            let selectRange = await page.findByCSS('.el-picker-panel__sidebar > button:nth-child('+(allowedDays.indexOf(days) + 1)+')');
            await selectRange.click();
            await driver.sleep(5000);

            return true;
        }
        else {
            return false;
        }
    };

    this.accountTrackerSelect = async function(platStr) {
        var trackerPos = 0;
        var trackers = await page.findElementsByCSS('#account-trackers tr');
        
        for (var t = 1; t <= trackers.length; t++) {
            var icons = await page.findElementsByCSS('#account-trackers tr:nth-child('+t+') .hide-mini-logos i');   

            for (var i = 1; i <= icons.length; i++) {
                var single = await page.findByCSS('#account-trackers tr:nth-child('+t+') .hide-mini-logos i:nth-child('+i+')')
                var classList = await single.getAttribute("class");
                var plat = classList.substr(classList.indexOf(' ')+1).split('-')[1];
                var style = await single.getAttribute('style');

                if (platStr.indexOf(plat) > -1 && style.indexOf('display: inline-block') > -1) {
                    trackerPos = t;
                    break;
                }
            }
        }

        return trackerPos;
    };
}