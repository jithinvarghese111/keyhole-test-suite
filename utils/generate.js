var randomWords = require('random-words');

module.exports = function() {
  this.randomEmail = function() {
    const strValues = 'abcdefghijk123456789'
    let strEmail = ''
    for (let i = 0; i < strValues.length; i++) {
      strEmail += strValues.charAt(Math.round(strValues.length * Math.random()))
    }
    return `${strEmail}@keyhole.test`
  };

  this.randomPassword = function() {
    const strValues = 'ABCDEFGabcdefg1234567890'
    let str = '0Aa'
    for (let i = 0; i < 8; i++) {
      str += strValues.charAt(Math.round(strValues.length * Math.random()))
    }
    return str
  };

  this.randomName = function() {
    const strValues = 'abcdefghijklmnopqrstuvwxyz'
    let str = ''
    for (let i = 0; i < 8; i++) {
      str += strValues.charAt(Math.round(strValues.length * Math.random()))
    }
    return str
  };

  this.randomNumber = function() {
    const strValues = '1234567890'
    let str = ''
    for (let i = 0; i < 10; i++) {
      str += strValues.charAt(Math.round(strValues.length * Math.random()))
    }
    return str
  };

  this.randomNumberFromTo = function(from, to) {
    let random = Math.floor(Math.random() * to) + from;

    return random
  }

  this.randomWord = function() {
    let random = randomWords();

    return random
  }

  this.randomArrayEmails = function() {
    let myArray = ['stemy@wdstech.com', 'sreedevi@wdstech.com']; 
    let rand = myArray[Math.floor(Math.random() * myArray.length)];
    
    return rand

  }

  this.randomInstAccounts = function() {
    let myArray = ['deadofwrite', 'inkandfable', 'true.living', 'weworewhat', 'chillhouse', 'babynative', 'wearelivingart', 'diet_prada', 'collectiveworld', 'americanfailure', 'therow', 'satiregram', 'keen', 'filmforher', 'moss_angeles']; 
    let rand = myArray[Math.floor(Math.random() * myArray.length)];
    
    return rand

  }

}
