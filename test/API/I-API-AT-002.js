/**
 * ### TEST CASE:
 * I-API-AT-002
 *
 * ### TEST TITLE:
 *
 * Retrieve data of optimization
 *
 * ### TEST SUMMARY:
 *
 * Retrieve data of optimization with valid token
 */

const { describe, it, after, before } = require("mocha");
const Page = require('../../lib/basePage');
const addContext = require("mochawesome/addContext");
const chai = require("chai");
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");
const config = require('../../utils/config');
const generate = require('../../utils/generate')();
chai.use(chaiAsPromised);
const fs = require("fs");
const request = require("request");

process.on("unhandledRejection", () => {});
(async function example() {
  try {
    describe("Retrieve data of optimization with valid token successfull", async function() {
      this.timeout(900000);
      let driver, page;

      beforeEach(async () => {
        page = new Page();
        driver = page.driver;
      });
      afterEach(async () => {
        await page.quit();
      });
      it("Retrieve data of optimization", async () => {
  
        request(
          "https://acctspyws.keyhole.co/v2.1/api.json?accesstoken=" +
            config.api_token + "&type=optimization&hash=dJsDyA",
          {
            method: "get"
          },
          function(err, res, body) {
            expect(res.statusCode).to.equal(200);
            console.log("Response:" + body);
          }
        );
      });
      it("Retrieve data of optimization with invalid token", async () => {
        request(
          "https://acctspyws.keyhole.co/v2.1/api.json?accesstoken=" +
            randomNumber() + "&type=optimization&hash=dJsDyA",
          {
            method: "get"
          },
          function(err, res, body) {          
            console.log("Response:" + body);
            expect(function(err) {
              if (err) return 'Sorry, an error occurred please try again.';
            })
          }
        );
      });
    });
  } catch (ex) {
    console.log(new Error(ex.message));
  }
})();
