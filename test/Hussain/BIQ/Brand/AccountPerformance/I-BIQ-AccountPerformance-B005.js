/**
 * ### TEST CASE:
 * I-BIQ-AccountPerformance-B005
 *
 * ### TEST TITLE:
 * Interact with Top posts.
 *
 * ### TEST SUMMARY:
 *  User should be able to Interact with Top posts.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Top posts.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Interact with Top posts', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[3]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));
                
                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let checkElem = await page.checkElementByCSS('#brands > header.key-pageHeader.key-pageHeader--mentions.key-pageHeader--hasSubHeader > div:nth-child(1) > h1');
                if (checkElem) {
                    let accountPerform = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[1]/a/span');
                    await accountPerform.click();
                    await driver.sleep(2000);
                }

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                status = 'Navigated into Account Performance.';

                await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/figure[2]/div[2]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/figure[3]/div[2]/div/div[1]'), 20000));

                let twt = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/figure[3]/div[2]/div/div[1]');
                await twt.click();
                steps += ' ,Clicked on twitter Post'
                await driver.sleep(2000);

                status += 'Twitter Post displayed'

                let tab1, tab2;

                await driver.getAllWindowHandles().then(function(windowHandles) {
                    tab1 = windowHandles[0];
                    tab2 = windowHandles[1];                
                });

                await driver.switchTo().window(tab1); 
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
                    

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


