/**
 * ### TEST CASE:
 * I-BIQ-AccountPerformance-B003
 *
 * ### TEST TITLE:
 * Preset date persists to URL.
 *
 * ### TEST SUMMARY:
 * When selecting a preset date in the date dropdown, the preset should be persisted in the URL.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Preset date persists to URL.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Preset date persists to URL.', async () => {
                await page.redirectToDashboard();

                let status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                status = 'Navigated into Account Performance.';

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let checkElem = await page.checkElementByCSS('#brands > header.key-pageHeader.key-pageHeader--mentions.key-pageHeader--hasSubHeader > div:nth-child(1) > h1');
                if (checkElem) {
                    let accountPerform = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[1]/a/span');
                    await accountPerform.click();
                    await driver.sleep(2000);
                }

                let pickDate = await page.findByXPath('//*[@id="brands"]/header[2]/div[1]/div/div[2]/span/div/input');
                await pickDate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[2]/div/div'), 20000));

                let startdate = await page.findByXPath('/html/body/div[2]/div/div/div[2]/div[2]/table/tbody/tr[2]/td[7]');
                await startdate.click();
                await driver.sleep(1000);

                let enddate = await page.findByXPath('/html/body/div[2]/div/div/div[2]/div[2]/table/tbody/tr[6]/td[5]');
                await enddate.click();
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));
               
                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.lastIndexOf('='));
                
                if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end') > -1) {
                    let status = '';

                    status += 'Date added to URL., ';

                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                    if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end') > -1) {
                        status += 'Date persisted upon reload.';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Brand Name': firstbrandName,
                            'Test status': status
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Brand Name': firstbrandName,
                            'Test status': 'Date not added to URL.'
                        }
                    });
                }          

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


