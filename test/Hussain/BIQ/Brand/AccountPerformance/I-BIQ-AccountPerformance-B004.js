/**
 * ### TEST CASE:
 * I-BIQ-AccountPerformance-B004
 *
 * ### TEST TITLE:
 * Interact with Engagement Timeline.
 *
 * ### TEST SUMMARY:
 *  User should be able to Interact with Engagement Timeline .
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Engagement Timeline.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Interact with Engagement Timeline.', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[3]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let checkElem = await page.checkElementByCSS('#brands > header.key-pageHeader.key-pageHeader--mentions.key-pageHeader--hasSubHeader > div:nth-child(1) > h1');
                if (checkElem) {
                    let accountPerform = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[1]/a/span');
                    await accountPerform.click();
                    await driver.sleep(1000);
                }

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                status = 'Navigated into Account Performance.';

                let checktwt = await page.checkElementByCSS('#brands > div.key-page.key-page--profiles > section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--postsTimeline > div.key-chartHeading > h3 > span');
                if (checktwt) {
                    let twtClick = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/figure[2]/div[2]/div[1]/div[1]/canvas');
                    await twtClick.click();
                    await driver.sleep(1000);
                    await twtClick.click();
                }

                status += 'hide graph';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
                    

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


