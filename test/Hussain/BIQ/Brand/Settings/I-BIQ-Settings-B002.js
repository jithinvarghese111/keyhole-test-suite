/**
 * ### TEST CASE:
 * I-BIQ-Settings-B002
 *
 * ### TEST TITLE:
 * Enable and Disable Brands.
 *
 * ### TEST SUMMARY:
 * User is trying to enable and disable Brands.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Enable and Disable Brands', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Enable and Disable Brands', async () => {
                await page.redirectToDashboard();

                let steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(2000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();
                await driver.sleep(10000);

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                status = 'Navigated into settings.';
                
                let disable = await page.findByXPath('//*[@id="key-toggle--brandStatus--b6lhLg"]/div/label[3]');
                await disable.click();
                steps += 'First brand disabled, ';
                await driver.sleep(3000);

                await page.scrollPage('//*[@id="brands"]/aside/div/div[9]/div/a/div');
                await driver.sleep(3000);

                let pausedBrands = await page.findByXPath('//*[@id="brands"]/aside/div/div[10]/div/a/div');
                await pausedBrands.click();
                await driver.sleep(2000);

                let enable = await page.findByXPath('//*[@id="key-toggle--brandStatus--1QykRT"]/div/label[1]');
                await enable.click();
                steps += 'Disabled brand enabled';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/div[2]/h2'), 30000));

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': 'Passed'
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


