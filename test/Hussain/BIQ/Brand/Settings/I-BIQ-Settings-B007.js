/**
 * ### TEST CASE:
 * I-BIQ-Settings-B007
 *
 * ### TEST TITLE:
 * Add Instagram Accounts - Brands.
 *
 * ### TEST SUMMARY:
 * User should be able to Add Twitter Accounts on their brands.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Instagram Accounts - Brands', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Add Instagram Accounts - Brands', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/span'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated into Settings';

                await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[2]/div[2]');
                await driver.sleep(2000);

                let connectInst = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[2]/div[2]/button[1]/p');
                await connectInst.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/section/div[5]/div[2]/div[2]/button[1]'), 20000));

                let instPersonal = await page.findByXPath('//*[@id="brands"]/section/div[5]/div[2]/div[2]/button[1]');
                await instPersonal.click();
                await driver.sleep(2000);

                let instName = await page.findByXPath('//*[@id="react-root"]/section/main/div/article/div/div/div/form/div[2]/div/label/input');
                await page.write(instName, 'arun@wdstech.com');

                let instPwd = await page.findByXPath('//*[@id="react-root"]/section/main/div/article/div/div/div/form/div[3]/div/label/input');
                await page.write(instPwd, 'micr@s@ft123');

                let login = await page.findByXPath('//*[@id="react-root"]/section/main/div/article/div/div/div/form/div[4]/button/div');
                await login.click();
                await driver.sleep(3000);

                let checkinst = await page.checkElementByCSS('body > pre');
                if (!checkinst) {
                    status += 'Twitter account added';
                }             
                else{
                    status += 'Error occured';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


