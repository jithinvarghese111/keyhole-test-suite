/**
 * ### TEST CASE:
 * I-BIQ-Settings-B006
 *
 * ### TEST TITLE:
 * Add Twitter Accounts - Brands.
 *
 * ### TEST SUMMARY:
 * User should be able to Add Twitter Accounts on their brands.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Twitter Accounts - Brands', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Add Twitter Accounts - Brands', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(2000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/span'), 30000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated into Settings';

                await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[2]/div[2]');
                await driver.sleep(2000);

                let checktwtAccount = await page.checkElementByCSS('#brands > div.key-page.key-page--settings > section > section > div > section.key-settings__section.key-settings__section--accounts > div > div.key-settings__account.key-settings__account--twitter > div.key-settings__accountBody > div > div.key-settings__accountUserInfo > div.key-settings__accountUserFullName');
                if (!checktwtAccount) {

                    let connectTwt = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[1]/div[2]/button[1]/p');
                    await connectTwt.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="username_or_email"]'), 30000));

                    let twtName = await page.findByXPath('//*[@id="username_or_email"]');
                    await page.write(twtName, 'qa@wdstech.com');

                    let twtPwd = await page.findByXPath('//*[@id="password"]');
                    await page.write(twtPwd, 'micr@s@ft123');
            
                    let submit = await page.findByXPath('//*[@id="allow"]');
                    await submit.click();
                    await driver.sleep(3000);

                    await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[2]/div[2]');
                    await driver.sleep(3000);

                    let checktwt = await page.checkElementByCSS('#brands > div.key-page.key-page--settings > section > section > div > section.key-settings__section.key-settings__section--accounts > div > div.key-settings__account.key-settings__account--twitter > div.key-settings__accountBody > div > div.key-settings__accountUserInfo');
                    if (checktwt) {
                        status += 'Twitter account added';
                    }             
                    else{
                        status += 'Test failed';
                    }
                }
                else{
                    status += 'Account already added.';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


