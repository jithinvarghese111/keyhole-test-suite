/**
 * ### TEST CASE:
 * I-BIQ-Settings-B009
 *
 * ### TEST TITLE:
 * Add Facebook Accounts - Brands.
 *
 * ### TEST SUMMARY:
 * User should be able to Add Facebook Accounts on their brands.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Facebook Accounts - Brands', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Add Facebook Accounts - Brands', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/span'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated into Settings';
                
                await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[2]/div[2]');
                await driver.sleep(2000);

                let connectfb = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[4]/div[2]/button[1]/p');
                await connectfb.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="email"]'), 20000));

                let fbName = await page.findByXPath('//*[@id="email"]');
                await page.write(fbName, 'vrinda@wdstech.com');

                let fbPwd = await page.findByXPath('//*[@id="pass"]');
                await page.write(fbPwd, 'micr@s@ft123');

                let login = await page.findByXPath('//*[@id="loginbutton"]');
                await login.click();
                await driver.sleep(3000);                

                let checkfb = await page.checkElementByCSS('#u_0_12 > div > div');
                if (!checkfb) {
                    status += 'Navigated Youtube login';
                }             
                else{
                    status += 'Error occured';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });

            });

            it('Checking Add unowned Youtube Accounts - Brands', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();                
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/span'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated into Settings';

                await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/section[3]/h2');
                await driver.sleep(2000);

                let addfb = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[4]/div[2]/button[2]/p');
                await addfb.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[4]/div[2]/button'), 20000));

                let fbconnection = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[4]/div[2]/button');
                await fbconnection.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="email"]'), 20000));

                let fbName = await page.findByXPath('//*[@id="email"]');
                await page.write(fbName, 'vrinda@wdstech.com');

                let fbPwd = await page.findByXPath('//*[@id="pass"]');
                await page.write(fbPwd, 'micr@s@ft123');

                let login = await page.findByXPath('//*[@id="loginbutton"]');
                await login.click();
                await driver.sleep(3000); 

                let checkfacebook = await page.checkElementByCSS('#u_0_12 > div > div');
                if (!checkfacebook) {
                    status += 'Navigated Youtube login';
                }             
                else{
                    status += 'Error occured';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


