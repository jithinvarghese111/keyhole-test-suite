/**
 * ### TEST CASE:
 * I-BIQ-Settings-B001
 *
 * ### TEST TITLE:
 * Navigate into settings.
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Navigate into settings using the side bar.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate into settings', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Navigate into settings', async () => {
                await page.redirectToDashboard();

                let status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();
                await driver.wait(until.elementLocated(By.css('#brands > div.key-page.key-page--settings > section > section > div > section.key-settings__section.key-settings__section--heading > div.key-settingsHeading__titleContainer > h1 > span'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                status = 'Navigated into settings.';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test status': status
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


