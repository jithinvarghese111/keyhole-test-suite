/**
 * ### TEST CASE:
 * I-BIQ-Settings-B008
 *
 * ### TEST TITLE:
 * Add Youtube Accounts - Brands.
 *
 * ### TEST SUMMARY:
 * User should be able to Add Twitter Accounts on their brands.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Youtube Accounts - Brands', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Add Youtube Accounts - Brands', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();                
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/span'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated into Settings';

                await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[2]/div[2]');
                await driver.sleep(2000);

                let connectyTube = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[3]/div[2]/button[1]/p');
                await connectyTube.click();
                await driver.sleep(2000);                

                let checkytube = await page.checkElementByCSS('#errorCode > b');
                if (!checkytube) {  

                    let ytEmail = await page.findById('identifierId');
                    await ytEmail.click();
                    await page.write(ytEmail, 'rogerambross@gmail.com');

                    let emailnext = await page.findByXPath('//*[@id="identifierNext"]/span/span');
                    await emailnext.click();
                    await driver.sleep(3000);

                    let ytPwd = await page.findByXPath('//*[@id="password"]/div[1]/div/div[1]/input');
                    await ytPwd.click();
                    await page.write(ytPwd, '123456Asd');

                    let submit = await page.findByXPath('//*[@id="passwordNext"]/span/span');
                    await submit.click();
                    await driver.sleep(2000);

                    let checkerr = await page.checkElementByCSS('#yDmH0d > div.JhUD8d.HWKDRd > h1');
                    if (checkerr) {
                        let err = await page.findByXPath('//*[@id="yDmH0d"]/div[1]/h1');
                        err = await err.getText();
                        status += 'Error: '+err;
                    }
                }             
                else{
                    status += 'Error occured';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });

            });

            it('Checking Add unowned Youtube Accounts - Brands', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/span'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated into Settings';
                
                await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/section[3]/h2');
                await driver.sleep(2000);

                let connectyTube = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[3]/div[2]/button[2]/p');
                await connectyTube.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[3]/div[2]/button'), 20000));                

                let unownedytube = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[3]/div[2]/button');
                await unownedytube.click();
                await driver.sleep(3000);

                let checkYtube = await page.checkElementByCSS('#logo');
                if (!checkYtube) {
                    let ytEmail = await page.findById('identifierId');
                    await ytEmail.click();
                    await page.write(ytEmail, 'rogerambross@gmail.com');

                    let emailnext = await page.findByXPath('//*[@id="identifierNext"]/span/span');
                    await emailnext.click();
                    await driver.sleep(3000);

                    let ytPwd = await page.findByXPath('//*[@id="password"]/div[1]/div/div[1]/input');
                    await ytPwd.click();
                    await page.write(ytPwd, '123456Asd');

                    let submit = await page.findByXPath('//*[@id="passwordNext"]/span/span');
                    await submit.click();
                    await driver.sleep(2000);

                    let checkerr = await page.checkElementByCSS('#yDmH0d > div.JhUD8d.HWKDRd > h1');
                    if (checkerr) {
                        let err = await page.findByXPath('//*[@id="yDmH0d"]/div[1]/h1');
                        err = await err.getText();
                        status += 'Error: '+err;
                    }
                }             
                else{
                    status += 'Error occured';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


