/**
 * ### TEST CASE:
 * I-BIQ-Settings-B005
 *
 * ### TEST TITLE:
 * Remove Twitter Accounts - Brands.
 *
 * ### TEST SUMMARY:
 * User should be able to Remove Twitter Accounts on their brands.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Remove Twitter Accounts - Brands', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Remove Twitter Accounts - Brands', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(2000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/span'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated into settings';

                await page.scrollPixel(200);
                await driver.sleep(3000);;

                let checktwtAccount = await page.checkElementByCSS('#brands > div.key-page.key-page--settings > section > section > div > section.key-settings__section.key-settings__section--accounts > div > div.key-settings__account.key-settings__account--twitter > div.key-settings__accountBody > div > div.key-settings__accountUserInfo > div.key-settings__accountUserFullName');
                if (checktwtAccount) {

                let removetwt = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[1]/div[2]/div/button/i');
                await removetwt.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/section/div[4]/div[2]/div[3]/button[2]'), 20000));

                let removeconfirm = await page.findByXPath('//*[@id="brands"]/section/div[4]/div[2]/div[3]/button[2]');
                await removeconfirm.click();
                status += 'Account removed successfully.'
                await driver.sleep(2000);

                } 
                else{

                    let addtwt = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[1]/div[2]/button[2]');
                    await addtwt.click();
                    await driver.sleep(2000);

                    let twtname = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[1]/div[2]/div[1]/input');
                    await twtname.click();
                    await page.write(twtname, 'shoes');

                    let addConnection = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[1]/div[2]/div[2]/button');
                    await addConnection.click();
                    steps += 'Twitter Accound added';
                    await driver.sleep(2000);

                    let removetwt = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[3]/div/div[1]/div[2]/div/button/i');
                    await removetwt.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/section/div[4]/div[2]/div[3]/button[2]'), 20000));

                    let removeconfirm = await page.findByXPath('//*[@id="brands"]/section/div[4]/div[2]/div[3]/button[2]');
                    await removeconfirm.click();
                    status += ' ,Account removed successfully.'
                    await driver.sleep(2000);
                }                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


