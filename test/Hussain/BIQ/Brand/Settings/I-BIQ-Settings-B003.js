/**
 * ### TEST CASE:
 * I-BIQ-Settings-B003
 *
 * ### TEST TITLE:
 * Editing Brands.
 *
 * ### TEST SUMMARY:
 * User should be able to perform an advanced edit on their brands.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Editing Brands', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Editing Brands', async () => {
                await page.redirectToDashboard();

                let steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(4000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let settings = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[5]/a');
                await settings.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/span'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated into settings';

                let editName = await page.findByCSS('#brands > div.key-page.key-page--settings > section > section > div > section.key-settings__section.key-settings__section--heading > div.key-settingsHeading__titleContainer > h1 > button');
                await editName.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/button'), 20000));

                let saveName = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[1]/div[1]/h1/button');
                await saveName.click();
                steps += 'Edited brand Name';
                await driver.sleep(1000);

                let checkinput = await page.checkElementByCSS('#brands > div.key-page.key-page--settings > section > section > div > section.key-settings__section.key-settings__section--keywords > div > div.key-settings__searchBuilder > div.key-settings__searchBuilderSectionWrapper.key-settings__searchBuilderSectionWrapper--keywords > div.key-filterSection__inputsWrapper.key-filterSection__inputsWrapper--or > div:nth-child(2) > input');
                if (checkinput) {
                    let collectPosts = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[1]/div[1]/div[2]/div[2]/input');
                    await collectPosts.click();
                    await page.write(collectPosts, 'Sparx');

                    let addposts = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[1]/div[1]/div[2]/button');
                    await addposts.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[1]/div[1]/div[2]/div[3]/input'), 20000));

                    let nextPosts = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/section[2]/div/div[1]/div[1]/div[2]/div[3]/input');
                    await nextPosts.click();
                    await page.write(nextPosts, 'puma');
                    steps += ' ,Added other posts';
                }
                
                let platforms = ['Twitter', 'Instagram'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#brands > div.key-page.key-page--settings > section > section > div > section.key-settings__section.key-settings__section--keywords > div > div.key-settings__searchBuilder > div.key-settings__searchBuilderSectionWrapper.key-settings__searchBuilderSectionWrapper--platforms > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await platformClick.click();
                    await driver.sleep(1000);
                }
                steps += ' ,Checked multiple platforms';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': 'Passed'
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


