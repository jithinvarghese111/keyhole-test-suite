/**
 * ### TEST CASE:
 * I-BIQ-Recommendations-B004
 *
 * ### TEST TITLE:
 * Interact with Post length and hashtags.
 *
 * ### TEST SUMMARY:
 *  User should be able to Post length and hashtags .
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Post length and hashtags.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Interact with Post length and hashtags', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let recommendation = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[2]/a/span');
                await recommendation.click();                
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));
                
                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Navigated to recommendation';

                await page.scrollPage('//*[@id="brands"]/div[1]/section/section/div/figure[1]/div[2]');
                await driver.sleep(3000);

                let postFrequency = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/figure[3]/div[2]/div/div[1]/div/div/div/div[2]');
                await postFrequency.click();
                status += 'Clicked on Post length frequency';   
                await driver.sleep(2000);

                let hashFrequency = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/figure[4]/div[2]/div/div[1]/div/div/div/div[2]');
                await hashFrequency.click();
                status += ' ,Clicked on hashtags frequency'; 
                await driver.sleep(2000);

                let optimalFrequency = await page.findByXPath('//*[@id="brands"]/div[1]/section/section/div/figure[5]/div[2]/div/div[1]/div/div/div/div[2]');
                await optimalFrequency.click();
                status += ' ,Clicked on Optimalhashtags frequency'; 
                await driver.sleep(2000);

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
                    

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


