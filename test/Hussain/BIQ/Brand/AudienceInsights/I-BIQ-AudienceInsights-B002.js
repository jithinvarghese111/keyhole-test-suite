/**
 * ### TEST CASE:
 * I-BIQ-AccountPerformance-B002
 *
 * ### TEST TITLE:
 * Get shareable Link.
 *
 * ### TEST SUMMARY:
 * User should be able to share the link on their Audience Insights.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Get shareable Link', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Get shareable Link', async () => {
                await page.redirectToDashboard();

                let status = '', test_result = '', steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a/div');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));

                let checkEle = await page.checkElementByCSS('#brands > section > div.key-modal.key-modal--mentionsOverlay.key-modal--show > div.key-modal__wrapper > div.key-modal__heading');
                if (checkEle) {
                    let popup = await page.findByXPath('//*[@id="brands"]/section/div[3]/div[2]/div[1]/button');
                    await popup.click();
                    await driver.sleep(1000);
                }
                
                let accountInsight = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[4]/a');
                await accountInsight.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                status = 'Navigated into Audience Insights.';

                let sharelink = await page.findByXPath('//*[@id="brands"]/header[2]/div[2]/button');
                await sharelink.click();
                steps += 'Share Link button clicked, ';
                await driver.wait(until.elementLocated(By.css('#brands > section > div.key-modal.key-modal--sharePage.key-modal--show'), 20000));

                let modalOpen = await page.findElementsByCSS('#brands > section > div.key-modal.key-modal--sharePage.key-modal--show');
                modalOpen = modalOpen.length;

                if (modalOpen > 0)
                    steps += 'Modal opened, '; 

                let copylink = await page.findByCSS('#brands > section > div.key-modal.key-modal--sharePage.key-modal--show > div.key-modal__wrapper > div.key-modal__body.key-modal__body--roundedCornersBottom > div > div > i');
                await copylink.click();
                steps += 'Copy link button clicked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/section/div[2]/div[2]/div[2]/div/div/input'), 20000));

                let link = await page.findByXPath('//*[@id="brands"]/section/div[2]/div[2]/div[2]/div/div/input');
                link = await link.getAttribute("value");
                steps += 'URL highlighted and copied!';

                if (link != "" && modalOpen > 0)
                    test_result = 'Test passed';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Shareable Link': link,
                        'Test steps': steps,
                        'Test status': test_result
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


