/**
 * ### TEST CASE:
 * I-BIQ-BrandMentions-B005
 *
 * ### TEST TITLE:
 * Exclude Retweets.
 *  
 * ### TEST SUMMARY:
 * User should able to Exclude Reweets.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Exclude Retweets', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Exclude Retweets', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);
                
                let brandmentions = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[3]/a');
                await brandmentions.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps = 'Naviagted to Brand Mentions';

                let exclude = await page.findByXPath('//*[@id="brands"]/header[2]/div[3]/div[1]/label');
                await exclude.click();
                steps += ' ,Clicked Exclude Retweet ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="mentionsTable"]/div/div[1]/div[3]'), 20000));

                let checkemptyBrands = await page.checkElementByCSS('#mentionsTable > div > div:nth-child(2) > div');
                if (checkemptyBrands) {
                    status = 'No brand Mentions found';
                } 
                else{

                    let checkRetweetPost = await page.checkElementByCSS('#brands > table > tbody > tr:nth-child(2) > td.key-table__column.key-table__column--post > span:nth-child(1) > i');
                    if(!checkRetweetPost) {
                        status = 'Excluded Retweets';
                    } 
                    else{
                        status = 'Retweets are not Excluded, tryagain later';
                    }
                }

                addContext(this.ctx, {
                    title: 'Other context',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
                              
                
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


