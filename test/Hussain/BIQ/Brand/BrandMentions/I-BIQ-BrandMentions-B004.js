/**
 * ### TEST CASE:
 * I-BIQ-BrandMentions-B004
 *
 * ### TEST TITLE:
 * Search with keyword.
 *  
 * ### TEST SUMMARY:
 * User should able to search Brand Mentions.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search - Brand mentions', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Search - Brand mentions', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(1000);
                
                let brandmentions = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[3]/a');
                await brandmentions.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[1]/h1'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Naviagted to Brand Mentions';

                let search = await page.findByXPath('//*[@id="brands"]/header[2]/div[3]/div[2]/input');
                await search.click();
                await page.write(search, 'Nike');
                await page.keyboardClick('#brands > header.key-pageHeader.key-pageHeader--mentions.key-pageHeader--hasSubHeader > div.key-pageHeader__row.key-subheader > div.key-subheader__wrapper--search > input');      
                await driver.wait(until.elementLocated(By.xpath('//*[@id="mentionsTable"]/div/div[1]/div[3]'), 20000));

                let checkpost = await page.checkElementByCSS('#mentionsSideView > div > div.key-chartEmptyState__title');
                if (checkpost) {
                    status = 'No Brand Mentions Found'
                }
                else{
                    status = 'Brand Mentions with search keyword Displayed'
                }

                addContext(this.ctx, {
                    title: 'Other context',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
                              
                
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


