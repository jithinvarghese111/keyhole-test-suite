/**
 * ### TEST CASE:
 * I-BIQ-BrandMentions-B003
 *
 * ### TEST TITLE:
 * Sentiment filters.
 *
 * ### TEST SUMMARY:
 * Sentiment should filter Sentiment.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sentiment filters - Brand mentions', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Sentiment filters - Brand mentions', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(2000);
                
                let brandmentions = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[3]/a');
                await brandmentions.click();
                await driver.wait(until.elementLocated(By.css('#brands > header.key-pageHeader.key-pageHeader--mentions.key-pageHeader--hasSubHeader > div:nth-child(1) > h1'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Naviagted to Brand Mentions';

                let sentiment = await page.findByXPath('//*[@id="brands"]/header[2]/div[3]/div[1]/div[2]/div[1]');
                await sentiment.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="brands"]/header[2]/div[3]/div[1]/div[2]/div[2]/label[1]/div'), 20000));                 

                let totalSenti = await page.findElementsByCSS('#brands > header.key-pageHeader.key-pageHeader--mentions.key-pageHeader--hasSubHeader > div.key-pageHeader__row.key-subheader > div.key-subheader__wrapper > div.key-subheader__filter.key-subheader__sentimentDropdown.key-subheader__sentimentDropdown--open > div.key-sentimentDropdown__dropdown > label > div > img');
            
                for(i= 1; i <= totalSenti.length; i++) {
                    let selectAll = await page.findByXPath(' //*[@id="brands"]/header[2]/div[3]/div[1]/div[2]/div[2]/label['+i+']/div/img');
                    let sentiName = await selectAll.getAttribute("alt"); 
                    await selectAll.click();
                    await selectAll.click();
                    status = ' ,Clicked on :'+sentiName;

                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Brand Name': firstbrandName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }                
                
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


