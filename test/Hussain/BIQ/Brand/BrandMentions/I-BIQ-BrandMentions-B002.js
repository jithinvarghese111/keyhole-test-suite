/**
 * ### TEST CASE:
 * I-BIQ-BrandMentions-B002
 *
 * ### TEST TITLE:
 * Platform filters.
 *
 * ### TEST SUMMARY:
 * Platform checkboxes should filter Brand Mention content
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Platform Filters - Brand mentions', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotohussainLogin();
            });

            it('Checking Platform Filters - Brand mentions', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let firstbrand = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/div/a');
                let firstbrandName = await firstbrand.getText();
                await firstbrand.click();
                await driver.sleep(2000);
                
                let brandmentions = await page.findByXPath('//*[@id="brands"]/aside/div/div[2]/ul/li[3]/a');
                await brandmentions.click();
                await driver.wait(until.elementLocated(By.css('#brands > header.key-pageHeader.key-pageHeader--mentions.key-pageHeader--hasSubHeader > div:nth-child(1) > h1'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(firstbrandName);
                steps += 'Naviagted to Brand Mentions';

                let totalPlatform = await page.findElementsByCSS('#brands > header.key-pageHeader.key-pageHeader--mentions.key-pageHeader--hasSubHeader > div:nth-child(1) > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox');
            
                for(i= 1; i <= totalPlatform.length; i++) {
                    let selectAll = await page.findByXPath('//*[@id="brands"]/header[2]/div[1]/div/div[1]/form/label['+i+']');
                    await selectAll.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 10000));
                    await selectAll.click();
                }
                status += 'Platform checkboxes filtered';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Brand Name': firstbrandName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();


