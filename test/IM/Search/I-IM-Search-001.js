/**
 * ### TEST CASE:
 * I-IM-Search-001
 *
 * ### TEST TITLE:
 * Search Active Influencers
 *
 * ### TEST SUMMARY:
 * User is trying to search for a particular Influencer from landing page
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search Influencers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Search with available Influencers', async () => {
                await page.redirectToIMDashboard();

                let searchBox = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__search.js-influencersFilters__search > input[type=text]');
                await page.write(searchBox, config.atInfluencerSearch);
                await driver.sleep(5000);

                let noResult = await page.checkElementByCSS('#influencers > tbody > tr:nth-child(2)');

                if (noResult) {
                    let totalInfluencer = await page.findElementsByCSS('#influencers > tbody > tr');
                    totalInfluencer = totalInfluencer.length - 1;

                    noResult_msg = 'Total Influenecers found after search: '+totalInfluencer;
                }
                else {
                	noResult_msg = 'Search result empty. No Influenecers found';
                }

                addContext(this.ctx, {
                    title: 'Other context',
                    value: {
                    	'Search keyword': config.atInfluencerSearch,
                        'Search result': noResult_msg
                    }
                });
            });

            it('Search with random keyword', async () => {
                await page.redirectToIMDashboard();

                for (var i = 1; i <= 3; i++) {
                	var random_name = randomName();

                	await page.clearFields('css', '#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__search.js-influencersFilters__search > input[type=text]');
                	await driver.sleep(2000);

                	let searchBox = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__search.js-influencersFilters__search > input[type=text]');
	                await page.write(searchBox, random_name);
	                await driver.sleep(2000);

	                let noResult = await page.checkElementByCSS('#influencers > tbody > tr:nth-child(2)');

	                if (noResult) {
                        let totalInfluencer = await page.findElementsByCSS('#influencers > tbody > tr');
                        totalInfluencer = totalInfluencer.length;

                        noResult_msg = 'Total Influenecers found after search: '+totalInfluencer;
	                }
	                else {
	                	noResult_msg = 'Search result empty. No Influenecers found';
	                }

	                addContext(this.ctx, {
	                    title: 'Other context',
	                    value: {
	                    	'Search keyword': random_name,
	                        'Search result': noResult_msg
	                    }
	                });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();