/**
 * ### TEST CASE:
 * I-IM-Dashboard-004
 *
 * ### TEST TITLE:
 * Influencer - Platform filters.
 *
 * ### TEST SUMMARY:
 * User should allow to check Total Count of Influencers withEach Social Media Account.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('. ./../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Influencer - Platform filters', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Influencer - Platform filters', async () => {
                await page.redirectToIMDashboard();

                let platforms = ['2', '3', '4'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/label['+platforms[i]+']');
                    await platformClick.click();
                    await driver.sleep(1000);
                }
                
                await page.scrollPage('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div/div[2]/a[2]');
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.total');
                if (checkEle) {
                    let totaltwtInflu = await page.findByXPath('//*[@id="key-pagination"]/div[1]/span[3]');
                    totaltwtInflu = await totaltwtInflu.getText();
                    addContext(this.ctx, 'Total Influencers with twitter Accounts: '+totaltwtInflu);
                     await driver.sleep(1000);

                }
                else{
                    let totalTwitterInflu = await page.findElementsByCSS('#influencers > tbody > tr > td.influencer__item--accounts > ul > li.influencer__item.influencer__item--user > div.influencerRow__profilePicture > img.platform.twitter');
                    totalTwitterInflu = await totalTwitterInflu.length - 1;
                    addContext(this.ctx, 'Total Influencers with  Twitter Accounts: '+ totalTwitterInflu);

                }

                await page.scrollPage('//*[@id="manage-accounts"]/div[1]/div/div[2]/button');
                await driver.sleep(3000);

                let accounts = ['1', '2'];

                for (i = 0; i < accounts.length; i++) {
                    let accountsClick = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/label['+accounts[i]+']');
                    await accountsClick.click();
                    await driver.sleep(1000);
                }

                let totalinstagramInflu = await page.findElementsByCSS('#influencers > tbody > tr > td.influencer__item--accounts > ul > li.influencer__item.influencer__item--user > div.influencerRow__profilePicture > img.platform.instagram-business');
                let totalinstagram = await totalinstagramInflu.length - 1;
                console.log(totalinstagram);
                addContext(this.ctx, 'Total Influencers with instagram Accounts: '+ totalinstagram); 

                await page.scrollPage('//*[@id="manage-accounts"]/div[1]/div/div[2]/button');
                await driver.sleep(3000);

                let account = ['2', '3'];

                for (i = 0; i < account.length; i++) {
                    let accountClick = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/label['+account[i]+']');
                    await accountClick.click();
                    await driver.sleep(1000);
                }

                await page.scrollPage('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div/div[2]/a[2]');
                await driver.sleep(3000);

                let checkElem = await page.checkElementByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.total');
                if (checkElem) {                    
                    let totalfbInf = await page.findByXPath('//*[@id="key-pagination"]/div[1]/span[3]');
                    totalfbInf = await totalfbInf.getText();
                    addContext(this.ctx, 'Total Influencers with facebook Accounts: '+totalfbInf);
                    await driver.sleep(1000);
                }
                else{
                    let totalfbInflu = await page.findElementsByCSS('#influencers > tbody > tr > td.influencer__item--accounts > ul > li.influencer__item.influencer__item--user > div.influencerRow__profilePicture > img.platform.facebook');
                    totalfbInflu = await totalfbInflu.length - 1;
                    addContext(this.ctx, 'Total Influencers with facebook Accounts: '+ totalfbInflu); 

                }

                await page.scrollPage('//*[@id="manage-accounts"]/div[1]/div/div[2]/button');
                await driver.sleep(3000);

                let plat = ['3', '4'];

                for (i = 0; i < plat.length; i++) {
                    let platClick = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/label['+plat[i]+']');
                    await platClick.click();
                    await driver.sleep(1000);
                }

                await page.scrollPage('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div/div[2]/a[2]');
                await driver.sleep(3000);

                let checkElemn = await page.checkElementByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.total');
                if (checkElemn) {                    
                    let totalfbInf = await page.findByXPath('//*[@id="key-pagination"]/div[1]/span[3]');
                    totalfbInf = await totalfbInf.getText();
                    addContext(this.ctx, 'Total Influencers with youtube Accounts: '+totalfbInf);
                    await driver.sleep(1000);
                }
                else{
                    let totalytInflu = await page.findElementsByCSS('#influencers > tbody > tr > td.influencer__item--accounts > ul > li.influencer__item.influencer__item--user > div.influencerRow__profilePicture > img.platform.youtube');
                    totalytInflu = await totalytInflu.length - 1;
                    addContext(this.ctx, 'Total Influencers with youtube Accounts: '+ totalytInflu); 

                }
                     
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();