/**
 * ### TEST CASE:
 * I-IM-Dashboard-010
 *
 * ### TEST TITLE:
 * Checking Unauthenticated Accounts
 *
 * ### TEST SUMMARY:
 * User is trying to edit, delete unauthenticated section 
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Unauthenticated Accounts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });
                
                await page.gotoLogin();
            });

            it('Navigated into Unauthenticated accounts', async () => {
                await page.redirectToIMDashboard();

                let authenticated = await page.findByXPath('        //*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div/div[1]/button[3]');
                await authenticated.click();
                await driver.sleep(1000);
                
                let checkEle = await page.checkElementByCSS('#influencers > tbody > tr:nth-child(2) > td.influencer__item.influencer__item--email.js-influencer__item--email');
                if (checkEle) {
                    addContext(this.ctx, 'Displayed Unauthenticated Accounts.');
                }
                else{
                    addContext(this.ctx, 'No Unauthenticated Accounts found');
                }
                                
            });

             it('Edit Unauthenticated Influencers', async () => {
                await page.redirectToIMDashboard();

                 let randomInfluencerName = randomWord();

                let unauthenticated = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div/div[1]/button[3]');
                await unauthenticated.click();
                await driver.sleep(1000);

                let influencerClick = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/p[1]');
                await influencerClick.click();
                await driver.sleep(1000);

                let editinfluencer = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/ul/li[1]');
                await editinfluencer.click();
                await driver.sleep(1000);               

                let instAccount = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[4]/input');
                await page.write(instAccount, randomInfluencerName);
                await driver.sleep(2000);

                let seconddata = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[4]/div/a/div');
                await seconddata.click();

                let fbAccount = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[6]/input');
                await page.write(fbAccount, randomInfluencerName);
                await driver.sleep(2000);

                let thirddata = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[6]/div/a[1]/div/h4');
                await thirddata.click();

                let saveaData = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[2]/a[2]');
                await saveaData.click();
                addContext(this.ctx, 'Unauthenticated Influencer edited successfully: '+randomInfluencerName);
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));
                                
            });

            it('Delete Unauthenticated Influencers', async () => {
                await page.redirectToIMDashboard();

                let randomInfluencerName = randomWord();

                let unauthenticated = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div/div[1]/button[3]');
                await unauthenticated.click();
                await driver.sleep(1000);

                let influencerClick = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/p[1]');
                await influencerClick.click();
                await driver.sleep(1000);

                let deleteInfluencer = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/ul/li[2]');
                await deleteInfluencer.click();
                await driver.sleep(1000);

                let confirmdelete = await page.findByXPath('//*[@id="remove-influencer-confirmation"]/div/div/div/div[2]/a[2]');
                await confirmdelete.click();
                await driver.sleep(1000);

                let searchAgain = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[1]/input');
                await searchAgain.click();
                await page.write(searchAgain, randomInfluencerName);
                await driver.sleep(2000);   

                let checkEle = await page.checkElementByCSS('#influencers > tbody > div');
                if (checkEle) {
                    addContext(this.ctx, 'Unauthenticated Influencer Deleted successfully: '+randomInfluencerName);
                }   
                                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();