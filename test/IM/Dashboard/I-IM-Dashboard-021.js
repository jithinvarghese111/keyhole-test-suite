/**
 * ### TEST CASE:
 * I-IM-Dashboard-021
 *
 * ### TEST TITLE:
 * Influencer - Export csv.
 *
 * ### TEST SUMMARY:
 * User navigates to the Influencers and ownload an csv file of Influencer information locally.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const assert = require('assert');
const Page = require('. ./../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Influencer - Export csv.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Influencer - Export csv.', async () => {
                await page.redirectToIMDashboard();

                let exportCSV = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__listAndFiltersCategory > div.influencers__listWrapper > div.influencers__listActions > button');
                await exportCSV.click();
                await driver.sleep(8000);

                let csvName = await page.findByCSS('#INFL_CSV_EXPORT');
                csvName = await csvName.getAttribute("download");

                const downloadPath = config.downloadPath+'\/'+csvName;
                const filename = `Keyhole_Influencer_${new Date().getTime()}.csv`;
                const renamePath = config.downloadPath+'\/'+filename;
                
                if (fs.existsSync(downloadPath)) {
                    fs.renameSync(downloadPath, renamePath)
                    
                    if (fs.existsSync(renamePath)) {
                        const stats = fs.statSync(renamePath)

                        if (parseFloat(stats.size) > 64) {
                            test_result = "Downloaded file";
                            passed = true
                        } else {
                            test_result = "Downloaded empty file.";
                            passed = true
                        }
                    }
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Downloade path': downloadPath,
                        'Renamed path': renamePath,
                        'Test status': test_result
                    }
                });
                     
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();