/**
 * ### TEST CASE:
 * I-IM-Dashboard-014
 *
 * ### TEST TITLE:
 * checking Pagination of the Influencers
 *
 * ### TEST SUMMARY:
 * User is trying to check pagination of the influencer is working
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('checking Pagination of the Influencers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User is trying to check pagination of the influencer is working', async () => {
                await page.redirectToIMDashboard();

                let steps = '';

                let totalPosts = await page.findByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.total');
                totalPosts = await totalPosts.getText();
                totalPosts = totalPosts.replace (/,/g, "");
                
                if (totalPosts > 10) {

                    for (var i = 2; i <= 5; i++) {
                    
                    await page.scrollPage('//*[@id="influencers"]/tbody/tr[11]');
                    await driver.sleep(3000);

                    let clickOption = await page.findByXPath('//*[@id="key-pagination"]/div[2]/span[2]/span['+i+']');
                    let clickText = await clickOption.getText();
                    addContext(this.ctx, 'Selected page Number: '+i+'');
                    await clickOption.click();
                    await driver.sleep(1000);

                    let totalBefore = await page.findElementsByCSS('#influencers > tbody > tr');
                    totalBefore = totalBefore.length;
                    addContext(this.ctx, 'Total influencer in page '+i+': '+totalBefore);

                    }
                }
                else
                    addContext(this.ctx, 'Not enough posts for the selected tracker');
                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();