/**
 * ### TEST CASE:
 * I-IM-Dashboard-012
 *
 * ### TEST TITLE:
 * Influencer Pagination
 *
 * ### TEST SUMMARY:
 * User is using to Influencer Pagination
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Influencer Pagination', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });
                
                await page.gotoLogin();
            });

            it('Influencer Pagination', async () => {
                await page.redirectToIMDashboard();

                await page.scrollPage('//*[@id="influencers"]/tbody/tr[11]/td[1]');
                await driver.sleep(3000);

                for (var i = 2; i <= 7; i++) {
                    let pagination = await page.findByXPath('//*[@id="key-pagination"]/div[2]/span[2]/span['+i+']');
                    let pageNumber = await pagination.getAttribute("data-page");
                    await pagination.click();
                    addContext(this.ctx, 'Page Number clicked: '+pageNumber);
                    await driver.sleep(2000);
                }
                                
            });
  
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();       