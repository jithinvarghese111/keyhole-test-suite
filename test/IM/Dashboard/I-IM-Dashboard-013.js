/**
 * ### TEST CASE:
 * I-IM-Dashboard-013
 *
 * ### TEST TITLE:
 * Go To campaign Tracker from Influencer
 *
 * ### TEST SUMMARY:
 * User going to campaign Tracker from Influencer
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Go To campaign Tracker from Influencer', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go To campaign Tracker from Influencer', async () => {
                await page.redirectToIMDashboard();

                let steps = '', randomInfluencerName = randomWord();

                let campaignTracker = await page.findByCSS('#manage-accounts > div.heading > div > div.heading__actions.heading__actions--influencers.active-influencers.heading__actions--addMarginTop > a');
                await campaignTracker.click();
                addContext(this.ctx, 'Redirected to campaign Tracker Page');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 200000));

                let activeTracker = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                activeTracker = await activeTracker.length;
                
                addContext(this.ctx, 'Total Active Hashtag & Keyword Tracker: '+activeTracker);
                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();