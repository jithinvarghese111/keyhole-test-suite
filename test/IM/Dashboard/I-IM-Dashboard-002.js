/**
 * ### TEST CASE:
 * I-IM-Dashboard-002
 *
 * ### TEST TITLE:
 * Delete Influencers
 *
 * ### TEST SUMMARY:
 * User is trying to delete a Influencer
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Delete Influencers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Delete Influencers', async () => {
                await page.redirectToIMDashboard();

                let steps = '', randomInfluencerName = randomWord();

                let addInfluencer = await page.findByXPath('//*[@id="manage-accounts"]/div[1]/div/div[2]/button');
                await addInfluencer.click();
                await driver.sleep(1000);
                
                let manualAdd = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[1]/button[2]');
                await manualAdd.click();
                await driver.sleep(2000);

                let name = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(2) > input[type=text]');
                await page.write(name, randomInfluencerName);

                let email = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(3) > input[type=email]');
                await page.write(email, randomInfluencerName+'@gmail.com');

                let twtAccount = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflAccs > label:nth-child(2) > input[type=text]');
                await page.write(twtAccount, randomInfluencerName);
                await driver.sleep(3000);

                let firstdata = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[1]/div[2]/label[1]/div/a[1]/div/h4');
                await firstdata.click();

                let saveInfluencer = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[2]/a[2]');
                await saveInfluencer.click();
                await driver.sleep(2000);

                let search = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[1]/input');
                await search.click();
                await page.write(search, randomInfluencerName);
                await driver.sleep(2000);

                let influencerClick = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/p[1]');
                await influencerClick.click();
                await driver.sleep(3000);

                let deleteInfluencer = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/ul/li[2]');
                await deleteInfluencer.click();
                await driver.sleep(2000);

                let confirmdelete = await page.findByXPath('//*[@id="remove-influencer-confirmation"]/div/div/div/div[2]/a[2]');
                await confirmdelete.click();
                await driver.sleep(1000);

                let searchAgain = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[1]/input');
                await searchAgain.click();
                await page.write(searchAgain, randomInfluencerName);
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#influencers > tbody > div');
                if (checkEle) {
                    addContext(this.ctx, 'Influencer Deleted successfully: '+randomInfluencerName);
                }
                                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();