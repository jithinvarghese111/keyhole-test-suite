/**
 * ### TEST CASE:
 * I-IM-Dashboard-018
 *
 * ### TEST TITLE:
 * Edit new Tag Influencers from Unauthenticated
 *
 * ### TEST SUMMARY:
 * User should be able to click on the Unauthenticated influencer and edit new tag 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Edit new Tag Influencers from Unauthenticated', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to click on the Unauthenticated influencer and edit new tag', async () => {
                await page.redirectToIMDashboard();

                let randomTag = randomWord(), randomname = randomName();

                let unauthenticated = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[1]/div[1]/button[3]');
                await unauthenticated.click();
                await driver.sleep(2000);

                let selectInfluencer = await page.findByCSS('#influencers > tbody > tr:nth-child(2) > td.influencer__item.influencer__item--checkbox > button');
                await selectInfluencer.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span'), 20000));

                let tagInfluencer = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span');
                await tagInfluencer.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/ul/li'), 20000));

                let checktag = await page.checkElementByCSS('#influencers > tbody > tr:nth-child(2) > td.influencer__item.influencer__item--email.js-influencer__item--email > div > span > span');
                if (checktag) {
                    let addTag = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/ul/li[2]');
                    await addTag.click();
                    await driver.sleep(2000);
                }else{                
                let createTag = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/ul/li');
                await createTag.click();
                await driver.sleep(2000);
                }

                let tagName = await page.findByXPath('//*[@id="keyjs-createInflTagInput"]');
                await page.write(tagName, randomTag);

                let apply = await page.findByCSS('#content-wrapper > section.key-modalContainer.key-modalContainer--show.key-modalContainer--createInflTag.keyjs-modalContainer--createInflTag > div > div.key-modal__wrapper > div.key-modal__footer > button');
                await apply.click();
                addContext(this.ctx, 'Tag Added to the selected Tracker');
                await driver.sleep(2000);

                let selectAgain = await page.findByCSS('#influencers > tbody > tr:nth-child(2) > td.influencer__item.influencer__item--checkbox > button');
                await selectAgain.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span'), 20000));

                let tagInfluencerClick = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span');
                await tagInfluencerClick.click();
                await driver.sleep(2000);

                let editTag = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__listAndFiltersCategory > div.influencers__listWrapper > div > div > div > ul > li.key-dropdownItem.keyjs-addInfluencersToTag > button > i');
                await editTag.click();
                await driver.sleep(2000);

                let tagname = await page.findById('keyjs-editInflTagInput');
                await page.write(tagname, randomname);

                let saveTag = await page.findByXPath('//*[@id="content-wrapper"]/section[2]/div/div[2]/div[3]/button[2]');
                await saveTag.click();
                await driver.sleep(2000);

                let influencerTagName = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[2]/div/span/span');
                influencerTagName = await influencerTagName.getText();

                assert.notEqual(influencerTagName, randomTag);
                addContext(this.ctx, 'Tag Name Edited Successfully');

            });
                
            it('User should be able to edit tag from Unauthenticated influencer and delete.', async () => {
                await page.redirectToIMDashboard();            

                let unauthenticated = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[1]/div[1]/button[3]');
                await unauthenticated.click();
                await driver.sleep(2000);

                let selectAgain = await page.findByCSS('#influencers > tbody > tr:nth-child(2) > td.influencer__item.influencer__item--checkbox > button');
                await selectAgain.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span'), 20000));

                let tagInfluencerClick = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span');
                await tagInfluencerClick.click();
                await driver.sleep(2000);

                let editTag = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__listAndFiltersCategory > div.influencers__listWrapper > div > div > div > ul > li.key-dropdownItem.keyjs-addInfluencersToTag > button > i');
                await editTag.click();
                await driver.sleep(2000);

                let deleteTag = await page.findByCSS('#content-wrapper > section.key-modalContainer.key-modalContainer--show.key-modalContainer--editInflTag.keyjs-modalContainer--editInflTag > div > div.key-modal__wrapper > div.key-modal__footer > button.kc-button.kc-button--caps.kc-button--small.kc-button--outline.kc-button--red.keyjs-deleteInflTagButton');
                await deleteTag.click();
                await driver.sleep(2000);

                let checkEle = await page.checkElementByCSS('#influencers > tbody > tr:nth-child(2) > td.influencer__item.influencer__item--email.js-influencer__item--email > div > span > span');
                if (!checkEle) {
                    addContext(this.ctx, 'Added Tag Deleted Successfully');
                }
                
            });

            
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();