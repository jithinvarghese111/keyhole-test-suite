	/**
 * ### TEST CASE:
 * I-IM-Dashboard-005
 *
 * ### TEST TITLE:
 * Analyze Influencer.
 *
 * ### TEST SUMMARY:
 * Users should be able to analyze Influencer.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('. ./../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Analyze Influencer', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Analyze Influencer', async () => {
                await page.redirectToIMDashboard();

                let randomInfluencerName = randomWord();

                let addInfluencer = await page.findByXPath('//*[@id="manage-accounts"]/div[1]/div/div[2]/button');
                await addInfluencer.click();
                await driver.sleep(1000);
                
                let manualAdd = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[1]/button[2]');
                await manualAdd.click();
                await driver.sleep(2000);

                let name = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(2) > input[type=text]');
                await page.write(name, randomInfluencerName);
                await driver.sleep(1000);

                let email = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(3) > input[type=email]');
                await page.write(email, randomInfluencerName+'@gmail.com');
                await driver.sleep(1000);

                let twtAccount = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflAccs > label:nth-child(2) > input[type=text]');
                await page.write(twtAccount, randomInfluencerName);
                await driver.sleep(3000);

                let firstdata = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[1]/div[2]/label[1]/div/a[1]/div/h4');
                await firstdata.click();
                await driver.sleep(3000);

                let saveInfluencer = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[2]/a[2]');
                await saveInfluencer.click();
                await driver.sleep(2000);

                let search = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[1]/input');
                await search.click();
                await page.write(search, randomInfluencerName);
                await driver.sleep(2000);
                
            	let analyze = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[2]/ul/li[3]/div');
            	await analyze.click();
            	await driver.wait(until.elementLocated(By.xpath('//*[@id="influencers"]/tbody/tr[2]/td[2]/ul/li[3]/a'), 200000));

            	let viewAnalytics = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[2]/ul/li[3]/a');
            	await viewAnalytics.click();
            	await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 200000));

            	let trackername = await page.findByXPath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span');
            	trackername = await trackername.getText();
            	console.log(trackername);
            	addContext(this.ctx, 'Analyzed Tracker Name: '+trackername);

            	let checkElem = await page.checkElementByCSS('#dashboard > div > span:nth-child(2)');
            	if (checkElem) {
            		let warningMsg = await page.findByXPath('//*[@id="dashboard"]/div/span[1]');
            		warningMsg = await warningMsg.getText();
            		console.log(warningMsg);
            		addContext(this.ctx, 'Warning Message: '+warningMsg);
            	}
                
                     
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();