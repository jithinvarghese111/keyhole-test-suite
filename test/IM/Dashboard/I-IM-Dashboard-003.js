/**
 * ### TEST CASE:
 * I-IM-Dashboard-001
 *
 * ### TEST TITLE:
 * Edit Influencers
 *
 * ### TEST SUMMARY:
 * User is trying to edit a Influencer
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Edit Influencers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Edit Influencers', async () => {
                await page.redirectToIMDashboard();

                let steps = '', randomInfluencerName = randomWord();

                let addInfluencer = await page.findByXPath('//*[@id="manage-accounts"]/div[1]/div/div[2]/button');
                await addInfluencer.click();
                await driver.sleep(1000);
                
                let manualAdd = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[1]/button[2]');
                await manualAdd.click();
                await driver.sleep(2000);

                let name = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(2) > input[type=text]');
                await page.write(name, randomInfluencerName);
                await driver.sleep(1000);

                let email = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(3) > input[type=email]');
                await page.write(email, randomInfluencerName+'@gmail.com');
                await driver.sleep(1000);

                let twtAccount = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflAccs > label:nth-child(2) > input[type=text]');
                await page.write(twtAccount, randomInfluencerName);
                await driver.sleep(3000);

                let firstdata = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[1]/div[2]/label[1]/div/a[1]/div/h4');
                await firstdata.click();

                let saveInfluencer = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[2]/a[2]');
                await saveInfluencer.click();
                await driver.sleep(1000);
            
                let search = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[1]/input');
                await search.click();
                await page.write(search, randomInfluencerName);
                await driver.sleep(3000);

                let influencerClick = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/p[1]');
                await influencerClick.click();
                await driver.sleep(2000);

                let editinfluencer = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/ul/li[1]');
                await editinfluencer.click(); 
                await driver.sleep(2000);               

                let instAccount = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[4]/input');
                await page.write(instAccount, randomInfluencerName);
                await driver.sleep(2000);

                let seconddata = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[4]/div/a/div');
                await seconddata.click();

                let fbAccount = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[6]/input');
                await page.write(fbAccount, randomInfluencerName);
                await driver.sleep(2000);

                let thirddata = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[6]/div/a[1]/div/h4');
                await thirddata.click();

                let ytAccount = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[7]/input');
                await page.write(ytAccount, randomInfluencerName);
                await driver.sleep(2000);

                let fourthdata = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[1]/label[7]/div/a[1]');
                await fourthdata.click();

                let saveaData = await page.findByXPath('//*[@id="edit-influencer"]/div/div/div/div[2]/a[2]');
                await saveaData.click();
                addContext(this.ctx, 'Influencer edited successfully: '+randomInfluencerName);
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));
                        
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();