/**
 * ### TEST CASE:
 * I-IM-Dashboard-008
 *
 * ### TEST TITLE:
 * Invite Influencers with csv file
 *
 * ### TEST SUMMARY:
 * User is trying to Invite Influencers
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Invite Influencers with csv file', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Invite Influencers with csv file', async () => {
                await page.redirectToIMDashboard(); 

                let addInfluencer = await page.findByXPath('//*[@id="manage-accounts"]/div[1]/div/div[2]/button');
                await addInfluencer.click();
                await driver.sleep(1000);
                
                let fileInput = await page.findById('fileUpload');
                await page.write(fileInput, 'C:/wamp64/www/keyhole-test-suite/files/SampleEmails - Sheet2.csv');
            
                let next = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[2]/div[2]/div[2]/div[2]/a[2]');
                await next.click();
                await driver.sleep(1000);

                let confirm = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[2]/div[3]/div[3]/a[2]');
                await confirm.click();

                let sendEmail = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[2]/div[4]/div[2]/a[2]');

                addContext(this.ctx, 'Invited Influencers Successfully');

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();