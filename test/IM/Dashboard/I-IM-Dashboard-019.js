/**
 * ### TEST CASE:
 * I-IM-Dashboard-019
 *
 * ### TEST TITLE:
 * Filter By Tag
 *
 * ### TEST SUMMARY:
 * User should be able to click on the influencer and filter it by tagname
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Filter By Tag', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to click on the influencer and filter it by tagname', async () => {
                await page.redirectToIMDashboard();

                let randomTag = randomWord(), randomname = randomName();    

                let checkTag = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__tags.js-influencersFilters__tags > div > div.dropdown-display-label > div > span > input[type=text]');
                if (checkTag) {

                    let filter = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__tags.js-influencersFilters__tags > div > div.dropdown-display-label > div > span > input[type=text]');
                    await filter.click();
                    await driver.wait(until.elementLocated(By.css('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__tags.js-influencersFilters__tags > div > div.dropdown-main > ul > li:nth-child(1)'), 2000));

                    let selectOption = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__tags.js-influencersFilters__tags > div > div.dropdown-main > ul > li:nth-child(1)');
                    let tagText = await selectOption.getText();
                    await selectOption.click();
                    await driver.sleep(2000);

                    let noResult = await page.checkElementByCSS('#influencers > tbody > tr.influencer__row');

                    if (noResult) {
                        let totalInfluencer = await page.findElementsByCSS('#influencers > tbody > tr');
                        totalInfluencer = totalInfluencer.length;

                        addContext(this.ctx, 'Total Influencers with tag: '+ totalInfluencer);
                    }
                    else {
                        addContext(this.ctx, 'Search result empty. No Influencers found');
                    }

                }
                else{
                    addContext(this.ctx, 'No influencer tag found or added');
                }

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();