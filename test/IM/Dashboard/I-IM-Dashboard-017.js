/**
 * ### TEST CASE:
 * I-IM-Dashboard-017
 *
 * ### TEST TITLE:
 * add new Tag to the Influencers from Unauthenticated
 *
 * ### TEST SUMMARY:
 * User should be able to click on the Unauthenticated influencer and add new tag 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('add new Tag to the Unauthenicated Influencers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to click on the Unauthenticated influencer and add new tag ', async () => {
                await page.redirectToIMDashboard();

                let randomTag = randomWord();

                let unauthenticated = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[1]/div[1]/button[3]');
                await unauthenticated.click();
                await driver.sleep(2000);

                let selectInfluencer = await page.findByCSS('#influencers > tbody > tr:nth-child(2) > td.influencer__item.influencer__item--checkbox > button');
                await selectInfluencer.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span'), 20000));

                let tagInfluencer = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span');
                await tagInfluencer.click();
                await driver.sleep(2000);

                let checktag = await page.checkElementByCSS('#influencers > tbody > tr:nth-child(2) > td.influencer__item.influencer__item--email.js-influencer__item--email > div > span > span');
                if (checktag) {
                    let addTag = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/ul/li[2]');
                    await addTag.click();
                    await driver.sleep(2000);
                }else{                
                    let createTag = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/ul/li');
                    await createTag.click();
                    await driver.sleep(2000);
                }

                let tagName = await page.findByXPath('//*[@id="keyjs-createInflTagInput"]');
                await page.write(tagName, randomTag);

                let apply = await page.findByCSS('#content-wrapper > section.key-modalContainer.key-modalContainer--show.key-modalContainer--createInflTag.keyjs-modalContainer--createInflTag > div > div.key-modal__wrapper > div.key-modal__footer > button');
                await apply.click();    
                await driver.wait(until.elementLocated(By.css('#influencers > tbody > tr:nth-child(2)'), 20000));

                let influencerTagName = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[2]/div/span/span');
                influencerTagName = await influencerTagName.getText();

                assert.equal(influencerTagName, randomTag);
                addContext(this.ctx, 'Influencer Tag Created successfully: '+influencerTagName);
                
            });

            it('User should be able to Add new tag into multiple Unautheticated Influencers.', async () => {
                await page.redirectToIMDashboard();

                let randomTag = randomWord();

                let unauthenticated = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[1]/div[1]/button[3]');
                await unauthenticated.click();
                await driver.sleep(2000);

                for (var i = 2; i <= 3; i++) {

                let selectInfluencer = await page.findByCSS('#influencers > tbody > tr:nth-child('+i+') > td.influencer__item.influencer__item--checkbox > button');
                await selectInfluencer.click();
                await driver.sleep(2000);
                }
                
                let tagInfluencer = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/div/span');
                await tagInfluencer.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/ul/li'), 20000));

                let createTag = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div[2]/div/div/div/ul/li[2]');
                await createTag.click();
                await driver.sleep(2000);

                let tagName = await page.findByXPath('//*[@id="keyjs-createInflTagInput"]');
                await page.write(tagName, randomTag);
                let apply = await page.findByCSS('#content-wrapper > section.key-modalContainer.key-modalContainer--show.key-modalContainer--createInflTag.keyjs-modalContainer--createInflTag > div > div.key-modal__wrapper > div.key-modal__footer > button');
                await apply.click();
                await driver.wait(until.elementLocated(By.css('#influencers > tbody > tr:nth-child(2)'), 20000));

                let influencerTagName = await page.findByXPath('//*[@id="influencers"]/tbody/tr[3]/td[2]/div/span/span');
                influencerTagName = await influencerTagName.getText();

                assert.equal(influencerTagName, randomTag);
                addContext(this.ctx, 'Tag added into multiple Influencers');
                
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();