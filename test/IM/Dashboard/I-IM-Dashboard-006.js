/**
 * ### TEST CASE:
 * I-IM-Dashboard-006
 *
 * ### TEST TITLE:
 * Remove social Account connection
 *
 * ### TEST SUMMARY:
 * User is trying to Remove social Account connection from Influencer
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Remove social Account connection', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });


            it('Remove social Account connection', async () => {
                await page.redirectToIMDashboard();

                let randomInfluencerName = randomWord();

                let addInfluencer = await page.findByXPath('//*[@id="manage-accounts"]/div[1]/div/div[2]/button');
                await addInfluencer.click();
                await driver.sleep(1000);
                
                let manualAdd = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[1]/button[2]');
                await manualAdd.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[1]/div[1]/p'), 200000));

                let name = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(2) > input[type=text]');
                await page.write(name, randomInfluencerName);

                let twtAccount = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflAccs > label:nth-child(2) > input[type=text]');
                await page.write(twtAccount, randomInfluencerName);
                await driver.sleep(3000);

                let firstdata = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[1]/div[2]/label[1]/div/a[1]/div/h4');
                await firstdata.click();

                let fbAccount = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflAccs > label:nth-child(5) > input[type=text]');
                await page.write(fbAccount, randomInfluencerName);
                await driver.sleep(3000);

                let seconddata = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[1]/div[2]/label[4]/div/a[1]/div/h4');
                await seconddata.click();

                let ytAccount = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflAccs > label:nth-child(6) > input[type=text]');
                await page.write(ytAccount, randomInfluencerName);
                await driver.sleep(3000);

                let thirddata = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[1]/div[2]/label[5]/div/a[1]/div/h4');
                await thirddata.click();

                let saveInfluencer = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[2]/a[2]');
                await saveInfluencer.click();
                await driver.sleep(2000);

                let search = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[1]/input');
                await search.click();
                await page.write(search, randomInfluencerName);
                await driver.sleep(2000);

                let totalAccounts = await page.findElementsByCSS('#influencers > tbody > tr.influencer__row > td.influencer__item--accounts > ul > li.influencer__item.influencer__item--user > div.influencerRow__profilePicture > img.platform');

                for (var i = 1; i < totalAccounts.length; i++) {
                    
                    let accountName = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[2]/ul['+i+']/li[1]/div[1]/img[2]');
                    accountName = await accountName.getAttribute("class");
                    let params = accountName.substring(accountName.indexOf(" ")+1);

                    let removeAccount = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[2]/ul['+i+']/li[4]/i');
                    await removeAccount.click();
                    await driver.sleep(1000);

                    let confirmRemove = await page.findByXPath('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                    await confirmRemove.click();
                    await driver.sleep(1000);

                    addContext(this.ctx, {
                        title: 'other Context',
                        value: {
                            'Influencer Name': randomInfluencerName,
                            'Test status': 'Removed Social account connection: '+params
                        }
                    });
                }

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();