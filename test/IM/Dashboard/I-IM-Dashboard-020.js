/**
 * ### TEST CASE:
 * I-IM-Dashboard-020
 *
 * ### TEST TITLE:
 * Edit new Tag Influencers from filter tag dropdown
 *
 * ### TEST SUMMARY:
 * User trying to edit and delete tag from filter Tag Drop-down
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Edit new Tag Influencers from filter tag dropdown', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Edit new Tag Influencers from filter tag dropdown', async () => {
                await page.redirectToIMDashboard();

                let randomTag = randomWord(), randomname = randomName();

                let checkFilter = await page.findByCSS('#manage-accounts >  div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__tags.js-influencersFilters__tags > div > div.dropdown-display-label > div > span > input[type=text]');
                if (checkFilter) {
                    let filterClick = await page.findByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__tags.js-influencersFilters__tags > div > div.dropdown-display-label > div > span > input[type=text]');
                    await filterClick.click();

                    let options = await page.findElementsByCSS('#manage-accounts > div.wrapper.accounts-wrapper > div.influencers.tab__content.js-tab__content.tab__content--active > div > div.influencers__filters > div.body > div.influencersFilters__tags.js-influencersFilters__tags > div > div.dropdown-main > ul > li');
                    options = await options.length;

                    if (options > 1) {
                        let selectfirst = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/div/div[2]/ul/li[1]');
                        selectfirst = await selectfirst.getText();

                        let selectOption = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/div/div[2]/ul/li[1]/button/i');
                        await selectOption.click();
                        await driver.sleep(2000);
                    }else{
                        let selectOption = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/div/div[2]/ul/li/button/i');
                        await selectOption.click();
                        await driver.sleep(2000);
                    }
                 

                let tagName = await page.findByXPath('//*[@id="keyjs-editInflTagInput"]');
                await page.write(tagName, randomTag);

                let save = await page.findByCSS('#content-wrapper > section.key-modalContainer.key-modalContainer--show.key-modalContainer--editInflTag.keyjs-modalContainer--editInflTag > div > div.key-modal__wrapper > div.key-modal__footer > button.kc-button.kc-button--caps.kc-button--small.kc-button--orange.keyjs-editInflTagButton');
                await save.click();
                addContext(this.ctx, 'Tag Added to the selected Tracker');
                await driver.sleep(2000);

                if (options > 1) {
                    let selectsecond = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/div/div[2]/ul/li[1]');
                    selectsecond = await selectsecond.getText();

                    let selectoption = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/div/div[2]/ul/li[1]/button/i');
                    await selectoption.click();
                    await driver.sleep(2000);
                }else{
                    let selectoption = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[2]/div/div[2]/ul/li/button/i');
                    await selectoption.click();
                    await driver.sleep(2000);
                }

                assert.notEqual(selectsecond, selectfirst);
                addContext(this.ctx, 'Tag Name Edited Successfully');
                }else{
                    addContext(this.ctx, 'No Tag Found or Added.');
                }

            });
                
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();