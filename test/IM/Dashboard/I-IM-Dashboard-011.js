/**
 * ### TEST CASE:
 * I-IM-Dashboard-011
 *
 * ### TEST TITLE:
 * Navigate to Expired Influencer
 *
 * ### TEST SUMMARY:
 * User is trying to Navigate to Expired Influencer account
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to Expired Influencer', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });
                
                await page.gotoLogin();
            });

            it('Navigate to Expired Influencer', async () => {
                await page.redirectToIMDashboard();

                let expired = await page.findByXPath('        //*[@id="manage-accounts"]/div[3]/div[3]/div/div[2]/div/div[1]/button[4]');
                await expired.click();
                await driver.sleep(1000);
                
                let checkEle = await page.checkElementByCSS('#influencers > tbody > tr.influencer__row > td.influencer__item.influencer__item--email.js-influencer__item--email');
                if (checkEle) {
                    addContext(this.ctx, 'Navigated to Expired Influencer.');
                }
                else{
                    addContext(this.ctx, 'No Expired Influencer found');
                }
                                
            });
  
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();