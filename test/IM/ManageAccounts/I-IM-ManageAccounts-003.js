/**
 * ### TEST CASE:
 * I-IM-ManageAccounts-003
 *
 * ### TEST TITLE:
 * Add authentication token - Facebook
 *
 * ### TEST SUMMARY:
 * Users should be able to add a Facebook account connection
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add authentication token - Facebook', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Add authentication token - Facebook', async () => {
                await page.redirectToIMDashboard();

                let steps = '', randomInfluencerName = randomWord();

                let addAccount = await page.findByXPath('//*[@id="influencers"]/tbody/tr[1]/td[2]/ul/li[1]/i');
                await addAccount.click();
                await driver.sleep(1000);
                
                let manualAdd = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[1]/button[2]');
                await manualAdd.click();
                await driver.sleep(2000);

                let name = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(2) > input[type=text]');
                await page.write(name, randomInfluencerName);
                await driver.sleep(1000);

                let email = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflInfo > label:nth-child(3) > input[type=email]');
                await page.write(email, randomInfluencerName+'@gmail.com');
                await driver.sleep(1000);

                let fbAccount = await page.findByCSS('#authenticate-influencers > div > div > div.modal-content.edit-influencer__modal-content.modal-tabber-content.modal-tabber-content--active > div.modal-body > div.modal-section-container > div.modal-section.modal-section--inflAccs > label:nth-child(5) > input[type=text]');
                await page.write(fbAccount, randomInfluencerName);
                await driver.sleep(3000);

                let firstdata = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[1]/div[2]/label[4]/div/a[1]/div/h4');
                await firstdata.click();

                let saveAccount = await page.findByXPath('//*[@id="authenticate-influencers"]/div/div/div[3]/div[2]/div[2]/a[2]');
                await saveAccount.click();
                await driver.sleep(2000);

                let search = await page.findByXPath('//*[@id="manage-accounts"]/div[3]/div[3]/div/div[1]/div[2]/div[1]/input');
                await search.click();
                await page.write(search, randomInfluencerName);
                await driver.sleep(2000);

                let checkEle = await page.checkElementByCSS('#influencers > tbody > tr.influencer__row > td.influencer__item--accounts > ul > li.influencer__item.influencer__item--user > div.influencerRow__profilePicture > img.platform.facebook');
                if (checkEle) {
                let influencerName = await page.findByXPath('//*[@id="influencers"]/tbody/tr[2]/td[1]/p[1]');
                influencerName = await influencerName.getText();
                assert.equal(influencerName, randomInfluencerName);
                addContext(this.ctx, 'Add Facebook authentication token - successfully: '+randomInfluencerName);
                }
                else{
                    addContext(this.ctx, 'Facebook authentication token not found');
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();