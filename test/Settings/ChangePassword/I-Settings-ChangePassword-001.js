/**
 * ### TEST CASE:
 * I-Settings-ChangePassword-001
 
 * ### TEST TITLE:
 * Change password - settings
 *
 * ### TEST SUMMARY:
 * User should be able to Change password from 'Settings' page 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Change password - settings', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            let total_test_cases = 9

            for (var i = 1; i <= total_test_cases; i++) {
                let test_title = '', test_curpwd = '', test_newpwd = '', test_conpwd = ''

                switch (i) {
                    case 1:
                        test_title = 'change pwd without current pwd,new pwd and confirm password'
                        break;
                    case 2:
                        test_title = 'change pwd with current pwd and empty new pwd,confirm password'
                        test_curpwd = config.userPass
                        break;
                    case 3:
                        test_title = 'change pwd with current pwd,confirm pwd and empty new pwd'
                        test_curpwd = config.userPass
                        test_conpwd = randomPassword()
                        break;
                    case 4:
                        test_title = 'change pwd with current pwd,new passowrd and empty confirm password'
                        test_curpwd = config.userPass
                        test_newpwd = config.usernewPass
                        break;
                    case 5:
                        test_title = 'change pwd with wrong current pwd ,new password and confirm password'                                     
                        test_curpwd = randomPassword()
                        test_newpwd = randomPassword()
                        test_conpwd = randomPassword()
                        break;
                    case 6:
                        test_title = 'change pwd with valid current pwd ,wrong new password and wrong confirm pwd'
                        test_curpwd = config.userPass
                        test_newpwd = randomPassword()
                        test_conpwd = randomPassword()
                        break;
                    case 7:
                        test_title = 'change pwd with valid current pwd ,valid new password and wrong confirm password'
                        test_curpwd = config.userPass
                        test_newpwd = config.usernewPass
                        test_conpwd = randomPassword()
                        break;
                    case 8:
                        test_title = 'change pwd with wrong current pwd and valid new password and confirm password'
                        test_curpwd = randomPassword()
                        test_newpwd = config.usernewPass
                        test_conpwd = config.usernewPass
                        break; 
                    case 9:
                        test_title = 'change pwd with valid current pwd and valid new password & confirm password.'
                        test_curpwd = config.userPass
                        test_newpwd = config.usernewPass
                        test_conpwd = config.usernewPass

                }

                it(test_title, async () => {
                    await page.redirectToDashboard();
                    
                    let checkEle = await page.checkElement('error_msgs');
                    if (checkEle) {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Test status': 'Unable to perform test..Password changed without confirmed new password.'
                            }
                        });
                    }
                    else{                        

                        let steps = '', status = '';

                        let dropdown = await page.findByXPath('//*[@id="user-email"]');
                        await dropdown.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                        let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                        await settings.click();
                        steps += 'Navigated to Settings Page '
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                        let changePwd = await page.findByXPath('//*[@id="General-Settings"]/nav/ul/li[2]/a');
                        await changePwd.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="Change-Password"]/form/h1'), 3000));

                        let currentPassword = await page.findByCSS('#Change-Password > form > fieldset > div:nth-child(1) > div > input');
                        await page.write(currentPassword, test_curpwd);

                        let newPassword = await page.findByCSS('#Change-Password > form > fieldset > div:nth-child(2) > div > input');
                        await page.write(newPassword, test_newpwd);

                        let confirmPassword = await page.findByCSS('#Change-Password > form > fieldset > div:nth-child(3) > div > input');
                        await page.write(confirmPassword, test_conpwd);

                        let saveChanges = await page.findByXPath('//*[@id="save_changes"]');
                        await saveChanges.click();
                        status += 'Edited Account Successfully'
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="Change-Password"]/form/h1'), 3000));
                        
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Current Password entered: ': test_curpwd,
                                'New Password entered: ': test_newpwd,
                                'Confirm Password entered: ': test_conpwd
                            }
                        });                                   
                    }
                });
            }    
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
