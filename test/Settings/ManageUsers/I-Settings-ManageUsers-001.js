/**
 * ### TEST CASE:
 * I-Settings-ManageUsers-001
 
 * ### TEST TITLE:
 *  Add New user -Manage Users
 *
 * ### TEST SUMMARY:
 * User should be able to Add New user from manage user Page
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add New user -Manage Users', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            let total_test_cases = 3

            for (var i = 1; i <= total_test_cases; i++) {
                let test_title = '', test_email = '', role_option = ''

                switch (i) {
                    case 1:
                        test_title = 'Invite User with filling all fields'
                        test_email = config.inviteEmail
                        role_option = randomNumberFromTo(1,2);
                        break;
                    case 2:
                        test_title = 'Invite User with already registered email'
                        test_email = config.usedEmail
                        role_option = randomNumberFromTo(1,3);
                        break;
                    case 3:
                        test_title = 'Invite User with same email'
                        test_email = config.userEmail
                        role_option = randomNumberFromTo(1,2);
                        break;
                
                }

                it(test_title, async () => {
                    await page.redirectToDashboard();

                    let steps = '', status = '';

                    let dropdown = await page.findByXPath('//*[@id="user-email"]');
                    await dropdown.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                    let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                    await settings.click();
                    steps += 'Navigated to settings, '
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                    let manageUsers = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(3) > a');
                    await manageUsers.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/h1'), 3000));

                    let addUser = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[2]/div[1]/button');
                    await addUser.click();
                    steps += 'add user button clicked';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="content-wrapper"]/section/div/div[2]/div/h3'), 3000));

                    let emailId = await page.findByXPath('//*[@id="emailaddress"]');
                    await emailId.click();
                    await page.write(emailId, test_email);

                    let role = await page.findByXPath('//*[@id="content-wrapper"]/section/div/div[2]/form/div/label[2]/div/div');
                    await role.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="content-wrapper"]/section/div/div[2]/form/div/label[2]/div/ul/li[1]'), 3000));
                  
                    let roleOption = await page.findByXPath('//*[@id="content-wrapper"]/section/div/div[2]/form/div/label[2]/div/ul/li['+role_option+']');
                    let roleText = await roleOption.getText();
                    await roleOption.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="content-wrapper"]/section/div/div[2]/form/button'), 3000));

                    let submit = await page.findByXPath('//*[@id="content-wrapper"]/section/div/div[2]/form/button');
                    await submit.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[2]/div[1]/button'), 3000));

                    let inviteMsgCheck = await page.checkElementByCSS('#content-wrapper > section > div > div.key-modal__wrapper > form > ul > li');

                        if (inviteMsgCheck) {
                            invite_msg = await page.findByCSS('#content-wrapper > section > div > div.key-modal__wrapper > form > ul > li');
                            invite_msg = await invite_msg.getText();
                        }
                        else
                            invite_msg = 'Error occured';
                    
                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Test steps': steps,
                            'Email: ': test_email,
                            'Role option selected': roleText,
                            'Invite message': invite_msg
                        }
                    });               
                });
            }

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
