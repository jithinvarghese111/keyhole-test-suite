 /**
 * ### TEST CASE:
 * I-Settings-ManageUsers-004
 
 * ### TEST TITLE:
 *  Edit subaccount -Manage Users
 *
 * ### TEST SUMMARY:
 * User should be able to Edit subaccount from Manage Users page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Edit subaccount -Manage Users', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Edit subaccount', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, '
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageUsers = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(3) > a');
                await manageUsers.click();
                steps += 'Navigated to Manage users page, '
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/h1'), 3000));

                await page.scrollPage('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/thead/tr/th[1]');
                await driver.sleep(2000);

                let role = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[2]/div/div/span');
                await role.click();
                let role_text = await role.getText();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[2]/div/ul/li[3]'), 3000));

                let role_opt = await randomNumberFromTo(2,4);

                let roleOption = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[2]/div/ul/li['+role_opt+']');
                await roleOption.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[2]/div/div/span'), 6000));

                let checkLimit = await page.checkElementByCSS('#Manage-Users > div > div > div.form__body > div.keyhole-table.users-table > div > table > tbody > tr:nth-child(2) > td.user__postLimit > p:nth-child(1) > strong > input');
                if (!checkLimit) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test steps': steps,
                            'Test status': 'Limit not applicable, '
                        }
                    });                    
                }
                else{

                    let limit = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[3]/p[1]/strong/input');
                    await limit.click();
                    await limit.clear();
                    await page.write(limit, '1000000');
                    await driver.wait(until.elementLocated(By.css('#Manage-Users > div > div > div.form__body > div.keyhole-table.users-table > div > table > tbody > tr.user.edited > td.user__names > p'), 3000));
                    
                    error_msg = await page.findByCSS('#Manage-Users > div > div > div.form__body > div.keyhole-table.users-table > div > table > tbody > tr.user.edited > td.user__names > p');
                    error_msg = await error_msg.getText();
                    assert.equal(error_msg, 'Limit cannot be greater than 100,000');

                    let limits = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[3]/p[1]/strong/input');
                    await limits.click();
                    await limits.clear();
                    await page.write(limits, '10000');
                    steps += 'Changed Limit of the subAccount';
                    let limitUsed = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[3]/p[2]');
                    let firstUsed = await limitUsed.getText();
                    steps += ' ,Error occured'+error_msg;

                    await page.scrollPage('//*[@id="Manage-Users"]/div/div/div[3]/div[3]/button');
                    await driver.sleep(1000);

                    let saveChange = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[3]/button');
                    let saveValue = await saveChange.getAttribute("class");

                    if (saveValue.indexOf('no-changes') > -1) {
                        status += 'Unable to click on Save Changes button'
                    }
                    else{
                        let saveChanges = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[3]/button');
                        await saveChanges.click();
                        status += 'Clicked on Save Changes button, '
                        await driver.sleep(2000);
                    }

                    await page.scrollPage('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[1]');
                    await driver.sleep(1000);
                    
                    let firstrole = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[2]/div/div/span');
                    let updatedRole = await firstrole.getText();
                    await driver.sleep(3000);

                    let checkpost = await page.checkElementByCSS('#Manage-Users > div > div > div.form__body > div.keyhole-table.users-table > div > table > tbody > tr:nth-child(2) > td.user__postLimit');
                    if (!checkpost) {
                        let firstuser = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[3]/p[1]/strong/input');
                        let updatedLimit = firstuser.getAttribute("value");
                    }
                    assert.notEqual(updatedRole, role_text);
                    status += 'Changes updated successfully';
                   
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test steps': steps,
                            'Changed role option': updatedRole,
                            'Test status': status
                        }
                    });
                }               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
