 /**
 * ### TEST CASE:
 * I-Settings-ManageUsers-003
 
 * ### TEST TITLE:
 * Remove subaccount -Manage Users
 *
 * ### TEST SUMMARY:
 * User should be able to Edit subaccount from Manage Users page
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Remove subaccount -Manage Users', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Remove subaccount ', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageUsers = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(3) > a');
                await manageUsers.click();
                steps += 'Navigated to Manage user, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/h1'), 3000));

                await page.scrollPage('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[1]/td[1]/div[1]');
                await driver.sleep(2000);

                let firstsubaccount = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[1]/div[1]');
                let subaccountText = await firstsubaccount.getText();

                let remove = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/div/div/span');
                await remove.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/div/ul/li'), 3000));

                let removeAccount = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/div/ul/li');
                await removeAccount.click();
                steps += 'clicked on Remove user option';
                await driver.sleep(3000);

                let secondsubaccount = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[1]/div[1]');
                let secondText = await secondsubaccount.getText();
                assert.notEqual(secondText, subaccountText);
                status = 'User removed successfully';
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
