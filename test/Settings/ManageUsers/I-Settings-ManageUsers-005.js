 /**
 * ### TEST CASE:
 * I-Settings-ManageUsers-005
 
 * ### TEST TITLE:
 * un-Remove subaccount user -Manage Users
 *
 * ### TEST SUMMARY:
 * User should be able to Edit subaccount from Manage Users page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('un-Remove User -Manage Users', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Un-Remove User', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, '
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageUsers = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(3) > a');
                await manageUsers.click();
                steps += 'Navigated to Manage users page, '
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/h1'), 3000));

                await page.scrollPage('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/thead/tr/th[1]');
                await driver.sleep(1000);

                let firstsubaccount = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[1]/div[1]');
                let subaccountText = await firstsubaccount.getText();

                let remove = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/div/div/span');
                await remove.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/div/ul/li'), 3000));

                let removeAccount = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/div/ul/li');
                await removeAccount.click();
                steps += 'clicked on Remove user option, '
                await driver.sleep(6000);

                let secondsubaccount = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[1]/div[1]');
                let secondText = await secondsubaccount.getText();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[1]/div[1]'), 8000));
                assert.notEqual(secondText, subaccountText);
                status = 'User removed successfully, ';

                await page.scrollPage('//*[@id="Manage-Users"]/div/div/div[3]/div[3]/label');
                await driver.sleep(2000);

                let showRemoveduser = await page.findByCSS('#Manage-Users > div > div > div.form__body > div.action-wrapper.action-wrapper--saveChanges > label > input');
                await showRemoveduser.click();
                await driver.sleep(3000);

                await page.scrollPage('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/thead/tr/th[1]');
                await driver.sleep(3000);

                let unremove = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/div/div');
                await unremove.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[3]/td[5]/div/ul/li'), 3000));

                let unemoveAccount = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/div/ul/li');
                await unemoveAccount.click();
                steps += 'clicked on un-Remove User option';
                await driver.sleep(6000);

                let checkRemove = await page.findByCSS('#Manage-Users > div > div > div.form__body > div.keyhole-table.users-table > div > table > tbody > tr:nth-child(2) > td.user__role > div > div > span');
                let removetext = await checkRemove.getText();
                assert.notEqual(removetext, 'Removed');
                status += 'un-Removed User successfully.';

                await page.scrollPage('//*[@id="Manage-Users"]/div/div/h1');
                await driver.sleep(3000);
                
                let activity = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[2]/div[2]/button');
                await activity.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-logs__modal-step1"]/div/div/h2/span'), 3000));

                let userActivity = await page.findByXPath('//*[@id="user-logs__modal-step1"]/div/div/div/table/tbody/tr[1]/td[3]/p');
                let latestactivity = await userActivity.getText();

                let closeactivity = await page.findByXPath('//*[@id="user-logs__modal-step1"]/div/div/h2/a');
                await closeactivity.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[2]/div[1]/button'), 3000));

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status,
                        'Latest user activity': latestactivity
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
