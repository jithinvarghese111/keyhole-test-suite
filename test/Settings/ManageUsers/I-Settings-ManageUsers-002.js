 /**
 * ### TEST CASE:
 * I-Settings-ManageUsers-002
 
 * ### TEST TITLE:
 *  Added New user exist in the list -Manage Users
 *
 * ### TEST SUMMARY:
 * User should be able to check Add New user exist in the user list -Manage Users
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Added New user exist in the list -Manage Users', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Added New user exist in the list', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, '
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageUsers = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(3) > a');
                await manageUsers.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[2]/div[1]/button'), 6000));

                let addUser = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[2]/div[1]/button');
                await addUser.click();
                steps += 'add user button clicked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="content-wrapper"]/section/div/div[2]/div/h3'), 3000));

                let email = config.inviteEmail;

                let emailId = await page.findByXPath('//*[@id="emailaddress"]');
                await emailId.click();
                await page.write(emailId, email);
                let emailText = await emailId.getText();

                let role = await page.findByXPath('//*[@id="content-wrapper"]/section/div/div[2]/form/div/label[2]/div/div');
                await role.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="content-wrapper"]/section/div/div[2]/form/div/label[2]/div/ul/li[1]'), 3000));
                
                let role_option = await randomNumberFromTo(1,3);

                let roleOption = await page.findByXPath('//*[@id="content-wrapper"]/section/div/div[2]/form/div/label[2]/div/ul/li['+role_option+']');
                let roleText = await roleOption.getText();
                await roleOption.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="content-wrapper"]/section/div/div[2]/form/div/label[2]/div/div/span'), 3000));

                let submit = await page.findByXPath('//*[@id="content-wrapper"]/section/div/div[2]/form/button');
                await submit.click();
                steps += 'Clicked submit button';
                await driver.sleep(3000);                

                await page.scrollPage('//*[@id="content-wrapper"]/section/div/div[2]/form/ul/li');
                await driver.sleep(2000);
                
                let invitemsg = await page.findByCSS('#content-wrapper > section > div > div.key-modal__wrapper > form > ul > li');
                invitemsg = await invitemsg.getText();
                
                let popupClose = await page.findByXPath('//*[@id="content-wrapper"]/section/div/div[2]/div/button');
                await popupClose.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Manage-Users"]/div/div/div[2]/div[1]/button'), 3000));

                let totalusers = await page.findElementsByCSS('#Manage-Users > div > div > div.form__body > div.keyhole-table.users-table > div > table > tbody > tr > td.user__names > div.user__fullname');
                for (var i = 1; i < totalusers.length; i++) {

                    let totallist = await page.findByXPath('//*[@id="Manage-Users"]/div/div/div[3]/div[2]/div/table/tbody/tr['+i+']/td[1]/div[1]');
                    let userText = await totallist.getText();
                    
                    if (userText.indexOf(email) > -1) {

                        status += 'Invited Account is existed in this list, ';
         
                        } 
                }
                status += 'Account is not existed in the list, please login invited account ..';
               
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Email: ': email,
                        'Role option selected': roleText,
                        'Test status': status,
                        'Invite message': invitemsg
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
