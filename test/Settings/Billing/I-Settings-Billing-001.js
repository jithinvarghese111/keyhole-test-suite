 /**
 * ### TEST CASE:
 * I-Settings-Billing-001
 
 * ### TEST TITLE:
 * Navigate to Billing / Subscriptions
 *
 * ### TEST SUMMARY:
 * 
 *User should be able to Navigate to Billing / Subscriptions.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to Billing / Subscriptions Page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Navigate to Billing / Subscriptions Page', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                let settingsUrl = await settings.getAttribute("href");
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, settingsUrl);

                let billing = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(6) > a');
                await billing.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="Subscriptions"]/div/h3'), 3000));

                let checkEle = await page.checkElementByCSS('#Subscriptions > div > h3');
                if (checkEle) {
                    status += 'Navigated to Billing / Subscriptions.';
                }
                else{
                    status += 'page not found';
                }

                let paypal = await page.findByXPath('//*[@id="Subscriptions"]/div/a');
                await paypal.click();
                status += ' ,Clicked on payPal';
                await driver.sleep(3000);

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
