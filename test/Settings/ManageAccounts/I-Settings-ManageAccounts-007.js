 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-007
 
 * ### TEST TITLE:
 * Re-connect facebook Accounts - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * User should be able to Add Facebook Accounts from Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Re-connect facebook Accounts ', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Re-connect facebook Account', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts, ';
                await driver.sleep(3000);

                await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                await driver.sleep(2000);

                let fbaccount = await page.findElementsByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > table > tbody > tr > td.account-info > div.account-names')
                let fbaccountCount = await fbaccount.length;
                status += 'Added Facebook account count: '+fbaccountCount;

                for (var i = 1; i < fbaccountCount; i++) {
                                   
                    let checkexpired = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > table > tbody > tr.inactive > td.action-items > div');
                    if (checkexpired) {
                        let re_connect = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[4]/table/tbody/tr['+i+']/td[2]/a');
                        await re_connect.click();
                        await driver.sleep(3000);

                        let checkfb = await page.checkElementByCSS('#email');
                        if (checkfb) {
                            let fbEmail = await page.findByXPath('//*[@id="email"]');
                            await fbEmail.click();
                            await page.write(fbEmail, 'qa@wdstech.com');

                            let fbPwd = await page.findByXPath('//*[@id="pass"]');
                            await fbPwd.click();
                            await page.write(fbPwd, 'micr@s@ft123');

                            let submit = await page.findByXPath('//*[@id="loginbutton"]');
                            await submit.click();
                            await driver.sleep(2000);

                            let checkfb = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                            if (checkfb) {
                                let accntName = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[2]');
                                let userName = await accntName.getText();
                                status += ' ,Reconnected facebook Account: '+userName;
                            }
                            else{
                                status += ' ,Facebook is not reconnected.';
                            }
                        } 
                        else{
                            status += 'Error Occured';
                        }  
                    }
                    else{
                        status += ' ,No other Expired account found.';
                    }
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
