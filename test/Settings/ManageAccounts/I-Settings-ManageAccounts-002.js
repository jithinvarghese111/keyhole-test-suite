 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-002
 
 * ### TEST TITLE:
 * Remove Twitter Accounts - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * User should be able to Remove Twitter Accounts from Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Remove Twitter Accounts  - Manage Connected Accounts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Remove Twitter Accounts ', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, '
               await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts page, '
                await driver.sleep(4000);

                let checkaccount = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                if (checkaccount) {
                    let twtfirstAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[1]');
                    let firstText = await twtfirstAcnt.getText();

                    let remove = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[2]/a');
                    await remove.click();
                    await driver.sleep(1000);

                    let confirmRemove = await page.findByXPath('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                    await confirmRemove.click();
                    await driver.sleep(2000);

                    let checktwt = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                    if (checktwt) {
                        let twitterAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[1]');
                        let secondText = await twitterAcnt.getText();
                        assert.notEqual(secondText, firstText);
                        status += 'Twitter Account removed successfully';

                        if (secondText == firstText) {
                            status += 'Twitter Account not removed';
                        } 
                              
                    }
                    else{

                        let checkexisted = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > div.no-connected-accounts > span');
                        if (checkexisted) {
                            status += 'twitter Account removed successfully'
                        }
                        else{
                            status += 'twitter Account is not removed.'
                        } 
                    }   
                }
                else{

                    let connectTwitter = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > div.account-heading > a > i');
                    await connectTwitter.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="username_or_email"]'), 3000));

                    let twtEmail = await page.findByXPath('//*[@id="username_or_email"]');
                    await twtEmail.click();
                    await page.write(twtEmail, 'qa@wdstech.com');

                    let twtPwd = await page.findByXPath('//*[@id="password"]');
                    await twtPwd.click();
                    await page.write(twtPwd, 'micr@s@ft123');

                    let submit = await page.findByXPath('//*[@id="allow"]');
                    await submit.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr[1]/td[1]/div[2]/div[1]'), 8000));

                    let checktwt = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                    if (checktwt) {
                        let twtAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[1]');
                            let firstText = await twtAcnt.getText();
                        steps += 'Twitter Account added: '+firstText;

                        let remove = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[2]/a');
                        await remove.click();
                        await driver.sleep(3000);

                        let confirmRemove = await page.findByXPath('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                        await confirmRemove.click();
                        await driver.sleep(3000);

                        let checkexisted = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-instagram.accounts-platform > div.no-connected-accounts > span');
                        if (checkexisted) {
                            status += 'Twitter Account removed successfully'
                        }
                        else{
                            status += 'Twitter Account is not removed.'
                        }                        
                    }
                    else{
                        status += 'Twitter Account is not added.';
                    }   
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });
            
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
