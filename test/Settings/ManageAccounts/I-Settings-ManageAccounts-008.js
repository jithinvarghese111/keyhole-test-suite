 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-008
 
 * ### TEST TITLE:
 * Remove Facebook Accounts - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * User should be able to Remove Twitter Accounts from Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Remove Facebook Accounts  - Manage Connected Accounts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Remove Facebook Accounts ', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts, ';
                await driver.sleep(2000);

                await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[3]');
                await driver.sleep(2000);

                let checkaccount = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > table > tbody > tr.active > td.account-info > div.account-names');
                if (checkaccount) {
                    let fbfirstAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[4]/table/tbody/tr[2]/td[1]/div[2]/div');
                    let firstText = await fbfirstAcnt.getText();

                    let remove = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[4]/table/tbody/tr[2]/td[2]/a');
                    await remove.click();
                    await driver.sleep(1000);

                    let confirmRemove = await page.findByXPath('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                    await confirmRemove.click();
                    await driver.sleep(2000);

                    let checkfb = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div');
                    if (checkfb) {
                        let facebookAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[4]/table/tbody/tr[2]/td[1]/div[2]/div');
                        let secondText = await facebookAcnt.getText();
                        assert.notEqual(secondText, firstText);
                        status += 'facebook Account removed successfully';                      
                    }
                    else{

                        let checkexisted = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > div.no-connected-accounts > span');
                        if (checkexisted) {
                            status += ' ,facebook Account removed successfully';
                        }
                        else{
                            status += ' ,facebook Account is not removed.';
                        } 
                    }   
                }
                else{

                    let connectFacebook = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > div.account-heading > a > i');
                    await connectFacebook.click();
                   await driver.wait(until.elementLocated(By.xpath('//*[@id="email"]'), 3000));

                    let fbEmail = await page.findByXPath('//*[@id="email"]');
                    await fbEmail.click();
                    await page.write(fbEmail, 'stemy@wdstech.com');

                    let fbPwd = await page.findByXPath('//*[@id="pass"]');
                    await fbPwd.click();
                    await page.write(fbPwd, 'micr@s@ft123');

                    let submit = await page.findByXPath('//*[@id="loginbutton"]');
                    await submit.click();
                    await driver.sleep(2000);

                    await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[3]/div[1]/div');
                    await driver.sleep(2000);

                    let checkfb = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div');
                    if (checkfb) {
                        let fbfirstAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[4]/table/tbody/tr/td[1]/div[2]/div');
                        let firstText = await fbfirstAcnt.getText();
                        status += 'Facebook accound added: '+firstText;

                        let remove = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[4]/table/tbody/tr[3]/td[2]/a');
                        await remove.click();
                        await driver.sleep(1000);

                        let confirmRemove = await page.findByXPath('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                        await confirmRemove.click();
                        await driver.sleep(2000);

                        let checkexisted = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > div.no-connected-accounts > span');
                        if (checkexisted) {
                            status += ' ,facebook Account removed successfully';
                        }
                        else{
                            status += ' ,facebook Account is not removed.';
                        }                        
                    }
                    else{
                        status += ' ,facebook Account is not added.';
                    }   
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });
            
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
