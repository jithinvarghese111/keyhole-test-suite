 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-012
 
 * ### TEST TITLE:
 * Removed YouTube  account exists  - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * check removed twitter account exist or not in Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Removed YouTube  account exists', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Removed YouTube  account exist or not ', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manageConnectedAccounts"]/div/h1'), 3000));

                await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                await driver.sleep(2000);

                let checkaccount = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div');
                if (checkaccount) {
                    let ytfirstAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[5]/table/tbody/tr/td[1]/div[2]/div');
                    let firstText = await ytfirstAcnt.getText();
            
                    let remove = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[5]/table/tbody/tr/td[2]/a');
                    await remove.click();
                    await driver.sleep(1000);

                    let confirmRemove = await page.findByXPath('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                    await confirmRemove.click();
                    await driver.sleep(2000);

                    let checkyt = await page.checkElementByCSS('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                    if (checkyt) {
                        let youtubetAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[1]');
                        let secondText = await youtubetAcnt.getText();
                        
                        if (secondText != firstText) {
                           status += ' ,Youtube Account removed successfully: '+firstText;
                        }
                        else{
                            status += ' ,Youtube Account is not removed.'; 
                        }
                                               
                    }
                    else{

                        let checkexisted = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > div.no-connected-accounts > span');
                        if (checkexisted) {
                            status += ' ,Youtube Account removed successfully: '+firstText;
                        }
                        else{
                            status += ' ,Youtube Account is not removed.';
                        } 
                    }

                    let connectYoutube = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > div.account-heading > a');
                    await connectYoutube.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="identifierId"]'), 3000));

                    await driver.navigate().back();
                    await driver.sleep(1000);

                    await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                    await driver.sleep(2000);

                    let checkytText = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names');

                    if (checkytText) {
                        let youtubeAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[5]/table/tbody/tr/td[1]/div[2]/div');
                        let secondText = await youtubeAcnt.getText();
                        assert.equal(secondText, firstText);
                        status += ' ,Removed Account Exist after getback from twitter: '+secondText;
                        if (secondText !=firstText) {
                            
                            status += ' ,Removed Account is not Exist after getback from twitter';
                        }
                         
                    }
                    else{
                        status += ' ,Removed Account Exist after getback from twitter.';
                    }

                    await driver.navigate().refresh();
                    await driver.sleep(5000);

                    await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                    await driver.sleep(2000);

                    let checkText = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names');

                    if (checkText) {
                        let youtbAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[1]');
                        let thirdText = await youtbAcnt.getText();
                        status += ' ,Removed account not exist upon reload.';
                        assert.equal(thirdText, firstText);
                        if (thirdText != firstText) {
                            status += ' ,Removed account exist upon reload: '+thirdText;
                        }
                           
                    }
                    else{
                         status += ' ,Removed account not exist upon reload.';
                    }

                }
                else{

                    let connectYoutube = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > div.account-heading > a > i');
                    await connectYoutube.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="identifierId"]'), 3000));

                    let checkEle = await page.checkElementByCSS('#view_container > div > div > div.pwWryf.bxPAYd > div > div > div > form > span > section > div > span > div > div > ul > li.M8HEDc.eARute.W7Aapd.bxPAYd.SmR8.znIWoc > div > div > div.BHzsHc');
                    if (checkEle) {
                        let addacnt = await page.findByXPath('//*[@id="view_container"]/div/div/div[2]/div/div/div/form/span/section/div/span/div/div/ul/li[2]/div/div/div[2]');
                        await addacnt.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="identifierId"]'), 3000));
                    }

                    let ytEmail = await page.findByXPath('//*[@id="identifierId"]');
                    await ytEmail.click();
                    await page.write(ytEmail, 'rogerambross@gmail.com');

                    let emailnext = await page.findByXPath('//*[@id="identifierNext"]/span/span');
                    await emailnext.click();
                    await driver.sleep(2000);
                    
                    let ytPwd = await page.findByXPath('//*[@id="password"]/div[1]/div/div[1]/input');
                    await ytPwd.click();
                    await page.write(ytPwd, '123456Asd');

                    let submit = await page.findByXPath('//*[@id="passwordNext"]/span/span');
                    await submit.click();
                    await driver.sleep(5000);

                    let checkAllow = await page.checkElementByCSS('#submit_approve_access > span');
                    if (checkAllow) {
                        let allow = await page.findByXPath('//*[@id="submit_approve_access"]/span/span');
                        await allow.click();
                        await driver.sleep(2000);
                    }

                    await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                    await driver.sleep(2000);

                    let checkyt = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names');
                    if (checkyt) {
                        let accntName = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[5]/table/tbody/tr/td[1]/div[2]/div');
                        let firstText = await accntName.getText();
                        status += 'Youtube Account added: '+firstText;

                        let remove = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[5]/table/tbody/tr/td[2]/a');
                        await remove.click();
                        await driver.sleep(1000);

                        let confirmRemove = await page.findByXPath('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                        await confirmRemove.click();
                        await driver.sleep(2000);

                        let checkexist = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > div.no-connected-accounts > span');
                        if (checkexist) {
                            status += ' ,Youtube Account removed successfully';
                        }
                        else{
                            status += ' ,Youtube Account not removed.';
                        }

                        let connectYoutube = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > div.account-heading > a');
                        await connectYoutube.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="headingText"]/span'), 3000));

                        await driver.navigate().back();
                        await driver.sleep(1000);

                        await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                        await driver.sleep(2000);

                        let checkytText = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names');

                        if (checkytText) {
                            let youtubeAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[5]/table/tbody/tr/td[1]/div[2]/div');
                            let secondText = await youtubeAcnt.getText();
                            assert.equal(secondText, firstText);
                            status += ' ,Removed Account Exist after getback from Youtube: '+secondText;

                            if (secondText !=firstText) {
                                status += ' ,Removed Account not Exist after getback from Youtube';
                            }
                              
                        }
                        else{
                            status += ' ,Removed Account Exist after getback from Youtube.';
                        }

                        await driver.navigate().refresh();
                        await driver.sleep(2000);

                        await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                        await driver.sleep(2000);

                        let checkText = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names');

                        if (checkText) {
                            let youtbAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[1]');
                            let thirdText = await youtbAcnt.getText();
                            assert.notEqual(thirdText, firstText);
                            status += ' ,Removed account not exist upon reload.';

                            if (thirdText == firstText) {
                                status += ' ,Removed account exist upon reload: '+thirdText;
                            }
                               
                        }
                        else{
                             status += ' ,Removed account not exist upon reload.';
                        }
                    }
                    else{
                        status += ' ,Youtube Account not added.';
                    }   
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });      
            });
            
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
