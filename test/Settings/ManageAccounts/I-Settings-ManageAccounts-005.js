 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-005
 
 * ### TEST TITLE:
 * Removed twitter account exists  - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * check removed twitter account exist or not in Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Removed account exists', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking removed twitter account exist or not ', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts, ';
                await driver.sleep(5000);

                for (var i = 0; i < 2; i++) {
                    let connectTwitter = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > div.account-heading > a > i');
                    await connectTwitter.click();
                    await driver.sleep(2000);
                        
                    let checktwtAcnt = await page.checkElementByCSS('#username_or_email');
                    if (checktwtAcnt) {

                        let twtEmail = await page.findByXPath('//*[@id="username_or_email"]');
                        await twtEmail.click();
                        await twtEmail.clear();
                        await page.write(twtEmail, 'qa@wdstech.com');

                        let twtPwd = await page.findByXPath('//*[@id="password"]');
                        await twtPwd.click();
                        await twtPwd.clear();
                        await page.write(twtPwd, 'micr@s@ft123');

                        let submit = await page.findByXPath('//*[@id="allow"]');
                        await submit.click();
                        await driver.sleep(5000);

                        let checktwt = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                        if (checktwt) {
                            let twtAcnt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[1]');
                                let firstText = await twtAcnt.getText();
                            steps += ' ,Twitter Account added: '+firstText;

                            let remove = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[2]/a');
                            await remove.click();
                            await driver.sleep(1000);

                            let confirmRemove = await page.findByXPath('//*[@id="remove-confirmation"]/div/div/div/div[2]/a[2]');
                            await confirmRemove.click();
                            await driver.sleep(2000);

                            let checkexist = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                            if (checkexist) {
                                steps += ' ,Twitter Account removed';
                            }
                            else{
                                steps += ' ,Twitter Account is not removed';
                            }
                        
                            let connectTwitter = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > div.account-heading > a > i');
                            await connectTwitter.click();
                            await driver.sleep(2000);

                            await driver.navigate().back();
                            await driver.sleep(1000);

                            let checkacnt = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                            if (checkacnt) {
                                status += ' ,Removed Account Exist after getback from twitter';
                            }
                            else{
                                status += ' ,Removed Account is not Exist after getback from twitter';
                            }

                            await driver.navigate().refresh();
                            await driver.sleep(5000);

                            let checkremovedacnt = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                            if (checkremovedacnt) {
                                status += ' ,Removed account exist upon reload.';
                            }
                            else{
                                status += ' ,Removed account is not exist upon reload.';
                            }

                        }
                    }
                    else{

                        let twtError = await page.findByCSS('#bd > div > p');
                        let twt_error = await twtError.getText();
                        console.log(twt_error);
                        status += ' ,Error Ocuured: '+twt_error;                        
                    }
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });
            
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
