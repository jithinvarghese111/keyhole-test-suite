 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-010
 
 * ### TEST TITLE:
 * Add YouTube Account - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * User should be able to Add YouTube  Accounts from Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add YouTube Account - Manage Connected Accounts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Add YouTube Account', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 1000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 1000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts page, ';
                await driver.sleep(2000);

                await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                await driver.sleep(2000);

                let checkaccount = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names');
                if (checkaccount) {

                    let totalAccounts = await page.findElementsByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div');
                    totalAccounts = await totalAccounts.length;

                    let connectYoutube = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > div.account-heading > a > i');
                    await connectYoutube.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="identifierId"]'), 2000));
                }
                else{
                    let connectYt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[5]/div[2]/a');
                    await connectYt.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="identifierId"]'), 2000));
                }

                let checkEle = await page.checkElementByCSS('#view_container > div > div > div.pwWryf.bxPAYd > div > div > div > form > span > section > div > span > div > div > ul > li.M8HEDc.eARute.W7Aapd.bxPAYd.SmR8.znIWoc > div > div > div.BHzsHc');
                if (checkEle) {
                    let addacnt = await page.findByXPath('//*[@id="view_container"]/div/div/div[2]/div/div/div/form/span/section/div/span/div/div/ul/li[2]/div/div/div[2]');
                    await addacnt.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="identifierId"]'), 2000));
                }

                let ytEmail = await page.findByXPath('//*[@id="identifierId"]');
                await ytEmail.click();
                await page.write(ytEmail, 'rogerambross@gmail.com');

                let emailnext = await page.findByXPath('//*[@id="identifierNext"]/span/span');
                await emailnext.click();
                await driver.sleep(3000);

                let ytPwd = await page.findByXPath('//*[@id="password"]/div[1]/div/div[1]/input');
                await ytPwd.click();
                await page.write(ytPwd, '123456Asd');

                let submit = await page.findByXPath('//*[@id="passwordNext"]/span/span');
                await submit.click();
                await driver.sleep(2000);

                let checkAllow = await page.checkElementByCSS('#submit_approve_access > span');
                if (checkAllow) {
                    let allow = await page.findByXPath('//*[@id="submit_approve_access"]/span/span');
                    await allow.click();
                    await driver.sleep(2000);
                }

                await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[4]');
                await driver.sleep(2000);

                let checkyt = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-youtube.accounts-platform > table > tbody > tr > td.account-info > div.account-names');
                if (checkyt) {
                    let accntName = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[5]/table/tbody/tr/td[1]/div[2]/div');
                    let userName = await accntName.getText();
                    status += 'Youtube Account added: '+userName;
                }
                else{
                    status += 'Test failed.';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
