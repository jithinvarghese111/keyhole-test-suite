 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-001
 
 * ### TEST TITLE:
 * Add Twitter Accounts - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * User should be able to Add Twitter Accounts from Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Twitter Accounts - Manage Connected Accounts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Add Twitter Accounts', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, '
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts page, '
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manageConnectedAccounts"]/div/h1'), 3000));

                let checkaccount = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                if (checkaccount) {

                    let connectTwitter = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > div.account-heading > a > i');
                    await connectTwitter.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="username_or_email"]'), 3000));
                    // await driver.sleep(2000);
                }
                else{
                    let connectTwt = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/div[2]/a');
                    await connectTwt.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="username_or_email"]'), 3000));
                    // await driver.sleep(2000);
                }

                let twtEmail = await page.findByXPath('//*[@id="username_or_email"]');
                await twtEmail.click();
                await page.write(twtEmail, 'qa@wdstech.com');

                let twtPwd = await page.findByXPath('//*[@id="password"]');
                await twtPwd.click();
                await page.write(twtPwd, 'micr@s@ft123');

                let submit = await page.findByXPath('//*[@id="allow"]');
                await submit.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr[1]/td[1]/div[2]/div[1]'), 8000));

                let checktwt = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                if (checktwt) {
                    let accntName = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[2]/table/tbody/tr/td[1]/div[2]/div[2]');
                    let userName = await accntName.getText();
                    status += 'Twitter Account added: '+userName;
                }
                else{
                    status += 'Twitter Account is not added.';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
