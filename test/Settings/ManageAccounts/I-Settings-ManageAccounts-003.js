 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-003
 
 * ### TEST TITLE:
 * Add Instagram Accounts - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * User should be able to Add Instagram Accounts from Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Instagram Accounts - Manage Connected Accounts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Add Instagram Accounts', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts, ';
                await driver.sleep(5000);

                let checkaccount = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-instagram.accounts-platform > table > tbody > tr:nth-child(1) > td.account-info > div.account-names > div.full-name');
                if (checkaccount) {

                    let connectInstagram = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-instagram.accounts-platform > div.account-heading > a > i');
                    await connectInstagram.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="react-root"]/section/main/div/article/div/div/div/form/div[2]/div/label/input'), 3000));
                }
                else{
                    let connectInst = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[3]/div[2]/a');
                    await connectInst.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="react-root"]/section/main/div/article/div/div/div/form/div[2]/div/label/input'), 3000));
                }

                let instEmail = await page.findByXPath('//*[@id="react-root"]/section/main/div/article/div/div/div/form/div[2]/div/label/input');
                await instEmail.click();
                await page.write(instEmail, 'arun@wdstech.com');

                let instPwd = await page.findByXPath('//*[@id="react-root"]/section/main/div/article/div/div/div/form/div[3]/div/label/input');
                await instPwd.click();
                await page.write(instPwd, 'micr@s@ft123');

                let submit = await page.findByXPath('//*[@id="react-root"]/section/main/div/article/div/div/div/form/div[4]/button/div');
                await submit.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="manageConnectedAccounts"]/div/div[3]/div[1]/div'), 3000));

                let checkinst = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-twitter.accounts-platform > table > tbody > tr > td.account-info > div.account-names > div.full-name');
                if (checkinst) {
                    let accntName = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[3]/table/tbody/tr/td[1]/div[2]/div[1]');
                    let userName = await accntName.getText();
                    status += 'Instagram Account added: '+userName;
                }
                else{
                    status += 'Instagram Account is not added.';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
