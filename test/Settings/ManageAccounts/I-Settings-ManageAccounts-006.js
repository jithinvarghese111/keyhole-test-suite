 /**
 * ### TEST CASE:
 * I-Settings-ManageAccounts-006
 
 * ### TEST TITLE:
 * Add Facebook Accounts - Manage Connected Accounts
 *
 * ### TEST SUMMARY:
 * User should be able to Add Facebook Accounts from Manage Connected Accounts page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Facebook Accounts - Manage Connected Accounts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Add Facebook Accounts', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let manageAccount = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(4) > a');
                await manageAccount.click();
                steps += 'Navigated to Manage Connected Accounts, ';
                await driver.sleep(5000);

                await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[3]/div[1]/div');
                await driver.sleep(2000);

                let checkaccount = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > table > tbody > tr.inactive > td.account-info > div.account-names > div');
                if (checkaccount) {

                    let connectFacebook = await page.findByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > div.account-heading > a > i');
                    await connectFacebook.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="email"]'), 3000));
                }
                else{
                    let connectFb = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[4]/table/tbody/tr[2]/td/a');
                    await connectFb.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="email"]'), 3000));
                }

                let fbEmail = await page.findByXPath('//*[@id="email"]');
                await fbEmail.click();
                await page.write(fbEmail, 'stemy@wdstech.com');

                let fbPwd = await page.findByXPath('//*[@id="pass"]');
                await fbPwd.click();
                await page.write(fbPwd, 'micr@s@ft123');

                let submit = await page.findByXPath('//*[@id="loginbutton"]');
                await submit.click();
                await driver.sleep(5000);

                await page.scrollPage('//*[@id="manageConnectedAccounts"]/div/div[3]/div[1]/div');
                await driver.sleep(2000);

                let checkfb = await page.checkElementByCSS('#manageConnectedAccounts > div > div.accounts-facebook.accounts-platform > table > tbody > tr:nth-child(3) > td.account-info > div.account-names > div');
                if (checkfb) {
                    let accntName = await page.findByXPath('//*[@id="manageConnectedAccounts"]/div/div[4]/table/tbody/tr[3]/td[1]/div[2]/div');
                    let userName = await accntName.getText();
                    status += 'Facebook Account added: '+userName;
                }
                else{
                    status += 'Facebook Account is not added.';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
