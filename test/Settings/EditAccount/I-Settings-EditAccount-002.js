/**
 * ### TEST CASE:
 * I-Settings-EditAccount-002
 
 * ### TEST TITLE:
 * Edit Account - settings
 *
 * ### TEST SUMMARY:
 * User should be able to Edit Account from 'Settings' page 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Edit Account', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Edit Account', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Clicked on Settings, '
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let firstName = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[1]/div/input');
                await firstName.clear();
                await page.write(firstName, 'Qa two');

                let lastName = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[2]/div/input');
                await lastName.clear();
                await page.write(lastName, 'keyhole');
                
                let email = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[3]/div/input');
                await email.clear();
                await page.write(email, 'qa+02@wdstech.com');

                let company = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[4]/div/input');
                await company.clear();
                await page.write(company, 'Keyhole');

                let phone = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[5]/div/input');
                await phone.clear();
                await page.write(phone, '0000000056-87-1');

                for (var i = 6; i <= 7; i++) {
                    let selectAll = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div['+i+']/div/div/label/input');
                    await selectAll.click();
                    await selectAll.click();
                }

                let saveChanges = await page.findByXPath('//*[@id="save_changes"]');
                await saveChanges.click();
                status += 'Edited Account Successfully'
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
