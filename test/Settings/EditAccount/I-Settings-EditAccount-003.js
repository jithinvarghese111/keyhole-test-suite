/**
 * ### TEST CASE:
 * I-Settings-EditAccount-003
 
 * ### TEST TITLE:
 * Mandatory fields- edit account
 *
 * ### TEST SUMMARY:
 * User should be able to check mandatory fields in edit account
 *
 */
 
const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Mandatory fileds- edit account', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            let total_test_cases = 9

            for (var i = 1; i <= total_test_cases; i++) {
                let test_title = '', test_firstname = '', test_lastname = '', test_email = '', test_company = '', test_phoneNumber = ''

                switch (i) {
                    case 1:
                        test_title = 'Edit with all empty fields'
                        break;
                    case 2:
                        test_title = 'Edit with Last Name,Email,Company,Phone Number and empty first Name'
                        test_lastname = randomName()
                        test_email = config.userEmail
                        test_company = config.accountCompany
                        test_phoneNumber = randomNumber()
                        break;
                    case 3:
                        test_title = 'Edit with first Name,Email,Company,Phone Number and empty Last Name'
                        test_firstname = randomName()
                        test_email = config.userEmail
                        test_company = config.accountCompany
                        test_phoneNumber = randomNumber()
                        break;
                    case 4:
                        test_title = 'Edit with first Name,Last Name,Company,Phone Number and empty Email'
                        test_firstname = randomName()
                        test_lastname = randomName()
                        test_company = config.accountCompany
                        test_phoneNumber = randomNumber()
                        break;
                    case 5:
                        test_title = 'Edit with first Name,Last Name,Email,Phone Number and empty Company'
                        test_firstname = randomName()
                        test_lastname = randomName()
                        test_email = config.userEmail
                        test_phoneNumber = randomNumber()
                        break;
                    case 6:
                        test_title = 'Edit with first Name,Last Name,Email,Company and empty Phone Number'
                        test_firstname = randomName()
                        test_lastname = randomName()
                        test_email = config.userEmail
                        test_company = config.accountCompany
                        break;
                    case 7:
                        test_title = 'Edit with invalid email and valid first Name,Last Name,Email,Company,phone number'
                        test_firstname = randomName()
                        test_lastname = randomName()
                        test_email = randomName()
                        test_company = config.accountCompany
                        test_phoneNumber = randomNumber()
                        break;
                    case 8:
                        test_title = 'Edit with invalid phone number and valid first Name,Last Name,Email,Company,email'
                        test_firstname = randomName()
                        test_lastname = randomName()
                        test_email = config.userEmail
                        test_company = config.accountCompany
                        test_phoneNumber = randomName()
                        break; 
                    case 9:
                        test_title = 'Edit with first Name,Last Name,Email,Company,Phone Number.'
                        test_firstname = config.accountFirstname
                        test_lastname = config.accountCompany
                        test_email = config.userEmail
                        test_company = config.accountCompany
                        test_phoneNumber = randomNumber()

                }

                it(test_title, async () => {
                    await page.redirectToDashboard();

                    let dropdown = await page.findByXPath('//*[@id="user-email"]');
                    await dropdown.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                    let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                    await settings.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                    let firstName = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[1]/div/input');
                    await firstName.click();
                    await firstName.clear();
                    await page.write(firstName, test_firstname);

                    let lastName = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[2]/div/input');
                    await lastName.click();
                    await lastName.clear();
                    await page.write(lastName, test_lastname);
                    
                    let email = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[3]/div/input');
                    await email.click();
                    await email.clear();
                    await page.write(email, test_email);

                    let company = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[4]/div/input');
                    await company.click();
                    await company.clear();
                    await page.write(company, test_company);

                    let phone = await page.findByXPath('//*[@id="General-Settings"]/form/fieldset/div[5]/div/input');
                    await phone.click();
                    await phone.clear();
                    await page.write(phone, test_phoneNumber);

                    let saveChanges = await page.findByXPath('//*[@id="save_changes"]');
                    await saveChanges.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                    let editMsgCheck = await page.checkElement('msgsx');

                    if (editMsgCheck) {
                        edit_msg = await page.findById('msgsx');
                        edit_msg = await edit_msg.getText();
                    }
                    else
                        edit_msg = 'Edited successfully';
                        
                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'First Name entered: ': test_firstname,
                            'Last Name entered: ': test_lastname,
                            'Email entered: ': test_email,
                            'Company entered: ': test_company,
                            'Phone Number entered: ': test_phoneNumber,
                            'Error message': edit_msg
                        }
                    });                                   
                });
            }  
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();