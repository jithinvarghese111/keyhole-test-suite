 /**
 * ### TEST CASE:
 * I-Settings-Brand settings-001
 
 * ### TEST TITLE:
 * Navigate to brand Settings Page 
 *
 * ### TEST SUMMARY:
 * 
 *User should be able to Navigate to white-Label Branding page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to brand Settings Page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigate to brand Settings Page', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let header = await page.findByXPath('//*[@id="header-container"]');
                let headerColor = await header.getCssValue("background-color");

                let dropdown = await page.findByXPath('//*[@id="user-email"]');
                await dropdown.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="user-dropdown"]/a[1]'), 3000));

                let settings = await page.findByXPath('//*[@id="user-dropdown"]/a[1]');
                await settings.click();
                steps += 'Navigated to settings, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="General-Settings"]/form/h1'), 3000));

                let brandSettings = await page.findByCSS('#General-Settings > nav > ul > li:nth-child(5) > a');
                await brandSettings.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="User-Branding"]/form/h1'), 3000));

                let pageName = await page.findByXPath('//*[@id="User-Branding"]/form/h1');
                pageName = await pageName.getText();
                expect(pageName).to.equal('Brand Settings');
                status += 'Navigated to Brand settings';

                let brandColor = await page.findByXPath('//*[@id="User-Branding"]/form/fieldset/div[2]/div[1]');
                brandColor = await brandColor.getAttribute("style");
                status += ' ,brand color: '+brandColor;

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status
                    }
                });               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
