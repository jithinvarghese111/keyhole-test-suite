/**
 * ### TEST CASE:
 * I-SM-Instagram-002
 *
 * ### TEST TITLE:
 * Profile data of Instagram Tracker from optimization
 *
 * ### TEST SUMMARY:
 * User is trying to check profile data of Instagram Tracker from optimization.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Profile data of Instagram Tracker from optimization', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Profile data of Instagram Tracker from optimization.', async () => {
                await page.redirectToDashboard();

                let steps = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(3000);
                
                let platforms = ['1', '3', '4'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platforms[i]+']');
                    await platformClick.click();
                    await driver.sleep(1000);
                }

                let firstTracker = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                steps += 'Redirected to Instagram tracker dashboard.';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));
                
                let optimization = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[3]/a');
                await optimization.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Optimization"]/div/div/div[3]/p/span'), 20000));
                
                let instName = await page.findByXPath('//*[@id="account"]/div/div[1]/div[1]/div[2]/div/a/p');
                let profilename = await instName.getText();
                steps += ' ,Instragram profile Name displayed :'+profilename;
                
                let instId = await page.findByXPath('//*[@id="account"]/div/div[1]/div[1]/div[2]/div/p');
                let profileId = await instId.getText();
                steps += ' ,Instragram profile Id displayed :'+profileId;
                
                let instBio = await page.findByXPath('//*[@id="account"]/div/div[1]/div[1]/div[2]/p/span');
                let profileBio = await instBio.getText();
                steps += ' ,Instragram profile Bio displayed :'+profileBio;

                let instweb = await page.findByXPath('//*[@id="account"]/div/div[1]/div[1]/div[2]/p/a');
                let profileWebsite = await instweb.getText();
                steps += ' ,Instragram profile Website displayed :'+profileWebsite;
                                                   
                addContext(this.ctx, {
                    title: 'Test status',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status' : 'Instagram Profile data displayed successfully on optimization.'
                    }
                }); 
                                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



