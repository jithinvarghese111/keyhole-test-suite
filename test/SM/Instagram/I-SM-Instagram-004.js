/**
 * ### TEST CASE:
 * I-SM-Instagram-004
 *
 * ### TEST TITLE:
 * Profile data of Instagram Tracker from stories
 *
 * ### TEST SUMMARY:
 * User is trying to check profile data of Instagram Tracker from stories.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Profile data of Instagram Tracker from stories', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Profile data of Instagram Tracker from stories.', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[4]/div[1]');
                await driver.wait(until.elementLocated(By.id('twt_check'), 30000));
                
                let platforms = ['1', '3', '4'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platforms[i]+']');
                    await platformClick.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 20000));
                }

                let firstTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                steps += 'Redirected to Instagram tracker dashboard.';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));
                
                let stories = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[5]/a');
                await stories.click();
                await driver.sleep(5000);
                
                let checkAccnt = await page.checkElementByCSS('#AT-Stories > div > div > div.info > p > span');
                if (checkAccnt) {

                    let checkPosts = await page.checkElementByCSS('#all-stories > article > p');
                    if (!checkPosts) {

                        let post = await page.findByXPath('//*[@id="all-stories"]/div/div[1]/table/tbody/tr');
                        await post.click();
                        await driver.sleep(1000);

                        let checkName = await page.checkElementByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.screen-name');
                        if (checkName) {
                            let accntName = await page.findByXPath('//*[@id="post-view"]/div[1]/div/div[1]/a/div/div[1]');
                            let profileName = await accntName.getText();
                            status += 'Instragram profile Name displayed: '+profileName;                  
                        }

                        let checkId = await page.checkElementByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.username');
                        if (checkId) {
                            let accntId = await page.findByXPath('//*[@id="post-view"]/div[1]/div/div[1]/a/div/div[2]');
                            let profileId = await accntId.getText();
                            status += ' ,Instragram profile Id displayed: '+profileId;
                        }
                        addContext(this.ctx, {
                            title: 'Test status',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': status
                            }
                        });
                    }
                    else{

                        addContext(this.ctx, {
                            title: 'Test status',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'We havent collected any stories from this account yet'
                            }
                        });
                    }
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test status',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Instagram not updated into Business Account'
                        }
                    });
                }                 
                                          
            });

            it('Checking Profile data of Instagram stories from card view.', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[4]/div[1]');
                await driver.wait(until.elementLocated(By.id('twt_check'), 50000));
                
                let platforms = ['1', '3', '4'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platforms[i]+']');
                    await platformClick.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 20000));
                }

                let firstTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                steps += 'Redirected to Instagram tracker dashboard.';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));
                
                let stories = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[5]/a');
                await stories.click();
                await driver.sleep(5000);

                let checkAccnt = await page.checkElementByCSS('#AT-Stories > div > div > div.info > p > span');
                if (checkAccnt) {
                    
                    let checkPosts = await page.checkElementByCSS('#all-stories > article > p');
                    if (!checkPosts) {

                        let cardView = await page.findByXPath('//*[@id="AT-Stories"]/div/div/div[4]/div[2]/button[2]');
                        await cardView.click();
                        await driver.sleep(1000);

                        let post = await page.findByXPath('//*[@id="all-stories"]/div/div[2]/div/div/div[1]/img');
                        await post.click();
                        await driver.sleep(1000);
                        
                        let checkName = await page.checkElementByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.screen-name');
                        if (checkName) {
                            let accntName = await page.findByXPath('//*[@id="post-view"]/div[1]/div/div[1]/a/div/div[1]');
                            let profileName = await accntName.getText();
                            status += 'Instragram profile Name displayed on stories popup: '+profileName;                 
                        }

                        let checkId = await page.checkElementByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.username');
                        if (checkId) {
                            let accntId = await page.findByXPath('//*[@id="post-view"]/div[1]/div/div[1]/a/div/div[2]');
                            let profileId = await accntId.getText();
                            status += ' ,Instragram profile Id displayed on stories popup: '+profileId;
                        }

                        addContext(this.ctx, {
                            title: 'Test status',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': status
                            }
                        });
                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Test status',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'We havent collected any stories from this account yet'
                            }
                        });
                    }
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test status',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Instagram not updated into Business Account'
                        }
                    });
                } 
                                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



