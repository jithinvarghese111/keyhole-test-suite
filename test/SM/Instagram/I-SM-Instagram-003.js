/**
 * ### TEST CASE:
 * I-SM-Instagram-003
 *
 * ### TEST TITLE:
 * Profile data of Instagram Tracker posts
 *
 * ### TEST SUMMARY:
 * User is trying to check Profile data of Instagram Tracker posts.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Profile data of Instagram Tracker posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Profile data of Instagram Tracker posts.', async () => {
                await page.redirectToDashboard();

                let steps = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[1]/h1');
                await driver.sleep(2000);
                // await driver.wait(until.elementLocated(By.id('twt_check'), 20000));
                
                let platforms = ['1', '3', '4'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platforms[i]+']');
                    await platformClick.click();
                    await driver.sleep(1000);
                }

                let firstTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                steps += 'Redirected to Instagram tracker dashboard.';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));
                
                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[4]/a');
                await posts.click();
                await driver.sleep(3000);
            
                let postPopup = await page.findByXPath('//*[@id="all-posts"]/div/div[1]/table/tbody/tr['+i+']/td[2]');
                await postPopup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="post-view"]/div[1]/div/div[1]/a/div/div[1]'), 20000));
                
                let instName = await page.findByXPath('//*[@id="post-view"]/div[1]/div/div[1]/a/div/div[1]');
                let profileName = await instName.getText();

                let instId = await page.findByXPath('//*[@id="post-view"]/div[1]/div/div[1]/a/div/div[2]');
                let profileId = await instId.getText();

                let closePopup = await page.findByXPath('//*[@id="post-view"]/div[1]/div/div[2]');
                await closePopup.click();   
                await driver.sleep(2000);
                
                addContext(this.ctx, {
                    title: 'Test status',
                    value: {
                        'Tracker name': trackerName,
                        'Instragram profile Name': profileName,
                        'Instragram profile Id': profileId,
                        'Test status' : 'Instagram Profile data displayed successfully on posts.'
                    }
                });    
                                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



