/**
 * ### TEST CASE:
 * I-SM-Instagram-005
 *
 * ### TEST TITLE:
 * Profile data of Instagram Tracker from follower Insights
 *
 * ### TEST SUMMARY:
 * User is trying to check profile data of Instagram Tracker from follower Insights.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Profile data of Instagram Tracker from follower Insights', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Profile data of Instagram Tracker from follower Insights.', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));
                
                let firstTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                steps += 'Redirected to Instagram tracker dashboard.';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));
                
                let followerInsight = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[6]/a');
                await followerInsight.click();
                await driver.sleep(1000);
                
                let checkAccnt = await page.checkElementByCSS('#AT-Followers-Insights > div > div > div.info > p > span');
                if (checkAccnt) {

                    let checkName = await page.checkElementByCSS('#account > div > div.top-bar > div.user-info > div.user-bio > div > a > p');
                    if (checkName) {
                        let accntName = await page.findByXPath('//*[@id="account"]/div/div[1]/div[1]/div[2]/div/a/p');
                        let profileName = await accntName.getText();
                        status += 'Instragram profile Name displayed on followers Insight: '+profileName;                 
                    }

                    let checkId = await page.checkElementByCSS('#account > div > div.top-bar > div.user-info > div.user-bio > div > p');
                    if (checkId) {
                        let accntId = await page.findByXPath('//*[@id="account"]/div/div[1]/div[1]/div[2]/div/p');
                        let profileId = await accntId.getText();
                        status += ' ,Instragram profile Id displayed on followers Insight: '+profileId;
                    }

                    addContext(this.ctx, {
                        title: 'Test status',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test status',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Instagram not updated into Business Account'
                        }
                    });
                }     
                                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



