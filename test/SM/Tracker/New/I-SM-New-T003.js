/**
 * ### TEST CASE:
 * I-SM-New-T003
 *
 * ### TEST TITLE:
 * Upgrade to Instagram business to see stories and follower Insights.
 *
 * ### TEST SUMMARY:
 * User trying to Upgrade to Instagram business to see stories.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Upgrade to Instagram business to see stories and follower insights.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking upgrade to Instagram business to see stories.', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let headerName = await page.findByXPath('//*[@id="account-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Social Media Account Analytics');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));                   
            
                let platform = ['1', '3', '4'];

                for (i = 0; i < platform.length; i++) {
                    let platformsClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platform[i]+']');
                    await platformsClick.click();
                    await driver.sleep(1000);
                }

                let instagramTracker = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await instagramTracker.getText();
                await instagramTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                let stories = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[5]/a');
                await stories.click();
                steps += 'Navigated into stories';
                await driver.sleep(1000);

                let upgrade = await page.findByXPath('//*[@id="upgradeToIgBusiness-lightbox"]/div/div/a');
                await upgrade.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="create_tracker"]/div/a'), 20000));

                let upgradeFb = await page.findByXPath('//*[@id="create_tracker"]/div/a');
                await upgradeFb.click();
                await driver.sleep(2000);

                let checkfb = await page.checkElementByCSS('#blueBarDOMInspector > div > div.loggedout_menubar_container > div > div > h1 > a > i');
                if (checkfb) {

                    let emailFb = await page.findByXPath('//*[@id="email"]');
                    await emailFb.click();
                    await page.write(emailFb, 'arundinesh@wdstech.com');

                    let pwdFb = await page.findByXPath('//*[@id="pass"]');
                    await pwdFb.click();
                    await page.write(pwdFb, 'micr@s@ft123');

                    let loginFb = await page.findByXPath('//*[@id="loginbutton"]');
                    await loginFb.click();
                    await driver.sleep(2000);

                    let checkfb = await page.checkElementByCSS('#instagram_business > div.wrapper > div');
                    if (checkfb) {
                        status += 'Unable to authenticate your Account';
                    }
                    else{
                        status += 'Instagram upgraded into business Account';
                    }
                    
               }
             
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker Name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
               
            });

            it('Checking upgrade to Instagram business to see follower insights.', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let headerName = await page.findByXPath('//*[@id="account-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Social Media Account Analytics');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));             
            
                let platform = ['1', '3', '4'];

                for (i = 0; i < platform.length; i++) {
                    let platformsClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platform[i]+']');
                    await platformsClick.click();
                    await driver.sleep(1000);
                }

                let instagramTracker = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await instagramTracker.getText();
                await instagramTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000)); 

                let followerInsights = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[6]/a');
                await followerInsights.click();
                steps += 'Navigated into follower Insights';
                await driver.sleep(1000);

                let checkinstBusiness = await page.checkElementByCSS('#AT-Followers-Insights > div > div > div.info > p > span');
                if (!checkinstBusiness) {

                    let upgradeinst = await page.findByXPath('//*[@id="upgradeToIgBusiness-lightbox"]/div/div/a');
                    await upgradeinst.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="create_tracker"]/div/a'), 20000));

                    let upgradefb = await page.findByXPath('//*[@id="create_tracker"]/div/a');
                    await upgradefb.click();
                    await driver.wait(until.elementLocated(By.css('#email'), 20000));

                    let checkFb = await page.checkElementByCSS('#email');
                    if (checkFb) {
                    let fbEmail = await page.findByXPath('//*[@id="email"]');
                    await fbEmail.click();
                    await page.write(fbEmail, 'arundinesh@wdstech.com');

                    let fbPwd = await page.findByXPath('//*[@id="pass"]');
                    await fbPwd.click();
                    await page.write(fbPwd, 'micr@s@ft123');

                    let login = await page.findByXPath('//*[@id="loginbutton"]');
                    await login.click();
                    await driver.sleep(2000);

                    let checkfb = await page.checkElementByCSS('#instagram_business > div.wrapper > div');
                    if (checkfb) {
                        status += 'Unable to authenticate your Account';
                    }
                    else{
                        status += 'Instagram upgraded into business Account.';
                    }

                   }
                                     
                }
                else{
                    status += 'Instagram already upgraded and follower Insights displayed.';
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker Name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();