/**
 * ### TEST CASE:
 * I-SM-New-T001
 *
 * ### TEST TITLE:
 * Total Count of Each Social Media Account.
 *
 * ### TEST SUMMARY:
 * User should allow to check Total Count of Each Social Media Account.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Total Count of Each Social Media Account', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Total Count of Each Social Media Account', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let headerName = await page.findByXPath('//*[@id="account-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                expect(headerName).to.equal('Social Media Account Analytics');

                await page.scrollPage('//*[@id="account-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                let platforms = ['2', '3', '4'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platforms[i]+']');
                    await platformClick.click();
                    await driver.sleep(1000);
                }
                
                let totalTwitter = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > span > i.fa.fa-twitter');
                let totalTwitterAccounts = await totalTwitter.length;

                status += 'Total Twitter Accounts: '+ totalTwitterAccounts;

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.sleep(1000);

                let accounts = ['2', '3', '4'];

                for (i = 0; i < accounts.length; i++) {
                    let accountsClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+accounts[i]+']');
                    await accountsClick.click();
                }

                let platform = ['1', '3', '4'];

                for (i = 0; i < platform.length; i++) {
                    let platformsClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platform[i]+']');
                    await platformsClick.click();
                    await driver.sleep(1000);
                }
            
                let totalInstagram = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > span > i.fa.fa-instagram');
                let totalInstagramAccounts = await totalInstagram.length;

                status += ' ,Total Instagram Accounts: '+ totalInstagramAccounts;

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.sleep(1000);

                let account = ['1', '3', '4'];

                for (i = 0; i < account.length; i++) {
                    let accountClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+account[i]+']');
                    await accountClick.click();
                }

                let plat = ['1', '2', '4'];

                for (i = 0; i < plat.length; i++) {
                    let platClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+plat[i]+']');
                    await platClick.click();
                    await driver.sleep(1000);
                }

                let totalFacebook = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > span > i.fa.fa-facebook');
                let totalFacebookAccounts = await totalFacebook.length;

                status += ' ,Total Facebook Accounts: '+totalFacebookAccounts;

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.sleep(1000);

                let socialAccount = ['1', '2', '4'];

                for (i = 0; i < socialAccount.length; i++) {
                    let socialAccountClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+socialAccount[i]+']');
                    await socialAccountClick.click();
                }

                let platf = ['1', '2', '3'];

                for (i = 0; i < platf.length; i++) {
                    let platfClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platf[i]+']');
                    await platfClick.click();
                    await driver.sleep(1000);
                }

                let totalYoutube = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > span > i.fa.fa-youtube');
                let totalYoutubeAccounts = await totalYoutube.length;

                status += ' ,Total Instagram Accounts: '+totalYoutubeAccounts; 

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
            
                        'Test status': status
                    }
                });
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();