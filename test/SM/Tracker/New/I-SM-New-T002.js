/**
 * ### TEST CASE:
 * I-SM-New-T002
 *
 * ### TEST TITLE:
 * stories and followers insight in Instagram Tracker.
 *
 * ### TEST SUMMARY:
 * User trying to check stories and followers insight in Instagram Tracker..
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('stories and followers insight in Instagram Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking stories and followers insight in Instagram Tracker', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '', stories = '', insight = '';

                let headerName = await page.findByXPath('//*[@id="account-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                expect(headerName).to.equal('Social Media Account Analytics');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                let platform = ['1', '3', '4'];

                for (i = 0; i < platform.length; i++) {
                    let platformsClick = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[2]/label['+platform[i]+']');
                    await platformsClick.click();
                    await driver.sleep(2000);
                }
            
                let instagramTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await instagramTracker.getText();
                await instagramTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                let checkStories = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[5]/a');
                await checkStories.click();
                await driver.sleep(1000);

                let checkEle = await page.checkElementByCSS('#upgradeToIgBusiness-lightbox');
                if (checkEle) {
                    let storiesText = await page.findByXPath('//*[@id="upgradeToIgBusiness-lightbox"]/div/p');
                    let storiesmsg = await storiesText.getText();
                    stories = 'stories not accessable: '+storiesmsg;

                    let storiesClose = await page.findByXPath('//*[@id="upgradeToIgBusiness-lightbox"]/button');
                    await storiesClose.click();
                    await driver.sleep(1000);
                }

                let checkInsights = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[6]/a/span');
                await checkInsights.click();
                await driver.sleep(1000);

                let checkElem = await page.checkElementByCSS('#upgradeToIgBusiness-lightbox');
                if (checkElem) {
                    let insightText = await page.findByXPath('//*[@id="upgradeToIgBusiness-lightbox"]/div/p');
                    let insightmsg = await insightText.getText();
                    insight = 'Follower insight not accessable: '+insightmsg;

                    let insightClose = await page.findByXPath('//*[@id="upgradeToIgBusiness-lightbox"]/div/p');
                    await insightClose.click();
                    
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker Name': trackerName,
                        'Stories status': stories,
                        'Follower insight status':insight
                    }
                });
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();