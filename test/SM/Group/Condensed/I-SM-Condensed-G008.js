/**
 * ### TEST CASE:
 * I-SM-Condensed-G008
 *
 * ### TEST TITLE:
 * Custom date range persists to URL  - condensed group dashboard
 *
 * ### TEST SUMMARY:
 * When selecting a Custom date range in the date dropdown, the Custom date range persists to URL in *condensed group dashboard.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {   
        describe('Custom date range persists to URL in condensed group dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Custom date range persists to URL', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 20000));

                let groupClick = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-createGroup'), 20000));

                let firstGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]'), 20000));
                
                let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div[1]/section[2]/div[1]/a[2]');
                await gotoCondensed.click();
                steps += 'Clicked on Condensed group dashboard';
                await driver.wait(until.elementLocated(By.id('pdf'), 20000));
                         
                let groupfilter = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div/div/div[2]/span');
                await groupfilter.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div/ul/li[1]'), 20000));

                let filterDate = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div/ul/li[1]');
                await filterDate.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/section[2]/div/div[2]/div[2]/span/div/input'), 20000));

                let pickDate = await page.findByXPath('//*[@id="tracker-group"]/div/div/section[2]/div/div[2]/div[2]/span/div/input');
                await pickDate.click();

                let startDate = await page.findByXPath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[2]/td[5]');
                await startDate.click();
            
                let endDate = await page.findByXPath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[6]/td[4]');
                await endDate.click();
                
                let setFilter = await page.findByXPath('//*[@id="tracker-group"]/div/div/section[2]/div/div[2]/div[3]/button[2]');
                await setFilter.click();
                await driver.wait(until.elementLocated(By.id('pdf'), 20000));

                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.indexOf('%'));
                
                if (firstParam.indexOf('custom') > -1) {
                    let status = '';

                    status += 'Custom Date Range added to URL., ';

                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.id('pdf'), 20000));

                    if (firstParam.indexOf('custom') > -1) {
                        status += 'Custom Date Range persisted upon reload.';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Test status': 'Custom Date not added to URL.'
                        }
                    });
                }              
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();