/**
 * ### TEST CASE:
 * I-SM-Condensed-G010
 *
 * ### TEST TITLE:
 * Interact with Follower growth  - condensed group dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to Interact with Follower growth from condensed Group Dashboard.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {   
        describe('Interact with Follower growth in condensed group dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with Follower growth from condensed Group Dashboard.', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '', result = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 20000));

                let groupClick = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-createGroup'), 20000));

                let firstGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]'), 20000));
                
                let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div[1]/section[2]/div[1]/a[2]');
                await gotoCondensed.click();
                steps += 'Clicked on Condensed group dashboard';
                await driver.wait(until.elementLocated(By.id('pdf'), 20000));

                await page.scrollPixel(1300);
                await driver.sleep(3000);
                
                let groupcheck = await page.findByXPath('//*[@id="tracker-group-followers"]/div[2]/div[1]/div/div[1]');
                await groupcheck.click();
                await driver.wait(until.elementLocated(By.css('#chart__follower-timeline > div.chart--noData > div'), 20000));

                let checkEle = await page.checkElementByCSS('#chart__follower-timeline > div.chart--noData > div');
                if (checkEle) {
                    result = 'Hide group data from timeline';

                    let groupUncheck = await page.findByXPath('//*[@id="tracker-group-followers"]/div[2]/div[1]/div/div[1]');
                    await groupUncheck.click();
                    await driver.wait(until.elementLocated(By.css('#chart__follower-timeline > canvas'), 20000));

                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group name': groupName,
                            'Test steps': steps,
                            'Test status': result
                        }
                    }); 
                   
                }
                
                let followerCount = await page.findByXPath('//*[@id="tracker-group-followers"]/div[1]/div/div/div/div/span');
                await followerCount.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group-followers"]/div[1]/div/div/div/ul/li[1]'), 20000));

                let totalFollowers = await page.findElementsByCSS('#tracker-group-followers > div.key-chartHeading > div > div > div > ul > li');

                for (var i = 1; i <= totalFollowers.length; i++) {
                    
                    let list = await page.findByXPath('//*[@id="tracker-group-followers"]/div[1]/div/div/div/ul/li['+i+']');
                    await list.click();
                    await driver.sleep(1000);

                    let labelClick = await page.findByXPath('//*[@id="tracker-group-followers"]/div[1]/div/div/div/div/span');
                    let labelName = await labelClick.getText();
                    await labelClick.click();
                    status = 'Clicked on :'+labelName;
                    await driver.sleep(1000);

                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }                  
                
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();