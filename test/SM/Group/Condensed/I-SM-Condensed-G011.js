/**
 * ### TEST CASE:
 * I-SM-Condensed-G010
 *
 * ### TEST TITLE:
 * Interact with Post Type  - condensed group dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to Interact with Post Type  from condensed Group Dashboard.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {   
        describe('Interact with Post Type in condensed group dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with Post Type from condensed Group Dashboard.', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '', result = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 20000));

                let groupClick = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-createGroup'), 20000));

                let firstGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]'), 20000));
                
                let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div[1]/section[2]/div[1]/a[2]');
                await gotoCondensed.click();
                steps += 'Clicked on Condensed group dashboard';
                await driver.wait(until.elementLocated(By.id('pdf'), 20000));

                await page.scrollPixel(1800);
                await driver.sleep(3000);
                
                let totalPosts = await page.findElementsByCSS('#post-type > div.key-chartContainer > div > div.key-chartsLegend.key-chartsLegend--postType > div > div.key-chartsLegend__keyword.key-chartsLegend__keyword--postType');
                for (var i = 1; i <= totalPosts.length; i++) {
                    
                    let hidePosts = await page.findByXPath('//*[@id="post-type"]/div[2]/div/div[2]/div['+i+']/div[2]');
                    let postname = await hidePosts.getText();
                    await hidePosts.click();
                    status = 'Hide Posts: '+postname;
                    await driver.sleep(1000);

                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }

                let total = await page.findByXPath('//*[@id="post-type"]/div[1]/div/ul/li[2]');
                await total.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="post-type"]/div[2]/div/div[2]/div[1]/div[1]'), 20000));

                let totalPercentage = await page.findElementsByCSS('#post-type > div.key-chartContainer > div > div.key-chartsLegend.key-chartsLegend--postType > div > div.key-chartsLegend__keyword.key-chartsLegend__keyword--postType');
                for (var i = 1; i <= totalPercentage.length; i++) {
                    let unhidePosts = await page.findByXPath('//*[@id="post-type"]/div[2]/div/div[2]/div['+i+']/div[2]');
                    let postsName = await unhidePosts.getText();
                    await unhidePosts.click();
                    status = 'Unhide Posts: '+postsName;
                    await driver.sleep(1000);

                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
                        
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();