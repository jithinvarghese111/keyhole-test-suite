/**
 * ### TEST CASE:
 * I-SM-Condensed-G002
 *
 * ### TEST TITLE:
 * Download condensed group data as xlsx
 *
 * ### TEST SUMMARY:
 * User should be allowed to download condensed group data in xlsx format
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Go to Condensed Dashboard from group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Download the condensed group information in xlsx format ', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';
            
                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]'), 20000));

                let groupClick = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-createGroup'), 10000));

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name');
                if (checkGroup) {
                
                    let firstGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                    let groupName = await firstGroup.getText();
                    await firstGroup.click();                    
                    await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.key-trackerSubHeader > div.key-trackerSubHeader__filters > a:nth-child(3) > span'), 20000));

                    let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div[1]/section[2]/div[1]/a[2]/span');
                    await gotoCondensed.click();
                    steps += 'Clicked on Condensed group dashboard';
                    await driver.wait(until.elementLocated(By.id('pdf'), 20000));   

                    let test_result = '';
                    const days = 30;
                    const toDate = new Date();
                    const fromDate = new Date();
                    fromDate.setDate(toDate.getDate() - days);
                    const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                    const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');

                    let downloadXLS = await page.findById('update');
                    await downloadXLS.click();
                    test_result = "Downloaded file";
                     await driver.wait(until.elementLocated(By.id('pdf'), 20000));   

                    const downloadPath = config.downloadPath+'\/'+groupName+'_(condensed)'+fromDateStr+'-'+toDateStr+'.xlsx';
                    const filename = `${groupName}_(condensed)_${new Date().getTime()}.xlsx`;
                    const renamePath = config.downloadPath+'\/'+filename;

                    console.log(downloadPath);
                    console.log(renamePath);

                    if (fs.existsSync(downloadPath)) {
                        fs.renameSync(downloadPath, renamePath)
                        
                        if (fs.existsSync(renamePath)) {
                            const stats = fs.statSync(renamePath)

                            if (parseFloat(stats.size) > 64) {
                                test_result = "Downloaded file";
                                passed = true
                            } else {
                                test_result = "Downloaded empty file... but that's what happens on env other than prod";
                                passed = true
                            }
                        }
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': test_result
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Test status': 'Could not load dashboard top devices and apps.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();