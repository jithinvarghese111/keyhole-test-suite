/**
 * ### TEST CASE:
 * I-SM-Condensed-G006
 *
 * ### TEST TITLE:
 * Show hidden group - condensed group dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to  Show hidden group in condensed group dashboard.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {   
        describe('Show hidden group - condensed group dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking  Show hidden group ', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 20000));

                let groupClick = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-createGroup'), 20000));

                let firstGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                let groupName = await firstGroup.getText();
                await firstGroup.click(); 
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]'), 20000));
                
                let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div[1]/section[2]/div[1]/a[2]');
                await gotoCondensed.click();
                steps += 'Clicked on Condensed group dashboard';
                await driver.wait(until.elementLocated(By.id('pdf'), 20000));
                         
                let groupfilter = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div/div/div[2]/span');
                await groupfilter.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div/ul/li[3]'), 20000));

                let hide = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div/ul/li[3]');
                await hide.click();
                await driver.sleep(2000); 

                let groupFilter = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div/div/div[2]/span');
                await groupFilter.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div/ul/li[3]'), 20000));

                let showgroup = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div/ul/li[3]');
                await showgroup.click();
                await driver.sleep(2000); 

                let groupshow = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div/div');
                let checkshow = await groupshow.getAttribute("class");
            
                if (checkshow.indexOf('inactive') > -1){

                    status += 'Test failed.';
                }
                else{
                    status += 'Show hidden group successfully.'
                }
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': groupName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();