/**
 * ### TEST CASE:
 * I-Accounts-001
 *
 * ### TEST TITLE:
 * Login Page Testing
 *
 * ### TEST SUMMARY:
 * User is trying to login with different credentials
 *
 * ### TEST STEPS:
 * 1. Navigate to a user login page
 * 2. Input different credentials
 * 3. Submitting the login form and record the result
 *
 * ### PRE CONDITION:
 * - Must not be logged in as a valid user
 * - Must have a valid credentials
 *
 * ### EXPECTED RESULTS:
 * - User should be able to login via correct credentials
 *		and not login via wrong credentials
 *
 * ### ACTUAL RESULTS:
 * - User is logged in to dashboard
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../lib/basePage');
const config = require('../../utils/config');
const generate = require('../../utils/generate')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Login Page Testing', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            let total_test_cases = 7

            for (var i = 1; i <= total_test_cases; i++) {
                let test_title = '', test_email = '', test_passw = '', login_msg = ''

                switch (i) {
                    case 1:
                        test_title = 'Login without email and password'
                        break
                    case 2:
                        test_title = 'Login with email and empty password'
                        test_email = randomEmail()
                        break
                    case 3:
                        test_title = 'Login with password and empty email'
                        test_passw = randomPassword()
                        break
                    case 4:
                        test_title = 'Login with wrong username and password'
                        test_email = randomEmail()
                        test_passw = randomPassword()
                        break
                    case 5:
                        test_title = 'Login with valid username and wrong password'
                        test_email = config.userEmail
                        test_passw = randomPassword()
                        break
                    case 6:
                        test_title = 'Login with wrong email and valid password'
                        test_email = randomEmail()
                        test_passw = config.userPass
                        break
                    default:
                        test_title = 'Login with valid email and password'
                        test_email = config.userEmail
                        test_passw = config.userPass
                }

                it(test_title, async () => {
                    let email = await page.findByCSS('input[type=text]');
                    await page.write(email, test_email);

                    let password = await page.findByCSS('input[type=password]');
                    await page.write(password, test_passw);

                    let submit = await page.findById('submit');
                    await submit.click();

                    let loginMsgCheck = await page.checkElement('error_msgs');

                    if (loginMsgCheck) {
                        login_msg = await page.findById('error_msgs');
                        login_msg = await login_msg.getText();
                    }
                    else
                        login_msg = 'Login success';

                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Email entered: ': test_email,
                            'Password entered: ': test_passw,
                            'Login message': login_msg
                        }
                    });
                });
            }

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();