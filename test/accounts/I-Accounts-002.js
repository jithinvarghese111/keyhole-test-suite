/**
 * ### TEST CASE:
 * I-Accounts-002
 *
 * ### TEST TITLE:
 * Signup Page Testing
 *
 * ### TEST SUMMARY:
 * User is trying to signup
 *
 * ### TEST STEPS:
 * 1. Navigate to a user signup page
 * 2. Input fields with some values
 * 3. Submitting the signup form and record the result
 *
 * ### PRE CONDITION:
 * - Must not be logged in as a valid user
 *
 * ### EXPECTED RESULTS:
 * - User should be able to signup via correct credentials
 *		and not signup via same credentials
 *
 * ### ACTUAL RESULTS:
 * - User is logged in to dashboard after success signup
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../lib/basePage');
const config = require('../../utils/config');
const generate = require('../../utils/generate')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Signup Page Testing', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoRegister();
            });

            let test_title = '', total_test_cases = 2, first_name = '', second_name = '', email = '', password = '', total_reason = 6, any_reason = 2

            for (var i = 1; i <= total_test_cases; i++) {
                switch (i) {
                    case 1:
                        test_title = 'Signup with filling all fields'
                        first_name = randomName()
                        second_name = randomName()
                        email = randomEmail()
                        password = randomPassword()
                        break
                    case 2:
                        test_title = 'Signup with already registered email'
                        first_name = randomName()
                        second_name = randomName()
                        email = config.userEmail
                        password = randomPassword()
                        break
                    default:
                        test_title = 'Signup with filling all fields'
                        first_name = randomName()
                        second_name = randomName()
                        email = randomEmail()
                        password = randomPassword()
                }

                it(test_title, async () => {
                    let first = await page.findById('input-firstname');
                    await page.write(first, first_name);

                    let second = await page.findById('input-lastname');
                    await page.write(second, second_name);

                    let emails = await page.findById('input-email');
                    await page.write(emails, email);

                    let pass = await page.findById('input-password');
                    await page.write(pass, password);

                    let options = await page.findById('select-primary-reason');
                    await options.click();
                    await driver.wait(until.elementLocated(By.css('#primary-reason-research-item'), 3000));

                    let first_option = await page.findByCSS('#select-primary-reason option:nth-child('+any_reason+')');
                    let first_option_text = await first_option.getText();
                    await first_option.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="registerform"]/div[6]/div'), 3000));

                    let agree = await page.findById('agree-terms');
                    await agree.click();

                    let lets_go = await page.findById('step2sub');
                    await lets_go.click();
                    await driver.wait(until.elementLocated(By.css('#msgs > li:nth-child(1)'), 3000));

                    let signupMsgCheck = await page.checkElement('error_msgs');

                    if (signupMsgCheck) {
                        signup_msg = await page.findById('error_msgs');
                        signup_msg = await signup_msg.getText();
                    }
                    else
                        signup_msg = 'Signup success';

                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'First name: ': first_name,
                            'Last name: ': second_name,
                            'Email: ': email,
                            'Password: ': password,
                            'Primary reason: ': first_option_text,
                            'Signup message': signup_msg
                        }
                    });
                });
            }

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();