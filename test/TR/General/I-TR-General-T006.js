/**
 * ### TEST CASE:
 * I-TR-General-006
 
 * ### TEST TITLE:
 * Redirect to Popular Posts
 *
 * ### TEST SUMMARY:
 * User should be able to redirect to a popular post on a social media platform
 *(It can be test only with 'qa@wdstech.com' account)
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Redirect to Popular Posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Redirect to Popular Posts', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let checkEle =  await page.checkElementByCSS('#items--restructured > a:nth-child(4) > div');
                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{

                            'Test status': 'Unable to perform test. Quick Trends option is not available.'
                        }
                    }); 
                }
                else{

                    let trends = await page.findByXPath('//*[@id="items--restructured"]/a[4]/div');
                    await trends.click();
                    await driver.sleep(3000);

                    let checklimit = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                    if (checklimit) {
                        let closepopup = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[5]/div[2]/div[1]/span');
                        await closepopup.click();
                        steps += 'Credit Limit Reached, ';
                        await driver.sleep(2000);
                    }

                    await page.scrollPage('//*[@id="trends"]/div/section[2]/div[1]/div/div');
                    await driver.sleep(1000);
                    
                    let firstTrack = await page.findByXPath('//*[@id="comparisons"]/div[2]/ul/li[2]/a/div[1]');
                    let trackerName = await firstTrack.getText();
                    await firstTrack.click();
                    steps += 'Clicked hashtags or keywords, ';
                    await driver.sleep(5000);



                    let checkPost = await page.checkElementByCSS('#popular-posts > div.chart-heading > p');

                    if (!checkPost) {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value:{

                                'Test status': 'Unable to perform test. Posts not available.'
                            }
                        });
                    }
                    else{

                        await page.scrollPage('//*[@id="mentions-over-time"]/div[3]');
                        await driver.sleep(3000);

                        let checkVideo = await page.checkElementByCSS('#popular-posts > div:nth-child(2) > div > article:nth-child(1) > div > div > video');
                        if (checkVideo) {
                            let totalvideoPost = await page.findElementsByCSS('#popular-posts > div:nth-child(2) > div > article> div > div > video');
                            await driver.sleep(3000);
                            let totalPost = await totalvideoPost.length;
                            status += 'Total video post: '+totalPost;
                            let videoClick = await page.findByXPath('//*[@id="popular-posts"]/div[2]/div/article[1]/div/div/video');
                            await videoClick.click();
                            status += ' ,Clicked on Video Post';
                            await driver.sleep(5000); 
                        }

                        let checkImage = await page.checkElementByCSS('#popular-posts > div:nth-child(2) > div > article > div > a.card__media > img');

                        if (checkImage) {
                            await page.scrollPage('//*[@id="popular-posts"]/div[2]/div/article[6]/div');
                            await driver.sleep(1000);
                            let totalImagePost = await page.findElementsByCSS('#popular-posts > div:nth-child(2) > div > article > div > a.card__media > img');
                            let totalImage = await totalImagePost.length;
                            status += ' ,Total Image post: '+totalImage;
                            let imageClick = await page.findByXPath('//*[@id="popular-posts"]/div[2]/div/article[9]/div/a[1]/img');
                            await imageClick.click();
                            status += ' ,Clicked on Image Post and redirected.';
                            await driver.sleep(5000); 
                        }

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];                
                        });

                        await driver.switchTo().window(tab1);
                        await driver.sleep(2000);
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': status
                            }
                        });
                    }

                }                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
