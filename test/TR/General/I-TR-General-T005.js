/**
 * ### TEST CASE:
 * I-TR-General-005
 
 * ### TEST TITLE:
 * Download PNG
 *
 * ### TEST SUMMARY:
 * User should be able to download a PNG file related to their trends search
 *(It can be test only with 'qa@wdstech.com' account)
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Download PNG', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Download PNG', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let checkEle =  await page.checkElementByCSS('#items--restructured > a:nth-child(4) > div');
                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{

                            'Test status': 'Unable to perform test. Quick Trends option is not available.'
                        }
                    }); 
                }
                else{

                    let test_result = '';
                    const days = 30;
                    const toDate = new Date();
                    const fromDate = new Date();
                    fromDate.setDate(toDate.getDate() - days);
                    const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                    const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');

                    let trends = await page.findByXPath('//*[@id="items--restructured"]/a[4]/div');
                    await trends.click();
                    await driver.sleep(3000);

                    let checklimit = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                    if (checklimit) {
                        let closepopup = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[5]/div[2]/div[1]/span');
                        await closepopup.click();
                        steps += 'Credit Limit Reached, ';
                        await driver.sleep(2000);
                    }

                    await page.scrollPage('//*[@id="comparisons"]/div[1]/p');
                    await driver.sleep(1000);
                    
                    let firstTrack = await page.findByXPath('//*[@id="comparisons"]/div[2]/ul/li[2]/a/div[1]');
                    let trackerName = await firstTrack.getText();
                    await firstTrack.click();
                    steps += 'Clicked hashtags or keywords, ';
                    await driver.sleep(5000);

                    let downloadPNG = await page.findByCSS('#trends > div > div.key-trackerHeader.key-trackerHeader--noSidebarRestructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button.key-trackerSubHeaderAction__button.keyjs-quickTrendsAction--downloadPNG > span');
                    await downloadPNG.click();
                    steps += 'Download PNG button clicked, ';
                    await driver.sleep(2000);

                    let modalOpen = await page.findElementsByCSS('#trends > div > section > div.modal.modal__download--png.modal--show');
                    modalOpen = modalOpen.length;

                    if (modalOpen > 0)
                        steps += 'Modal opened, ';

                    let imageClick = await page.findByXPath('//*[@id="trends"]/div/section/div[7]/div[2]/div[2]/div/a/img');
                    await imageClick.click();
                    status += 'Download PNG '
                    await driver.sleep(5000);

                    const downloadPath = config.downloadPath+'\/'+trackerName+'_'+fromDateStr+'-'+toDateStr+'_'+new Date().getTime()+'.png';
                    const filename = `${trackerName}_image_.png`;
                    const renamePath = config.downloadPath+'\/'+filename;

                    if (fs.existsSync(downloadPath)) {
                        fs.renameSync(downloadPath, renamePath)
                        
                        if (fs.existsSync(renamePath)) {
                            const stats = fs.statSync(renamePath)

                            if (parseFloat(stats.size) > 32) {
                                test_result = "Downloaded file";
                                passed = true
                            } else {
                                test_result = "Downloaded empty file... but that's what happens on env other than prod";
                                passed = true
                            }
                        }
                    }                        
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': status 
                        }
                    });
                }                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
