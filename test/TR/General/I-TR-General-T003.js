/**
 * ### TEST CASE:
 * I-TR-General-003
 
 * ### TEST TITLE:
 * Shareable Link - Trends
 *
 * ### TEST SUMMARY:
 * User should be able to provide a 'Shareable Trends' link
 *(It can be test only with 'qa@wdstech.com' account)
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sharable Link - Trends', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Sharable Link - Trends', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let checkEle =  await page.checkElementByCSS('#items--restructured > a:nth-child(4) > div');
                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{

                            'Test status': 'Unable to perform test. Quick Trends option is not available.'
                        }
                    }); 
                }
                else{

                    let trends = await page.findByXPath('//*[@id="items--restructured"]/a[4]/div');
                    await trends.click();
                    await driver.sleep(3000);

                    let checklimit = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                    if (checklimit) {
                        let closepopup = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[5]/div[2]/div[1]/span');
                        await closepopup.click();
                        steps += 'Credit Limit Reached, ';
                        await driver.sleep(2000);
                    }

                    await page.scrollPage('//*[@id="comparisons"]/div[1]/p');
                    await driver.sleep(1000);
                    
                    let firstTrack = await page.findByXPath('//*[@id="comparisons"]/div[2]/ul/li[2]/a/div[1]');
                    let trackerName = await firstTrack.getText();
                    await firstTrack.click();
                    steps += 'Clicked hashtags or keywords, ';
                    await driver.sleep(5000);

                    let shareLink = await page.findByCSS('#trends > div > div.key-trackerHeader.key-trackerHeader--noSidebarRestructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > button.key-trackerSubHeaderAction__button.keyjs-quickTrendsAction--share > span');
                    await shareLink.click();
                    steps += 'share Link clicked, ';
                    await driver.sleep(2000);

                    let modalOpen = await page.findElementsByCSS('#trends > div > section > div.modal.modal__share.modal--show');
                    modalOpen = modalOpen.length;

                    if (modalOpen > 0)
                        steps += 'Modal opened, ';

                    let link = await page.findByXPath('//*[@id="trends"]/div/section/div[3]/div[2]/div[2]/textarea');
                    link = await link.getText();
                    steps += 'Link highlighted!';

                    if (link != "" && modalOpen > 0)
                        test_result = 'Test passed';
                        
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Shared Link': link,
                            'Test status': test_result 
                        }
                    });
                }                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
