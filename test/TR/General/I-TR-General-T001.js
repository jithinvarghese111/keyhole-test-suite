/**
 * ### TEST CASE:
 * I-TR-General-001
 
 * ### TEST TITLE:
 * Navigate to Trends page
 *
 * ### TEST SUMMARY:
 * User should be able to navigate to the 'Trends' page from the sidebar
 *(It can be test only with qa@wdstech.com account)
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to Trends page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Navigate to Trends page', async () => {
                await page.redirectToDashboard();

                let status = '';

                let checkEle =  await page.checkElementByCSS('#items--restructured > a:nth-child(4) > div');
                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{

                            'Test status': 'Unable to perform test. Quick Trends option is not available.'
                        }
                    }); 
                }
                else{

                    let trends = await page.findByXPath('//*[@id="items--restructured"]/a[4]/div');
                    await trends.click();
                    await driver.sleep(5000);

                    let checkTitle = await page.checkElementByCSS('#trends > div > section.trends__charts > div.wrapper > div > h3');
                    if (checkTitle) {
                        status += 'Quick Trends Page Available.';
                    }
                    else{
                        status += 'Page not found';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': status
                        }
                    });
                }                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
