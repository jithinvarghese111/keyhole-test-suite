/**
 * ### TEST CASE:
 * I-TR-General-002
 
 * ### TEST TITLE:
 * Recent Searches
 *
 * ### TEST SUMMARY:
 * User should be redirected to their Recent Search
 *(It can be test only with 'qa@wdstech.com' account)
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Recent Searches', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Recent Searches', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '', name = '';

                let checkEle =  await page.checkElementByCSS('#items--restructured > a:nth-child(4) > div');
                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{

                            'Test status': 'Unable to perform test. Quick Trends option is not available.'
                        }
                    }); 
                }
                else{

                    let trends = await page.findByXPath('//*[@id="items--restructured"]/a[4]/div');
                    await trends.click();
                    await driver.sleep(3000);

                    let checklimit = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                    if (checklimit) {
                        let closepopup = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[5]/div[2]/div[1]/span');
                        await closepopup.click();
                        steps += 'Credit Limit Reached';
                        await driver.sleep(2000);
                    }

                    await page.scrollPage('//*[@id="comparisons"]/div[1]/p');
                    await driver.sleep(1000);
                    
                    let recentSearches = await page.findElementsByCSS('#comparisons > div.chart-container > ul > li > a > div.comparison__keywords');
                    recentSearches = await recentSearches.length;
                    status += 'Total Recent Posts: '+recentSearches;
                    await driver.sleep(2000);
                    
                    for (let i = 1; i <= recentSearches; i++) {
                        let textTrends = await page.findByXPath('//*[@id="comparisons"]/div[2]/ul/li['+i+']/a/div[1]');
                        let searchText = await textTrends.getText();
                        status += ' ,searchText: '+searchText;
                        await driver.sleep(3000);
                    }

                    let firstTrack = await page.findByXPath('//*[@id="comparisons"]/div[2]/ul/li[2]/a/div[1]');
                    let trackerName = await firstTrack.getText();
                    await firstTrack.click();
                    steps += 'Clicked hashtags or keywords';
                    await driver.sleep(5000);

                    let secondTrack = await page.findByCSS('#trends > div > div.key-trackerHeader.key-trackerHeader--noSidebarRestructured > div.key-trackerHeader__wrapper > h1 > span');
                    let secondName = await secondTrack.getText();
                    await driver.sleep(3000);

                    if (trackerName == secondName) {
                        name += 'Displayed hashtags or keywords page ';
                    }
                    else{
                        name += 'Page not found';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test steps': steps,
                            'Recent Searches': status,
                            'Test status': name 
                        }
                    });
                }                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
