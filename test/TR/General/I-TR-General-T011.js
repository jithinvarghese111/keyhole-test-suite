/**
 * ### TEST CASE:
 * I-TR-General-011
 
 * ### TEST TITLE:
 * Add a Search Term (Search Bar)
 *
 * ### TEST SUMMARY:
 * User should be able to add a search term from the search bar
 *(It can be test only with 'qa@wdstech.com' account)
 */
var webdriver = require('selenium-webdriver');
const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add a Search Term (Search Bar)', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Add a Search Term (Search Bar)', async () => {
                await page.redirectToDashboard();

                let status = '';

                let checkEle =  await page.checkElementByCSS('#items--restructured > a:nth-child(4) > div');
                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{

                            'Test status': 'Unable to perform test. Quick Trends option is not available.'
                        }
                    }); 
                }
                else{

                    let trends = await page.findByXPath('//*[@id="items--restructured"]/a[4]/div');
                    await trends.click();
                    await driver.sleep(3000);

                    let checklimit = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                    if (checklimit) {
                        let closepopup = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[5]/div[2]/div[1]/span');
                        await closepopup.click();
                        status += 'Credit Limit Reached.';
                        await driver.sleep(2000);
                    }
                    else{
                        let trendSearch = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[1]/div/div/input');
                        await trendSearch.click();
                        await page.write(trendSearch, 'Rain');
                        await page.keyboardClick('#trends > div > section.trends__charts > div.wrapper > div > div > input');
                        await driver.sleep(5000);
                    
                        let checkkeyword = await page.checkElementByCSS('#trends > div > section.trends__controls > div > div > div:nth-child(1) > div > div.keyword__search-term');
                        if (checkkeyword) {
                            let keySearch = await page.findByXPath('//*[@id="trends"]/div/section[1]/div/div/div[1]/div/div[1]');
                            let searchTerm = await keySearch.getText();
                            status += 'keyword Search: '+searchTerm;
                        }
                        
                        let checkCount = await page.checkElementByCSS('#mentions-over-time > div:nth-child(2) > div.charts-legend > div > div.legend__total');

                        if (checkCount) {
                        let postCount = await page.findByCSS('#mentions-over-time > div:nth-child(2) > div.charts-legend > div > div.legend__total');
                        postCount = await postCount.getText();
                        status += 'Total posts related to search Keyword:'+postCount;
                        await driver.sleep(2000);

                        await page.scrollPage('//*[@id="popular-posts"]/div[1]');
                        await driver.sleep(1000);
                        }

                        let checkPost = await page.checkElementByCSS('#popular-posts > div:nth-child(2) > div > article:nth-child(1) > div > div > video');

                        if (checkPost) {
                            let postClick = await page.findByXPath('//*[@id="popular-posts"]/div[2]/div/article[1]/div/div/video');
                            await postClick.click();
                            status += ' ,Clicked on Post';
                            await driver.sleep(5000); 
                        }
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': status
                        }
                    });
                    
                }                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
