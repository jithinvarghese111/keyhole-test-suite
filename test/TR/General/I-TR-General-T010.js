/**
 * ### TEST CASE:
 * I-TR-General-010
 
 * ### TEST TITLE:
 * Change date -Trends
 *
 * ### TEST SUMMARY:
 * User should be able to change date and filter the trends topic posts.
 *(It can be test only with 'qa@wdstech.com' account)
 */
var webdriver = require('selenium-webdriver');
const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Change date -Trends', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Change date -Trends', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let checkEle =  await page.checkElementByCSS('#items--restructured > a:nth-child(4) > div');
                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{

                            'Test status': 'Unable to perform test. Quick Trends option is not available.'
                        }
                    }); 
                }
                else{

                    let trends = await page.findByXPath('//*[@id="items--restructured"]/a[4]/div');
                    await trends.click();
                    await driver.sleep(3000);

                    let checklimit = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                    if (checklimit) {
                        let closepopup = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[5]/div[2]/div[1]/span');
                        await closepopup.click();
                        steps += 'Credit Limit Reached, ';
                        await driver.sleep(2000);
                    }

                    let dateChange = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[1]/div/span[1]/button');
                    await dateChange.click();
                    await driver.sleep(2000);

                    let pickDate = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[2]/div[2]/div[2]/span/div/input');
                    await pickDate.click();
                    await driver.sleep(10000);

                    let pageClick = await randomNumberFromTo(1,4);
                    console.log(pageClick);
                    await driver.sleep(5000);

                    let startdate = await page.findByXPath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[2]/td['+pageClick+']');
                    await startdate.click();
                    await driver.sleep(5000);

                    let pageClicks = await randomNumberFromTo(1,4);
                    console.log(pageClicks);
                    await driver.sleep(5000);

                    let enddate = await page.findByXPath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[6]/td['+pageClicks+']');
                    await enddate.click();
                    await driver.sleep(5000);
                    
                    let checklimits = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                    if (checklimits) {
                        let closePopup = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[5]/div[2]/div[1]/span');
                        await closePopup.click();
                        await driver.sleep(2000);
                    }

                    await page.scrollPage('//*[@id="comparisons"]/div[1]/p');
                    await driver.sleep(1000);
                    
                    let firstTrack = await page.findByXPath('//*[@id="comparisons"]/div[2]/ul/li[2]/a/div[1]');
                    let trackerName = await firstTrack.getText();
                    await firstTrack.click();
                    steps += 'Clicked hashtags or keywords, ';
                    await driver.sleep(5000);

                    // console.log(fromDate.valueOf()); 
                    let currentURL = await driver.getCurrentUrl();

                    if (currentURL.indexOf('start') > -1 && currentURL.indexOf('end') > -1){
                        let status = '';

                        status += 'Start and End date added to URL., ';

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Current URL': currentURL,
                                'Test status': status
                            }
                        });
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': status,
                            'test steps': steps
                        }
                    });
                }               
            });


            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
