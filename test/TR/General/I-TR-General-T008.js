/**
 * ### TEST CASE:
 * I-TR-General-008
 
 * ### TEST TITLE:
 * Redirect to buy more credits
 *
 * ### TEST SUMMARY:
 * User should be able to click the button Get More Credits to be redirected to get more credits
 *(It can be test only with 'qa@wdstech.com' account)
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Redirect to buy more credits', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Redirect to buy more credits', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let checkEle =  await page.checkElementByCSS('#items--restructured > a:nth-child(4) > div');
                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{

                            'Test status': 'Unable to perform test. Quick Trends option is not available.'
                        }
                    }); 
                }
                else{

                    let trends = await page.findByXPath('//*[@id="items--restructured"]/a[4]/div');
                    await trends.click();
                    await driver.sleep(3000);

                    let checklimit = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                    if (checklimit) {
                        let closepopup = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[5]/div[2]/div[1]/span');
                        await closepopup.click();
                        steps += 'Credit Limit Reached, ';
                        await driver.sleep(2000);
                    }

                    await page.scrollPage('//*[@id="trends"]/div/section[2]/div[1]/div/div');
                    await driver.sleep(1000);
                    
                    let firstTrack = await page.findByXPath('//*[@id="comparisons"]/div[2]/ul/li[2]/a/div[1]');
                    let trackerName = await firstTrack.getText();
                    await firstTrack.click();
                    steps += 'Clicked hashtags or keywords, ';
                    await driver.sleep(5000);

                    let goTrendsList = await page.findByXPath('//*[@id="trends"]/div/div[1]/div[2]/div[1]/a');
                    await goTrendsList.click();
                    await driver.sleep(5000);

                    let checkEle = await page.checkElementByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');

                    if (checkEle) {

                        let modalOpen = await page.findElementsByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show');
                        modalOpen = modalOpen.length;

                        if (modalOpen > 0)
                            steps += 'Limit Reached, ';

                        let getMoreCredit = await page.findByCSS('#trends > div > section.trends__charts > div.modal.modal__credit-limit.modal--show > div.modal__wrapper > div.modal__body > p:nth-child(2) > a');
                        await getMoreCredit.click();
                        await driver.sleep(2000);                        

                        await page.scrollPage('//*[@id="quicktrendsCredit"]/div[2]/h2');
                        await driver.sleep(1000);

                        let gotoCheckout = await page.findByXPath('//*[@id="trendsCreditCta-1"]');
                        await gotoCheckout.click();
                        await driver.sleep(3000);

                        let creditcard_Number = await page.findByXPath('//*[@id="trends-credits-payment-form"]/div[2]/div/input');
                        await creditcard_Number.click();
                        await page.write(creditcard_Number, '5555555555554444');
                        await driver.sleep(1000);
                      
                        let cvc = await page.findByXPath('//*[@id="trends-credits-payment-form"]/div[3]/div/input');
                        await page.write(cvc, '544');
                        await driver.sleep(1000);

                        let expiration_Month = await page.findByXPath('//*[@id="trends-credits-payment-form"]/div[4]/div/input[1]');
                        await page.write(expiration_Month, '04');

                        let expiration_Year = await page.findByXPath('//*[@id="trends-credits-payment-form"]/div[4]/div/input[2]');
                        await page.write(expiration_Year, '2021');
                        await driver.sleep(1000);

                        let purchase = await page.findByXPath('//*[@id="trendsCreditCta-2"]');
                        steps += 'Clicked Purchase button.';
                        await driver.sleep(2000);
                        
                        var validations = await page.findByXPath('//*[@id="trends-credits__modal"]/div/div/div/div[2]/div/div/p');
                        text = await validations.getText();
                        console.log(text);
                        status = 'Validation for invalid details: '+text;
                        await driver.sleep(2000);
                        
                    }
                    else{
                        let moreCredit = await page.findByXPath('//*[@id="trends"]/div/section[2]/div[1]/div/span[2]/a/span');
                        await moreCredit.click();
                        await driver.sleep(3000);

                        await page.scrollPage('//*[@id="quicktrendsCredit"]/div[2]/h2');
                        await driver.sleep(1000);

                        let gotoCheckout = await page.findByXPath('//*[@id="trendsCreditCta-1"]');
                        await gotoCheckout.click();
                        await driver.sleep(3000);

                        let creditcard_Number = await page.findByXPath('//*[@id="request-form-payment"]/div[1]/label[2]/input');
                        await page.write(creditcard_Number, '5555555555554444');
                        await driver.sleep(1000);
                      
                        let cvc = await page.findByXPath('//*[@id="trends-credits-payment-form"]/div[3]/div/input');
                        await page.write(cvc, '544');
                        await driver.sleep(1000);

                        let expiration_Month = await page.findByXPath('//*[@id="trends-credits-payment-form"]/div[4]/div/input[1]');
                        await page.write(expiration_Month, '04');

                        let expiration_Year = await page.findByXPath('//*[@id="trends-credits-payment-form"]/div[4]/div/input[2]');
                        await page.write(expiration_Year, '2021');
                        await driver.sleep(1000);

                        let purchase = await page.findByXPath('//*[@id="trendsCreditCta-2"]');
                        steps += 'Clicked Purchase button.';
                        await driver.sleep(2000);

                        let checkValidation = await page.checkElementByCSS('#trends-credits__modal > div > div > div > div.trackerDataContent > div > div > p');
                        if (checkValidation) {
                            var validation = await page.findByXPath('//*[@id="trends-credits__modal"]/div/div/div/div[2]/div/div/p');
                            text = await validation.getText();
                            status = 'Validation for invalid details:' +text;
                        }
                    }
                        
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
