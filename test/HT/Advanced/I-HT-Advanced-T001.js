 /**                                                                                             
 * ### TEST CASE:
 * I-HT-Advanced-001
 
 * ### TEST TITLE:
 * Navigating to the advanced tracker page
 *
 * ### TEST SUMMARY:
 * User is able to navigate to the advanced tracker page from their sidebar
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigating to the advanced tracker page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Navigating to the advanced tracker page', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                let advanceTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/div/div/a');
                let advanceTrackerName = await advanceTracker.getText();
                steps += 'Navigated to advance Tracker Page '
                await advanceTracker.click();                              
                await driver.wait(until.elementLocated(By.css('#advancedEdit > div > div.key-trackerHeader.key-trackerHeader--restructured > div > h1'), 3000));

                let checkEle = await page.checkElementByCSS('#advancedEdit > div > div.key-trackerHeader.key-trackerHeader--restructured > div > h1');

                if (checkEle) {
                    let getText = await page.findByXPath('//*[@id="advancedEdit"]/div/div[1]/div/h1');
                    getText = await getText.getText();
                    status += 'Page Title:' +getText
                    status += ' ,New Advance Tracker Page Displayed.'
                }
                else {
                    status = 'page not found'
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test Steps': steps,
                        'Test status': status
                    }
                });  

            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
