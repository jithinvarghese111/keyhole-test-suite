/**                                                                                   
 * ### TEST CASE:
 * I-HT-Advanced-002
 
 * ### TEST TITLE:
 * Adding a new advanced tracker
 *
 * ### TEST SUMMARY:
 * User is able to click on Add New (Advanced) to create a new tracker with specific filters
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Adding a new advanced tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Adding a new advanced tracker', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                let advanceTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/div/div/a');
                let advanceTrackerName = await advanceTracker.getText();
                steps += 'Navigated to advance Tracker Page '
                await advanceTracker.click();                              
                await driver.wait(until.elementLocated(By.css('#advancedEdit > div > div.key-trackerHeader.key-trackerHeader--restructured > div > h1'), 3000));

                let searchTerms = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div/div[1]/div[2]/div[1]/input');
                await searchTerms.clear();
                await page.write(searchTerms, randomKeyword);
                steps += ' ,Added search Terms';

                let platforms = ['Instagram', 'News', 'Blogs','Forums'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--refineSearch.key-filterSection--open > div.key-filterSection__content > div.key-filterSection__row.key-filterSection__row--flex > div > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 2000));
                }
                steps += ' ,Checked multiple platforms';

                await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[1]/div[2]');
                await driver.sleep(1000);

                let checkElem = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--subTrackers.key-filterSection--open > div.key-filterSection__content > div > p > span');

                if(!checkElem){
                    let subTracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div/div[1]/i');
                    await subTracker.click();
                    await driver.wait(until.elementLocated(By.css('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--subTrackers.key-filterSection--open > div.key-filterSection__content > div > div > div:nth-child(1) > input'), 3000));
                }

                let firstSubtracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div/div/div[1]/input');
                await firstSubtracker.clear();
                await page.write(firstSubtracker, 'footstep');

                let secondSubtracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/input');
                await secondSubtracker.clear();
                await page.write(secondSubtracker, 'track');
                steps += ' ,Updated sub Trackers'

                let subTrackerClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/i');
                await subTrackerClose.click();

                let checkEle = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--filters.key-filterSection--open > div.key-filterSection__content.key-filterSection__content--noPadding > div:nth-child(1) > p > span');

                if(!checkEle){
                    let filtersOpen = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div/div[1]/i');
                    await filtersOpen.click();
                    await driver.wait(until.elementLocated(By.css('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--filters.key-filterSection--open > div.key-filterSection__content.key-filterSection__content--noPadding > div:nth-child(1) > p > span'), 3000));
                }

                let filtersClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div/div[1]/i');
                await filtersClose.click();
                await driver.sleep(1000);

                let checkEles = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--startStopTracker.key-filterSection--open > div.key-filterSection__content > div:nth-child(2) > p > span');
                if(!checkEles){
                    let startStopTrackerOpen = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[4]/div/div[1]/i');
                    await startStopTrackerOpen.click();
                    await driver.wait(until.elementLocated(By.css('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--startStopTracker.key-filterSection--open > div.key-filterSection__content > div:nth-child(1) > div > span > div > input'), 3000));
                }
                
                let futureStartDate = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[4]/div[2]/div[1]/div/span/div/input');
                await futureStartDate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[12]/div/div[1]/div[2]/div[1]/span[1]/div/input'), 3000));

                let changeStartDate = await page.findByXPath('/html/body/div[12]/div/div[1]/div[2]/div[1]/span[1]/div/input');
                await changeStartDate.click();
                await changeStartDate.clear();
                await page.write(changeStartDate, '2019-08-03');

                if(changeStartDate){
                    status += 'Updated future Start Date'
                }
                else{
                   status += 'Date selection failed'
                }

                let dateSave = await page.findByXPath('/html/body/div[12]/div/div[2]/button');
                await dateSave.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[4]/div[2]/div[2]/div/span/div/input'), 3000));

                let futureEndDate = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[4]/div[2]/div[2]/div/span/div/input');
                await futureEndDate.click();
                await driver.sleep(2000);

                let changeDate = await page.findByXPath('/html/body/div[12]/div/div[1]/div[2]/div[1]/span[1]/div/input');
                await changeDate.click();
                await changeDate.clear();
                await page.write(changeDate, '2020-07-01');

                if(changeDate){
                    status += 'Updated future End Date';
                }
                else{
                   status += 'Date selection failed';
                };

                let datesSave = await page.findByXPath('/html/body/div[12]/div/div[2]/button');
                await datesSave.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[4]/div[2]/div[1]/div/span/div/input'), 3000));

                let startStopTrackerClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[4]/div/div[1]/i');
                await startStopTrackerClose.click();
                await driver.sleep(1000);

                await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[1]');
                await driver.sleep(1000);

                let checkE = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--sharing.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--customURL.key-filterSection--open > div.key-filterSection__content > div:nth-child(1) > p > span');

                if(!checkE){
                    let customTracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/i');
                    await customTracker.click();
                    await driver.wait(until.elementLocated(By.css('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--sharing.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--customURL.key-filterSection--open > div.key-filterSection__content > div:nth-child(1) > div > input'), 3000));
                }

                let customPageUrl = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/input');
                await customPageUrl.click();
                await customPageUrl.clear();
                await page.write(customPageUrl, randomKeyword);
                let trackerName = await customPageUrl.getAttribute("value");

                let pageTitle = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[2]/div/input');
                await pageTitle.click();
                await pageTitle.clear();
                await page.write(pageTitle, 'Real-time Tracker: '+randomKeyword);

                if(customPageUrl && pageTitle){
                    status += ' ,Updated Custom Page URL & Page Title';
                }
                else{
                   status += 'Custom Page URL & Page Title updation failed';
                }

                let customTrackerClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/i');
                await customTrackerClose.click();
                await driver.sleep(1000);

                let saveTracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[3]/div[1]/div[2]/button');
                await saveTracker.click();
                await driver.sleep(10000);

                let hashtagTrack = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtagTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a'), 3000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let secondtrackerName = await firstTrack.getText();
                await firstTrack.click();
                
                if(randomKeyword != secondtrackerName){
                    status = 'New Advance Tracker not Added, tryagain later';
                }
                assert.equal(randomKeyword, secondtrackerName);
                status = 'New Advance Tracker Added successfully';
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test Steps': steps,
                        'Test status': status
                    }
                });  

            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
