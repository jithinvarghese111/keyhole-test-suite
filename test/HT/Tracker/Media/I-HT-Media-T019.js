/**
 * ### TEST CASE:
 * I-HT-Media-T019
 
 * ### TEST TITLE:
 * check Post display mode
 *
 * ### TEST SUMMARY:
 *  User is trying to click on the media wall, after opening the same click on Advanced filter and change Post display mode grid into slideshow and viceversa.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('change Post display mode from advance filter', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should able to change Post display mode grid into slideshow and viceversa', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div/div[2]'), 100000));

                let clickAdvanced = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3)');
                await clickAdvanced.click();

                let displayClick = await page.findByCSS('#advancedFilters > form > div:nth-child(4) > div:nth-child(2) > div > div > label:nth-child(4)');
                await displayClick.click();

                let hideFilter = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3) > span');
                await hideFilter.click();
                await driver.sleep(2000);

                let checkSlideshow = await page.checkElementByCSS('#media-wall > div.wallContent__wrapper > div > div.slideshowCard > div > div.slideshowCard__image > div');
                if (checkSlideshow) {
                    addContext(this.ctx, 'Post Display Mode changed into slideshow');
                }

                let clickFilter= await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3)');
                await clickFilter.click();

                let displayModeClick = await page.findByCSS('#advancedFilters > form > div:nth-child(4) > div:nth-child(2) > div > div > label:nth-child(2)');
                await displayModeClick.click();

                let hidefilter = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3) > span');
                await hidefilter.click();
                await driver.sleep(1000);

                let checkgrid = await page.checkElementByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__text');
                if (checkgrid) {
                    addContext(this.ctx, 'Post Display Mode changed into Grid');
                }
                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();

