/**
 * ### TEST CASE:
 * I-HT-Media-002
 
 * ### TEST TITLE:
 * Interact with the Media page
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the media table by sorting media posts by Recent/Top posts,
 * clicking on a media card should redirect the user to the social media post,
 * and user should be able to filter social media posts by a time range
 *
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with the Media page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Interact with the Media page', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let mediaWall = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[6]/a/span');
                await mediaWall.click();
                steps += 'Navigated to Media Wall';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let firstPostText = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/div/p');
                firstPostText = await firstPostText.getText();

                let pickDate = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/div/div[2]/span/div');
                await pickDate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[13]/div/div/div[2]'), 3000));

                let pageClick = await randomNumberFromTo(3,6);

                let startdate = await page.findByXPath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[2]/td['+pageClick+']');
                await startdate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[2]/td['+pageClick+']'), 3000));

                let pageClicks = await randomNumberFromTo(1,4);

                let enddate = await page.findByXPath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[4]/td['+pageClicks+']');
                await enddate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[4]/td['+pageClicks+']'), 3000));

                await driver.navigate().refresh();
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let secondPostText = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/div/p');
                secondPostText = await secondPostText.getText();
                assert.equal(secondPostText, firstPostText);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/div/p'), 3000));
            
                if (secondPostText == firstPostText) {
                    status += 'Post not filtered by date range';
                }
                else{
                    status += 'filtered posts by date range';
                }

                let advance = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]');
                await advance.click();
                await driver.wait(until.elementLocated(By.id('media-wall'), 3000));

                let topPost = await page.findByXPath('//*[@id="advancedFilters"]/form/div[2]/div[1]/div/div/label[2]');
                await topPost.click();
                status += ' ,Top Posts Displayed';

                let hideAdvance = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]');
                await hideAdvance.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div'), 2000));

                let advanceSort = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]/span');
                await advanceSort.click();
                await driver.wait(until.elementLocated(By.id('media-wall'), 3000));

                let recentPost = await page.findByXPath('//*[@id="advancedFilters"]/form/div[2]/div[1]/div/div/label[1]');
                await recentPost.click();
                status += ' ,Recent Posts Displayed';
                await driver.wait(until.elementLocated(By.id('sort_ts'), 3000));

                let hideAdvanceSort = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]/span');
                await hideAdvanceSort.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div'), 2000));

                let advanceOpen = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]/span');
                await advanceOpen.click();
                await driver.wait(until.elementLocated(By.id('media-wall'), 3000));

                let darkTheme = await page.findByXPath('//*[@id="advancedFilters"]/form/div[5]/div/div/div/label[2]');
                await darkTheme.click();
                status += ' ,Changed into dark theme';
                await driver.wait(until.elementLocated(By.id('theme_dark'), 3000));

                let advanceClose = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]/span');
                await advanceClose.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div'), 2000));

                let post = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/div');
                await post.click();
                status += ' ,Redirected to the social media post.';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
