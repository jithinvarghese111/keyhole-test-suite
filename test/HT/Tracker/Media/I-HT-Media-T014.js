/**
 * ### TEST CASE:
 * I-HT-Media-T014
 
 * ### TEST TITLE:
 * Delete Post
 *
 * ### TEST SUMMARY:
 *  User is trying to delete post from media Wall.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check Delete Post', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check Delete Post from media wall', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));
            
                let firstPost = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image > div > p');
                firstPost = await firstPost.getText();

                await page.movetoEle('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image > div > p');

                let select = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/i');
                await select.click();
                await driver.sleep(1000);

                let deletePost = await page.findByCSS('#bulk-manage__modal > button.bulk-manage__button.bulk-manage__button--red');
                await deletePost.click();
                await driver.sleep(1000);
                
                let deleteConfirm = await page.findByCSS('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div.key-modal.key-modal--mediaWallBulkDelete.key-modal--show > div.key-modal__wrapper > div.key-modal__footer > button.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--red.keyjs-deletePost');
                await deleteConfirm.click();
                await driver.sleep(8000);

                let secondPost = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image > div > p');
                secondPost = await secondPost.getText();

                assert.notEqual(secondPost, firstPost);
                addContext(this.ctx, 'Post deleted Successfully ');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();