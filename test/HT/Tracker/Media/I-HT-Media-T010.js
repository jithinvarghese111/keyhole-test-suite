/**
 * ### TEST CASE:
 * I-HT-Media-010
 
 * ### TEST TITLE:
 *  Search posts -Media
 *
 * ### TEST SUMMARY:
 * User is trying to search for a particular Posts from Media
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search posts -Media', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Search with available Posts', async () => {
                await page.redirectToDashboard();

                let steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(1000);

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let editTracker = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[7]/a');
                await editTracker.click();
                steps += 'Navigated to Edit Tracker page';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[1]/div/h1/strong'), 2000));

                await page.scrollPixel(1000);
                await driver.sleep(3000);

                let checkElemn = await page.checkElementByCSS('#key-toggle--mediaModeration > div > label:nth-child(3)');
                if(!checkElemn){
                    let mediaOpen = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[3]/div/div[1]/i');
                    await mediaOpen.click();
                }

                let mediaYes = await page.findByXPath('//*[@id="key-toggle--mediaModeration"]/div/label[1]');
                await mediaYes.click();
                steps += ' ,Turn on Media Wall moderation';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="key-toggle--mediaModeration"]/div/label[1]'), 2000));

                let mediaClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[3]/div[1]/div[1]/i');
                await mediaClose.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[3]/div/div[2]/span'), 2000));

                let saveChanges = await page.findByXPath('//*[@id="advancedEdit"]/div/div[3]/div[1]/div[2]/button');
                await saveChanges.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));
                
                let mediaWall = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[6]/a/span');
                await mediaWall.click();
                steps += ' ,Navigated to media Wall';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 4000));
               
                let searchBox = await page.findByCSS('#simpleSearch');
                await searchBox.click();
                await page.write(searchBox, 'anything');
                
                let searchSubmit = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/button');
                await searchSubmit.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/div'), 4000));

                let result = await page.checkElementByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1)');
                if (result) {
                    let result = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div > div > div.key-postCard__image > div');
                    let totalPosts = await result.length;
                    result_msg = 'Total Posts found after search: '+totalPosts;
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/div'), 3000));
                }
                else{
                    result_msg = 'Search result empty. No Posts found';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test Steps': steps,
                        'Search result': result_msg
                    }
                });  
                      
            });

            it('Search with random keyword', async () => {
                await page.redirectToDashboard();

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(1000);

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));
                
                let mediaWall = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[6]/a/span');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 4000));

                for (var i = 1; i <= 3; i++) {
                    var random_name = randomName();

                    let searchBox = await page.findByXPath('//*[@id="simpleSearch"]');
                    await searchBox.click();
                    await searchBox.clear();
                    await page.write(searchBox, random_name);
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                    let searchSubmit = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/button');
                    await searchSubmit.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                    let result = await page.checkElementByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image');

                    if (result) {
                        let result = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div > div > div.key-postCard__image > div');
                        let totalPosts = await result.length;
                        result_msg = 'Total Posts found after search: '+totalPosts;
                    }
                    else{
                        result_msg = 'Search result empty. No Posts found';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Search keyword': random_name,
                            'Search result': result_msg
                        }
                    });  
                }

                let editTrackers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[7]/a');
                await editTrackers.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[1]/div/h1/strong'), 2000));

                await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[2]/div/div[2]/span');
                await driver.sleep(3000);  

                let checkElem = await page.checkElementByCSS('#key-toggle--mediaModeration > div > label:nth-child(4)');
                if(!checkElem){
                    let mediawallOpen = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[3]/div/div[1]/i');
                    await mediawallOpen.click();
                }

                let mediaNo = await page.findByXPath('//*[@id="key-toggle--mediaModeration"]/div/label[2]');
                await mediaNo.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="key-toggle--mediaModeration"]/div/label[1]'), 2000));

                let mediawallClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[3]/div[1]/div[1]/i');
                await mediawallClose.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[3]/div/div[2]/span'), 2000));

                let savedChanges = await page.findByXPath('//*[@id="advancedEdit"]/div/div[3]/div[1]/div[2]/button');
                await savedChanges.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
