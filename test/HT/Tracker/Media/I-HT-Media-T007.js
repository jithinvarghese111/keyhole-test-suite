/**
 * ### TEST CASE:
 * I-HT-Media-007
 
 * ### TEST TITLE:
 * Sort persists to URL
 *
 * ### TEST SUMMARY:
 * When changing sort, sort state should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sort persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Sort persists to URL', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let mediaWall = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[6]/a/span');
                await mediaWall.click();
                steps += 'Navigated to mediaWall';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let advanceOpen = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]');
                await advanceOpen.click();
                await driver.wait(until.elementLocated(By.id('advancedFilters'), 2000));

                let sortPost = await page.findByXPath('//*[@id="advancedFilters"]/form/div[2]/div[1]/div/div/label[2]');
                await sortPost.click();
                await driver.wait(until.elementLocated(By.id('sort_rt'), 2000));

                let advanceClose = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]/span');
                await advanceClose.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div'), 2000));

                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                console.log(params);
            
                if (params.indexOf('sort') > -1 && params.indexOf('rt') > -1) {

                    let status = '';

                    status += 'Sort added to URL., ';

                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div'), 2000));

                    if (params.indexOf('sort') > -1 && params.indexOf('rt') > -1) {
                        status += 'Sort persisted upon reload.';
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Current URL': currentURL,  
                            'Test status': status
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Sort not added to URL.'
                        }
                    });
                }              
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
