/**
 * ### TEST CASE:
 * I-HT-Media-T015
 
 * ### TEST TITLE:
 * Select multiple media wall post and change status
 *
 * ### TEST SUMMARY:
 * User is trying to select multiple post from media wall and change status
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Select multiple media wall post and change status', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Select multiple media wall posts', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong > strong'), 20000));

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.sleep(2000);
                
                let checkOnOff = await page.checkElementByCSS('.key-toggle.key-toggle--off');

                if (checkOnOff) {
                    let clickOn = await page.findByCSS('#key-toggle--mediaModeration > div > label:nth-child(4)');
                    await clickOn.click();
                    await driver.sleep(5000);
                }

                let totalPosts = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div');
                totalPosts = totalPosts.length;
                addContext(this.ctx, 'Total Post: '+totalPosts);

                if (totalPosts > 5) totalPosts = 5;

                for (let i = 1; i <= totalPosts; i++) {
                    await page.scrollPage('//*[@id="media-wall"]/div[2]/div['+i+']');
                    await driver.sleep(1000);

                    let selectPost = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child('+i+') > div > div.key-postCard__text > div.key-postCard__visibility > div');
                    await selectPost.click();
                    addContext(this.ctx, 'Select '+i+' post success');
                    await driver.sleep(2000);
                }
            });

            it('Select multiple media wall posts and change visibility', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong > strong'), 20000));

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.sleep(2000);
                
                let checkOnOff = await page.checkElementByCSS('.key-toggle.key-toggle--off');

                if (checkOnOff) {
                    let clickOn = await page.findByCSS('#key-toggle--mediaModeration > div > label:nth-child(4)');
                    await clickOn.click();
                    await driver.sleep(5000);
                }

                let totalPosts = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div');
                totalPosts = totalPosts.length;
                addContext(this.ctx, 'Total Post: '+totalPosts);

                if (totalPosts > 5) totalPosts = 5;

                for (let i = 1; i <= totalPosts; i++) {
                    await page.scrollPage('//*[@id="media-wall"]/div[2]/div['+i+']');
                    await driver.sleep(1000);

                    let selectPost = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child('+i+') > div > div.key-postCard__text > div.key-postCard__visibility > div');
                    await selectPost.click();
                    addContext(this.ctx, 'Select '+i+' post success');
                    await driver.sleep(2000);
                }

                let clickVis = await page.findByCSS('#bulk-manage__modal > div > button');
                await clickVis.click();
                await driver.sleep(1000);

                let clickPublic = await page.findByCSS('#bulk-manage__modal > div > ul > li.visibilityList__item.js-postCard__updateVisibility.public');
                await clickPublic.click();
                await driver.sleep(1000);

                addContext(this.ctx, 'Selected posts visibility to Public success');
            });

            it('Select multiple media wall posts and cancel selection', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong > strong'), 20000));

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.sleep(2000);
                
                let checkOnOff = await page.checkElementByCSS('.key-toggle.key-toggle--off');

                if (checkOnOff) {
                    let clickOn = await page.findByCSS('#key-toggle--mediaModeration > div > label:nth-child(4)');
                    await clickOn.click();
                    await driver.sleep(5000);
                }

                let totalPosts = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div');
                totalPosts = totalPosts.length;
                addContext(this.ctx, 'Total Post: '+totalPosts);

                if (totalPosts > 5) totalPosts = 5;

                for (let i = 1; i <= totalPosts; i++) {
                    await page.scrollPage('//*[@id="media-wall"]/div[2]/div['+i+']');
                    await driver.sleep(1000);

                    let selectPost = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child('+i+') > div > div.key-postCard__text > div.key-postCard__visibility > div');
                    await selectPost.click();
                    addContext(this.ctx, 'Select '+i+' post success');
                    await driver.sleep(2000);
                }

                let clickCancel = await page.findByCSS('#bulk-manage__modal > button');
                await clickCancel.click();
                await driver.sleep(1000);

                addContext(this.ctx, 'Selected posts cancelled');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();