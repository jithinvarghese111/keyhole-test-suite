/**
 * ### TEST CASE:
 * I-HT-Media-T013
 
 * ### TEST TITLE:
 *  Check MEDIA WALL MODERATION
 *
 * ### TEST SUMMARY:
 *  User is trying to click on the media wall moderation and handle the search.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check MEDIA WALL MODERATION', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check media wall moderation and handle the search', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.sleep(5000);

                let totalBefore = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div');
                addContext(this.ctx, 'Total media wall before moderation: '+totalBefore.length);

                let onMod = await page.findByCSS('#key-toggle--mediaModeration > div > label:nth-child(4)');
                await onMod.click();

                for (let i = 1; i <= 3; i++) {
                    let clickDrop = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters.key-trackerSubHeader__filters--thirdWidth.keyjs-mediaWallActions--filters > form > div.key-trackerSubHeader__dropdownWrapper.key-dropdown__select');
                    await clickDrop.click();
                    await driver.sleep(1000);

                    let getLabel = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters.key-trackerSubHeader__filters--thirdWidth.keyjs-mediaWallActions--filters > form > div.key-trackerSubHeader__dropdownWrapper.key-dropdown__select > ul > li:nth-child('+i+')');
                    getLabel = await getLabel.getText();

                    let dropDown = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters.key-trackerSubHeader__filters--thirdWidth.keyjs-mediaWallActions--filters > form > div.key-trackerSubHeader__dropdownWrapper.key-dropdown__select > ul > li:nth-child('+i+')');
                    await dropDown.click();
                    totalBefore = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div');
                    addContext(this.ctx, 'Total media wall after '+getLabel+' moderation: '+totalBefore.length);
                    await driver.sleep(1000);
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();

