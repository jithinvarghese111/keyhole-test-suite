/**
 * ### TEST CASE:
 * I-HT-Media-009
 
 * ### TEST TITLE:
 * Visibility is editable
 *
 * ### TEST SUMMARY:
 * When Media Moderation is on, post visibility may be changed
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Visibility is editable', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Visibility is editable', async () => {
                await page.redirectToHTDashboard();

                let status = '';
                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstHT.getText();
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.sleep(10000);
                                    
                let moderationValue = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/label');
                moderationValue = await moderationValue.getAttribute("for");
                let param = moderationValue.substr(moderationValue.indexOf('__')+1);
                let firstParam = param.substr(0, param.lastIndexOf('-'));

                if (firstParam.indexOf('on') > -1) {
                    let mediawallClick = await page.findByXPath('//*[@id="key-toggle--mediaModeration"]/div/label[2]');
                    await mediawallClick.click();
                    await driver.sleep(1000);
                }

                let checkPublic = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div/div[3]/div[1]/div[1]');
                checkPublic = await checkPublic.getAttribute("class");
                let value = checkPublic.substr(checkPublic.lastIndexOf('__')+1);
                if (value.indexOf('selected') > -1) {

                    await page.movetoEle('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image > div');
                    
                    let select = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/i');
                    await select.click();
                    await driver.sleep(3000);

                    let changevisiblity = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[2]');
                    await changevisiblity.click();
                    await driver.sleep(3000);

                    let changePrivacy = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[2]/ul/li[2]');
                    await changePrivacy.click();
                    await driver.sleep(3000);

                    let searchPrivacy = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/div[1]/span');
                    await searchPrivacy.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/div[1]/ul/li[1]'), 10000));

                    let selectedPrivate = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/div[1]/ul/li[2]');
                    await selectedPrivate.click();
                    await driver.sleep(3000);

                    let checkElem = await page.checkElementByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__text > div.key-postCard__visibility > div.key-postCard__visibilityWrapper.js-postCard__visibility.js-doNotLink.key-postCard__visibilityWrapper--selected');
                    if(checkElem){
                        status += 'posts visibility changed into Private';
                    }
                    
                }
                else{

                    await page.movetoEle('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image > div');

                    let selection = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/i');
                    await selection.click();
                    await driver.sleep(3000);

                    let setvisiblity = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[2]');
                    await setvisiblity.click();
                    await driver.sleep(3000);

                    let changePrivacy = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[2]/ul/li[1]');
                    await changePrivacy.click();
                    await driver.sleep(3000);

                    let postprivacy = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/div[1]/span');
                    await postprivacy.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/div[1]/ul/li[1]'), 10000));

                    let setPrivacy = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/div[1]/ul/li[1]');
                    await setPrivacy.click();
                    await driver.sleep(3000);
                    
                    let checkEle = await page.checkElementByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__text > div.key-postCard__visibility > div.key-postCard__visibilityWrapper.js-postCard__visibility.js-doNotLink.key-postCard__visibilityWrapper--selected');
                    if(checkEle){
                        status += 'posts visibility changed into Public';
                    }

                }
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test status': status
                    }
                }); 
                  
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
