/**
 * ### TEST CASE:
 * I-HT-Media-T018
 
 * ### TEST TITLE:
 *  Check Embed Media Page in advanced filter
 *
 * ### TEST SUMMARY:
 *  User is trying to click on the media wall, after opening the same click on Advanced filter and embed the media page.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check Embed Media Page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check Embed Media Page in advanced filter', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div/div[2]'), 100000));

                let clickAdvanced = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3)');
                await clickAdvanced.click();

                let embedClick = await page.findByCSS('#advancedFilters > form > div.key-advancedFilters__column.advancedFilters__footer > div > button:nth-child(1)');
                await embedClick.click();

                let checkEle = await page.checkElementByCSS('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div.key-modal.key-modal--embed.key-modal--show');
                if (checkEle) {
                    let modalOpen = await page.findElementsByCSS('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div.key-modal.key-modal--embed.key-modal--show');
                    modalOpen = modalOpen.length;

                    if (modalOpen > 0)
                        addContext(this.ctx, 'Modal opened, ');

                    let copySnippet = await page.findByCSS('.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--solidGreen.key-button--copy.copy--left');
                    await copySnippet.click();
                    addContext(this.ctx, 'Copy snippet button clicked, ');
                    await driver.wait(until.elementLocated(By.id('mediaWall__embed'), 2000));

                    let code = await page.findById('mediaWall__embed');
                    code = await code.getText();
                    
                    let url = await driver.getCurrentUrl();

                    addContext(this.ctx, 'Embed Code: '+code);
                    addContext(this.ctx, 'URL highlighted and copied!');
                }else{
                    addContext(this.ctx, 'Error Occured');
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();

