/**
 * ### TEST CASE:
 * I-HT-Media-004
 
 * ### TEST TITLE:
 * Platform Filters - Media
 *
 * ### TEST SUMMARY:
 * Platform checkboxes should filter Media content
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Platform Filters - Media', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Platform Filters - Media', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let mediaWall = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[6]/a/span');
                await mediaWall.click();
                steps += 'Navigated to mediaWall';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let totalPlatform = await page.findElementsByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox');

                if (totalPlatform.length < 2) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Test requires multiple platforms.'
                        }
                    });
                }
                else{
                    for(i= 1; i <= totalPlatform.length; i++) {
                    let selectAll = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/div/div[1]/form/label['+i+']');
                    let title = await selectAll.getAttribute("class");
                    let titleName = title.substr(title.lastIndexOf('-')+1);
                    let tname = titleName.substr(0, titleName.lastIndexOf(' '));

                    if (title.indexOf('hide') > -1) {
                        status += ' ,hidden platforms are :' +tname;
                    }
                    else{
                        
                        await selectAll.click();
                        await driver.wait(until.elementLocated(By.id('twt_check'), 1000));
                        await selectAll.click();
                        }
                    }
                }

                status += ' ,Platform checkboxes filtered.';
                await driver.wait(until.elementLocated(By.id('twt_check'), 1000));

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });       
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
