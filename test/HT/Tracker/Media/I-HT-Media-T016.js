/**
 * ### TEST CASE:
 * I-HT-Media-T016
 *
 * ### TEST TITLE:
 * Clear selection of post
 *
 * ### TEST SUMMARY:
 *  User is trying to clear selection of each post after selected it from media Wall.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Clear selection of Post ', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Clear selection of post', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));
                
                let firstPost = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image > div > p');
                firstPost = await firstPost.getText();

                await page.movetoEle('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image > div > p');

                let select = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div/div[1]/i');
                await select.click();
                await driver.sleep(2000);

                let clearSelection = await page.findByCSS('#bulk-manage__modal > button.bulk-manage__button.bulk-manage__button--blue.js-bulk-manage__toggle');
                await clearSelection.click();
                await driver.sleep(1000);
                addContext(this.ctx, 'Post Selection Cleared.');
            });

            it('Clear selection of multiple post', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));
            
                let firstPost = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1) > div > div.key-postCard__image > div > p');
                firstPost = await firstPost.getText();

                for (var i = 1; i <= 3; i++) {

                await page.hoveronEle('//*[@id="media-wall"]/div[2]/div['+i+']/div/div[2]/div');
                await driver.sleep(2000);       

                let postedBy = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child('+i+') > div > div.key-postCard__text > div.key-postCard__userWrapper > div > div.key-postCard__infoWrapper > div.key-postCard__username');
                postedBy = await postedBy.getText();
                await driver.sleep(2000);

                let select = await page.findByXPath('//*[@id="media-wall"]/div['+i+']/div[1]/div/div[1]/i');
                await select.click();
                await driver.sleep(2000);

                let clearSelection = await page.findByCSS('#bulk-manage__modal > button.bulk-manage__button.bulk-manage__button--blue.js-bulk-manage__toggle');
                await clearSelection.click();
                await driver.sleep(5000);
                addContext(this.ctx, 'selection cleared for the Post which is posted by: '+postedBy);
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();