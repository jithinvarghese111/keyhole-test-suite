/**
 * ### TEST CASE:
 * I-HT-Media-T017
 *
 * ### TEST TITLE:
 * Filter posts with advanced filters option
 *
 * ### TEST SUMMARY:
 * User should be able to filter the post using advanced filter options
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Filter posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Filter posts using advanced filter', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                let postcolor = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div');
                let firstPostcolor = await postcolor.getCssValue("background-color");

                let clickAdvanced = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3)');
                await clickAdvanced.click();

                let displayPostText = await page.findByCSS('#advancedFilters > form > div:nth-child(4) > div:nth-child(1) > div > div > label:nth-child(4)');
                await displayPostText.click();
                await driver.sleep(3000);

                let postTheme = await page.findByCSS('#advancedFilters > form > div:nth-child(5) > div > div > div > label:nth-child(4)');
                await postTheme.click();
                await driver.sleep(1000);

                let hideFilter = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3)');
                await hideFilter.click();
                await driver.sleep(3000);

                let checkEle = await page.checkElementByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(3) > div > div.key-postCard__text > div.key-postCard__description');
                if (checkEle) {
                  addContext(this.ctx, 'Displayed Post Text');
                }else{
                  addContext(this.ctx, 'Posts Text no found.');
                }
                
                let postclr = await page.findByXPath('//*[@id="media-wall"]/div[2]/div[1]/div');
                let secondPostclr = await postclr.getCssValue("background-color");

                assert.notEqual(secondPostclr, firstPostcolor);
                addContext(this.ctx, 'Post Theme Changed');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();