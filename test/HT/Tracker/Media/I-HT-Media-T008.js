/**
 * ### TEST CASE:
 * I-HT-Media-008
 
 * ### TEST TITLE:
 * Exclude RT
 *
 * ### TEST SUMMARY:
 * User should be able to exclude retweeted content
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Exclude RT', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Exclude RT', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let mediaWall = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[6]/a/span');
                await mediaWall.click();
                steps += 'Navigated to mediaWall';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 4000));

                let advanceOpen = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]');
                await advanceOpen.click();
                await driver.wait(until.elementLocated(By.css('#advancedFilters > form > div:nth-child(2) > div:nth-child(2) > div > div > label:nth-child(4)'), 3000));

                let excludeRetweet = await page.findByXPath('//*[@id="advancedFilters"]/form/div[2]/div[2]/div/div/label[2]');
                await excludeRetweet.click();
                await driver.wait(until.elementLocated(By.id('excludeRetweets_on'), 2000));

                let advanceClose = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]/span');
                await advanceClose.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedFilters"]/form/div[2]/div[2]/div/div/label[2]'), 3000));  
                    
                let totalPost = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div > div');

                for (i= 1; i <= totalPost.length; i++) {
                    let allPost = await page.findByXPath('//*[@id="media-wall"]/div[2]/div['+i+']/div/div[1]/div');
                    let getText = await allPost.getText();

                    if(getText.indexOf('RT') > -1){
                        status += 'Retweets are not Excluded, try again later';
                    }
                    else{
                        
                        status += ' ,Excluded Retweets from all post.';
                    }
                } 
                await driver.wait(until.elementLocated(By.id('twt_check'), 5000));                 
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test Steps': steps,
                        'Test status': status
                    }
                });  
                      
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
