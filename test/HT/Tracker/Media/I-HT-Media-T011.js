/**
 * ### TEST CASE:
 * I-HT-Media-T011
 
 * ### TEST TITLE:
 *  Check media wall exists in the twitter, instagram, facebook etc.
 *
 * ### TEST SUMMARY:
 * User is trying to click on the media wall, after opening the same in social media platform the post is checked.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search posts -Media', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check media wall exists in social media', async () => {
                await page.redirectToHTDashboard();

                let firstHT = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                await firstHT.click();

                let mediaWall = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(6) > a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="media-wall"]/div[2]/div[1]/div/div[2]'), 30000));

                let checkCount = await page.findElementsByCSS('#media-wall > div.wallContent__wrapper > div');
                checkCount = checkCount.length;

                if (checkCount > 0) {
	                let clickFirst = await page.findByCSS('#media-wall > div.wallContent__wrapper > div:nth-child(1)');
	                let selectedPlat = await clickFirst.getAttribute("data-platform");
	                addContext(this.ctx, 'Clicked Platform: '+selectedPlat);
	                await clickFirst.click();
	                await driver.sleep(5000);

	                let url = await driver.getCurrentUrl();
	                addContext(this.ctx, 'Keyhole page URL: '+url);

	                let tab1, tab2;

	                await driver.getAllWindowHandles().then(function(windowHandles) {
	                    tab1 = windowHandles[0];
	                    tab2 = windowHandles[1];                
	                });

	                await driver.switchTo().window(tab2);
	                url = await driver.getCurrentUrl();
	                addContext(this.ctx, selectedPlat+' tab URL: '+url);

	                if (url.indexOf(selectedPlat) !== -1) addContext(this.ctx, 'Page exists');
	                else addContext(this.ctx, 'Page not exists');
	            }
	            else addContext(this.ctx, 'Media wall empty');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
