/**
 * ### TEST CASE:
 *  I-HT-Historical Data-001
 
 * ### TEST TITLE:
 * Historical Data Backfill -contact us
 *
 * ### TEST SUMMARY:
 * Requesting for upgrade your plan from historical data backfill 
 * 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Historical Data Backfill -contact us', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Historical Data Backfill -contact us', async () => {
                await page.redirectToDashboard();

                let platform = '' , steps = '' , status = '';
                
                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]'), 3000));

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let historical_Data = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[8]/a/span');
                await historical_Data.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="request-form-1"]/div[1]/h1'), 3000));

                var tracker_dropdown = await page.findByXPath('//*[@id="dd"]');
                var tracker_hash = await tracker_dropdown.getAttribute("data-hash");
                if (hash == tracker_hash ) {
                    status += 'Already selected tracker :'+trackerName;
                }

                var fromDate = await page.findByXPath('//*[@id="backfill_start"]');
                await fromDate.click();
                await page.write(fromDate, '2019-08-01');
                steps += ', Selected start Date';
                 await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_start"]'), 3000));

                var toDate = await page.findByXPath('//*[@id="backfill_end"]');
                await toDate.click();
                await toDate.clear();
                await page.write(toDate, '2019-08-07');
                steps += ', Selected End Date';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_end"]'), 6000));

                var readMore = await page.findByXPath('//*[@id="request-form-1"]/div[1]/p/a');
                await readMore.click();
                steps += ' ,Clicked on readMore';
                await driver.sleep(3000);

                let tab1, tab2;

                await driver.getAllWindowHandles().then(function(windowHandles) {
                    tab1 = windowHandles[0];
                    tab2 = windowHandles[1];                
                });

                await driver.switchTo().window(tab1);  

                var continue_Request = await page.findByXPath('//*[@id="get_gnip_volume"]');
                await continue_Request.click();
                steps += ', Clicked on Continue';
                await driver.sleep(3000);

                let contactUs = await page.findByXPath('//*[@id="request-form-2"]/div[3]/p/a');
                await contactUs.click();
                steps += ', Clicked on Contact Us.';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="contact-us-section"]/div[1]/div/h1'), 3000));

                let name = await page.findByXPath('//*[@id="name"]');
                await name.click();
                await name.clear();
                await page.write(name, 'Karen Gleichner');

                let email = await page.findByXPath('//*[@id="email"]');
                await email.click();
                await email.clear();
                await page.write(email, 'qa+02@wdstech.com');

                let phone = await page.findByXPath('//*[@id="phone"]');
                await phone.click();
                await phone.clear();
                await page.write(phone, '000000000001-562-828');

                let options = await page.findById('reason');
                await options.click();
                let first_option = await page.findByCSS('#reason > option:nth-child(5)');
                let first_option_text = await first_option.getText();
                await first_option.click();
                await driver.wait(until.elementLocated(By.id('reason'), 3000));

                let message = await page.findByXPath('//*[@id="message"]');
                await message.click();
                await message.clear();
                await page.write(message, 'Test Request');

                let submit = await page.findByXPath('//*[@id="submit"]');
                await submit.click();
                status += ' ,Sent Request'
                await driver.wait(until.elementLocated(By.xpath('//*[@id="response"]/span'), 3000));

                let sucess_msg = await page.findByXPath('//*[@id="response"]/span');
                sucess_msg = await sucess_msg.getText();
                status += ' ,Sucess message :'+sucess_msg;
            
                await driver.navigate().back()
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 4000));                              
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status                      
                    }
                });
            });

            it('Historical Data Backfill - demo-request', async () => {
                await page.redirectToDashboard();

                let platform = '' , steps = '' , status = '';
                
                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]'), 3000));

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let historical_Data = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[8]/a/span');
                await historical_Data.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="request-form-1"]/div[1]/h1'), 3000));

                var tracker_dropdown = await page.findByXPath('//*[@id="dd"]');
                var tracker_hash = await tracker_dropdown.getAttribute("data-hash");
                if (hash == tracker_hash ) {
                    status += 'Already selected tracker :'+trackerName;
                }
                
                await driver.sleep(5000);

                let fromDate = await page.findByXPath('//*[@id="backfill_start"]');
                await fromDate.click();
                await page.write(fromDate, '2019-08-01');
                steps += ', Selected start Date';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_start"]'), 3000));

                let toDate = await page.findByXPath('//*[@id="backfill_end"]');
                await toDate.click();
                await toDate.clear();
                await page.write(toDate, '2019-08-07');
                steps += ', Selected End Date';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_end"]'), 6000));

                let readMore = await page.findByXPath('//*[@id="request-form-1"]/div[1]/p/a');
                await readMore.click();
                steps += ' ,Clicked on readMore';
                await driver.sleep(3000);

                let tab1, tab2;

                await driver.getAllWindowHandles().then(function(windowHandles) {
                    tab1 = windowHandles[0];
                    tab2 = windowHandles[1];                
                });

                await driver.switchTo().window(tab1);              
                let continue_Request = await page.findByXPath('//*[@id="get_gnip_volume"]');
                await continue_Request.click();
                steps += ', Clicked on Continue';
                await driver.sleep(5000);

                let contactUs = await page.findByXPath('//*[@id="request-form-2"]/div[3]/p/a');
                await contactUs.click();
                steps += ', Clicked on Contact Us.';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="contact-us-section"]/div[1]/div/p[3]/a'), 3000));

                let demoRequest = await page.findByXPath('//*[@id="contact-us-section"]/div[1]/div/p[3]/a');
                await demoRequest.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="content"]/div/div/div/section/div/div/div/div/div/section/div/div/div[1]/div/div/div[2]/div/h1'), 3000));

                let fname = await page.findById('form-field-firstname');
                await fname.click();
                await page.write(fname, 'Karen');

                let lname = await page.findById('form-field-lastname');
                await lname.click();
                await page.write(lname, 'Gleichner');

                let email = await page.findById('form-field-email');
                await email.click();
                await page.write(email, 'qa+02@wdstech.com');

                let phone = await page.findById('form-field-phonenumber');
                await phone.click();
                await page.write(phone, '000000000001-562-828');

                let companyName = await page.findById('form-field-companyname');
                await companyName.click();
                await page.write(companyName, 'keyhole');

                let country = await page.findById('form-field-companyname');
                await country.click();

                let firstOption = await page.findByXPath('//*[@id="form-field-country"]/option[3]');
                await firstOption.click();

                let companySize = await page.findById('form-field-companysize');
                await companySize.click();

                let selectOption = await page.findByXPath('//*[@id="form-field-companysize"]/option[5]');
                await selectOption.click();

                let usecases = await page.findById('form-field-usecases');
                await usecases.click();

                let usecaseOption = await page.findByXPath('//*[@id="form-field-usecases"]/option[3]');
                await usecaseOption.click();
                
                let agency = await page.findById('form-field-agency');
                await agency.click();

                let agencyNo = await page.findByXPath('//*[@id="form-field-agency"]/option[2]');
                await agencyNo.click();

                let projectdate = await page.findById('form-field-project_start_date');
                await projectdate.click();

                let dateOptions = await page.findByXPath('//*[@id="form-field-project_start_date"]/option[2]');
                await dateOptions.click();

                let duration = await page.findById('form-field-project_duration');
                await duration.click();

                let durationOptions = await page.findByCSS('#form-field-project_duration > option:nth-child(2)');
                await durationOptions.click();
                await driver.sleep(3000);

                await page.scrollPixel(400);
                await driver.sleep(3000);

                let histdata = await page.findById('form-field-historical_data');
                await histdata.click();

                let histYes = await page.findByXPath('//*[@id="form-field-historical_data"]/option[2]');
                await histYes.click();
                
                let monthlybudget = await page.findById('form-field-budget');
                await monthlybudget.click();

                let budgetOptions = await page.findByXPath('//*[@id="form-field-budget"]/option[3]');
                await budgetOptions.click();

                let note = await page.findById('form-field-message');
                await note.click();
                await page.write(note, 'Test Request');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="form-field-termsOfService"]'), 3000));

                let checkbox = await page.findByXPath('//*[@id="form-field-termsOfService"]');
                await checkbox.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="requestdemosubmitbutton"]'), 3000));

                let submit = await page.findByXPath('//*[@id="requestdemosubmitbutton"]');
                await submit.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="content"]/div/div/div/section/div/div/div/div/div/section/div/div/div[2]/div/div/div[2]/div/form/div[2]'), 3000));
            
                let sucess_msg = await page.findByXPath('//*[@id="content"]/div/div/div/section/div/div/div/div/div/section/div/div/div[2]/div/div/div[2]/div/form/div[2]');
                sucess_msg = await sucess_msg.getText();
                status += ' ,Sucess message :'+sucess_msg;
            
                await driver.navigate().back()
                await driver.wait(until.elementLocated(By.xpath('//*[@id="contact-us-section"]/div[1]/div/h1'), 3000));
                
                await driver.navigate().back()
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));
                                                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status                      
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();