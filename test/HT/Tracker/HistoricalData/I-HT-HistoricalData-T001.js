/**
 * ### TEST CASE:
 *  I-HT-Historical Data-001
 
 * ### TEST TITLE:
 * Requesting Historical Data backfill 
 *
 * ### TEST SUMMARY:
 * Trying to Send a request for a historical data backfill from specific tracker,
 * 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Requesting Historical Data Backfill (Enterprise)', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Requesting Historical Data Backfill (Enterprise)', async () => {
                await page.redirectToDashboard();

                let platform = '' , steps = '' , status = '';
                
                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]'), 3000));

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let historical_Data = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[8]/a/span');
                await historical_Data.click();
                steps += 'Displayed historical Data popup';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="request-form-1"]/div[1]/h1'), 3000));

                var tracker_dropdown = await page.findByXPath('//*[@id="dd"]');
                var tracker_hash = await tracker_dropdown.getAttribute("data-hash");
                if (hash == tracker_hash ) {
                    status += 'Already selected the same tracker :'+trackerName;
                }
    
                await driver.sleep(5000);

                var fromDate = await page.findByXPath('//*[@id="backfill_start"]');
                await fromDate.click();
                await page.write(fromDate, '2019-08-01');
                steps += ', Selected start Date';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_start"]'), 3000));

                var toDate = await page.findByXPath('//*[@id="backfill_end"]');
                await toDate.click();
                await toDate.clear();
                await page.write(toDate, '2019-08-07');
                steps += ', Selected End Date';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_end"]'), 3000));
                
                var continue_Request = await page.findByXPath('//*[@id="get_gnip_volume"]');
                await continue_Request.click();
                steps += ', Clicked on Continue';
                await driver.sleep(5000);
                
                var back = await page.findByXPath('//*[@id="request-form-2"]/div[3]/button[1]');
                await back.click();
                await driver.sleep(5000);

                var close = await page.findByXPath('//*[@id="backfill__modal-step1"]/div/a');
                await close.click();
                steps += ', Closed historical_Data modal';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': 'Success'                       
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();