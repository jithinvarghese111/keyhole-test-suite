/**
 * ### TEST CASE:
 * I-HT-Dashboard-T047
 
 * ### TEST TITLE:
 * Interact with Influential Users from SubTracker Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the Influential Users from SubTracker Dashboard
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Influential Users from SubTracker Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with Influential Users from SubTracker Dashboard', async () => {
                await page.redirectToHTDashboard();

                let status = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));
                
                let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                let subTrackerName = await subTrack.getText();
                await subTrack.click();
                await driver.sleep(1000);

                let list = await page.findByCSS('#react-select-2-option-0');
                await list.click();
                addContext(this.ctx, 'clicked on subTracker');
                await driver.sleep(3000);

                let checkPost = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div');

                if (checkPost) {

                    await page.scrollPixel(600);
                    await driver.sleep(3000);
  
                    let freqUsers = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div:nth-child(2)');
                    await freqUsers.click();

                    let firstUser = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(2) > div.key-chart--influencers > div > div:nth-child(1) > div:nth-child(1) > div.key-profilePicture.key-profilePicture--twitter');
                    await firstUser.click();
                    addContext(this.ctx, 'Influencer page visited');
              
                    let tab1, tab2;

                    await driver.getAllWindowHandles().then(function(windowHandles) {
                        tab1 = windowHandles[0];
                        tab2 = windowHandles[1];
                    });

                    await driver.switchTo().window(tab1);
                    await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div.el-tabs__item.is-active'), 5000));

                }
                else{
                    addContext(this.ctx, 'Unable to perform test. subTracker does not have posts.');
                }
                            
            });

            it('Embed the Influential Users from SubTracker Dashboard', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));
                
                let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                await subTrack.click();
                await driver.sleep(1000);

                let list = await page.findByCSS('#react-select-2-option-0');
                let subTrackerName = await list.getText();
                await list.click();
                addContext(this.ctx, 'clicked on subTracker: '+subTrackerName);
                await driver.sleep(3000);

                let checkPost = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div');

                if (checkPost) {

                    await page.scrollPixel(600);
                    await driver.sleep(3000);
  
                    let freqUsers = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div:nth-child(2)');
                    await freqUsers.click();

                    let embedClick = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.key-chartOptions > button:nth-child(2)');
                    await embedClick.click();
                    steps += 'Embed button clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[2]/div[1]/div[2]/div[1]/h3 '), 3000));

                    let modalOpen = await page.findElementsByCSS('#hashtag-tracking .key-modal.key-modal--dashboardEmbed.key-modal--show');
                    modalOpen = modalOpen.length;

                    if (modalOpen > 0)
                        steps += 'Modal opened, ';

                    let copySnippet = await page.findByCSS('.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--solidGreen.key-button--copy.copy--left');
                    await copySnippet.click();
                    steps += 'Copy snippet button clicked, ';
                    await driver.wait(until.elementLocated(By.id('dashboard__embed'), 3000));

                    let code = await page.findById('dashboard__embed');
                    let codeName = await code.getText();
                    let params = codeName.substring(codeName.indexOf("_")+1);
                    let firstParam = params.substr(0, params.indexOf('?'));
                    let secondParm = firstParam.replace(/^.{1}/g, firstParam[0].toUpperCase());

                    assert.equal(secondParm, subTrackerName);
                    test_result = 'URL with subTracker highlighted and copied!';

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'SubTracker name': subTrackerName,
                            'Embed code': codeName,
                            "Test steps": steps,
                            'Test status': test_result
                        }
                    });

                }
                else{
                    addContext(this.ctx, 'Unable to perform test. subTracker does not have posts.');
                }
                            
            });

            it('Download the Influential Users information in CSV format from subTracker Dashboard', async () => {
                await page.redirectToHTDashboard();

                let test_result = '';
                const days = 30;
                const toDate = new Date();
                const fromDate = new Date();
                fromDate.setDate(toDate.getDate() - days);
                const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));
                
                let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                await subTrack.click();
                await driver.sleep(1000);

                let list = await page.findByCSS('#react-select-2-option-0');
                let subTrackerName = await list.getText();
                await list.click();
                addContext(this.ctx, 'clicked on subTracker');
                await driver.sleep(3000);

                let checkPost = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div');

                if (checkPost) {

                    await page.scrollPixel(600);
                    await driver.sleep(3000);
  
                    let freqUsers = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div:nth-child(2)');
                    await freqUsers.click();

                    let csvClick = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.key-chartOptions > button:nth-child(1)');
                    await csvClick.click();
                    await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div.el-tabs__item.is-active'), 3000));

                    const downloadPath = config.downloadPath+'\/'+'Keyhole_'+trackerName+'_'+subTrackerName+'('+hash+')_'+fromDateStr+'-'+toDateStr+'_top_tweets.csv';
                    const filename = `Keyhole_${trackerName}(${hash})_topPosts_${new Date().getTime()}.csv`;
                    const renamePath = config.downloadPath+'\/'+filename;

                    console.log(downloadPath);
                    console.log(renamePath);

                    if (fs.existsSync(downloadPath)) {
                        fs.renameSync(downloadPath, renamePath)
                        
                        if (fs.existsSync(renamePath)) {
                            const stats = fs.statSync(renamePath)

                            if (parseFloat(stats.size) > 64) {
                                test_result = "Downloaded file";
                                passed = true
                            } else {
                                test_result = "Downloaded empty file... but that's what happens on env other than prod";
                                passed = true
                            }
                        }
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': test_result
                        }
                    });
                }
                else {
                    addContext(this.ctx, 'Unable to perform test. subTracker does not have posts.');
                }     
            });


            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();