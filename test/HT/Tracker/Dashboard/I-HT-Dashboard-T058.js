/**
 * ### TEST CASE:
 * I-HT-Dashboard-T058
 
 * ### TEST TITLE:
 * Customize Dashboard Chart order
 *
 * ### TEST SUMMARY:
 * User should be able to Hide Top Posts, Related Topic, Sentiment, Location, Top Website, Gender, Top Devices and Apps, Most Linked Websites, Social Media Post Types, Influential Users Chart from HT Tracker 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Drag and Drop Top Posts Chart', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });
            
            it('Drag and Drop Top Posts Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);   

                let element = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(1) > div > span');
                let elementDestination = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(3) > div > span');
                const actions = await driver.actions({bridge: true});
                await driver.actions().mouseDown(element).perform();
                await actions.move({duration:5000,origin:element,elementDestination}).perform();
                // await driver.actions(element).mouseMove(elementDestination).perform();
                await driver.actions().mouseUp(element).perform();                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();