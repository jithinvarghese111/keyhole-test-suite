/**
 * ### TEST CASE:
 * I-HT-Dashboard-T027
 
 * ### TEST TITLE:
 * Embed Gender Chart
 *
 * ### TEST SUMMARY:
 *  User should be able to Embed Gender Chart
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Embed Gender Chart', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Embed Gender Chart', async () => {
                await page.redirectToDashboard();

                let platform = '' , steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[1]/div'), 2000));

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[2]/a');
                await driver.sleep(10000);

                var gender = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[9]/div[2]');
                await gender.click();

                var gender_Embed = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[9]/div[2]/div[3]/button[2]/div');
                await gender_Embed.click();
                steps += 'Embed button clicked, ';
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div > div.key-modal__wrapper > div.key-modal__heading > h3'), 3000));
             
                var copy_snippet = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[3]/button[2]');
                await copy_snippet.click();
                steps += 'Copy snippet button clicked, ';
                await driver.wait(until.elementLocated(By.id('dashboard__embed'), 2000));
        
                let copied_link = await page.findByCSS('#dashboard__embed');
                let link = await copied_link.getAttribute("value");
                steps += 'URL highlighted and copied!' +link;
                await driver.sleep(5000);

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps
                        
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();