/**
 * ### TEST CASE:
 * I-HT-Dashboard-T011
 
 * ### TEST TITLE:
 * Interact with Related Topics
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the related topics section of Hashtag tracking
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Related Topics', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with the related topics section of Hashtag tracking', async () => {
                await page.redirectToHTDashboard();

                let status = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));
                
                let didLoad = await page.checkElementByCSS('.key-chartBox--topics');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[1]/div/div[1]'), 20000));

                if (didLoad) {
                    let getPost = await page.checkElementByCSS('.key-chartEmptyState');

                    if (getPost) {
                        const allowedDays = [365];
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)];

                        let changeDate = await setDateRangeNew(days);
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 5000));

                        let getPost1 = await page.checkElementByCSS('.key-chartEmptyState');

                        if (getPost1) {
                            status = 0;
                        }
                        else {
                            status = 1;
                        }
                    }
                    else {
                        status = 1;
                    }

                    if (status) {
                        let hideHeader = await page.findById('header-container');
                        await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                        
                        await page.scrollPixel(800);
                        await driver.sleep(3000);

                        let keywordClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[4]/div[2]/div[1]/div[1]/div/div/div/div[2]');
                        await keywordClick.click();
                        await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topics > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(2) > div > div > svg > g > text:nth-child(1)'), 2000));

                        let hashtagClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[4]/div[2]/div[1]/div[1]/div/div/div/div[1]');
                        await hashtagClick.click();
                        await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topics > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div > div > svg > g > text:nth-child(1)'), 2000));

                        let clickKeyword = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topics > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div > div > svg > g > text:nth-child(1)');
                        await clickKeyword.click();
                        await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-sideview.key-sideview--dashboardTopicsView.key-sideview--active'), 5000));

                        let popup = await page.checkElementByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--dashboardTopicsView.key-sideview--active');
                        
                        if (popup) {
                            let hashtag = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--dashboardTopicsView.key-sideview--active > div.key-modalHeader > div > h3');
                            hashtag = await hashtag.getText();

                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Hashtag selected': hashtag,
                                    'Test status': 'Hastag popup loaded.'
                                }
                            });
                        }
                        else {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Hastag popup not loaded.'
                                }
                            });
                        }
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Tracker does not have hashtag cloud data.'
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Could not load dashboard related topics.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();