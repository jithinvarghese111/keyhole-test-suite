/**
 * ### TEST CASE:
 * I-HT-Dashboard-T016
 
 * ### TEST TITLE:
 * Download Influential Users as CSV
 *
 * ### TEST SUMMARY:
 * User should be allowed to download the influential users information in CSV format
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Download Influential Users as CSV', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Download the influential users information in CSV format', async () => {
                await page.redirectToDashboard();

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let test_result = '';
                const days = 365;
                const toDate = new Date();
                const fromDate = new Date();
                fromDate.setDate(toDate.getDate() - days);
                const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                
                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[1]');
                await driver.sleep(1000);

                let inputTracker = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div');
                await inputTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[2]/button[1]'), 2000));

                let csvClick = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.key-chartOptions > button:nth-child(1)');
                await csvClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[1]/div/div/div/div[1]'), 2000));

                const downloadPath = config.downloadPath+'\/'+'Keyhole_'+trackerName+'('+hash+')_'+fromDateStr+'-'+toDateStr+'_influencers.csv';
                const filename = `Keyhole_${trackerName}(${hash})_influencers_${new Date().getTime()}.csv`;
                const renamePath = config.downloadPath+'\/'+filename;

                console.log(downloadPath);
                console.log(renamePath);

                if (fs.existsSync(downloadPath)) {
                    fs.renameSync(downloadPath, renamePath)
                    
                    if (fs.existsSync(renamePath)) {
                        const stats = fs.statSync(renamePath)

                        if (parseFloat(stats.size) > 64) {
                            test_result = "Downloaded file";
                            passed = true
                        } else {
                            test_result = "Downloaded empty file... but that's what happens on env other than prod";
                            passed = true
                        }
                    }
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Downloade path': downloadPath,
                        'Renamed path': renamePath,
                        'Test status': test_result
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();