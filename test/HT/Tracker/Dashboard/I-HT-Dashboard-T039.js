/**
 * ### TEST CASE:
 * I-HT-Dashboard-T039
 *
 * ### TEST TITLE:
 * Navigated into Subtracker
 *
 * ### TEST SUMMARY:
 * User should be able to create Subtracker and Navigated into Subtracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigated into Subtracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigated into Subtracker', async () => {
                await page.redirectToHTDashboard();
                
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let checkSubTracker = await page.checkElementByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                if (checkSubTracker) {
                    let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                    await subTrack.click();
                    await driver.sleep(1000);

                    let list = await page.findByCSS('#react-select-2-option-0');
                    await list.click();
                    addContext(this.ctx, 'Navigated into subTracker');
                    await driver.sleep(1000);

                }
                else{

                    let edit = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li.sidebar__sub-item.sidebar__edit > a');
                    await edit.click();
                    await driver.sleep(5000);

                    await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div');
                    await driver.sleep(2000);

                    let checkEle = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--subTrackers.key-filterSection--open > div.key-filterSection__content > div > p > span');
                    if (!checkEle) {

                        let subTracker = await page.findByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--subTrackers > div > div.key-filterSection__headingTitle > span');
                        await subTracker.click();
                        await driver.sleep(2000);
                    }

                    let trackerList = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div/div/div[1]/input');
                    await trackerList.clear();
                    await page.write(trackerList, 'helloworld');
                    addContext(this.ctx, 'Added subTracker');
                    await driver.sleep(3000);

                    
                    let saveChanges = await page.findByCSS('#advancedEdit > div > div.key-advancedEdit__toolbar.key-toolbar > div.key-toolbar__wrapper > div.key-toolbar__content.key-toolbar__content--right > button');
                    await saveChanges.click();
                    await driver.sleep(2000);

                    let saveTracker = await page.findByXPath('//*[@id="advancedEdit"]/div/section/div[1]/div[2]/div[3]/button[2]');
                    await saveTracker.click();
                    await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let checkTracker = await page.checkElementByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                    if (checkTracker) {
                        let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                        await subTrack.click();
                        await driver.sleep(1000);

                        let list = await page.findByCSS('#react-select-2-option-0');
                        await list.click();
                        addContext(this.ctx, 'Navigated into subTracker');
                        await driver.sleep(1000);

                    }
                    else{
                        addContext(this.ctx, 'No subTracker found');
                    }
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();