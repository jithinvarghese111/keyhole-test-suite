/**
 * ### TEST CASE:
 * I-HT-Dashboard-T004
 
 * ### TEST TITLE:
 * Downloading XLS
 *
 * ### TEST SUMMARY:
 * User should be able to download an excel file of hashtag tracking information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading XLS', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Download a XLS file of HT information locally', async () => {
                await page.redirectToHTDashboard();

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let test_result = '';
                const days = 30;
                const toDate = new Date();
                const fromDate = new Date();
                fromDate.setDate(toDate.getDate() - days);
                const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let xlsLink = await page.findByCSS('#hashtag-tracking #update');
                await xlsLink.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                const downloadPath = config.downloadPath+'\/'+'Keyhole_'+trackerName+'('+hash+')_'+fromDateStr+'-'+toDateStr+'.xls';
                const filename = `Keyhole_${trackerName}(${hash})_topPosts_${new Date().getTime()}.xls`;
                const renamePath = config.downloadPath+'\/'+filename;

                console.log(downloadPath);
                console.log(renamePath);

                if (fs.existsSync(downloadPath)) {
                    fs.renameSync(downloadPath, renamePath)
                    
                    if (fs.existsSync(renamePath)) {
                        const stats = fs.statSync(renamePath)

                        if (parseFloat(stats.size) > 64) {
                            test_result = "Downloaded file";
                            passed = true
                        } else {
                            test_result = "Downloaded empty file... but that's what happens on env other than prod";
                            passed = true
                        }
                    }
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Downloade path': downloadPath,
                        'Renamed path': renamePath,
                        'Test status': test_result
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();