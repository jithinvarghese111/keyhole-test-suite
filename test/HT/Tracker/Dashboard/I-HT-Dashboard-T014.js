/**
 * ### TEST CASE:
 * I-HT-Dashboard-T014
 
 * ### TEST TITLE:
 * Interact with Influential Users
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the influential users section of Hashtag tracking
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Influential Users', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with the influential users section of Hashtag tracking', async () => {
                await page.redirectToHTDashboard();

                let status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));
                
                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let didLoad = await page.checkElementByCSS('.key-chartBox--sizeLarge.key-chartBox--topStats');
                await driver.sleep(10000);

                if (didLoad) {
                    let getPost = await page.checkElementByCSS('.key-chartEmptyState');

                    if (getPost) {
                        const allowedDays = [365];
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)];

                        let changeDate = await setDateRangeNew(days);
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                        let getPost1 = await page.checkElementByCSS('.key-chartEmptyState');

                        if (getPost1) {
                            status = 0;
                        }
                        else {
                            status = 1;
                        }
                    }
                    else {
                        status = 1;
                    }

                    if (status) {
                        let steps = '';
                        let hideHeader = await page.findById('header-container');
                        await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);                     
                        await page.scrollPixel(1200);
                        await driver.sleep(4000);
                       
                        let frequentClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[1]/div/div/div/div[2]');
                        await frequentClick.click();
                        steps += 'Most frequent tab clicked, ';
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div/div'), 2000));

                        let engagingClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[1]/div/div/div/div[1]');
                        await engagingClick.click();
                        steps += 'Most engaging tab clicked';
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[1]/div[1]/div/div'), 2000));

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': 'Success'
                            }
                        });
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Tracker does not have hashtag cloud data.'
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Could not load dashboard related topics.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();