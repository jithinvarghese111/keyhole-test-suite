/**
 * ### TEST CASE:
 * I-HT-Dashboard-T037
 *
 * ### TEST TITLE:
 * Selecting active tracker from dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to go to active tracker dashboard
 * User should also be able to select active tracker from 
 	left menu and goto selected tracker dashboard
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking active tracker from left side menu', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking tracker page exists', async () => {
                await page.redirectToHTDashboard();

                let checkTracker = await page.checkElementByCSS('.active-tracker');

                if (checkTracker) {
                	let clickTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                	await clickTracker.click();

                	let clickLabel = await page.findByCSS('input#sidebar__tracker-dropdown--check');
                	await driver.executeScript("arguments[0].setAttribute('style','display:block')", clickLabel);
                	await clickLabel.click();

                	let getCount = await page.findElementsByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > li:nth-child(2) > div > ul > li');
                	getCount = getCount.length;

                	let clickTrack = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > li:nth-child(2) > div > ul > li:nth-child(2) > a');
                	await clickTrack.click();

                	let getNewName = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong');
                	getNewName = await getNewName.getText();

                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'Success',
                            'New tracker name': getNewName
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No tracker available.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();