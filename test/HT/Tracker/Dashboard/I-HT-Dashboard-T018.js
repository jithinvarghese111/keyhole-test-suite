/**
 * ### TEST CASE:
 * I-HT-Dashboard-T018
 
 * ### TEST TITLE:
 * Embed Top Devices and Apps
 *
 * ### TEST SUMMARY:
 * User should be able to embed the top devices and apps on their own blog or website
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Embed Top Devices and Apps', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Embed the top devices and apps on their own blog or website', async () => {
                await page.redirectToDashboard();

                let test_result = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 5000));

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                let didLoad = await page.checkElementByCSS('.key-chartBox--sizeLarge.key-chartBox--topStats');
                await driver.sleep(10000);

                if (didLoad) {
                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[10]/div[1]');
                    await driver.sleep(3000);

                    let inputTracker = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1)');
                    await inputTracker.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[2]/button[1]'), 2000));

                    let embedClick = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.key-chartOptions > button:nth-child(2)');
                    await embedClick.click();
                    steps += 'Embed button clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[1]/h3'), 2000));

                    let modalOpen = await page.findElementsByCSS('#hashtag-tracking .key-modal.key-modal--dashboardEmbed.key-modal--show');
                    modalOpen = modalOpen.length;

                    if (modalOpen > 0)
                        steps += 'Modal opened, ';

                    let copySnippet = await page.findByCSS('.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--solidGreen.key-button--copy.copy--left');
                    await copySnippet.click();
                    steps += 'Copy snippet button clicked, ';
                    await driver.wait(until.elementLocated(By.id('dashboard__embed'), 2000));

                    let code = await page.findById('dashboard__embed');
                    code = await code.getText();
                    steps += 'URL highlighted and copied!';

                    if (code != "" && modalOpen > 0)
                        test_result = 'Test passed';

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Embed code': code,
                            "Test steps": steps,
                            'Test status': test_result
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Could not load dashboard top devices and apps.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();