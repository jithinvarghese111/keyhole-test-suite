/**
 * ### TEST CASE:
 * I-HT-Dashboard-T055
 
 * ### TEST TITLE:
 * Customize Dashboard Chart order
 *
 * ### TEST SUMMARY:
 * User should be able to Hide Top Posts, Related Topic, Sentiment, Location, Top Website, Gender, Top Devices and Apps, Most Linked Websites, Social Media Post Types, Influential Users Chart from HT Tracker 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Customize Top Posts Chart', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });
            

            it('Hide Top Posts Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideTopPost = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(1) > div > button');
                await hideTopPost.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkTopposts = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartHeading > h3 > span');
                if (!checkTopposts) {

                    addContext(this.ctx, 'Top Posts Chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Top Posts Chart Displayed');
                }
                            
            });

            it('Hide Related Topics Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideRelatedTopics = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(2) > div > button');
                await hideRelatedTopics.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkRelatedTopics = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topics > div.key-chartHeading');
                if (!checkRelatedTopics) {

                    addContext(this.ctx, 'Related Topics Chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Related Topics Chart Displayed');
                }
                            
            });

            it('Hide Sentiment Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideSentiment = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(3) > div > button');
                await hideSentiment.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkSentiment = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiments > div.key-chartHeading');
                if (!checkSentiment) {

                    addContext(this.ctx, 'Sentiment Chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Sentiment Chart Displayed');
                }
                            
            });

            it('Hide Location Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideLocation = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(4) > div > button');
                await hideLocation.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkLocation = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--locations > div.key-chartHeading');
                if (!checkLocation) {

                    addContext(this.ctx, 'Location Chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Location Chart Displayed');
                }
                            
            });

            it('Hide Top Websites Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideTopWebsite = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(5) > div > button');
                await hideTopWebsite.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkTopWebsite = await page.checkElementByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(5) > div > button');
                if (!checkTopWebsite) {

                    addContext(this.ctx, 'Top Website Chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Top Website Chart Displayed');
                }
                            
            });

            it('Hide Gender Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideGender = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(6) > div > button');
                await hideGender.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkGender = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--genders > div.key-chartHeading');
                if (!checkGender) {

                    addContext(this.ctx, 'Gender Chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Gender Chart Displayed');
                }
                            
            });

            it('Hide Top Devices and Apps Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideTopDevices = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(7) > div > button');
                await hideTopDevices.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkTopDevices = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartHeading');
                if (!checkTopDevices) {

                    addContext(this.ctx, 'Top Devices and Apps Chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Top Devices and Apps Chart Displayed');
                }
                            
            });

            it('Hide Most Linked Websites Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideLinkedWeb = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(8) > div > button');
                await hideLinkedWeb.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkLinkedWeb = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartHeading');
                if (!checkLinkedWeb) {

                    addContext(this.ctx, 'Most Linked Website chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Most Linked Website chart Displayed');
                }
                            
            });

            it('Hide Social Media Post Types Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideSocialMedia = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(9) > div > button');
                await hideSocialMedia.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkSocialMedia = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postTypes > div.key-chartHeading');
                if (!checkSocialMedia) {

                    addContext(this.ctx, 'Social Media Post Types chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Social Media Post Types chart Displayed');
                }
                            
            });

            it('Hide Influential Users Chart', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let customize = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(5)');
                await customize.click();
                await driver.sleep(2000);

                let hideInfluential = await page.findByCSS('#hashtag-tracking > div > div.key-reactPortal.key-reactPortal--customizeSlideout > div > div:nth-child(3) > div:nth-child(10) > div > button');
                await hideInfluential.click();

                await page.scrollPixel(150);
                await driver.sleep(1000);

                let checkInfluential = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartHeading');
                if (!checkInfluential) {

                    addContext(this.ctx, 'Influential Users Chart is Currently hidden');

                }else{
                    addContext(this.ctx, 'Influential Users Chart Displayed');
                }
                            
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();