/**
 * ### TEST CASE:
 * I-HT-Dashboard-T044
 
 * ### TEST TITLE:
 * Interact with Top Websites That Mention News, Blogs, forums
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the Top Websites That Mention News, Blogs, forums of Hashtag tracking
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Top Websites That Mention News, Blogs, forums of Hashtag tracking', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with Top Websites That Mention News, Blogs, forums', async () => {
                await page.redirectToHTDashboard();

                let status = '';

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));
                
                await page.scrollPixel(200);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartHeading > h3 > span'), 20000));

                await page.scrollPixel(800);
                await driver.sleep(2000);       

                let totalwebsite = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencersByDomainRank > div.key-chartContainer > div.key-chart--influencersByDomainRank > div > div.key-listContent.key-listContent--domains > p > a');

                for (var i = 1; i < totalwebsite.length; i++) {
                    
                    let topwebsite = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[6]/div[2]/div[1]/div/div[2]/p['+i+']/a')
                    let websitename = await topwebsite.getText();

                    let rankvalue = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[6]/div[2]/div[1]/div/div[2]/p['+i+']/span');
                    rankvalue = await rankvalue.getText();
                    await topwebsite.click();

                    let tab1, tab2;

                    await driver.getAllWindowHandles().then(function(windowHandles) {
                        tab1 = windowHandles[0];
                        tab2 = windowHandles[1];                
                    });

                    await driver.switchTo().window(tab1);

                    addContext(this.ctx, 'Clicked Domain: '+websitename+'-->Rank Value: '+rankvalue);
                    
                }
                            
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();