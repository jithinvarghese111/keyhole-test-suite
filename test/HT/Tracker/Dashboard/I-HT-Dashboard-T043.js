/**
 * ### TEST CASE:
 * I-HT-Dashboard-T043
 *
 * ### TEST TITLE:
 * SubTracker dashboard SHARE button
 *
 * ### TEST SUMMARY:
 * User should be able to go to HT subTracker dashboard and click share button and share the SubTracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('SubTracker dashboard SHARE button', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to HT subTracker dashboard and click share button and share the subtracker', async () => {
                await page.redirectToHTDashboard();

                let checkTracker = await page.checkElementByCSS('.active-tracker');

                if (checkTracker) {
                	let clickTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                	await clickTracker.click();

                	let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                    let subTrackerName = await subTrack.getText();
                    await subTrack.click();
                    await driver.sleep(1000);

                    let list = await page.findByCSS('#react-select-2-option-0');
                    await list.click();
                    addContext(this.ctx, 'Navigated into subTracker');
                    await driver.sleep(2000);

                    let clickShare = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[3]');
                    await clickShare.click();
                    await driver.wait(until.elementLocated(By.id('dashboardShareModal__link'), 30000));

                    let onDate = await page.findByCSS('#key-toggle--linkSharingEditDateRange > div > label:nth-child(3)');
                    await onDate.click();

                    let onReports = await page.findByCSS('#key-toggle--linkSharingExportReports > div > label:nth-child(3)');
                    await onReports.click();

                    let onPrediction = await page.findByCSS('#key-toggle--linkSharingViewPredictions > div > label:nth-child(3)');
                    await onPrediction.click();

                    let inputEmail = await page.findByCSS('#linkSharingSendEmail');
                    await page.write(inputEmail, 'jithin@wdstech.com');

                    let send = await page.findByCSS('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div.key-modal.key-modal--dashboardShare.key-modal--kindaWide.key-modal--show > div.key-modal__wrapper > div.key-modal__body.key-modal__body--roundedCornersBottom > div:nth-child(3) > div > button');
                    await send.click();

                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'SubTracker details shared to specified email'
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No tracker available.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();