/**
 * ### TEST CASE:
 * I-HT-Dashboard-T041
 
 * ### TEST TITLE:
 * Downloading PDF of subTracker
 *
 * ### TEST SUMMARY:
 * User should be able to download a PDF file of subTracker information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading PDF of subTracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Downloading PDF of subTracker information locally', async () => {
                await page.redirectToHTDashboard();
                
                await page.scrollPage('//*[@id="hashtag-trackers"]/div[5]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr/td[2]/div[1]/a');
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                let subTrackerName = await subTrack.getText();
                await subTrack.click();
                await driver.sleep(1000);

                let list = await page.findByCSS('#react-select-2-option-0');
                await list.click();
                addContext(this.ctx, 'Navigated into subTracker');
                await driver.sleep(1000);

                let pdfLink = await page.findByCSS('#pdf');
                await pdfLink.click();
                // This process sometimes takes more than 30 seconds
                await driver.sleep(20000);
                // await driver.wait(until.elementLocated(By.css('body > div.key-notificationTray.visible > button'), 50000));

                let checkEle = await page.checkElementByCSS('body > div.key-notificationTray.visible > button');

                if (checkEle) {

                    let viewPdf = await page.findByCSS('body > div.key-notificationTray.visible > button');
                    await viewPdf.click();

                    let tab1, tab2;

                    await driver.getAllWindowHandles().then(function(windowHandles) {
                        tab1 = windowHandles[0];
                        tab2 = windowHandles[1];
                    });
                    await driver.switchTo().window(tab1);
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 30000));

                    addContext(this.ctx, {
                        title: 'other Context',
                        value: {
                            'Tracker name': subTrackerName,
                            'Test status': 'Download file'
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'other Context',
                        value: {
                            'Tracker name': subTrackerName,
                            'Test status': 'Error with download'
                        }
                    });
                }

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();