/**
 * ### TEST CASE:
 * I-HT-Dashboard-T024
 
 * ### TEST TITLE:
 * Interact with Sentiments
 *
 * ### TEST SUMMARY:
 *  User should be able to Navigate to the Sentiments page 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Sentiments', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Interact with Sentiments', async () => {
                await page.redirectToDashboard();

                let platform = '', steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[1]/div'), 2000));

                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[7]/div[2]'), 8000));
                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[7]/div[2]');
                await driver.sleep(5000);

                let sentimentBreakdown = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[7]/div[3]/a');
                let sentimentLink = await sentimentBreakdown.getAttribute("href");
                expect(sentimentLink).to.equal('https://keyhole.co/hashtag-tracking/sentiment/'+hash+'/'+trackerName+'');
                await sentimentBreakdown.click();
                steps += 'Clicked on Sentiment';
                await driver.wait(until.elementLocated(By.css('#trackerSentiment > div > h2 > strong'), 2000));
     
                let title = await driver.getTitle();
                expect(title).to.include(trackerName);

                var trackerSentiment = await page.checkElementByCSS('#trackerSentiment > div > h2 > strong');
                status += 'Sentiment Tracker available';
                await driver.wait(until.elementLocated(By.css('#trackerSentiment > div > h2 > strong'), 2000));

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();