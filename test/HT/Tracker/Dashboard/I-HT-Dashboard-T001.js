/**
 * ### TEST CASE:
 * I-HT-Dashboard-T001
 *
 * ### TEST TITLE:
 * Checking HT Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to go to tracker dashboard
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking HT Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking tracker page exists', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let secondTrack = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong');
                secondTrack = await secondTrack.getText();
                let actualTitle = await driver.getTitle();
                expect(actualTitle).to.include(trackerName);

                if (secondTrack == trackerName) {
                    status = 'Success';
                }
                else {
                    status = 'Page not found';
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test status': status
                    }
                });
            });

            it('Checking tracker page posts', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let secondTrack = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong');
                secondTrack = await secondTrack.getText();
                let actualTitle = await driver.getTitle();
                expect(actualTitle).to.include(trackerName);

                if (secondTrack == trackerName) {
                    let checkElement = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > div > div.key-chartEmptyState__title');

                    if (checkElement) {
                        let getText = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div/div[2]');
                        status = await getText.getText();
                    }
                    else {
                        status = 'Posts found'
                    }
                }
                else {
                    status = 'Page not found';
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();