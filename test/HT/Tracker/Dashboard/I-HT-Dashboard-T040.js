/**
 * ### TEST CASE:
 * I-HT-Dashboard-T040
 *
 * ### TEST TITLE:
 * Check Subtracker exists or not
 *
 * ### TEST SUMMARY:
 * User trying to to check HT tracker have Subtracker or not
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check Subtracker exists or not', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check Subtracker exists or not', async () => {
                await page.redirectToHTDashboard();
                
                await page.scrollPage('//*[@id="hashtag-trackers"]/div[5]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));
               
                let checkEle = await page.checkElementByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                if (checkEle) {
                    let allsubtracker = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/div[1]/div/div[1]/div[1]');
                    await allsubtracker.click();
                    await driver.wait(until.elementLocated(By.css('#react-select-2-option-0'), 20000));

                    let subTrackerName = await page.findByCSS('#react-select-2-option-0');
                    subTrackerName = await subTrackerName.getText();
                    addContext(this.ctx, 'Listed subTracker: '+subTrackerName);
                    await driver.sleep(2000);
                }else{
                    addContext(this.ctx, 'No subTracker Found');
                }
                               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();