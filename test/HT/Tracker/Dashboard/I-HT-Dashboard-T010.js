/**
 * ### TEST CASE:
 * I-HT-Dashboard-T010
 
 * ### TEST TITLE:
 * Download top posts as CSV
 *
 * ### TEST SUMMARY:
 * User should be allowed to download the top posts information in CSV format
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Download top posts as CSV', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Download the top post information in CSV format', async () => {
                await page.redirectToHTDashboard();

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));
                
                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let test_result = '';
                const days = 30;
                const toDate = new Date();
                const fromDate = new Date();
                fromDate.setDate(toDate.getDate() - days);
                const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let didLoad = await page.checkElementByCSS('#hashtag-tracking .key-chartBox--topPosts');
                if (didLoad) {
                    await page.scrollPixel(800);
                    await driver.sleep(3000);

                    let inputTracker = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div.el-tabs__item.is-active');
                    await inputTracker.click();
                    await driver.wait(until.elementLocated(By.css('#hashtag-tracking .key-chartBox--topPosts .key-chartContainer .key-chartOptions button:nth-child(1)'), 3000));

                    let csvClick = await page.findByCSS('#hashtag-tracking .key-chartBox--topPosts .key-chartContainer .key-chartOptions button:nth-child(1)');
                    await csvClick.click();
                    await driver.wait(until.elementLocated(By.css('#hashtag-tracking .key-chartBox--topPosts .key-chartContainer .key-chartOptions button:nth-child(1)'), 3000));

                    const downloadPath = config.downloadPath+'\/'+'Keyhole_'+trackerName+'('+hash+')_'+fromDateStr+'-'+toDateStr+'_top_tweets.csv';
                    const filename = `Keyhole_${trackerName}(${hash})_topPosts_${new Date().getTime()}.csv`;
                    const renamePath = config.downloadPath+'\/'+filename;

                    console.log(downloadPath);
                    console.log(renamePath);

                    if (fs.existsSync(downloadPath)) {
                        fs.renameSync(downloadPath, renamePath)
                        
                        if (fs.existsSync(renamePath)) {
                            const stats = fs.statSync(renamePath)

                            if (parseFloat(stats.size) > 64) {
                                test_result = "Downloaded file";
                                passed = true
                            } else {
                                test_result = "Downloaded empty file... but that's what happens on env other than prod";
                                passed = true
                            }
                        }
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': test_result
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. Tracker does not have posts.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();