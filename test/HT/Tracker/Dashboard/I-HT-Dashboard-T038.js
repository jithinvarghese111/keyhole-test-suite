/**
 * ### TEST CASE:
 * I-HT-Dashboard-T038
 *
 * ### TEST TITLE:
 * Tracker dashboard SHARE button
 *
 * ### TEST SUMMARY:
 * User should be able to go to HT dashboard and click share button and share the tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Tracker dashboard SHARE button', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to HT dashboard and click share button and share the tracker', async () => {
                await page.redirectToHTDashboard();

                let checkTracker = await page.checkElementByCSS('.active-tracker');

                if (checkTracker) {
                	let clickTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                	await clickTracker.click();

                	let clickShare = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[3]');
                    await clickShare.click();
                    await driver.wait(until.elementLocated(By.id('dashboardShareModal__link'), 30000));

                    let onDate = await page.findByCSS('#key-toggle--linkSharingEditDateRange > div > label:nth-child(3)');
                    await onDate.click();

                    let onReports = await page.findByCSS('#key-toggle--linkSharingExportReports > div > label:nth-child(3)');
                    await onReports.click();

                    let onPrediction = await page.findByCSS('#key-toggle--linkSharingViewPredictions > div > label:nth-child(3)');
                    await onPrediction.click();

                    let inputEmail = await page.findByCSS('#linkSharingSendEmail');
                    await page.write(inputEmail, 'jithin@wdstech.com');

                    let send = await page.findByCSS('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div.key-modal.key-modal--dashboardShare.key-modal--kindaWide.key-modal--show > div.key-modal__wrapper > div.key-modal__body.key-modal__body--roundedCornersBottom > div:nth-child(3) > div > button');
                    await send.click();

                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'Tracker details shared to specified email'
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No tracker available.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();