/**
 * ### TEST CASE:
 * I-HT-Dashboard-T003
 
 * ### TEST TITLE:
 * Downloading PDF
 *
 * ### TEST SUMMARY:
 * User should be able to download a PDF file of hashtag tracking information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading PDF', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Download a PDF file of HT information locally', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let pdfLink = await page.findByCSS('#hashtag-tracking #pdf');
                await pdfLink.click();
                // This process sometimes takes more than 30 seconds
                await driver.wait(until.elementLocated(By.css('.key-notificationTray.visible'), 20000));

                let checkEle = await page.checkElementByCSS('.key-notificationTray.visible');

                if (checkEle) {
                    let viewPdf = await page.findByXPath('/html/body/div[13]/button');
                    await viewPdf.click();

                    let tab1, tab2;

                    await driver.getAllWindowHandles().then(function(windowHandles) {
                        tab1 = windowHandles[0];
                        tab2 = windowHandles[1];
                    });
                    await driver.switchTo().window(tab1);
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                    addContext(this.ctx, {
                        title: 'other Context',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Download file'
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'other Context',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Error with download'
                        }
                    });
                }

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();