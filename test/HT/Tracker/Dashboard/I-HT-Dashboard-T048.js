/**
 * ### TEST CASE:
 * I-HT-Dashboard-T048
 
 * ### TEST TITLE:
 * Interact with Sentiment from SubTracker Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the Influential Users from SubTracker Dashboard
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Sentiment from SubTracker Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact Sentiment from SubTracker Dashboard', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));
                
                let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                await subTrack.click();
                await driver.sleep(1000);

                let list = await page.findByCSS('#react-select-2-option-0');
                let subTrackerName = await list.getText();
                await list.click();
                addContext(this.ctx, 'clicked on subTracker: '+subTrackerName);
                await driver.sleep(3000);

                let checkPost = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div');

                if (checkPost) {

                    await page.scrollPixel(1000);
                    await driver.sleep(3000);
  
                    let sentiment = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer');
                    await sentiment.click();

                    
                    let totalSentiment = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span > span:nth-child(2)');
                    for (var i = 1; i <= totalSentiment.length; i++) {
                        let sentimentClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[2]/span['+i+']/span[1]');
                        let sentimentName = await sentimentClick.getText();
                        await sentimentClick.click();
                        await sentimentClick.click();

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'SubTracker name': subTrackerName,
                                'Test status': 'Clicked on: '+sentimentName
                            }
                        });
                    }

                }
                else{
                    addContext(this.ctx, 'Unable to perform test. subTracker does not have posts.');
                }
                            
            });

            it('Embed the Sentiment from SubTracker Dashboard', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));
                
                let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                await subTrack.click();
                await driver.sleep(1000);

                let list = await page.findByCSS('#react-select-2-option-0');
                let subTrackerName = await list.getText();
                await list.click();
                addContext(this.ctx, 'clicked on subTracker: '+subTrackerName);
                await driver.sleep(3000);

                let checkPost = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div');

                if (checkPost) {

                    await page.scrollPixel(900);
                    await driver.sleep(3000);
  
                    let sentiment = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer');
                    await sentiment.click();

                    let embedClick = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer > div.key-chartOptions > button');
                    await embedClick.click();
                    steps += 'Embed button clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[1]/h3/span'), 3000));

                    let modalOpen = await page.findElementsByCSS('#hashtag-tracking .key-modal.key-modal--dashboardEmbed.key-modal--show');
                    modalOpen = modalOpen.length;

                    if (modalOpen > 0)
                        steps += 'Modal opened, ';

                    let copySnippet = await page.findByCSS('.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--solidGreen.key-button--copy.copy--left');
                    await copySnippet.click();
                    steps += 'Copy snippet button clicked, ';
                    await driver.wait(until.elementLocated(By.id('dashboard__embed'), 3000));

                    let code = await page.findById('dashboard__embed');
                    let codeName = await code.getText();
                    let params = codeName.substring(codeName.indexOf("_")+1);
                    let firstParam = params.substr(0, params.indexOf('?'));
                    let secondParm = firstParam.replace(/^.{1}/g, firstParam[0].toUpperCase());

                    assert.equal(secondParm, subTrackerName);
                    test_result = 'URL with subTracker highlighted and copied!';

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'SubTracker name': subTrackerName,
                            'Embed code': codeName,
                            "Test steps": steps,
                            'Test status': test_result
                        }
                    });

                }
                else{
                    addContext(this.ctx, 'Unable to perform test. subTracker does not have posts.');
                }
                            
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();