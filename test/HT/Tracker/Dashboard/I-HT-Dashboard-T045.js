/**
 * ### TEST CASE:
 * I-HT-Dashboard-T045
 
 * ### TEST TITLE:
 * Time Range persists to URL
 *
 * ### TEST SUMMARY:
 * When selecting a Time Range in the date dropdown, the date should be persisted in the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() { 
    try {
        describe('Time Range persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Time Range persists to URL', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '', status = '';

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.sleep(4000);

                const allowedDays = [1, 7, 30]
                const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                let changeDate = await setDateRangeNew(days)

                if (changeDate) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Selected date period': days,
                            'Test status': 'Success'
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Date selection failed'
                        }
                    });
                }
            
                let currentURL = await driver.getCurrentUrl();
                status += 'currentURL :'+currentURL;
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.lastIndexOf('='));
    
                if (firstParam.indexOf('days') > -1 && firstParam.indexOf(days)) {
                    steps += '. Date added to URL., ';
                }

                await driver.navigate().refresh();
                await driver.sleep(2000);

                if (firstParam.indexOf('days') > -1 && firstParam.indexOf(days)) {
                    status += ', Date persisted upon reload.';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
 
 