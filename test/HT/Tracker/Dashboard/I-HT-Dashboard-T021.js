/**
 * ### TEST CASE:
 * I-HT-Dashboard-T021
 
 * ### TEST TITLE:
 * Embed Social Media Post Types
 *
 * ### TEST SUMMARY:
 * User should be able to embed the Social Media Post Types on their own blog or website
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Embed Social Media Post Types', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Embed the Social Media Post Types on their own blog or website', async () => {
                await page.redirectToDashboard();

                let test_result = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 5000));

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);                

                let didLoad = await page.checkElementByCSS('.key-chartBox--sizeLarge.key-chartBox--topStats');

                if (didLoad) {
                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[12]/div[1]');
                    await driver.sleep(2000);

                    let inputTracker = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer');
                    await inputTracker.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[12]/div[2]/div[3]/button[2]/span'), 2000));

                    let embedClick = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-chartOptions > button:nth-child(2)');
                    await embedClick.click();
                    steps += 'Embed button clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[1]/h3'), 2000));

                    let modalOpen = await page.findElementsByCSS('#hashtag-tracking .key-modal.key-modal--dashboardEmbed.key-modal--show');
                    modalOpen = modalOpen.length;

                    if (modalOpen > 0)
                        steps += 'Modal opened, ';

                    let copySnippet = await page.findByCSS('.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--solidGreen.key-button--copy.copy--left');
                    await copySnippet.click();
                    steps += 'Copy snippet button clicked, ';
                    await driver.wait(until.elementLocated(By.id('dashboard__embed'), 2000));

                    let code = await page.findById('dashboard__embed');
                    code = await code.getText();
                    steps += 'URL highlighted and copied!';

                    if (code != "" && modalOpen > 0)
                        test_result = 'Test passed';

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Embed code': code,
                            "Test steps": steps,
                            'Test status': test_result
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Could not load dashboard Social Media Post Types.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();