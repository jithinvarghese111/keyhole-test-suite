/**
 * ### TEST CASE:
 * I-HT-Dashboard-T042
 
 * ### TEST TITLE:
 * Downloading XLS of SubTracker
 *
 * ### TEST SUMMARY:
 * User should be able to download an excel file of subTracker information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading XLS of SubTracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Download a Downloading XLS of SubTracker information locally', async () => {
                await page.redirectToHTDashboard();

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[5]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr/td[2]/div[1]/a'), 2000));

                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)');
                hash = await hash.getAttribute("data-track");

                let test_result = '';
                const toDate = new Date();    
                const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                
                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[6]/div[2]/table/tbody/tr/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                let subTrackerName = await subTrack.getText();
                await subTrack.click();
                await driver.sleep(1000);

                let list = await page.findByCSS('#react-select-2-option-0');
                await list.click();
                addContext(this.ctx, 'Navigated into subTracker');
                await driver.sleep(2000);

                let date = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-dateBox > span > div > input');
                await date.click();
                await driver.sleep(3000);

                let allTime = await page.findByXPath('/html/body/div[13]/div/div/div[1]/button[7]');
                await allTime.click();
                await driver.sleep(2000);

                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.indexOf('&'));
                let secondParam = firstParam.substring(firstParam.indexOf("=")+1);
                const dateStr = secondParam.replace(/-/g, '');

                let xlsLink = await page.findByCSS('#update');
                await xlsLink.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                const downloadPath = config.downloadPath+'\/'+'Keyhole_'+trackerName+'_'+subTrackerName +'('+hash+')_'+dateStr+'-'+toDateStr+'.xls';
                const filename = `Keyhole_${trackerName}_${subTrackerName   }(${hash})_${new Date().getTime()}.xls`;
                const renamePath = config.downloadPath+'\/'+filename;

                if (fs.existsSync(downloadPath)) {
                    fs.renameSync(downloadPath, renamePath)
                    
                    if (fs.existsSync(renamePath)) {
                        const stats = fs.statSync(renamePath)

                        if (parseFloat(stats.size) > 64) {
                            test_result = "Downloaded file";
                            passed = true
                        } else {
                            test_result = "Downloaded empty file... but that's what happens on env other than prod";
                            passed = true
                        }
                    }
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': subTrackerName,
                        'Downloade path': downloadPath,
                        'Renamed path': renamePath,
                        'Test status': test_result
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();