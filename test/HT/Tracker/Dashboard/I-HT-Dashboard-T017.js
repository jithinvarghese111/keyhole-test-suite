/**
 * ### TEST CASE:
 * I-HT-Dashboard-T017
 
 * ### TEST TITLE:
 * Interact with Top Devices and Apps
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the top devices and apps section of Hashtag tracking
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Top Devices and Apps', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with the top devices and app section of Hashtag tracking', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let didLoad = await page.checkElementByCSS('.key-chartBox--sizeLarge.key-chartBox--topStats');
                await driver.sleep(10000);

                if (didLoad) {
                    let getPost = await page.checkElementByCSS('.key-chartEmptyState');

                    if (getPost) {
                        const allowedDays = [365];
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)];

                        let changeDate = await setDateRangeNew(days);
                        await driver.sleep(5000);

                        let getPost1 = await page.checkElementByCSS('.key-chartEmptyState');

                        if (getPost1) {
                            status = 0;
                        }
                        else {
                            status = 1;
                        }
                    }
                    else {
                        status = 1;
                    }

                    if (status) {
                        let steps = '';
                        let hideHeader = await page.findById('header-container');
                        await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                        await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[10]/div[1]');
                        await driver.sleep(3000);

                        let breakDownClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[10]/div[2]/div[1]/div[1]/div/div/div/div[2]');
                        await breakDownClick.click();
                        steps += 'Full device breakdown tab clicked, ';
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[10]/div[2]/div[1]/div[2]/div[2]/div/div[1]/span[1]'), 5000));

                        let summaryClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[10]/div[2]/div[1]/div[1]/div/div/div/div[1]');
                        await summaryClick.click();
                        steps += 'Summary tab clicked';
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[10]/div[2]/div[1]/div[2]/div[1]/div[1]/div'), 5000));

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': 'Success'
                            }
                        });
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Tracker does not have hashtag cloud data.'
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Could not load dashboard top devices and apps.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();