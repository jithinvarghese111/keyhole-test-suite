/**
 * ### TEST CASE:
 * I-HT-Dashboard-T036
 *
 * ### TEST TITLE:
 * Interact with Timeline
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the timeline section of Hashtag tracking
 * 
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Timeline', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with Timeline', async () => {
        
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let checkPost = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > div > div.key-chartEmptyState__title');
                if (!checkPost) {

                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[1]/div');
                    await driver.sleep(3000);

                    const actions = driver.actions({bridge: true}); 
                    let eleme = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--timeline.key-chartBox--timelineDefault > div.key-chartContainer > div.echarts-for-react.key-chart--timeline > div:nth-child(1) > canvas'); 
                    await actions.move({duration:5000,origin:eleme,x:35,y:20}).perform();
                    await eleme.click();
                    await driver.sleep(10000);

                    status = 'Post found';
                }
                else{
                    status = 'No posts Found';
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test status': status
                    }
                });
            });

            

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();