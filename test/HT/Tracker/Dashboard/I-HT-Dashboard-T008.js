/**
 * ### TEST CASE:
 * I-HT-Dashboard-T008
 
 * ### TEST TITLE:
 * Interact with Top Posts
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the top posts section of Hashtag tracking
 * User should also be able to visit the top posters twitter profile and scroll the top posts
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Top Posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with the top posts section of Hashtag tracking', async () => {
                await page.redirectToHTDashboard();

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));
                
                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let didLoad = await page.checkElementByCSS('#hashtag-tracking .key-chartBox--topPosts');
                await driver.wait(until.elementLocated(By.css('.key-topStats__statNumber'), 8000));

                if (didLoad) {
                    let getPost = await page.findElementsByCSS('.key-topStats__statNumber');
                    getPost = getPost.length;

                    if (!getPost) {
                        const allowedDays = [365];
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)];

                        let changeDate = await setDateRangeNew(days);
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 5000));

                        let getPostAgain = await page.findElementsByCSS('.key-topStats__statNumber');
                        getPostAgain = getPostAgain.length;

                        if (!getPostAgain) {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Unable to perform test. Tracker does not have posts.'
                                }
                            });
                        }
                        else {
                            let checkPlatform = await page.findElementsByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox');

                            let arr = [];
                            for (let i = 1; i <= checkPlatform.length; i++) {
                                let getPlatform = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/div/div[1]/form/label['+i+']');
                                let platformName = await getPlatform.getAttribute("title");

                                arr.push({ platformName });
                            }

                            await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[1]');
                            await driver.sleep(2000);

                            let firstPost = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]');
                            await firstPost.click();
                           
                            let tab1, tab2;

                            await driver.getAllWindowHandles().then(function(windowHandles) {
                                tab1 = windowHandles[0];
                                tab2 = windowHandles[1];
                            });

                            await driver.switchTo().window(tab2);
                            await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[1]/h3/span'), 5000));

                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Top post page visited'
                                }
                            });
                        }
                    }
                    else {
                        let checkPlatform = await page.findElementsByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox');

                        let arr = [];
                        for (let i = 1; i <= checkPlatform.length; i++) {
                            let getPlatform = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/div/div[1]/form/label['+i+']');
                            let platformName = await getPlatform.getAttribute("title");

                            arr.push({ platformName });
                        }

                        await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[1]');
                        await driver.sleep(2000);

                        let firstPost = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]');
                        await firstPost.click();
                  
                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab1);
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[1]/h3/span'), 5000));

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Top post page visited'
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Could not load dashboard top posts.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();