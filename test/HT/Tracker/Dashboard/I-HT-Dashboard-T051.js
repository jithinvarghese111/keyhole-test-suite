/**
 * ### TEST CASE:
 * I-HT-Dashboard-T051
 
 * ### TEST TITLE:
 * Interact with each graph tab section and check enable and disable the same
 *
 * ### TEST SUMMARY:
 * User should be able to interact with each graph tab section and check enable and disable the same
 *
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with each graph tab section and check enable and disable the same', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });
            
            it('Interact with Sentiment', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[7]');
                await driver.sleep(1000);

                let clickPositive = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(1)');
                await clickPositive.click();
                await driver.sleep(1000);

                let positiveCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(1).key-customLegend__item--hidden');
                if (positiveCheck) addContext(this.ctx, 'Positive tab disable success');
                else addContext(this.ctx, 'Positive tab disable failed');

                let clickNeutral = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(2)');
                await clickNeutral.click();
                await driver.sleep(1000);

                let neutralCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(2).key-customLegend__item--hidden');
                if (neutralCheck) addContext(this.ctx, 'Neutral tab disable success');
                else addContext(this.ctx, 'Neutral tab disable failed');

                let clickNegative = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(3)');
                await clickNegative.click();
                await driver.sleep(1000);

                let negativeCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--sentiment > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(3).key-customLegend__item--hidden');
                if (negativeCheck) addContext(this.ctx, 'Negative tab disable success');
                else addContext(this.ctx, 'Negative tab disable failed');
            });

            it('Interact with Gender', async () => {
                await page.redirectToHTDashboard();

                let test_result = '', steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[9]');
                await driver.sleep(1000);

                let clickMale = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--gender > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(1)');
                await clickMale.click();
                await driver.sleep(1000);

                let maleCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--gender > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(1).key-customLegend__item--hidden');
                if (maleCheck) addContext(this.ctx, 'Male tab disable success');
                else addContext(this.ctx, 'Male tab disable failed');

                let clickFemale = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--gender > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(2)');
                await clickFemale.click();
                await driver.sleep(1000);

                let femaleCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--gender > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(2).key-customLegend__item--hidden');
                if (femaleCheck) addContext(this.ctx, 'Female tab disable success');
                else addContext(this.ctx, 'Female tab disable failed');
            });

            it('Interact with Top Devices and Apps', async () => {
                await page.redirectToHTDashboard();

                let test_result = '', steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[9]');
                await driver.sleep(1000);

                let clickIphone = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-customLegend.key-customLegend--wide > span:nth-child(1)');
                await clickIphone.click();
                await driver.sleep(1000);

                let iphoneCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-customLegend.key-customLegend--wide > span:nth-child(1).key-customLegend__item--hidden');
                if (iphoneCheck) addContext(this.ctx, 'iPhone tab disable success');
                else addContext(this.ctx, 'iPhone tab disable failed');

                let clickAndroid = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-customLegend.key-customLegend--wide > span:nth-child(2)');
                await clickAndroid.click();
                await driver.sleep(1000);

                let androidCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-customLegend.key-customLegend--wide > span:nth-child(2).key-customLegend__item--hidden');
                if (androidCheck) addContext(this.ctx, 'Android tab disable success');
                else addContext(this.ctx, 'Android tab disable failed');

                let clickDesktop = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-customLegend.key-customLegend--wide > span:nth-child(3)');
                await clickDesktop.click();
                await driver.sleep(1000);

                let desktopCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-customLegend.key-customLegend--wide > span:nth-child(3).key-customLegend__item--hidden');
                if (desktopCheck) addContext(this.ctx, 'Desktop tab disable success');
                else addContext(this.ctx, 'Desktop tab disable failed');

                let clickOther = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-customLegend.key-customLegend--wide > span:nth-child(4)');
                await clickOther.click();
                await driver.sleep(1000);

                let otherCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--devices > div.key-chartContainer.key-chartContainer--tabOnEdge > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-customLegend.key-customLegend--wide > span:nth-child(4).key-customLegend__item--hidden');
                if (otherCheck) addContext(this.ctx, 'Other tab disable success');
                else addContext(this.ctx, 'Other tab disable failed');
            });

            it('Interact with Social Media Post Types', async () => {
                await page.redirectToHTDashboard();

                let test_result = '', steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[12]');
                await driver.sleep(1000);

                let clickOriginal = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(1)');
                await clickOriginal.click();
                await driver.sleep(1000);

                let originalCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(1).key-customLegend__item--hidden');
                if (originalCheck) addContext(this.ctx, 'Original tab disable success');
                else addContext(this.ctx, 'Original tab disable failed');

                let clickReply = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(2)');
                await clickReply.click();
                await driver.sleep(1000);

                let replyCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(2).key-customLegend__item--hidden');
                if (replyCheck) addContext(this.ctx, 'Reply tab disable success');
                else addContext(this.ctx, 'Reply tab disable failed');

                let clickRetweet = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(3)');
                await clickRetweet.click();
                await driver.sleep(1000);

                let retweetCheck = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend.key-customLegend--wide > span:nth-child(3).key-customLegend__item--hidden');
                if (retweetCheck) addContext(this.ctx, 'Retweet tab disable success');
                else addContext(this.ctx, 'Retweet tab disable failed');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();