/**
 * ### TEST CASE:
 * I-HT-Dashboard-T009
 
 * ### TEST TITLE:
 * Embed Top Posts
 *
 * ### TEST SUMMARY:
 * User should be able to embed the top posts on their own blog or website
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Embed Top Posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Embed the top posts on their own blog or website', async () => {
                await page.redirectToHTDashboard();

                let test_result = '', steps = '';

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));
                
                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 10000));

                let didLoad = await page.checkElementByCSS('#hashtag-tracking .key-chartBox--topPosts');

                if (didLoad) {

                    await page.scrollPixel(800);
                    await driver.sleep(3000);

                    let inputTracker = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div.el-tabs__item.is-active');
                    await inputTracker.click();
                    await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.key-chartOptions > button:nth-child(2)'), 3000));

                    let embedClick = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.key-chartOptions > button:nth-child(2)');
                    await embedClick.click();
                    steps += 'Embed button clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[1]/h3'), 3000));

                    let modalOpen = await page.findElementsByCSS('#hashtag-tracking .key-modal.key-modal--dashboardEmbed.key-modal--show');
                    modalOpen = modalOpen.length;

                    if (modalOpen > 0)
                        steps += 'Modal opened, ';

                    let copySnippet = await page.findByCSS('.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--solidGreen.key-button--copy.copy--left');
                    await copySnippet.click();
                    steps += 'Copy snippet button clicked, ';
                    await driver.wait(until.elementLocated(By.id('dashboard__embed'), 3000));

                    let code = await page.findById('dashboard__embed');
                    code = await code.getText();
                    steps += 'URL highlighted and copied!';

                    if (code != "" && modalOpen > 0)
                        test_result = 'Test passed';

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Embed code': code,
                            "Test steps": steps,
                            'Test status': test_result
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. Tracker does not have posts.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();