/**
 * ### TEST CASE:
 * I-HT-Dashboard-T020
 
 * ### TEST TITLE:
 * Interact with Social Media Post Types
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the Social Media Post Types section of Hashtag tracking
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Social Media Post Types', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with the Social Media Post Types section of Hashtag tracking', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 5000));

                let didLoad = await page.checkElementByCSS('.key-chartBox--sizeLarge.key-chartBox--topStats');
                await driver.sleep(10000);

                if (didLoad) {
                    let getPost = await page.checkElementByCSS('.key-chartEmptyState');

                    if (getPost) {
                        const allowedDays = [365];
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)];

                        let changeDate = await setDateRangeNew(days);
                        await driver.sleep(5000);

                        let getPost1 = await page.checkElementByCSS('.key-chartEmptyState');

                        if (getPost1) {
                            status = 0;
                        }
                        else {
                            status = 1;
                        }
                    }
                    else {
                        status = 1;
                    }

                    if (status) {
                        let steps = '';
                        let hideHeader = await page.findById('header-container');
                        await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);                
                        await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[12]/div[1]');
                        await driver.sleep(2000);

                        let originalClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[12]/div[2]/div[2]/span[1]');
                        await originalClick.click();
                        steps += 'Original button clicked, ';
                        await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend > span.key-customLegend__item.key-customLegend__item--hidden'), 2000));

                        let replyClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[12]/div[2]/div[2]/span[2]');
                        await replyClick.click();
                        steps += 'Reply button clicked, ';
                        await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend > span:nth-child(2)'), 2000));

                        let retweetClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[12]/div[2]/div[2]/span[3]');
                        await retweetClick.click();
                        steps += 'Retweet button clicked';
                        await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--pieChart.key-chartBox--postType > div.key-chartContainer > div.key-customLegend > span:nth-child(3)'), 2000));

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': 'Success'
                            }
                        });
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Tracker does not have hashtag cloud data.'
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Could not load dashboard Social Media Post Types.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();