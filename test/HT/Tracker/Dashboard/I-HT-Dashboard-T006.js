/**
 * ### TEST CASE:
 * I-HT-Dashboard-T006
 
 * ### TEST TITLE:
 * Embed Graph
 *
 * ### TEST SUMMARY:
 * User should be able to embed the timeline graph on their own blog or website
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Embed Graph', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking embed the timeline graph', async () => {
                await page.redirectToHTDashboard();

                let test_result = '',steps = '';
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let inputTracker = await page.findByCSS('.key-chartBox--timeline .key-chartContainer');
                await driver.actions({bridge: true}).sendKeys(webdriver.Key.ARROW_DOWN).click(inputTracker).sendKeys(webdriver.Key.ARROW_UP).perform();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[2]/div[2]/div[3]/button[2]/span'), 2000));

                let embedClick = await page.findByCSS('#hashtag-tracking .key-chartBox .key-chartOptions button:nth-child(2)');
                await embedClick.click();
                steps += 'Embed button clicked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[1]/h3'), 2000));

                let modalOpen = await page.findElementsByCSS('#hashtag-tracking .key-modal.key-modal--dashboardEmbed.key-modal--show');
                modalOpen = modalOpen.length;

                if (modalOpen > 0)
                    steps += 'Modal opened, ';

                let copySnippet = await page.findByCSS('.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--solidGreen.key-button--copy.copy--left');
                await copySnippet.click();
                steps += 'Copy snippet button clicked, ';
                await driver.wait(until.elementLocated(By.id('dashboard__embed'), 2000));

                let code = await page.findById('dashboard__embed');
                code = await code.getText();
                steps += 'URL highlighted and copied!';

                if (code != "" && modalOpen > 0)
                    test_result = 'Test passed';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Embed code': code,
                        "Test steps": steps,
                        'Test status': test_result
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();