/**
 * ### TEST CASE:
 * I-HT-TrendingTopics-005
 
 * ### TEST TITLE:
 * Checking all the posts
 *
 * ### TEST SUMMARY:
 * User should be able to checking all the posts
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking all the posts ', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking all the Trending Topics posts', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 4000));

                let url = await driver.getCurrentUrl();
                expect(url).to.equal(hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.sleep(4000);

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="activity-over-time"]/div[3]/p/strong'), 6000));

                let checkEle = await page.checkElementByCSS('#posts-table > div > div > div > table > tbody > tr:nth-child(1) > td.key-post__user.user');

                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. No posts found in the selected date range.'
                        }
                    });
                }
                else{

                    let totalPost = await page.findElementsByCSS('#posts-table > div > div > div > table > tbody > tr > td.key-post__postText');
                    status += ' Total Post Displayed :' +totalPost.length;
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            it('Checking with Load more posts', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 4000));

                let url = await driver.getCurrentUrl();
                expect(url).to.equal(hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div');
                await driver.sleep(5000);

                let checkEle = await page.checkElementByCSS('#posts-table > div > div > div > table > tbody > tr:nth-child(1) > td.key-post__user.user');

                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. No posts found in the selected date range.'
                        }
                    });
                }
                else{

                    await page.scrollPage('#posts-table > button');
                    await driver.wait(until.elementLocated(By.css('#posts-table > button'), 3000));

                    for(i= 1; i <= 4; i++) {
                        
                        let checkLoadPost = await page.checkElementByCSS('#posts-table > button');
                        if(checkLoadPost){
                            let loadPost = await page.findByXPath('//*[@id="posts-table"]/button');
                            await loadPost.click();                        
                            await driver.wait(until.elementLocated(By.xpath('//*[@id="posts-table"]/button'), 3000));                            
                        }
                        else{    
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Load more post button no found.'
                                }
                            });
                        }                    
                    }

                    let totalPost = await page.findElementsByCSS('#posts-table > div > div > div > table > tbody > tr > td.key-post__postText');
                    status += 'Total Post after clicked on Load more Post button:' +totalPost.length ;                         
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }                                  
            });

            it('Checking Trending Topics posts Sorting', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 4000));

                let url = await driver.getCurrentUrl();
                expect(url).to.equal(hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));

                await page.scrollPage('//*[@id="activity-over-time"]/div[3]');
                await driver.sleep(5000);

                let checkEle = await page.checkElementByCSS('#posts-table > div > div > div > table > tbody > tr:nth-child(1) > td.key-post__user.user');

                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. No posts found in the selected date range.'
                        }
                    });
                }
                else{

                    let userSort = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/thead/tr/th[1]');
                    let uName = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[1]/span/span[1]');
                    let userName = await uName.getText();
                    await userSort.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[1]/span/span[1]'), 3000));

                    let useraftrSort = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/thead/tr/th[1]');
                    let secondUserName = await useraftrSort.getText();
                    assert.notEqual(secondUserName, userName);
                    status += 'Sorted with user Name';

                    let postSort = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/thead/tr/th[2]');
                    let pname = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[2]');
                    let postName = await pname.getText();
                    await postSort.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[1]/span/span[1]'), 3000));

                    let postaftrSort = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[2]');
                    let secondPostName = await postaftrSort.getText();
                    assert.notEqual(secondPostName, postName);
                    status += ' ,Sorted with Trending topic Posts';

                    let engSort = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/thead/tr/th[3]');
                    let engname = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[3]');
                    let engName = await engname.getText();
                    await engSort.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[1]/span/span[1]'), 3000));

                    let engaftrSort = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[3]');
                    let secondEngName = await engaftrSort.getText();
                    assert.notEqual(secondEngName, engName);
                    status += ' ,Sorted with Engagement';

                    let dateSort = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/thead/tr/th[5]');
                    let datename = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[5]');
                    let dateName = await datename.getText();
                    await dateSort.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[1]/span/span[1]'), 3000));

                    let dateaftrSort = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[5]');
                    let secondDateName = await dateaftrSort.getText();
                    assert.notEqual(secondDateName, dateName);
                    status += ' ,Sorted with Date.';
                     
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
