/**
 * ### TEST CASE:
 * I-HT-TrendingTopics-003
 
 * ### TEST TITLE:
 *  Trending Topics Timeline Checking
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the Trending Topics Timeline
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Trending Topics Timeline', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Trending Topics Timeline', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '', topicsname = '', subTopicsname = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 4000));

                let url = await driver.getCurrentUrl();
                expect(url).to.equal(hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 4000));              

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div');
                await driver.sleep(10000);

                let checkEle = await page.checkElementByCSS('#posts-table > div > div > div > table > thead > tr > th.key-table__sortToggle.keyjs-table__sortToggle.keyjs-post__user');

                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. No posts found in the selected date range.'
                        }
                    });
                }
                else{

                    let totalTopics = await page.findElementsByCSS('#key-topKeywords-table > div > div > div > table > tbody > tr > td.key-topKeyword__keyword.key-keyword__word');
                    totalTopics = await totalTopics.length;
                    status += 'Total topics: '+totalTopics;

                    let rising = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div[1]/figure/div/div/div/div/div[1]/div/div/div/div[2]');
                    await rising.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-risingKeywords-table"]/div/div/div/table/tbody/tr[1]/td[2]/span'), 3000));

                    let totalRisingTopics = await page.findElementsByCSS('#key-risingKeywords-table > div > div > div > table > tbody > tr > td.key-risingKeyword__keyword.key-keyword__word');
                    totalRisingTopics = await totalRisingTopics.length;
                    status += ' ,Total Rising topics: '+totalRisingTopics;
                    await driver.sleep(2000);

                    let addsubTopic = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div/div/button/span');
                    await addsubTopic.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-topKeywords-table"]/div/div/div/table/tbody/tr[1]/td[2]'), 3000));

                    let totalSubTopics = await page.findElementsByCSS('#key-topKeywords-table > div > div > div > table > tbody > tr > td.key-topKeyword__keyword.key-keyword__word');
                    totalSubTopics = await totalSubTopics.length - 1;
                    status += ' ,Total SubTopics: '+totalSubTopics;

                    let subrising = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div[2]/figure/div/div/div/div/div[1]/div/div/div/div[2]');
                    await subrising.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-risingKeywords-table"]/div/div/div/table/tbody/tr[1]/td[2]'), 6000));

                    let totalrisingSubTopics = await page.findElementsByCSS('#key-risingKeywords-table > div > div > div > table > tbody > tr > td.key-risingKeyword__keyword.key-keyword__word');
                    totalrisingSubTopics = await totalrisingSubTopics.length - 1;
                    status += ' ,Total Rising SubTopics: '+totalrisingSubTopics;

                    let topics = await page.findByXPath('//*[@id="key-risingKeywords-table"]/div/div/div/table/tbody/tr[4]');
                    let topicName = await topics.getText();
                    await topics.click();
                    topicsname = 'Clicked topic: '+topicName;
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-risingKeywords-table"]/div/div/div/table/tbody/tr[4]/td[2]'), 8000));

                    let subTopics = await page.findByXPath('//*[@id="key-risingKeywords-table"]/div/div/div/table/tbody/tr[2]/td[2]');
                    let subTopicName = await subTopics.getText();
                    await subTopics.click();
                    subTopicsname = 'Clicked subTopic: '+subTopicName;
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-risingKeywords-table"]/div/div/div/table/tbody/tr[1]'), 10000));

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status,
                            'Topics Name': topicsname,
                            'Sub Topics': subTopicsname
                        }
                    });        
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
