/**
 * ### TEST CASE:
 * I-HT-TrendingTopics-T011
 
 * ### TEST TITLE:
 * Trending topics post filter using POST TEXT, ENGAGEMENTS and DATE
 *
 * ### TEST SUMMARY:
 * Checking trending topics post filter using POST TEXT, ENGAGEMENTS and DATE
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Trending topics post filter using POST TEXT, ENGAGEMENTS and DATE', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking trending topics post filter using POST TEXT, ENGAGEMENTS and DATE', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));
                await driver.sleep(10000);

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                await page.scrollPage('//*[@id="posts-table"]');
                await driver.sleep(3000);

                let clickText = await page.findByCSS('#posts-table > div > div > div > table > thead > tr > th.key-table__sortToggle.keyjs-table__sortToggle.keyjs-post__postText');
                await clickText.click();
                await driver.sleep(1000);
                let checkText = await page.checkElementByCSS('#posts-table > div > div > div > table > tbody > tr:nth-child(1) > td.key-post__postText.key-table__column--sortedBy');
                if (checkText) addContext(this.ctx, 'Post sorted by POST TEXT success');
                else addContext(this.ctx, 'Post sorted by POST TEXT failed');

                let clickEng = await page.findByCSS('#posts-table > div > div > div > table > thead > tr > th.key-table__sortToggle.keyjs-table__sortToggle.keyjs-post__engagements');
                await clickEng.click();
                await driver.sleep(1000);
                let checkEng = await page.checkElementByCSS('#posts-table > div > div > div > table > tbody > tr:nth-child(1) > td.key-post__engagements.number.key-table__column--sortedBy');
                if (checkEng) addContext(this.ctx, 'Post sorted by ENGAGEMENTS success');
                else addContext(this.ctx, 'Post sorted by ENGAGEMENTS failed');

                let clickDate = await page.findByCSS('#posts-table > div > div > div > table > thead > tr > th.key-table__sortToggle.keyjs-table__sortToggle.keyjs-post__date');
                await clickDate.click();
                await driver.sleep(1000);
                let checkDate = await page.checkElementByCSS('#posts-table > div > div > div > table > tbody > tr:nth-child(1) > td.key-post__date.date.key-table__column--sortedBy');
                if (checkDate) addContext(this.ctx, 'Post sorted by DATE success');
                else addContext(this.ctx, 'Post sorted by DATE failed');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();