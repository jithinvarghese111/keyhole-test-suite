/**
 * ### TEST CASE:
 * I-HT-TrendingTopics-T013
 
 * ### TEST TITLE:
 * Check top topics appending to mention over time
 *
 * ### TEST SUMMARY:
 * Checking trending topics top keywords appending to mention over time
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking trending topics top keywords appending to mention over time', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check top topics appending to mention over time', async () => {
                await page.redirectToDashboard();

                await driver.sleep(3000);

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                await hashtag.click();
                await driver.sleep(3000);

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(3000);

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[4]/div[2]/table/tbody/tr/td[2]/div[1]/a');
                await firstTrack.click();
                await driver.sleep(3000);

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                await driver.sleep(5000);

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.sleep(5000);

                let clickFirst = await page.findByCSS('#key-topKeywords-table > div > div > div > table > tbody > tr:nth-child(2) > td.key-topKeyword__keyword.key-keyword__word');
                let firstText = await clickFirst.getText();
                addContext(this.ctx, 'Selected Text: '+firstText);
                await clickFirst.click();
                await driver.sleep(3000);

                let addedText = await page.findByCSS('#activity-over-time > div.key-chartHeading > h3 > strong');
                addedText = await addedText.getText();
                addContext(this.ctx, 'Added Text: '+addedText);

                assert.equal(firstText, addedText);
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();