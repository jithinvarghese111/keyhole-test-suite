/**
 * ### TEST CASE:
 * I-HT-TrendingTopics-007
 
 * ### TEST TITLE:
 * Preset date persists to URL
 *
 * ### TEST SUMMARY:
 * When selecting a preset date in the date dropdown, the preset should be persisted in the URL
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Preset date persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Preset date persists to URL', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                expect(url).to.equal(hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));

                let pickDate = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/div/div[2]/span/div');
                await pickDate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[13]/div'), 3000));

                let pageClick = await randomNumberFromTo(1,4);
        
                let startdate = await page.findByXPath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[2]/td['+pageClick+']');
                await startdate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[2]/td['+pageClick+']'), 3000));

                let pageClicks = await randomNumberFromTo(1,5);

                let enddate = await page.findByXPath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[6]/td['+pageClicks+']');
                await enddate.click();
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.lastIndexOf('='));

                if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end') > -1){
                    let status = '';

                    status += 'Preset date added to URL., ';

                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                    if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end') > -1) {
                        status += 'Preset date persisted upon reload.';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Current URL': currentURL,
                            'Test status': status
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Preset date not added to URL.'
                        }
                    });
                }                
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
