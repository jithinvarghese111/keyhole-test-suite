/**
 * ### TEST CASE:
 * I-HT-TrendingTopics-T009
 
 * ### TEST TITLE:
 * Topics persist to Mention Over Time
 *
 * ### TEST SUMMARY:
 * When selecting a Topic, check Mention Over Time is changed or not
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Topics persist to Mention Over Time', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('After selecting Top Topic, check Mention Over Time is changed or not', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));

                let topCount = await page.findElementsByCSS('#key-topKeywords-table > div > div > div > table > tbody > tr.key-topKeyword__row');
                topCount = topCount.length;

                if (topCount > 5) topCount = 5;

                for (let i = 0; i <= topCount; i++) {
                	let singleClick = await page.findByXPath('//*[@id="key-topKeywords-table"]/div/div/div/table/tbody/tr['+i+']');
                	let topKeyword = await page.findByXPath('//*[@id="key-topKeywords-table"]/div/div/div/table/tbody/tr['+i+']/td[2]/span');
                	topKeyword = await topKeyword.getText();
                	await singleClick.click();
                	await driver.sleep(5000);

                	let mentionKeyword = await page.findByXPath('//*[@id="activity-over-time"]/div[1]/h3/strong');
                	mentionKeyword = await mentionKeyword.getText();

                	addContext(this.ctx, 'Top keyword clicked #'+i+': '+topKeyword);

                	if (topKeyword == mentionKeyword) addContext(this.ctx, 'Status #'+i+': Matched');
                	else addContext(this.ctx, 'Status #'+i+': Not Matched');
                }
            });

            it('After selecting Rising Topic, check Mention Over Time is changed or not', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));

                let risingClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div/figure/div/div/div/div/div[1]/div/div/div/div[2]');
                await risingClick.click();

                let risingCount = await page.findElementsByCSS('#key-risingKeywords-table > div > div > div > table > tbody > tr');
                risingCount = risingCount.length;

                if (risingCount > 5) risingCount = 5;

                for (let i = 0; i <= risingCount; i++) {
                	let singleClick = await page.findByXPath('//*[@id="key-risingKeywords-table"]/div/div/div/table/tbody/tr['+i+']');
                	let topKeyword = await page.findByXPath('//*[@id="key-risingKeywords-table"]/div/div/div/table/tbody/tr['+i+']/td[2]/span');
                	topKeyword = await topKeyword.getText();
                	await singleClick.click();
                	await driver.sleep(5000);

                	let mentionKeyword = await page.findByXPath('//*[@id="activity-over-time"]/div[1]/h3/strong');
                	mentionKeyword = await mentionKeyword.getText();

                	addContext(this.ctx, 'Rising keyword clicked #'+i+': '+topKeyword);

                	if (topKeyword == mentionKeyword) addContext(this.ctx, 'Status #'+i+': Matched');
                	else addContext(this.ctx, 'Status #'+i+': Not Matched');
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();