/**
 * ### TEST CASE:
 * I-HT-TrendingTopics-T011
 
 * ### TEST TITLE:
 * Add subtopic button check
 *
 * ### TEST SUMMARY:
 * Checking add subtopic button, on click top and rising selection from subtopic is changed or not
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add subtopic button check', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking add subtopic button, on click top and rising selection', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let trendingTopics = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[4]/a/span');
                await trendingTopics.click();
                steps += 'Navigated to Trending Topics';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));
                await driver.sleep(10000);

                let buttonClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div/div/button');
                await buttonClick.click();
                await driver.sleep(5000);

                let checkAdded = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > div.key-keywordActivityContainer.key-chartContainer.key-trendingTopics__layer2Shown > section > div:nth-child(2)');

                if (checkAdded) {
                	addContext(this.ctx, 'Subtopic section added');

                	let checkContent = await page.findElementsByCSS('#key-risingKeywords-table > div > div > div > table > tbody > tr:nth-child(2) > td.key-risingKeyword__keyword.key-keyword__word > span');

                	if (checkContent) {
	                	let countTop = await page.findElementsByCSS('#key-topKeywords-table > div > div > div > table > tbody > tr');
	                	countTop = countTop.length - 1;

	                	addContext(this.ctx, 'Total top keyword: '+countTop);

	                	let clickRising = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div[2]/figure/div/div/div/div/div[1]/div/div/div/div[2]');
	                	await clickRising.click();

	                	let countRising = await page.findElementsByCSS('#key-risingKeywords-table > div > div > div > table > tbody > tr');
	                	countRising = countRising.length - 1;

	                	addContext(this.ctx, 'Total rising keyword: '+countRising);

	                	let clickTop = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/div[1]/section/div[2]/figure/div/div/div/div/div[1]/div/div/div/div[1]');
	                	await clickTop.click();
	                	await driver.sleep(10000);

	                	if (countTop > 5) countTop = 5;

	                	for (let i = 2; i <= countTop; i++) {
	                		let topKey = await page.findByCSS('.key-keywordSelector__table--2 > div #key-topKeywords-table > div > div > div > table > tbody > tr:nth-child('+i+') > td.key-topKeyword__keyword.key-keyword__word > span');
	                		topKey = await topKey.getText();
	                		addContext(this.ctx, 'Top keyword from subtopics '+i+': '+topKey);

	                		let clickTopKey = await page.findByCSS('.key-keywordSelector__table--2 > div #key-topKeywords-table > div > div > div > table > tbody > tr:nth-child('+i+')');
	                		await clickTopKey.click();
	                		await driver.sleep(5000);

	                		let getMention = await page.findByXPath('//*[@id="activity-over-time"]/div[1]/h3/strong[2]');
	                		getMention = await getMention.getText();
	                		addContext(this.ctx, 'Mention overtime keyword '+i+': '+getMention);

	                		if (getMention == topKey) addContext(this.ctx, '#'+i+': Keywords are same');
	                		else addContext(this.ctx, '#'+i+': Keywords are not same');

	                		await driver.sleep(5000);

	                		assert.equal(topKey, getMention);
	                	}
	                }
	                else addContext(this.ctx, 'Content not loaded');
                }
                else addContext(this.ctx, 'Subtopic not added');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();