/**
 * ### TEST CASE:
 * I-HT-Influencers-T017
 
 * ### TEST TITLE:
 * Influencers per page pagination
 *
 * ### TEST SUMMARY:
 * User is trying to select influencers per page dropdown count and checking the counts are changed or not
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Influencers per page pagination', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking influencers per page pagination', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.sleep(8000);

                let checkElement = await page.checkElementByCSS('#influencersTable > table > tbody > tr');
                
                if (checkElement) {
                    let totalPosts = await page.findByCSS('#hashtag-tracking > div > section.route-section > div > div > div.key-paginationContainer > div.key-pagination__info > strong.key-pagination__total');
                    totalPosts = await totalPosts.getText();
                    totalPosts = totalPosts.replace (/,/g, "");

                    if (totalPosts > 10) {
                        let totalBefore = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                        totalBefore = totalBefore.length;
                        addContext(this.ctx, 'Total infleuncers list before filter: '+totalBefore);

                        await page.scrollPage('//*[@id="influencersTable"]/table/tbody/tr[25]');
                        await driver.sleep(2000);

                        let clickDrop = await page.findByCSS('#hashtag-tracking > div > section.route-section > div > div > div.key-paginationContainer > div.key-pagination__perPage > select');
                        await clickDrop.click();
                        await driver.sleep(1000);

                        let clickOption = await page.findByCSS('#hashtag-tracking > div > section.route-section > div > div > div.key-paginationContainer > div.key-pagination__perPage > select > option:nth-child(1)');
                        let clickText = await clickOption.getText();
                        addContext(this.ctx, 'Selected influencers list per page: '+clickText);
                        await clickOption.click();
                        await driver.sleep(1000);

                        let totalAfter = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                        totalAfter = totalAfter.length;
                        addContext(this.ctx, 'Total influencers list after filter: '+totalAfter);

                        assert.equal(totalAfter, clickText);

                        if (totalAfter == clickText)
                            addContext(this.ctx, 'Influencers per page selection success');
                        else
                            addContext(this.ctx, 'Influencers per page selection failed');
                    }
                    else
                        assert.fail('Not enough influencers for the selected tracker');
                }
                else assert.fail('No post found');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();