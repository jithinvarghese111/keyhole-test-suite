/**
 * ### TEST CASE:
 * I-HT-Influencers-003
 
 * ### TEST TITLE:
 * Simple search
 *
 * ### TEST SUMMARY:
 * User should be able to do a simple search (the `Filter Accounts and Bios` search box)
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Simple search', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Simple search', async () => {
                await page.redirectToHTDashboard();

                let steps = '', status = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.sleep(2000);

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let searchAccount = await page.findByCSS('#simpleSearch');
                await page.write(searchAccount, 'am');
                steps += 'search with Account name, ';

                let searchKey = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters.key-trackerSubHeader__filters--thirdWidth.keyjs-influencers__filters > form > button');
                await searchKey.click();
                await driver.sleep(5000);

                let searchResult = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                searchResult = searchResult.length;
                status += 'Total Search Results with user name :' +searchResult;
                await driver.sleep(1000);

                let searchBio = await page.findByCSS('#simpleSearch');
                await page.clearFields('css', '#simpleSearch');
                await page.write(searchBio, 'love');
                await searchKey.click();
                steps += 'search with Bio, ';

                let searchBios = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                searchBios = searchBios.length;
                status += ' ,Total Search Results with bio :' +searchBios;
                await driver.sleep(5000);

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();