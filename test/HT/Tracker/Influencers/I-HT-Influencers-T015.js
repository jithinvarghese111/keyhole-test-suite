/**
 * ### TEST CASE:
 * I-HT-Influencers-T015
 
 * ### TEST TITLE:
 * Navigate to the Influencers page, click on each posts Start Tracking button to track the keyword
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Influencers page, click on each post Start Tracking button to track the keyword
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to the Influencers page, click on each posts Start Tracking button', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on each post Start Tracking button', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.sleep(8000);

                let checkElement = await page.checkElementByCSS('#influencersTable > table > tbody > tr');
                
                if (checkElement) {
                    let totalRows = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                    totalRows = totalRows.length;

                    if (totalRows > 0) {
                        if (totalRows > 5)
                            totalRows = 5;

                        let trackerName1 = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--user > span > span.key-influencers__linkWrapper > span.key-table__userInfoUsername');
                        trackerName1 = await trackerName1.getText();

                        let startTracking = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--user > span > span.key-influencers__linkWrapper > span.key-influencers__startTracking');
                        steps += 'Clicked start tracking link';
                        await startTracking.click();
                        await driver.sleep(2000);

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);

                        let url = await driver.getCurrentUrl();

                        let status = '';

                        if (url == 'https://keyhole.co/account-tracking/preview') status = 'Passed';
                        else status = 'failed';

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Selected tracker name': trackerName1,
                                'Start tracker URL': url,
                                'Test steps': steps,
                                'Test status': status
                            }
                        });
                    }
                    else addContext(this.ctx, 'No post found');
                }
                else addContext(this.ctx, 'No post found');
            });

            it('Click on each post Start Tracking button from the modal', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));
              
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.sleep(8000);

                let checkElement = await page.checkElementByCSS('#influencersTable > table > tbody > tr');
                
                if (checkElement) {
                    let totalRows = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                    totalRows = totalRows.length;

                    if (totalRows > 0) {
                        if (totalRows > 5)
                            totalRows = 5;

                        let clickPost = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child(1)');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let trackerName1 = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__user > div.key-userDetails > span.key-influencers__linkWrapper.key-influencers__linkWrapper--small > a');
                        trackerName1 = await trackerName1.getText();

                        let startTracking = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__user > div.key-userDetails > span.key-influencers__linkWrapper.key-influencers__linkWrapper--small > span');
                        steps += 'Clicked start tracking link';
                        await startTracking.click();
                        await driver.sleep(2000);

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);

                        let url = await driver.getCurrentUrl();

                        let status = '';

                        if (url == 'https://keyhole.co/account-tracking/preview') status = 'Passed';
                        else status = 'failed';

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Selected tracker name': trackerName1,
                                'Start tracker URL': url,
                                'Test steps': steps,
                                'Test status': status
                            }
                        });
                    }
                    else addContext(this.ctx, 'No post found');
                }
                else addContext(this.ctx, 'No post found');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();