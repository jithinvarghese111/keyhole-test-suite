/**
 * ### TEST CASE:
 * I-HT-Influencers-T013
 
 * ### TEST TITLE:
 * Navigate to the Influencers page, check posts and click view on social buttons
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Influencers page, check posts and click view on social buttons from modal
 *
 */

 const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to the Influencers page, check posts and click view on social buttons', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking navigate to the Influencers page, check posts and click view on social buttons', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));
              
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));
                
                let totalRows = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                totalRows = totalRows.length;

                if (totalRows > 0) {
                	let postClick = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr[1]');
                	await postClick.click();
                	await driver.sleep(5000);

                	let modalClick = await page.findByCSS('#influencersTableSideview > table > tbody > tr');
                	await modalClick.click();
                	await driver.sleep(5000);

                	let checkElement = await page.checkElementByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__post.key-sideviewPost__post--noSplit > div > a');

                	let button;

                	if (checkElement) {
                		button = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__post.key-sideviewPost__post--noSplit > div > a');
                	}
                	else {
                		button = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__post.key-sideviewPost__post--horizontalSplit > div.key-sideviewPost__postText > a');
                	}

                	let urlBefore = await button.getAttribute("href");
                	await button.click();

                	addContext(this.ctx, 'URL before click: '+urlBefore);

                	let tab1, tab2;

	                await driver.getAllWindowHandles().then(function(windowHandles) {
	                    tab1 = windowHandles[0];
	                    tab2 = windowHandles[1];                
	                });

	                await driver.switchTo().window(tab2);
	                let url = await driver.getCurrentUrl();

	                addContext(this.ctx, 'URL after click: '+url);
	                assert.equal(urlBefore, url);

	                if (urlBefore == url) addContext(this.ctx, 'URL same');
	                else addContext(this.ctx, 'URL not same');

                	await driver.sleep(5000);
                	
                }
                else addContext(this.ctx, 'No post found');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();