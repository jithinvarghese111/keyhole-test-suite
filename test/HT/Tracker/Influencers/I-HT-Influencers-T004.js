/**
 * ### TEST CASE:
 * /**
 * ### TEST CASE:
 * I-HT-Influencers-004
 
 * ### TEST TITLE:
 * Advanced search
 *
 * ### TEST SUMMARY:
 * Users should be able to do advanced search with the advanced dropdown as well as reset their search
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Advanced search', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Advanced search', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '', status = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.sleep(2000);

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let advancedSearch = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]');
                await advancedSearch.click();
                steps += 'Advance search clicked ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedFilters"]'), 6000));

                let searchBio = await page.findByXPath('//*[@id="bio"]');
                await page.write(searchBio, 'b');

                let searchPosts = await page.findByXPath('//*[@id="posts"]');
                await page.write(searchPosts, '1');

                let postGreaterthan = await page.findByXPath('//*[@id="advancedFilters"]/form/div[2]/div[1]/div/div/label[1]');
                await postGreaterthan.click();

                let searchFollowers = await page.findByXPath('//*[@id="followers"]');
                await page.write(searchFollowers, '3,70689');

                let postLessthan = await page.findByXPath('//*[@id="advancedFilters"]/form/div[3]/div[1]/div/div/label[2]');
                await postLessthan.click();

                let searchLocation = await page.findByXPath('//*[@id="location"]');
                await page.write(searchLocation, '');

                let searchEngagement = await page.findByXPath('//*[@id="engagement"]');
                await page.write(searchEngagement, '40');

                let postGreater = await page.findByXPath('//*[@id="advancedFilters"]/form/div[2]/div[1]/div/div/label[1]');
                await postGreater.click();

                let searchImpressions = await page.findByXPath('//*[@id="impressions"]');
                await page.write(searchImpressions, '3,706');

                let postLess = await page.findByXPath('//*[@id="advancedFilters"]/form/div[3]/div[2]/div/div/label[2]');
                await postLess.click();

                let submitButton = await page.findByXPath('//*[@id="advancedFilters"]/form/div[4]/button[1]/span');
                await submitButton.click();
                status += 'Displayed Advance search results, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedFilters"]'), 6000));

                let hideSearch = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]');
                await hideSearch.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="influencersTable"]/table/thead/tr/th[1]'), 3000));

                let results = await page.checkElementByCSS('#influencersTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--user > span > span.key-table__userInfoName');
                
                if(results) {
                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/div/div/div[2]/div[1]');
                    await driver.sleep(2000);
                    let result = await page.findByCSS('#hashtag-tracking > div > section.route-section > div > div > div.key-paginationContainer > div.key-pagination__info > strong.key-pagination__total');
                    let advanceResults = await result.getText();
                    status += 'Advance search results Count:'+advanceResults;
                }
                else {
                    let noResults = await page.findByCSS('#hashtag-tracking > div > section.route-section > div > div > div.key-pageEmptyState > div > div.key-chartEmptyState__title');
                    let emptyResult = await noResults.getText();
                    status += ', Advance Search Result:'+emptyResult;
                }
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
 
 