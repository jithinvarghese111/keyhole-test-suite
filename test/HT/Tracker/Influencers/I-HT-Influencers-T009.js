/**
 * ### TEST CASE:
 * /**
 * ### TEST CASE:
 * I-HT-Influencers-009
 
 * ### TEST TITLE:
 * Sort persists to URL
 *
 * ### TEST SUMMARY:
 * When doing sort, sorting should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sort persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Sort persists to URL', async () => {
                await page.redirectToHTDashboard();
                let searchName = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.sleep(2000);
    
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));
                
                let search = await page.findByCSS('#simpleSearch');
                await page.write(search, 'ab');               
                 
                let searchKey = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters.key-trackerSubHeader__filters--thirdWidth.keyjs-influencers__filters > form > button');
                await searchKey.click();
                steps += 'Typed search term and run search ';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));
  
                let postSort = await page.findByCSS('#influencersTable > table > thead > tr > th:nth-child(2)');
                await postSort.click();
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.lastIndexOf('='));
                                    
                if (firstParam.indexOf('search') > -1 && firstParam.indexOf('sort') > -1) {
                    let status = '';

                    status += 'Sort added to URL., ';

                    await driver.navigate().refresh();

                    if (firstParam.indexOf('search') > -1 && firstParam.indexOf('sort') > -1) {
                        status += 'Sort persisted upon reload.';
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Sort not added to URL.'
                        }
                    });
                }   
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
 
 