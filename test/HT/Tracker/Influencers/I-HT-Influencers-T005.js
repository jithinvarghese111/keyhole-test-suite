/**
 * ### TEST CASE:
 * /**
 * ### TEST CASE:
 * I-HT-Influencers-005
 
 * ### TEST TITLE:
 * Preset date persists to URL
 *
 * ### TEST SUMMARY:
 * When selecting a preset date in the date dropdown, the preset should be persisted in the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Preset date persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Preset date persists to URL', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '', status = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.sleep(2000);

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));
                
               let pickDate = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/div/div[2]/span/div');
                await pickDate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[13]/div/div/div[2]'), 3000));

                let startdate = await page.findByXPath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[2]/td[3]');
                await startdate.click();

                let enddate = await page.findByXPath('/html/body/div[13]/div/div/div[2]/div[1]/table/tbody/tr[6]/td[3]');
                await enddate.click();
                await driver.sleep(1000);

                let currentURL = await driver.getCurrentUrl();
                status += 'currentURL :'+currentURL;
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.lastIndexOf('='));
    
                if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end')) {
                    steps += '. Date added to URL., ';
                }

                await driver.navigate().refresh();
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end')) {
                    status += ', Date persisted upon reload.';
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
 
 