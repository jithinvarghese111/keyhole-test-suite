/**
 * ### TEST CASE:
 * I-HT-Influencers-T018
 
 * ### TEST TITLE:
 * Navigate to the Influencers page, check posts and retweets and comments counts.
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Influencers page, check posts and retweets and comments counts.
 *
 */

 const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to the Influencers page, check posts and retweets and comments counts.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigate to the Influencers page, check posts and retweets and comments counts.', async () => {
                await page.redirectToHTDashboard();

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                addContext(this.ctx, 'Navigated to the Influencers page');
                await driver.sleep(2000);
                
                let totalRows = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                totalRows = totalRows.length;

                if (totalRows > 0) {

                    let postClick = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr[1]');
                    await postClick.click();
                    await driver.sleep(2000);

                    let modalClick = await page.findByCSS('#influencersTableSideview > table > tbody > tr');
                    await modalClick.click();
                    await driver.sleep(5000);

                    let reTweet = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__statLine > div:nth-child(2) > span:nth-child(1) > span');
                    reTweet = await reTweet.getText();
                    addContext(this.ctx, 'retweets Counts: '+reTweet);

                    let comments = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__statLine > div:nth-child(2) > span:nth-child(2) > span');
                    comments = await comments.getText();
                    addContext(this.ctx, 'comments Counts: '+comments);

                    let closeModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-modalHeader > button');
                    await closeModal.click();
               
                }
                else addContext(this.ctx, 'No post found'); 
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();   