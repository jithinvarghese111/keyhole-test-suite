/**
 * ### TEST CASE:
 * /**
 * ### TEST CASE:
 * I-HT-Influencers-010
 
 * ### TEST TITLE:
 * Advanced persists to URL
 *
 * ### TEST SUMMARY:
 * When changing advanced filters, advanced state should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Advanced persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Advanced persists to URL', async () => {
                await page.redirectToHTDashboard();

                let searchName = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));
                
                let advanceSearch = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions.keyjs-influencers__actions > button.key-trackerSubHeaderAction__button.keyjs-influencersActions--advanced > span');
                await advanceSearch.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedFilters"]'), 3000));
                
                let searchBio = await page.findByXPath('//*[@id="bio"]');
                await page.write(searchBio, 'b');
                
                let postGreaterthan = await page.findByXPath('//*[@id="advancedFilters"]/form/div[2]/div[1]/div/div/label[1]');
                await postGreaterthan.click();
                
                let searchPosts = await page.findByXPath('//*[@id="posts"]');
                await page.write(searchPosts, '1');
                
                let postLessthan = await page.findByXPath('//*[@id="advancedFilters"]/form/div[3]/div[1]/div/div/label[2]');
                await postLessthan.click();
                
                let searchFollowers = await page.findByXPath('//*[@id="followers"]');
                await page.write(searchFollowers, '3,70689');
                
                let searchLocation = await page.findByXPath('//*[@id="location"]');
                await page.write(searchLocation, '');
                
                let postGreater = await page.findByXPath('//*[@id="advancedFilters"]/form/div[2]/div[1]/div/div/label[1]');
                await postGreater.click();

                let searchEngagement = await page.findByXPath('//*[@id="engagement"]');
                await page.write(searchEngagement, '40');

                let postLess = await page.findByXPath('//*[@id="advancedFilters"]/form/div[3]/div[2]/div/div/label[2]');
                await postLess.click();

                let searchImpressions = await page.findByXPath('//*[@id="impressions"]');
                await page.write(searchImpressions, '3,706');
                
                let submitButton = await page.findByXPath('//*[@id="advancedFilters"]/form/div[4]/button[1]/span');
                await submitButton.click();
                steps += 'Displayed Advance search results. ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedFilters"]'), 3000));

                let postSort = await page.findByCSS('#influencersTable > table > thead > tr > th:nth-child(2)');
                await postSort.click();
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.lastIndexOf('='));
                                    
                if (firstParam.indexOf('bio') > -1 && firstParam.indexOf('followers') > -1 && firstParam.indexOf('lesser') > -1 && firstParam.indexOf('greater') > -1 && firstParam.indexOf('posts') > -1 && firstParam.indexOf('impressions') > -1 && firstParam.indexOf('engagement') > -1 )  {
                    let status = '';

                    status += 'advance Search terms added to URL., ';
                    
                    await driver.navigate().refresh();

                    if (firstParam.indexOf('bio') > -1 && firstParam.indexOf('followers') > -1 && firstParam.indexOf('lesser') > -1 && firstParam.indexOf('greater') > -1 && firstParam.indexOf('posts') > -1 && firstParam.indexOf('impressions') > -1 && firstParam.indexOf('engagement') > -1 ) {
                        status += 'Advance State persisted upon reload.';
                    }
                    
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Advance State not added to URL.'
                        }
                    });
                }   
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
 
 