/**
 * ### TEST CASE:
 * I-HT-Influencers-T014
 
 * ### TEST TITLE:
 * Navigate to the Influencers page, click on each post and check values in modal header
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Influencers page, click on each post and check values in modal header
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to the Influencers page, click on each post and check values in modal header', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on each post and check values in modal header', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));
              
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.sleep(8000);
                
                let totalRows = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                totalRows = totalRows.length;

                if (totalRows > 0) {
                    if (totalRows > 5)
                        totalRows = 5;

                    for (let i = 1; i <= totalRows; i++) {
                        addContext(this.ctx, '-------------------------');

                    	let postClick = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr['+i+']');
                    	await postClick.click();
                    	await driver.sleep(5000);

                        let userName = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__user > div.key-userDetails > span:nth-child(1) > a');
                        userName = await userName.getText();
                        addContext(this.ctx, 'Username: '+userName);

                        let totalPosts = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(1) > span.key-modalHeader__stat--number');
                        totalPosts = await totalPosts.getText();
                        addContext(this.ctx, 'Total posts: '+totalPosts);

                        let avgEng = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(2) > span.key-modalHeader__stat--number');
                        avgEng = await avgEng.getText();
                        addContext(this.ctx, 'Average engagement: '+avgEng);

                        let followers = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(3) > span.key-modalHeader__stat--number');
                        followers = await followers.getText();
                        addContext(this.ctx, 'Followers: '+followers);

                        let impressions = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(4) > span.key-modalHeader__stat--number');
                        impressions = await impressions.getText();
                        addContext(this.ctx, 'Impressions: '+impressions);

                        let exposure = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(5) > span.key-modalHeader__stat--number');
                        exposure = await exposure.getText();
                        addContext(this.ctx, 'Exposure: '+exposure);

                        let closeModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else addContext(this.ctx, 'No post found');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();