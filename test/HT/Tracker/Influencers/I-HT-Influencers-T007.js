/**
 * ### TEST CASE:
 * /**
 * ### TEST CASE:
 * I-HT-Influencers-007
 
 * ### TEST TITLE:
 * Platform checkboxes persists to URL
 *
 * ### TEST SUMMARY:
 * When checking the platform checkboxes, the state should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Platform checkboxes persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Platform checkboxes persists to URL', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '', status = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));
        
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));
                
                let totalPlatform = await page.findElementsByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox');

                if (totalPlatform.length < 2) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Test requires multiple platforms.'
                        }
                    });
                }
                else {
                    for (let i = 1; i <= totalPlatform.length; i++) {
                        let platformName = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/div/div[1]/form/label[1]');
                        platformName = await platformName.getAttribute("title");

                        platform += platformName+', ';
                    }

                    if (platform.indexOf('Twitter') === -1) {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Test requires twitter.'
                            }
                        });
                    }
                    else {
                        let twtPlatform = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox--twitter > i');
                        await twtPlatform.click();
                        await driver.wait(until.elementLocated(By.id('twt_check'), 2000));

                        let currentURL = await driver.getCurrentUrl();
                        let params = currentURL.substring(currentURL.indexOf("?")+1);
                        let firstParam = params.substr(0, params.indexOf('&'));
                        
                        if (firstParam.indexOf('twt') > -1 && firstParam.indexOf('0') > -1) {
                            let status = '';

                            status += 'Platforms added to URL., ';

                            await driver.navigate().refresh();
                            await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                            if (firstParam.indexOf('twt') > -1 && firstParam.indexOf('0') > -1) {
                                status += 'Platforms persisted upon reload.';
                            }

                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': status
                                }
                            });
                        }
                        else {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Platforms not added to URL.'
                                }
                            });
                        }
                    }
                }
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
 
 