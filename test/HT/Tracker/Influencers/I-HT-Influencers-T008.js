/**
 * ### TEST CASE:
 * /**
 * ### TEST CASE:
 * I-HT-Influencers-008
 
 * ### TEST TITLE:
 * Search persists to URL
 *
 * ### TEST SUMMARY:
 * When doing search, the search keyword should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Search persists to URL', async () => {
                await page.redirectToHTDashboard();

                let searchName = '', steps = '', status = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));
                
                let search = await page.findByCSS('#simpleSearch');
                await page.write(search, 'am');
                                
                let serachKeyword = await page.findByXPath('//*[@id="simpleSearch"]');
                serachKeyword = await serachKeyword.getAttribute("value");
                searchName += serachKeyword+', '; 

                if (searchName.indexOf(serachKeyword) === -1) {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Test requires serachKeyword.'
                            }
                        });
                    }
                else{    
                    let searchKey = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters.key-trackerSubHeader__filters--thirdWidth.keyjs-influencers__filters > form > button');
                    await searchKey.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                    let currentURL = await driver.getCurrentUrl();
                    let params = currentURL.substring(currentURL.indexOf("?")+1);
                    let firstParam = params.substr(0, params.lastIndexOf('='));
                                        
                    if (firstParam.indexOf('search') > -1 && firstParam.indexOf('&') > -1) {
                        let status = '';

                        status += 'Search added to URL., ';

                        await driver.navigate().refresh();
                        await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                        if (firstParam.indexOf('search') > -1 && firstParam.indexOf('&') > -1) {
                            status += 'Search with keyword persisted upon reload.';
                        }

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': status

                            }
                        });
                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Search not added to URL.'
                            }
                        });
                    }
                }    
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
 
 