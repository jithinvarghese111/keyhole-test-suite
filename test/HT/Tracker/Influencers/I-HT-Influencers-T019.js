/**
 * ### TEST CASE:
 * I-HT-Influencers-T019
 *
 * ### TEST TITLE:
 * Checking user profile exists in Modal of Influencers.
 *
 * ### TEST SUMMARY:
 * User is trying to click on the user profile link from Modal of Influencers. and checking the user profile page is exists.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking user profile exists in Modal of Influencers.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigate to the Influencers page, Checking user profile exists in Modal', async () => {
                await page.redirectToHTDashboard();

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                addContext(this.ctx, 'Navigated to the Influencers page');
                await driver.sleep(2000);
                
                let totalCount = await page.findElementsByCSS('#influencersTable > table > tbody > tr');    
                    totalCount = totalCount.length;

                if(totalCount > 5)
                    (totalCount = 5)
                for (let i = 1; i <= totalCount; i++) {
                    addContext(this.ctx, '--------------------');

                    let username = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+') > td.key-table__column.key-table__column--user > span > span.key-table__userInfoName');
                    username = await username.getText();

                    let clickSingle = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+')');
                    await clickSingle.click();
                    await driver.sleep(2000);

                    let totalPost = await page.findElementsByCSS('#influencersTableSideview > table > tbody > tr');
                    addContext(this.ctx, 'Total posts in modal: '+totalPost.length);

                    let userModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__user > div.key-userDetails > span:nth-child(1) > a');
                    userModal = await userModal.getText();

                    assert.equal(userModal, username);
                    
                    let profileURL = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__user > div.key-userDetails > span:nth-child(1) > a');
                    let profileURLText = await profileURL.getAttribute("href");
                    addContext(this.ctx, 'Profile URL from Dashboard: '+profileURLText);
                    await profileURL.click();
                    await driver.sleep(1000);

                    let tab1, tab2;

                    await driver.getAllWindowHandles().then(function(windowHandles) {
                        tab1 = windowHandles[0];
                        tab2 = windowHandles[1];
                    });

                    await driver.switchTo().window(tab2);
                    let url = await driver.getCurrentUrl();
                    addContext(this.ctx, 'URL after click #: '+url);

                    assert.equal(url, profileURLText);

                    if (url === profileURLText) addContext(this.ctx, 'Both user links are same');
                    else addContext(this.ctx, 'User Links are not same');
                    await driver.close();
                    await driver.switchTo().window(tab1);

                    let closeModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > button');
                    await closeModal.click();
                    await driver.sleep(1000);
                }
           
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();