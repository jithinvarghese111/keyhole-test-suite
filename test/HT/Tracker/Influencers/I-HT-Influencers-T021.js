/**
 * ### TEST CASE:
 * I-HT-Influencers-T021
 
 * ### TEST TITLE:
 * Downloading CSV File
 *
 * ### TEST SUMMARY:
 * User should be able to download an csv file of Influencers hashtag tracker information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking of downloading CSV', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking of downloading CSV', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                let hash = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.trackerList__manageCheck > input[type=checkbox]');
                hash = await hash.getAttribute("value");
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper'), 3000));

                let influencers = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(3) > a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.sleep(2000);
                
                let selectDate = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-dateBox > span > div > input');
                await selectDate.click();
                await driver.sleep(2000);

                let selectWeek = await page.findByCSS('.el-picker-panel__sidebar > button:nth-child(2)');
                await selectWeek.click();
                await driver.sleep(5000);

                let test_result = '';
                const days = 6;
                const toDate = new Date();
                const fromDate = new Date();
                fromDate.setDate(toDate.getDate() - days);
                const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');

                let csv = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions.keyjs-influencers__actions > button.key-trackerSubHeaderAction__button.keyjs-influencersActions--export');
                await csv.click();
                await driver.sleep(10000);

                const downloadPath = config.downloadPath+'\/'+'Keyhole_'+trackerName+'('+hash+')_'+fromDateStr+'-'+toDateStr+'_influencers'+'.csv';
                const filename = `Keyhole_${trackerName}_(${hash})_${new Date().getTime()}.csv`;
                const renamePath = config.downloadPath+'\/'+filename;
                
                if (fs.existsSync(downloadPath)) {
                    fs.renameSync(downloadPath, renamePath)
                    
                    if (fs.existsSync(renamePath)) {
                        const stats = fs.statSync(renamePath)

                        if (parseFloat(stats.size) > 64) {
                            test_result = "Downloaded file";
                            passed = true
                        } else {
                            test_result = "Downloaded empty file.";
                            passed = true
                        }
                    }
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Downloade path': downloadPath,
                        'Renamed path': renamePath,
                        'Test status': test_result
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();