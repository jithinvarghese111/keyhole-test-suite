/**
 * ### TEST CASE:
 * I-HT-Influencers-T016
 
 * ### TEST TITLE:
 * Checking POSTS, AVG ENG, FOLLOWERS, IMPRESSIONS, EXPOSURE counts from table and modal are same
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Influencers page, click on each post and check various counts are same in the modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking POSTS, AVG ENG, FOLLOWERS, IMPRESSIONS, EXPOSURE counts from table and modal are same', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on each post and check various counts from table are same in modal', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.sleep(10000);

                let checkElement = await page.checkElementByCSS('#influencersTable > table > tbody > tr');
                
                if (checkElement) {
                    let totalRows = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                    totalRows = totalRows.length;

                    if (totalRows > 0) {
                        if (totalRows > 5)
                            totalRows = 5;

                        for (let i = 1; i <= totalRows; i++) {

                            addContext(this.ctx, '---------------'+i+'----------------');
                            let postBefore = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+') > td:nth-child(2)');
                            postBefore = await postBefore.getText();

                            let avgEngBefore = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+') > td.key-table__column.key-table__column--sortMetrics');
                            avgEngBefore = await avgEngBefore.getText();

                            let followersBefore = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+') > td:nth-child(4)');
                            followersBefore = await followersBefore.getText();

                            let impressBefore = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+') > td:nth-child(5)');
                            impressBefore = await impressBefore.getText();

                            let expoBefore = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+') > td:nth-child(6)');
                            expoBefore = await expoBefore.getText();

                            let clickPost = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+')');
                            await clickPost.click();
                            await driver.sleep(2000);

                            let postModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(1) > span.key-modalHeader__stat--number');
                            postModal = await postModal.getText();

                            let avgEngModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(2) > span.key-modalHeader__stat--number');
                            avgEngModal = await avgEngModal.getText();

                            let followersModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(3) > span.key-modalHeader__stat--number');
                            followersModal = await followersModal.getText();

                            let impressModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(4) > span.key-modalHeader__stat--number');
                            impressModal = await impressModal.getText();

                            let expoModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > div > div.key-modalHeader__stats > div:nth-child(5) > span.key-modalHeader__stat--number');
                            expoModal = await expoModal.getText();

                            let closeModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div > button');
                            await closeModal.click();
                            await driver.sleep(1000);

                            if (postBefore == postModal) addContext(this.ctx, 'POST counts are same');
                            else addContext(this.ctx, 'POST counts are not same');

                            if (avgEngBefore == avgEngModal) addContext(this.ctx, 'AVG ENG counts are same');
                            else addContext(this.ctx, 'AVG ENG counts are not same');

                            if (followersBefore == followersModal) addContext(this.ctx, 'FOLLOWERS counts are same');
                            else addContext(this.ctx, 'FOLLOWERS counts are not same');

                            if (impressBefore == impressModal) addContext(this.ctx, 'IMPRESSIONS counts are same');
                            else addContext(this.ctx, 'IMPRESSIONS counts are not same');

                            if (expoBefore == expoModal) addContext(this.ctx, 'EXPOSURE counts are same');
                            else addContext(this.ctx, 'EXPOSURE counts are not same');

                            assert.equal(postBefore, postModal);
                            assert.equal(avgEngBefore, avgEngModal);
                            assert.equal(followersBefore, followersModal);
                            assert.equal(impressBefore, impressModal);
                            assert.equal(expoBefore, expoModal);
                        }
                    }
                    else assert.fail('Not enough influencers for the selected tracker');
                }
                else assert.fail('No post found');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();