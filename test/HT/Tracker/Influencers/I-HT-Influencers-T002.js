/**
 * ### TEST CASE:
 * I-HT-Influencers-002
 
 * ### TEST TITLE:
 * Sort influencers
 *
 * ### TEST SUMMARY:
 * User should be able to sort influencers by posts, engagement, followers, impressions, and exposure
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sort influencers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Sort influencers', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.sleep(2000);

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let postSort = await page.findByXPath('//*[@id="influencersTable"]/table/thead/tr/th[2]');
                await postSort.click();
                steps += 'sort influencers by posts , ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]'), 5000));

                let firstUser = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]/span/span[1]');
                let postName = await firstUser.getText();
                steps += 'first User name after sorted by posts:,' +postName;
                                
                let engSort = await page.findByXPath('//*[@id="influencersTable"]/table/thead/tr/th[3]');
                await engSort.click();
                steps += 'sorted influencers by engagement , ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]'), 5000));

                let firstUserEng = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]/span/span[1]');
                let engName = await firstUserEng.getText();
                steps += 'first User name after sorted by Engagement:,' +engName;
                
                let followersSort = await page.findByXPath('//*[@id="influencersTable"]/table/thead/tr/th[4]');
                await followersSort.click();
                steps += ', sorted influencers by followers , ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]'), 5000));

                let firstUserFollowers = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]/span/span[1]');
                let followersName = await firstUserFollowers.getText();
                steps += ' ,first User name after sorted by follwers:,' +followersName;

                let impressionsSort = await page.findByXPath('//*[@id="influencersTable"]/table/thead/tr/th[5]');
                await impressionsSort.click();
                steps += ', sorted influencers by impressions , ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]'), 5000));

                let firstUserImpressions = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]/span/span[1]');
                let impressionsName = await firstUserImpressions.getText();
                steps += ' ,first User name after sorted by impressions:,' +impressionsName;
            
                let exposureSort = await page.findByXPath('//*[@id="influencersTable"]/table/thead/tr/th[6]');
                await exposureSort.click();
                steps += ', sorted influencers by impressions , ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]'), 5000));

                let firstUserExposure = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]/span/span[1]');
                let exposureName = await firstUserExposure.getText();
                steps += ' ,first User name after sorted by Exposure:,' +exposureName;
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': 'Success'
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();