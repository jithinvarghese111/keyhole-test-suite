/**
 * ### TEST CASE:
 * I-HT-Influencers-T012
 
 * ### TEST TITLE:
 * Navigate to the Influencers page and check posts
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Influencers page and check users having more than 1 post
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to the Influencers page and check posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Navigate to the Influencers page and check posts', async () => {
                await page.redirectToHTDashboard();

                let platform = '', steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));
                
                let totalRows = await page.findElementsByCSS('#influencersTable > table > tbody > tr');
                totalRows = totalRows.length;

                for (let i = 1; i <= totalRows; i++) {
                	let postCount = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+') > td:nth-child(2)');
                	postCount = await postCount.getText();
                	addContext(this.ctx, 'Post #'+i+': '+postCount);

                	if (postCount > 1) {
                		let checkPost = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr['+i+']/td[1]/span/span[1]')
                		checkPost = await checkPost.getText();
                		addContext(this.ctx, 'Clicked post username #'+i+': '+checkPost);

                		let postClick = await page.findByXPath('//*[@id="influencersTable"]/table/tbody/tr['+i+']');
                		await postClick.click();
                		await driver.sleep(1000);

                		let firstPostClick = await page.findByXPath('//*[@id="influencersTableSideview"]/table/tbody/tr[1]');
                		await firstPostClick.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[3]/div[2]/div/div/div/button[2]'), 30000));

                		for (let j = 1; j <= postCount - 1; j++) {
                			let nextClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/div[2]/div/div/div/button[2]');
                			await nextClick.click();
                			addContext(this.ctx, 'Next button clicked #'+i);
                		}

                		for (let k = 1; k <= postCount - 1; k++) {
                			let prevClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/div[2]/div/div/div/button[1]');
                			await prevClick.click();
                			addContext(this.ctx, 'Previous button clicked #'+i);
                		}

                		let closeSec = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/div[1]/button');
                		await closeSec.click();
                		await driver.sleep(1000);
                	}

                	await page.scrollPage('//*[@id="influencersTable"]/table/tbody/tr['+i+']');
                	await driver.sleep(1000);
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
