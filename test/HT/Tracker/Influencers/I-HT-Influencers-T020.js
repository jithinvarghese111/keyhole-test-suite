/**
 * ### TEST CASE:
 * I-HT-Influencers-T020
 *
 * ### TEST TITLE:
 * Click on each posts HT or AT Tracking link to track the keyword from Influencer Posts.
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Influencers, click on each posts HT or AT Tracking link to track the keyword.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Click on each posts HT or AT Tracking link to track the keyword from Influencer Posts.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigate to the Influencers page, Checking  HT or AT Tracking link to track the keyword.', async () => {
                await page.redirectToHTDashboard();

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));
                
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                addContext(this.ctx, 'Clicked on Tracker: '+trackerName);
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topStats > div > div:nth-child(1) > span.key-topStats__statNumber'), 20000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                addContext(this.ctx, 'Navigated to the Influencers page');
                
                let totalCount = await page.findElementsByCSS('#influencersTable > table > tbody > tr');    
                    totalCount = totalCount.length;

                if (totalCount > 0) {
                    if (totalCount > 5) totalCount = 5;
                for (let i = 1; i <= 5; i++) {
                    addContext(this.ctx, '--------------------');
                    
                    let clickSingle = await page.findByCSS('#influencersTable > table > tbody > tr:nth-child('+i+')');
                    await clickSingle.click();
                    await driver.sleep(1000);

                    let clickPosts = await page.findByCSS('#influencersTableSideview > table > tbody > tr');
                    await clickPosts.click();

                    let checkHT = await page.checkElementByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__post.key-sideviewPost__post--horizontalSplit > div.key-sideviewPost__postText > p > a');
                    if (checkHT == true) {
                        let startTracking = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__post.key-sideviewPost__post--horizontalSplit > div.key-sideviewPost__postText > p > a'); 
                        let htTrackerName = await startTracking.getText();    
                        addContext(this.ctx, 'Clicked HT tracking link '+htTrackerName);
                        await startTracking.click();

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);

                        let url = await driver.getCurrentUrl();

                        let status = '';

                        if (url == 'https://keyhole.co/preview') addContext(this.ctx, 'Passed');
                        else addContext(this.ctx, 'Failed');
                        await driver.close();
                        await driver.switchTo().window(tab1);
                    }
                    else addContext(this.ctx, 'No HT tacker Link Found');
                                              
                    let checkAT = await page.checkElementByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__post.key-sideviewPost__post--noSplit > div > p > a');
                    if (checkAT == true) {
                        let startTracking = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-sideviewInnerPost > article > section.key-sideviewPost__post.key-sideviewPost__post--noSplit > div > p > a'); 
                        let atTrackerName = await startTracking.getText();    
                        addContext(this.ctx, 'Clicked AT tracking link '+atTrackerName);
                        await startTracking.click();

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);

                        let url = await driver.getCurrentUrl();

                        let status = '';

                        if (url == 'https://keyhole.co/account-tracking/preview') addContext(this.ctx, 'Passed');
                        else addContext(this.ctx, 'Failed');

                        await driver.close();
                        await driver.switchTo().window(tab1);
                    }
                    else addContext(this.ctx, 'No AT tacker Link Found');
                        
                    let closeModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--influencerView.key-sideview--active > div.key-modalHeader > button');
                    await closeModal.click();
                    await driver.sleep(1000);
                }
            } else assert.fail('No Influencers Found.');

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();