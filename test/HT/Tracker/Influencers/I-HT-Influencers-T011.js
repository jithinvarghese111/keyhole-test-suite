/**
 * ### TEST CASE:
 * /**
 * ### TEST CASE:
 * I-HT-Influencers-011
 
 * ### TEST TITLE:
 * Page persists to URL
 *
 * ### TEST SUMMARY:
 * When changing pagination, page should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Page persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Page persists to URL', async () => {
                await page.redirectToHTDashboard();

                let searchName = '', steps = '', status = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 20000));
              
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));

                let influencers = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[3]/a');
                await influencers.click();
                steps += 'Navigated to the Influencers page, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 6000));
                
                await driver.wait(until.elementLocated(By.xpath('//*[@id="influencersTable"]/table/thead/tr/th[1]'), 10000));
                await page.scrollPixel(2100);
                await driver.sleep(3000);

                let pagination = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/div/div/div[2]/div[3]/button[4]');
                await pagination.click();
                steps += 'Working with pagination ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="influencersTable"]/table/tbody/tr[1]/td[1]'), 6000));

                let changePerPage = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > div > div > div.key-paginationContainer > div.key-pagination__perPage > select > option');                
                for(i= 1; i <= changePerPage.length; i++) {
                    let selectAll = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/div/div[1]/form/label['+i+']');
                    await selectAll.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 2000));                    
                }                
                status += 'Checked influencers per page';

                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.lastIndexOf('='));
                                    
                if (firstParam.indexOf('page') > -1 && firstParam.indexOf('&') > -1){
                    let status = '';

                    status += 'Page added to URL., ';

                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 6000));

                    if (firstParam.indexOf('page') > -1 && firstParam.indexOf('&') > -1){
                        status += 'Page persisted upon reload.';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Page not added to URL.'
                        }
                    });
                }   
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
 
 