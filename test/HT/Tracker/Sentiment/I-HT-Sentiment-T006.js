/**
 * ### TEST CASE:
 * I-HT-Sentiment-006
 
 * ### TEST TITLE:
 * Filter Sentiment with date
 *
 * ### TEST SUMMARY:
 * User should be able to Filter Sentiment with date
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Filter Sentiment with date', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Filter Sentiment with date', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 4000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let sentiment = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[5]/a');
                await sentiment.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));

                const allowedDays = [1, 7, 14, 30]

                const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                let changeDate = await setDateRangeNew(days);
                steps += 'Date Selected ';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));            

                let checkEle = await page.checkElementByCSS('#sentiment-charts > div.key-chartHeading > h3 > span.key-chartHeading__breadcrumbs.key-chartHeading__breadcrumbs--hasHome > span > span');
                if(checkEle){

                await page.scrollPage('//*[@id="sentiment-charts"]/div[2]');
                await driver.sleep(3000);

                let daysFilter = await page.findByXPath('//*[@id="trackerSentiment"]/div/div/div[2]/h3/span[1]');
                daysFilter = await daysFilter.getText();
                status += 'Showing post from: '+daysFilter;
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'No posts found in the selected date range.'
                        }
                    });
                }
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
