/**
 * ### TEST CASE:
 * I-HT-Sentiment-T007
 
 * ### TEST TITLE:
 * Positive and negative keyword persistent to sentiment timeline
 *
 * ### TEST SUMMARY:
 * User should be able to click on positive and negative keywords and check the sentiment timeline
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Positive and negative keyword persistent to sentiment timeline', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Positive sentiment persistent to url', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 4000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let sentiment = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[5]/a');
                await sentiment.click();
                // await driver.wait(until.elementLocated(By.xpath('//*[@id="trackerSentiment"]/div/div'), 3000));
                await driver.sleep(5000);

                await page.scrollPage('//*[@id="trackerSentiment"]/div/div');

                let checkCount = await page.findElementsByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__content > div:nth-child(1) > div > div > table > tbody > tr');
                checkCount = checkCount.length;

                if (checkCount > 5) checkCount = 5;

                if (checkCount > 0) {
                	for (let i = 2; i <= checkCount; i++) {
                		let clickRow = await page.findByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__content > div:nth-child(1) > div > div > table > tbody > tr:nth-child('+i+')');
                		await clickRow.click();

                		let keywords = await page.findByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__content > div:nth-child(1) > div > div > table > tbody > tr:nth-child('+i+') > td.key-sentiment__keywordWord > span');
                		keywords = await keywords.getText();

                		await page.scrollPage('//*[@id="hashtag-tracking"]/div/div[1]/div/h1');
                		await driver.sleep(5000);

                		addContext(this.ctx, 'Keyword #'+i+': '+keywords);

                		let timelineCount = await page.findElementsByCSS('#sentiment-charts > div.key-chartHeading > h3 > span.key-chartHeading__breadcrumbs.key-chartHeading__breadcrumbs--hasHome > span');
                		timelineCount = timelineCount.length;

                		let status = 0;

                		for (let j = 2; j <= timelineCount; j++) {
                			let timelineKey = await page.findByCSS('#sentiment-charts > div.key-chartHeading > h3 > span.key-chartHeading__breadcrumbs.key-chartHeading__breadcrumbs--hasHome > span:nth-child('+j+')');
                			timelineKey = await timelineKey.getText();

                			if (timelineKey.indexOf(keywords) !== -1) status = 1;
                			else status = 0;
                		}

                		if (status == 1) addContext(this.ctx, keywords+' persistent: Exists');
                		else addContext(this.ctx, keywords+' persistent: Not Exists');

                		await page.scrollPage('//*[@id="trackerSentiment"]/div/div');
                		await driver.sleep(5000);
                	}
                }
            });

			it('Negative sentiment persistent to url', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 4000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let sentiment = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[5]/a');
                await sentiment.click();
                await driver.sleep(5000);

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.sleep(5000);

                await page.scrollPage('//*[@id="trackerSentiment"]/div/div');
                await driver.sleep(5000);

                let negativeClick = await page.findByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__header > div > div > div > div:nth-child(2)');
                await negativeClick.click();
                await driver.sleep(5000);

                let checkCount = await page.findElementsByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__content > div:nth-child(2) > div > div > table > tbody > tr');
                checkCount = checkCount.length;

                addContext(this.ctx, 'Check count: '+checkCount);

                if (checkCount > 5) checkCount = 5;

                if (checkCount > 0) {
                	for (let i = 2; i <= checkCount; i++) {
                		let clickRow = await page.findByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__content > div:nth-child(2) > div > div > table > tbody > tr:nth-child('+i+')');
                		await clickRow.click();

                		let keywords = await page.findByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__content > div:nth-child(2) > div > div > table > tbody > tr:nth-child('+i+') > td.key-sentiment__keywordWord > span');
                		keywords = await keywords.getText();

                		await page.scrollPage('//*[@id="hashtag-tracking"]/div/div[1]/div/h1');
                		await driver.sleep(5000);

                		addContext(this.ctx, 'Keyword #'+i+': '+keywords);

                		let timelineCount = await page.findElementsByCSS('#sentiment-charts > div.key-chartHeading > h3 > span.key-chartHeading__breadcrumbs.key-chartHeading__breadcrumbs--hasHome > span');
                		timelineCount = timelineCount.length;

                		let status = 0;

                		for (let j = 2; j <= timelineCount; j++) {
                			let timelineKey = await page.findByCSS('#sentiment-charts > div.key-chartHeading > h3 > span.key-chartHeading__breadcrumbs.key-chartHeading__breadcrumbs--hasHome > span:nth-child('+j+')');
                			timelineKey = await timelineKey.getText();

                			if (timelineKey.indexOf(keywords) !== -1) status = 1;
                			else status = 0;
                		}

                		if (status == 1) addContext(this.ctx, keywords+' persistent: Exists');
                		else addContext(this.ctx, keywords+' persistent: Not Exists');

                		await page.scrollPage('//*[@id="trackerSentiment"]/div/div');
                		await driver.sleep(5000);
                	}
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();