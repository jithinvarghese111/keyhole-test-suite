/**
 * ### TEST CASE:
 * I-HT-Sentiment-T008
 *
 * ### TEST TITLE:
 * Date Range persists to currentURL
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate into sentiment Page and change the time range and check date Range persists to currentURL.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Date Range persists to currentURL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigate into sentiment Page, Checking date range persists to currentURL', async () => {
                await page.redirectToHTDashboard();

                let status = '';
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let clickSenti = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(5) > a');
                await clickSenti.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1'), 20000));

                const allowedDays = [1, 7, 14, 30, 90, 365]
                const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                let changeDate = await setDateRangeNew(days)

                if (changeDate) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Selected date period': days,
                            'Test status': 'Success'
                        }
                    });

                    let currentURL = await driver.getCurrentUrl();
                    status += 'currentURL :'+currentURL;
                    let params = currentURL.substring(currentURL.indexOf("?")+1);

                    if (params.indexOf('days') > -1 && params.indexOf(days) > -1) {
                        status += '. Date added to URL., ';
                    }

                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                    if (params.indexOf('days') > -1 && params.indexOf(days) > -1) {
                        status += ', Date persisted upon reload.';
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Current URL': currentURL,  
                            'Test status': status
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Date selection failed'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();