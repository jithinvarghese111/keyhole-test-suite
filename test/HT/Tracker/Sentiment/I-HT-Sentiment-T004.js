/**
 * ### TEST CASE:
 * I-HT-Sentiment-004
 
 * ### TEST TITLE:
 * keyword driving sentiment
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the keyword driving sentiment
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('keyword driving sentiment', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking keyword driving sentiment', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 4000));

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let sentiment = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[5]/a');
                await sentiment.click();
                steps += 'Navigated to sentiment page';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 3000));
                
                await page.scrollPage('//*[@id="sentiment-charts"]/div[3]');
                await driver.sleep(6000);

                let checkEle = await page.checkElementByCSS('#posts-table > div > div > div > table > tbody > tr:nth-child(1)');
                // await driver.sleep(10000);

                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. No posts found in the selected date range.'
                        }
                    });
                }
                else{
                    let totalPositiveKeywords = await page.findElementsByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__content > div:nth-child(2) > div > div > table > tbody > tr');
                    status += 'Total Positive Keywords :' +totalPositiveKeywords.length 

                    let negativeKeywords = await page.findByXPath('//*[@id="trackerSentiment"]/div/div/div[1]/figure/div/div[1]/div/div/div/div[2]');
                    await negativeKeywords.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="trackerSentiment"]/div/div/div[1]/figure/div/div[2]/div[2]/div/div/table/tbody/tr[1]/td[1]'), 3000));

                    let totalNegativeKeywords = await page.findElementsByCSS('#trackerSentiment > div > div > div:nth-child(1) > figure > div > div.el-tabs__content > div:nth-child(2) > div > div > table > tbody > tr');
                    status += ' ,Total Negative Keywords :' +totalNegativeKeywords.length ;

                    let editKeywords = await page.findByXPath('//*[@id="trackerSentiment"]/div/div/div[1]/figure/button/i');
                    await editKeywords.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[1]/h3'), 3000));

                    let addPositive = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[2]/div[1]/div/div/div/input');
                    await addPositive.click();
                    await addPositive.clear();
                    await page.write(addPositive, 'game');
                    steps += ' ,Added Positive keyword' ;
                
                    let addNeutral = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[2]/div[2]/div/div/div/input');
                    await addNeutral.click();
                    await addNeutral.clear();
                    await page.write(addNeutral, 'middle');
                    steps += ' ,Added Neutral keyword' ;

                    let addNegative = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[2]/div[3]/div/div/div/input');
                    await addNegative.click();
                    await addNegative.clear();
                    await page.write(addNegative, 'player');
                    steps += ' ,Added Negative keyword' ;
                    await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div > div.key-modal__wrapper > div.key-modal__footer > div > button.key-modal__footerCTA.key-modal__footerCTA--green'), 3000));

                    let checkElem = await page.findByCSS('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div > div.key-modal__wrapper > div.key-modal__footer > div > button.key-modal__footerCTA.key-modal__footerCTA--green');
                    let saveCheck = await checkElem.getAttribute("class");

                    if (saveCheck.indexOf('disabled') > -1) {
                        let closeButton = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[3]/button');
                        await closeButton.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="trackerSentiment"]/div/div/div[1]/figure/div/div[1]/div/div/div/div[2]'), 3000));
                    }
                    else{
                        let save = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[2]/div/div[2]/div[3]/div/button[1]');
                        await save.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="trackerSentiment"]/div/div/div[1]/figure/div/div[1]/div/div/div/div[2]'), 3000));
                    }

                    status += ' ,Keywords driving sentiment loaded' ;
                }
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
