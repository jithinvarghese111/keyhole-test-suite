/**
 * ### TEST CASE:
 * I-HT-Edit-004
 
 * ### TEST TITLE:
 * Delete Tracker from Edit page
 *
 * ### TEST SUMMARY:
 * User should be able to navigate to the edit page,
 * scroll down and click the delete tracker button,
 * then verify that the tracker was indeed deleted.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Delete Tracker from Edit page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Delete Tracker from Edit page', async () => {
                await page.redirectToDashboard();

                let steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                expect(url).to.equal(hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let editTracker = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[7]/a');
                await editTracker.click();
                steps += 'Navigated to Edit Tracker page';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[1]/div/h1/strong'), 3000));

                await page.scrollPixel(2000);
                await driver.sleep(3000);

                let deleteTracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[4]/div[2]/div/div/button');
                await deleteTracker.click();
                steps += ' ,Delete tracker link clicked';
                await driver.wait(until.elementLocated(By.css('#advancedEdit > div > section > div.key-modal.key-modal--deleteTracker.key-modal--show > div.key-modal__wrapper > div.key-modal__footer > button.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--red'), 3000));

                let confirmDelete = await page.findByCSS('#advancedEdit > div > section > div.key-modal.key-modal--deleteTracker.key-modal--show > div.key-modal__wrapper > div.key-modal__footer > button.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--red');
                await confirmDelete.click();
                steps += ' ,Delete button clicked ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let secondHashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                await secondHashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                let checkEle = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                if (checkEle) {

                    let afterTrackerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                    afterTrackerName = await afterTrackerName.getText();
                    assert.notEqual(afterTrackerName, trackerName);
                    status = 'Tracker deleted successfully';
                }
                else{
                    status = 'Tracker deleted successfully';
                }

                addContext(this.ctx, {
                    title: 'Test Steps',
                    value: {
                        'Steps': steps
                    }
                });

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
