/**
 * ### TEST CASE:
 * I-HT-Edit-003
 
 * ### TEST TITLE:
 * Re-analyze tracker - ignore keyword
 *
 * ### TEST SUMMARY:
 * After editing tracker platform filters, user should be able to
 * re-analyze past data to ignore all posts with a certain keyword
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Re-analyze tracker - ignore keyword', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Re-analyze tracker - ignore keyword', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000));

                let url = await driver.getCurrentUrl();
                expect(url).to.equal(hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 20000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let editTracker = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[7]/a');
                await editTracker.click();
                steps += 'Navigated to Edit Tracker page';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[1]/div/h1/strong'), 20000));

                await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div');
                await driver.sleep(2000);

                let checkEle = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--filters.key-filterSection--open > div.key-filterSection__content.key-filterSection__content--noPadding > div:nth-child(1) > p > span');

                if(!checkEle){
                let filtersOpen = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div/div[1]/i');
                await filtersOpen.click();
                await driver.write(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[1]/p/span'), 20000));
                }

                await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[6]');
                await driver.sleep(2000);   

                let checkElem = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--filters.key-filterSection--open > div.key-filterSection__content.key-filterSection__content--noPadding > div.key-filterSection__row.key-filterSection--filterIgnoreDomain.key-filterSection__row--subSection > div.key-filterSection__content > div > label > span');
                
                if(!checkElem){
                let ignorePost = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[8]/div/div[1]/i');
                await ignorePost.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[8]/div[2]/div/div/div/div/input'), 20000));
                }

                let domain = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[8]/div[2]/div/div/div/div/input');
                await domain.click();
                await page.write(domain, 'nil');
                steps =+ ' ,Ignored posts from specific domain'

                await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div');
                await driver.sleep(2000);
                
                let filtersClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div/div[1]/i');
                await filtersClose.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div/div[2]/span'), 20000));

                let saveChanges = await page.findByXPath('//*[@id="advancedEdit"]/div/div[3]/div[1]/div[2]/button');
                await saveChanges.click();
                await driver.wait(until.elementLocated(By.css('#advancedEdit > div > section > div.key-modal.key-modal--kindaWide.key-modal--saveChanges.key-modal--show > div.key-modal__wrapper > div.key-modal__body'), 20000));

                let modalOpen = await page.findElementsByCSS('#advancedEdit > div > section > div.key-modal.key-modal--kindaWide.key-modal--saveChanges.key-modal--show > div.key-modal__wrapper > div.key-modal__body');
                modalOpen = modalOpen.length;

                if (modalOpen > 0)
                    status += 'Modal opened';

                let re_analyze = await page.findByCSS('#advancedEdit > div > section > div.key-modal.key-modal--kindaWide.key-modal--saveChanges.key-modal--show > div.key-modal__wrapper > div.key-modal__footer > button.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--blue');
                await re_analyze.click();
                steps += ' ,Re-analyze Previous Data Button Clicked. ';
                await driver.wait(until.elementLocated(By.css('#reprocessing > h2'), 20000));

                let checkElement = await page.checkElementByCSS('#reprocessing > h2');
           
                if (checkElement) {
                    let getText = await page.findByXPath('//*[@id="reprocessing"]/h2');
                    status = await getText.getText();
                }
                else {
                    status = 'message found'
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
