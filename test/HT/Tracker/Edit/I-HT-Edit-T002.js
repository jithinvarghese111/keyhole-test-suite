/**
 * ### TEST CASE:
 * I-HT-Edit-002
 
 * ### TEST TITLE:
 * Editing Tracker
 *
 * ### TEST SUMMARY:
 * User should be able to perform an advanced edit on their tracker. This includes
 * more advanced querying for search terms, querying multiple platforms, selecting
 * specific users, setting start and stop dates for the tracker, filtering by
 * location and language, creating sub=categories for the filters, ignoring
 * posts with specific words or from specific accounts, setting alerts and/or
 * predictions as well as setting a custom URL and page Title for the track
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Editing Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Editing Tracker', async () => {
                await page.redirectToDashboard();

                let randomname = randomName(), steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 20000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let editTracker = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[7]/a');
                await editTracker.click();
                steps += 'Navigated to Edit Tracker page';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[1]/div/h1/strong'), 20000));

                let checkterms = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--refineSearch.key-filterSection--open > div.key-filterSection__content > div:nth-child(1) > div > div:nth-child(1) > div.key-corgiBuilder__searches > button');
                
                if (checkterms) {
                    let addTerms = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/button');
                    await addTerms.click();
                    await driver.sleep(1000);
                }

                let searchTerms = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[1]/div/div[1]/div/div[1]/div[2]/div[2]/input');
                await searchTerms.click();
                await searchTerms.clear();
                await page.write(searchTerms, randomname);
                steps += ' ,Added search Terms';

                let platforms = ['News', 'Blogs','Forums'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--refineSearch.key-filterSection--open > div.key-filterSection__content > div.key-filterSection__row.key-filterSection__row--flex > div > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await driver.sleep(1000);
                }
                steps += ' ,Checked multiple platforms';

                await page.scrollPixel(300);
                await driver.sleep(3000);

                let checkElem = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--subTrackers.key-filterSection--open > div.key-filterSection__content > div > div > div:nth-child(1) > input');

                if(!checkElem){
                    let subTracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/span');
                    await subTracker.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div/div/div[1]/input'), 20000));
                }

                let firstSubtracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div/div/div[1]/input');
                await firstSubtracker.clear();
                await page.write(firstSubtracker, randomname);

                let secondSubtracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/input');
                await secondSubtracker.clear();
                await page.write(secondSubtracker, randomname);
                steps += ' ,Updated sub Trackers';

                let subTrackerClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div/div[1]/i');
                await subTrackerClose.click();

                let checkEle = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--filters.key-filterSection--open > div.key-filterSection__content.key-filterSection__content--noPadding > div:nth-child(1) > p > span');

                if(!checkEle){
                    let filtersOpen = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div/div[1]/i');
                    await filtersOpen.click();
                    await driver.sleep(1000);
                }

                await page.scrollPixel(700);
                await driver.sleep(3000);

                let checkElements = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--searchSettings.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--filters.key-filterSection--open > div.key-filterSection__content.key-filterSection__content--noPadding > div.key-filterSection__row.key-filterSection--filterIgnoreAccount.key-filterSection__row--subSection > div.key-filterSection__content > div > div > div > div > input');

                if(!checkElements){
                    let ignorePostAccount = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[6]/div/div[2]/span');
                    await ignorePostAccount.click();
                    await driver.sleep(3000);
                }   
                else{
                    let ignorePostAccounts = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[6]/div/div[2]/span');
                    await ignorePostAccounts.click();
                    await driver.sleep(3000);
                }
                
                let domain = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[6]/div[2]/div/div/div/div/input');
                await domain.click();
                await page.write(domain, 'jeni');
                status += 'Ignored posts from specific social media account';

                let ignorePostClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[2]/div[6]/div[1]/div[2]/span');
                await ignorePostClose.click();
                await driver.sleep(2000);

                await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[2]/div/div[2]/span');
                await driver.sleep(2000);

                let filtersClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[1]/div[2]/div[3]/div[1]/div[2]/span');
                await filtersClose.click();
                await driver.sleep(3000);

                await page.scrollPage('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[1]/div/strong');
                await driver.sleep(10000);

                let checkE = await page.checkElementByCSS('#advancedEdit > div > div.key-wrapper > div.key-advancedEdit__section.key-advancedEdit__section--sharing.key-chartBox.key-chartBox--noLogo > div.key-chartContainer > div.key-filterSection.key-filterSection--customURL.key-filterSection--open > div.key-filterSection__content > div:nth-child(1) > p > span');

                if(!checkE){
                    let customTracker = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/i');
                    await customTracker.click();
                    await driver.sleep(1000);
                }

                let customPageUrl = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/input');
                await customPageUrl.click();
                await customPageUrl.clear();
                await page.write(customPageUrl, trackerName);

                let pageTitle = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[1]/div[2]/div[2]/div/input');
                await pageTitle.click();
                await pageTitle.clear();
                await page.write(pageTitle, 'Real-time Tracker: '+trackerName);

                if(customPageUrl && pageTitle){
                    status += ' ,Updated Custom Page URL & Page Title';
                }
                else{
                   status += 'Custom Page URL & Page Title updation failed';
                }

                let customTrackerClose = await page.findByXPath('//*[@id="advancedEdit"]/div/div[2]/div[2]/div[2]/div[1]/div/div[1]/i');
                await customTrackerClose.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="advancedEdit"]/div/div[3]/div[1]/div[2]/button'), 20000));

                let saveChanges = await page.findByXPath('//*[@id="advancedEdit"]/div/div[3]/div[1]/div[2]/button');
                await saveChanges.click();
                await driver.sleep(20000);

                let futureData = await page.findByXPath('//*[@id="advancedEdit"]/div/section/div[1]/div[2]/div[3]/button[2]');
                await futureData.click();
                await driver.sleep(5000);
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
