 /** 
 * ### TEST CASE:
 * I-HT-Posts-019
 
 * ### TEST TITLE:
 * Bulk edit sentiment
 *
 * ### TEST SUMMARY:
 *  User should be able to edit the sentiment of multiple posts.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Bulk edit sentiment', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Bulk edit sentiment', async () => {
                await page.redirectToHTDashboard();

                let steps = '', status = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to the Posts';
                await driver.sleep(4000);

                let checkEle = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(1)');

                if (!checkEle) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Test requires Posts.'
                        }
                    });
                }
                else{

                    let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');

                    if (totalPost.length < 2) {

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Test requires Multiple Posts.'
                            }
                        });
                    }
                    else{
                        
                        let sentiEdit = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[7]/i');
                        await sentiEdit.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[7]/div/div[3]/button[1]'), 2000));

                        let sentiImage = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[7]/img');
                        sentiImage = await sentiImage.getAttribute("src");
                        let senti = sentiImage.substr(sentiImage.indexOf('_')+1);
                        let firstSentimentName = senti.substr(0, senti.lastIndexOf('.'));   
                       
                        let firstPostCheck = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div/label/input');
                        await firstPostCheck.click();                       
                        await driver.wait(until.elementLocated(By.id('bulk-manage__modal'), 4000));

                        for (let i = 2; i <= 4; i++) {
                        let secondPostCheck = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr['+i+']/td[1]');
                        await secondPostCheck.click();
                
                        }

                        let editSentiment = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[2]');
                        await editSentiment.click();
                        steps += ' ,clicked Edit Sentiment'
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="bulk-manage__modal"]/button[2]/ul/li[1]/div/img'), 4000));

                        let checkSentiment = await page.checkElementByCSS('#bulk-manage__modal > button.bulk-manage__button.bulk-manage__button--yellow.keyjs-bulkManage__sentiment');

                        if (checkSentiment) {

                            let SentiClick = await randomNumberFromTo(1,5);

                            let slightPositive = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[2]/ul/li['+SentiClick+']/div/img');
                            let sentimentName = await slightPositive.getAttribute("alt")
                            await slightPositive.click();
                            assert.notEqual(sentimentName, firstSentimentName);
                            await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 4000));                            
                            status = 'Sentiment Changed successfully';

                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test Steps': steps,
                                    'changed Sentiment': sentimentName,
                                    'Test status': status
                                }
                            });
                        }                                               
                        else{
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Sentiment Tooltip is not found.'
                                }
                            });
                        }
                        
                    }               
                }
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
