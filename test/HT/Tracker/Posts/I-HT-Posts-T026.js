 /**                                                                                           
 * ### TEST CASE:
 * I-HT-Posts-T026
 
 * ### TEST TITLE:
 * Check post grid filter and Embed Post  
 *
 * ### TEST SUMMARY:
 * User should be able to check post and click grid in filter bar and Embed Post by hovering on it.
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
    	describe('Check post grid filter and Embed Post', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check post grid filter and Embed Post', async () => {
                await page.redirectToHTDashboard();

                let steps = '';
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                let posts = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(2) > a');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts');
                await driver.sleep(8000);

                let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalPost = totalPost.length;

                if (totalPost > 0) {
                	let gridClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > div.key-iconToggleContainer.keyjs-postsActions--toggleView > button.key-button.key-button--noBorder.key-button--noFill.keyjs-postsActions--cardView');
                	await gridClick.click();
                	await driver.sleep(2000);

                	let checkGrid = await page.checkElement('key-postsMasonry');

                	if (checkGrid) addContext(this.ctx, 'Grid view enabled');
                	else addContext(this.ctx, 'Grid view not enabled');

                	//hover on an element
                    const actions = driver.actions({bridge: true}); 
                    var elem = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[1]/div[1]'); 
                    await actions.move({duration:5000,origin:elem,x:0,y:0}).perform();
                    await driver.sleep(2000);

                    let embedClick = await page.findByCSS('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--post > div.key-chartOptions > button:nth-child(2)');
                    await embedClick.click();
                    steps += 'Embed button clicked, ';
                    await driver.sleep(2000);

                    let copySnippet = await page.findByCSS('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--embed > button');
                    await copySnippet.click();
                    steps += 'Copy snippet button clicked, ';
                
                    let code = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[4]/textarea');
                    code = await code.getText();
                    steps += 'URL highlighted and copied!';

                    if (code != "")
                        test_result = 'Test passed';

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Embed code': code,
                            "Test steps": steps,
                            'Test status': test_result
                        }
                    });
                }
                else {
                    addContext(this.ctx, 'No post available');
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();