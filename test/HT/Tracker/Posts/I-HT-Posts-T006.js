/**
 * ### TEST CASE:
 * I-HT-Posts-006
 
 * ### TEST TITLE:
 * Post Pagination
 *
 * ### TEST SUMMARY:
 * Pagination on posts page should take you to the appropriate url
 * (clicking 2 should take you to page = 2)
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Post Pagination', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Post Pagination', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to posts';
                await driver.sleep(4000);

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/div/div/div/div[2]');
                await driver.sleep(5000);

                let pageClick = await randomNumberFromTo(3,7);

                let pagination = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/div/div/div/div[2]/button['+pageClick+']');
                let pageNumber = await pagination.getAttribute("data-page");
                await pagination.click();
                status += 'Page Number clicked: '+pageNumber;
                await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 3000));
                                
                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.indexOf('&'));
                
                if (firstParam.indexOf('page') > -1 && firstParam.indexOf('=') > -1 && firstParam.indexOf(pageNumber) > -1) {
                    let status = '';

                    status += 'Page Number added to URL.';

                    await driver.navigate().refresh();

                    if (firstParam.indexOf('page') > -1 && firstParam.indexOf('=') > -1 && firstParam.indexOf(pageNumber) > -1) {
                        status += ' ,Page Number persisted upon reload.';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': status
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Page Number not added to URL.'
                        }
                    });
                }
                addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
