/**                                                                                                     
 * ### TEST CASE:
 * I-HT-Posts-031
 
 * ### TEST TITLE:
 * Exclude ReTweets persists to URL
 *
 * ### TEST SUMMARY:
 * When user perform with exclude reteets, check exclude retweets is persist to the URL or not.
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Exclude ReTweets persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('check exclude retweets is persist to the URL or not', async () => {
                await page.redirectToHTDashboard();

                let steps = '', status = '';

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(2) > a > span');
                await posts.click();
                steps += 'Navigated to the Posts';
                await driver.sleep(8000);

                let checkEle = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(1)');
                if (!checkEle) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Test requires Posts.'
                        }
                    });
                }
                else{

                    let excludeRT = await page.findById('excludeRT');
                    await excludeRT.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'),3000));

                    let currentURL = await driver.getCurrentUrl();
                    let params = currentURL.substring(currentURL.indexOf("&")+1);

                    if (params.indexOf('excludeRT') > -1 && params.indexOf('1') > -1) {

                        let status = '';

                        status += 'Exclude ReTweets added to URL., ';

                        await driver.navigate().refresh();
                        await driver.wait(until.elementLocated(By.id('twt_check'),3000));

                        if (params.indexOf('excludeRT') > -1 && params.indexOf('1') > -1) {
                            status += 'Exclude ReTweets persisted upon reload.';
                        }

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Current URL': currentURL,  
                                'Test status': status
                            }
                        });
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Exclude ReTweets not added to URL.'
                            }
                        });
                    }    
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
