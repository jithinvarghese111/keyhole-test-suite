/**
 * ### TEST CASE:
 * I-HT-Posts-T039
 
 * ### TEST TITLE:
 * Navigate to the posts, Check Reset Button after filer the post.
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the posts,  Check Reset Button after filer the posts.
 *
 */

 const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check Reset Button after filer the post.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {      
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigate to the posts, Check Reset Button after filer the post.', async () => {
                await page.redirectToHTDashboard();

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 2000));

                let posts = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(2) > a');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts');
                await driver.sleep(10000);

                let totalRows = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalRows = totalRows.length;

                if (totalRows > 0) {
                	
                    let firstPost = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--post > span');
                    firstPost = await firstPost.getText();

                    let keyword = randomWord();
                    let inputRandom = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > form > input');
                    await page.write(inputRandom, 'test');
                    addContext(this.ctx, 'Keyword searched: '+keyword);

                    let searchBtn = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > form > button');
                    await searchBtn.click();
                    await driver.sleep(2000);

                    let clickReset = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > button.key-button.key-button--red.key-button--outline.key-button--caps');
                    await clickReset.click();
                    await driver.sleep(5000);

                    let secondPost = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--post > span');
                    secondPost = await secondPost.getText();

                    if (secondPost == firstPost) addContext(this.ctx, 'RESET Button working Successfully'); else addContext(this.ctx, 'Failed');

                    assert.equal(secondPost, firstPost);
                                    	
                }
                else assert.fail('No post found');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();