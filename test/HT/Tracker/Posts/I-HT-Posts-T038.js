 /**                                                                                           
 * ### TEST CASE:
 * I-HT-Posts-T038
 
 * ### TEST TITLE:
 * Navigate to the Posts, click on each posts HT or AT Tracking link to track the keyword.
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Posts, click on each posts HT or AT Tracking link to track the keyword.
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('click on each posts HT or AT Tracking link to track the keyword.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on each posts HT or AT Tracking link to track the keyword.', async () => {
                await page.redirectToHTDashboard();

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(2) > a');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts');
                await driver.sleep(8000);

                let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalPost = totalPost.length;

                if (totalPost > 0) {
                	if (totalPost > 5) totalPost = 5;
                	
                    for (let i = 1; i <= totalPost; i++) {
                    addContext(this.ctx, '--------------------');

                    let filter = await page.findByCSS('#key-postsTable > table > thead > tr');
                    await filter.click();
                    await driver.sleep(2000);
                    
                    let clickSingle = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child('+i+')');
                    await clickSingle.click();
                    await driver.sleep(1000);

                    let checkHT = await page.checkElementByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                    if (checkHT) {
                        let startTracking = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag'); 
                        let htTrackerName = await startTracking.getText();    
                        addContext(this.ctx, 'Clicked HT tracking link '+htTrackerName);
                        await startTracking.click();
                        await driver.sleep(2000);

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);

                        let url = await driver.getCurrentUrl();

                        let status = '';

                        if (url == 'https://keyhole.co/preview') addContext(this.ctx, 'Passed');
                        else addContext(this.ctx, 'Failed');
                        await driver.close();
                        await driver.switchTo().window(tab1);
                    }
                    else addContext(this.ctx, 'No HT tacker Link Found');
                                              
                    let checkAT = await page.checkElementByCSS('#post-view > div.content > div > div > div.post-caption > a');
                    if (checkAT) {
                        let startTracking = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a'); 
                        let atTrackerName = await startTracking.getText();    
                        addContext(this.ctx, 'Clicked AT tracking link '+atTrackerName);
                        await startTracking.click();
                        await driver.sleep(2000);

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);

                        let url = await driver.getCurrentUrl();

                        let status = '';

                        if (url == 'https://keyhole.co/account-tracking/preview') addContext(this.ctx, 'Passed');
                        else addContext(this.ctx, 'Failed');

                        await driver.close();
                        await driver.switchTo().window(tab1);
                    }
                    else addContext(this.ctx, 'No AT tacker Link Found');
                        
                    let closeModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div > button');
                    await closeModal.click();
                    await driver.sleep(1000);
                  }
                }
                else {
                    addContext(this.ctx, 'No post available')
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();