/**
 * ### TEST CASE:
 * I-HT-Posts-015
 
 * ### TEST TITLE:
 * Geo persists to URL
 *
 * ### TEST SUMMARY:
 * When doing geo search, the geo search keyword should persist to the URL
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Geo persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Geo persists to URL', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to the Posts';
                await driver.sleep(6000);

                let checkEle = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(1)');
                if (!checkEle) {                    
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value:{
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. Tracker does not have posts.'
                        }
                    });                    
                }
                else{

                    let searchGeography = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/input[2]');
                    await searchGeography.click();
                    await page.write(searchGeography, 'Australia');
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/button'), 3000));

                    let submit = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/button');
                    await submit.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 4000));

                    let geoKeyword = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/form/input[2]');
                    geoKeyword = await geoKeyword.getAttribute("value");

                    let currentURL = await driver.getCurrentUrl();

                    if (currentURL.indexOf('geo') > -1 && currentURL.indexOf(geoKeyword) > -1) {
                        let status = '';

                        status += 'Geo added to URL., ';

                        await driver.navigate().refresh();
                        await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                        if (currentURL.indexOf('geo') > -1 && currentURL.indexOf(geoKeyword) > -1) {
                            status += 'Geo persisted upon reload.';
                        }

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Geo Keyword': geoKeyword,
                                'Current URL': currentURL,
                                'Test status': status
                            }
                        });

                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Geo not added to URL.'
                            }
                        });
                    }
                }
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
