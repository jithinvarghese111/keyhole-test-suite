/**
 * ### TEST CASE:
 * I-HT-Posts-T027
 
 * ### TEST TITLE:
 * Checking Domain counts from table and modal are same
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Posts page, click on each post and check Domain counts are same in the modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Domain counts from table and modal are same', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on each post and check various counts from table are same in modal', async () => {
                await page.redirectToHTDashboard();

                let steps = '';
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts');
                await driver.sleep(5000);

                let checkElement = await page.checkElementByCSS('#key-postsTable > table > tbody > tr');
                
                if (checkElement) {
                    let domainBefore = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--sortMetrics.key-table__column--shorterWidth.key-table__column--stats');
                    domainBefore = await domainBefore.getText();

                    let clickPost = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1)');
                    await clickPost.click();
                    await driver.sleep(1000);

                    let domainModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > article > section.key-sideviewPost__statLine > div:nth-child(2) > span.key-sideviewPost__stat.key-sideviewPost__stat--text > span');
                    domainModal = await domainModal.getText();

                    let closeModal = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div > button');
                    await closeModal.click();
                    await driver.sleep(1000);

                    if (domainBefore == domainModal) addContext(this.ctx, 'Domain counts are same');
                    else addContext(this.ctx, 'Domain counts are not same');

                    assert.equal(domainBefore, domainModal);
                }
                else addContext(this.ctx, 'No post found');
                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();