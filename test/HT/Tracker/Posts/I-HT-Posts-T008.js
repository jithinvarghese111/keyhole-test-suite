/**
 * ### TEST CASE:
 * I-HT-Posts-008
 
 * ### TEST TITLE:
 * Deleting Posts - UI Updated
 *
 * ### TEST SUMMARY:
 * User should be able to delete posts from a tracker
 * by selecting them in the post page table view
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Deleting Posts - UI Updated', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Deleting Posts', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to posts';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 4000));

                let firstpostTitle = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[2]/span');
                firstpostTitle = await firstpostTitle.getText();

                let postCheck = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div/label/input');
                await postCheck.click();
                await driver.wait(until.elementLocated(By.id('bulk-manage__modal'), 4000));

                let checkEle = await page.checkElementByCSS('#bulk-manage__modal > button.bulk-manage__button.bulk-manage__button--red');

                if(checkEle){
                    let deletePost = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[1]');
                    await deletePost.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/section[2]/div[2]/div[2]/div[1]'), 3000));

                    let confirmDelete = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[2]/div[2]/div[2]/div[2]/button[1]');
                    await confirmDelete.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 6000));

                    await driver.navigate().refresh();
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': 'Unable to perform test. Delete button not found.'
                        }
                    });
                }

                let secondpostTitle = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[2]/span');
                secondpostTitle = await secondpostTitle.getText();
                assert.notEqual(firstpostTitle, secondpostTitle);

                let cardView = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/div[1]/button[2]');
                await cardView.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsMasonry"]/div/article[1]/div[1]/div[1]'), 3000));

                let thirdpostTitle = await page.findByCSS('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--post > div.key-masonryCard__post > div.key-masonryCard__postText');
                thirdpostTitle = await thirdpostTitle.getText();
                assert.notEqual(firstpostTitle, thirdpostTitle);
                
                if ((firstpostTitle == secondpostTitle) && (firstpostTitle == thirdpostTitle))
                        status = 'post not deleted, tryagain later';
                    else
                        status = 'Post deleted successfully';

                    addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });                                     
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
