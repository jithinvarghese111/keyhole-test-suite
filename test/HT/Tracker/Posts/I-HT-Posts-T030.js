 /**                                                                                           
 * ### TEST CASE:
 * I-HT-Posts-T030
 
 * ### TEST TITLE:
 * Check post grid filter and View  
 *
 * ### TEST SUMMARY:
 * User should be able to check post and click grid in filter bar and View Post by hovering on it.
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
    	describe('Check post grid filter and View  ', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check post grid filter and View  ', async () => {
                await page.redirectToHTDashboard();

                let steps = '';
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Posts"]/div/div/div[3]/p/span'), 200000));

                let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalPost = totalPost.length;

                if (totalPost > 0) {
                	let gridClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/div[1]/button[2]');
                	await gridClick.click();
                	await driver.sleep(1000);

                	let checkGrid = await page.checkElement('key-postsMasonry');

                	if (checkGrid) addContext(this.ctx, 'Grid view enabled');
                	else addContext(this.ctx, 'Grid view not enabled');

                	//hover on an element
                    const actions = driver.actions({bridge: true}); 
                    var elem = await page.findByCSS('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--post > div.key-masonryCard__post'); 
                    await actions.move({duration:5000,origin:elem,x:0,y:0}).perform();

                    let socialMediaName = await page.findByCSS('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--post > div.key-masonryCard__post > div.key-masonryCard__postUser > div.key-masonryCard__postUserDetails > span.key-masonryCard__userUsername');
                    socialMediaName = await socialMediaName.getText();

                    let viewClick = await page.findByCSS('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--post > div.key-chartOptions > button:nth-child(3)');
                    await viewClick.click();
                    addContext(this.ctx, 'Post Viewed on: '+socialMediaName);
  
                }
                else {
                    addContext(this.ctx, 'No post available');
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();