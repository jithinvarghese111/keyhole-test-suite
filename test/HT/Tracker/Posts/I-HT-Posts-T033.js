/**
 * ### TEST CASE:
 * I-HT-Posts-T033
 
 * ### TEST TITLE:
 * Navigate to the posts, check post is Retweeted
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the posts, check post is Retweeted
 *
 */

 const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to the posts modal, check post is Retweeted', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigate to the posts modal, check post is Retweeted', async () => {
                await page.redirectToHTDashboard();

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1'), 2000));

                let posts = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(2) > a > span');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts');
                await driver.sleep(8000);
                
                let totalRows = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalRows = totalRows.length;

                if (totalRows > 0){
                	
                    for (var i = 1; i <= totalRows; i++) {
                        let postClick = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr['+i+']');
                        await postClick.click();
                        await driver.sleep(2000);

                        let checkRT = await page.checkElementByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > article > section.key-sideviewPost__post.key-sideviewPost__post--noSplit > div > p.key-sideviewPost__postTextRetweetFlag');
                        if (checkRT) addContext(this.ctx, 'Retweeted Post: '+i+'--'); else addContext(this.ctx, 'No Retweeted Post found: '+i+'');
                    };
                } else assert.fail('No post found');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();