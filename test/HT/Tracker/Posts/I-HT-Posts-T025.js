 /**                                                                                           
 * ### TEST CASE:
 * I-HT-Posts-T025
 
 * ### TEST TITLE:
 * Check post grid filter and edit Post sentiment 
 *
 * ### TEST SUMMARY:
 * User should be able to check post and click grid in filter bar and edit edit Post sentiment by hovering on post.
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
    	describe('Check post grid filter and edit Post sentiment', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check post grid filter and edit Post sentiment', async () => {
                await page.redirectToHTDashboard();

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts');
                await driver.sleep(4000);

                let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalPost = totalPost.length;

                if (totalPost > 0) {
                	let gridClick = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/div[1]/button[2]');
                	await gridClick.click();
                	await driver.sleep(1000);

                	let checkGrid = await page.checkElement('key-postsMasonry');

                	if (checkGrid) addContext(this.ctx, 'Grid view enabled');
                	else addContext(this.ctx, 'Grid view not enabled');

                	//hover on an element
                    const actions = driver.actions({bridge: true}); 
                    var elem = await page.findByCSS('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--post > div.key-masonryCard__post'); 
                    await actions.move({duration:5000,origin:elem,x:0,y:0}).perform();

                    let sentiImage = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[2]/span[1]/span[2]/img');
                    let sentiName = await sentiImage.getAttribute("src");
                    let sentim = sentiName.substr(sentiName.indexOf('_')+1);
                    let firstSentimentName = sentim.substr(0, sentim.lastIndexOf('.')); 

                    let editSentiment = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[1]/div[2]/button[1]');
                    await editSentiment.click();
                    await driver.wait(until.elementLocated(By.css('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--editSentiment > h3'), 20000));

                    let sentiTypes = await randomNumberFromTo(1,5);

                    let sentiClick = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[3]/div[2]/button['+sentiTypes+']');                   
                    await sentiClick.click();                    
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsMasonry"]/div/article[1]/div[1]/div[1]/div[1]/div[2]/span[1]'), 20000));                    

                    let secondSenti = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[2]/span[1]/span[2]/img');
                    secondSenti = await secondSenti.getAttribute("src");
                    let senti = secondSenti.substr(secondSenti.indexOf('_')+1);
                    let secondSentimentName = senti.substr(0, senti.lastIndexOf('.')); 
                    assert.notEqual(secondSentimentName, firstSentimentName);
                    addContext(this.ctx, 'Post Sentiment Changed into: '+secondSentimentName);

                }
                else {
                    addContext(this.ctx, 'No post available');
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();