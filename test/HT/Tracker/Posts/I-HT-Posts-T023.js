 /**                                                                                           
 * ### TEST CASE:
 * I-HT-Posts-T023
 
 * ### TEST TITLE:
 * Check post filters (REGION, COUNTRY, DOMAIN RANK, ENGAGEMENT)
 *
 * ### TEST SUMMARY:
 * User should be able to check post filters on click table title
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
    	describe('Check post filters', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking post filters on click table title', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));
 
                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to the Posts';
                await driver.sleep(4000);

                let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalPost = totalPost.length;

                if (totalPost > 0) {
                    let textRegionBefore = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--sortMetrics');
                    textRegionBefore = await textRegionBefore.getText();

                	let clickRegion = await page.findByCSS('#key-postsTable > table > thead > tr > th.key-table__headerRow.key-table__headerRow--user.key-table__headerRow--sortToggle');
                    await clickRegion.click();
                    await driver.sleep(1000);

                    let textRegionAfter = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--sortMetrics.key-table__column--sortedBy');
                    textRegionAfter = await textRegionAfter.getText();

                    if (textRegionBefore !== textRegionAfter) addContext(this.ctx, 'REGION filter success');
                    else addContext(this.ctx, 'REGION filter failed');

                    let textCountryBefore = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td:nth-child(4)');
                    textCountryBefore = await textCountryBefore.getText();

                    let clickCountry = await page.findByCSS('#key-postsTable > table > thead > tr > th:nth-child(4)');
                    await clickCountry.click();
                    await driver.sleep(1000);

                    let textCountryAfter = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--sortMetrics.key-table__column--sortedBy');
                    textCountryAfter = await textCountryAfter.getText();

                    if (textCountryBefore !== textCountryAfter) addContext(this.ctx, 'COUNTRY filter success');
                    else addContext(this.ctx, 'COUNTRY filter failed');

                    let textDomainBefore = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td:nth-child(5)');
                    textDomainBefore = await textDomainBefore.getText();

                    let domainClick = await page.findByCSS('#key-postsTable > table > thead > tr > th:nth-child(5)');
                    await domainClick.click();

                    let textDomainAfter = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--sortMetrics.key-table__column--sortedBy.key-table__column--shorterWidth');
                    textDomainAfter = await textDomainAfter.getText();

                    if (textDomainBefore !== textDomainAfter) addContext(this.ctx, 'DOMAIN RANK filter success');
                    else addContext(this.ctx, 'DOMAIN RANK filter failed');

                    let textEngBefore = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--sortMetrics.key-table__column--stats');
                    textEngBefore = await textEngBefore.getText();

                    let clickEng = await page.findByCSS('#key-postsTable > table > thead > tr > th:nth-child(6)');
                    await clickEng.click();

                    let textEngAfter = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--sortMetrics.key-table__column--sortedBy.key-table__column--stats');
                    textEngAfter = await textEngAfter.getText();

                    if (textEngBefore !== textEngAfter) addContext(this.ctx, 'ENGAGEMENT filter success');
                    else addContext(this.ctx, 'ENGAGEMENT filter failed');
                }
                else {
                    addContext(this.ctx, 'No post available')
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();