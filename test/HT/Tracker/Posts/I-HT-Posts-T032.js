/**
 * ### TEST CASE:
 * I-HT-Posts-T032  
 
 * ### TEST TITLE:
 * Navigate to the posts, check profile exits in Modal
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the posts, check profile exits in modal
 *
 */

 const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to the posts modal, check profile exits', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {      
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Navigate to the posts modal, check profile exits', async () => {
                await page.redirectToHTDashboard();

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 2000));

                let posts = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(2) > a');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts');
                await driver.sleep(8000);
                
                let totalRows = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalRows = totalRows.length;

                if (totalRows > 0) {
                	let postClick = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]');
                	await postClick.click();
                    await driver.sleep(1000);
                    
                    let profile = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div > div > div > div.key-userDetails.key-userDetails--fullWidth > span:nth-child(1) > a');
                	let urlBefore = await profile.getAttribute("href");
                	await profile.click();
                    await driver.sleep(1000);

                	addContext(this.ctx, 'URL before click: '+urlBefore);

                	let tab1, tab2;

	                await driver.getAllWindowHandles().then(function(windowHandles) {
	                    tab1 = windowHandles[0];
	                    tab2 = windowHandles[1];                
	                });

	                await driver.switchTo().window(tab2);
	                let url = await driver.getCurrentUrl();

	                addContext(this.ctx, 'URL after click: '+url);
	                assert.equal(urlBefore, url);

	                if (urlBefore == url) addContext(this.ctx, 'URL same');
	                else addContext(this.ctx, 'URL not same');
                    await driver.close();
                	await driver.switchTo().window(tab1);
                	
                }
                else addContext(this.ctx, 'No post found');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();