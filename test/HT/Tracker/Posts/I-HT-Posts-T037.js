/**
 * ### TEST CASE:
 * I-HT-Posts-T037
 
 * ### TEST TITLE:
 * Search posts
 *
 * ### TEST SUMMARY:
 * User is trying to input random search keywords and recording the search results
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';
process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking with input random search keywords and recording the search results', async () => {
                await page.redirectToHTDashboard();

                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                addContext(this.ctx, 'Navigated to the Posts.');
                await driver.sleep(10000);
                
                let totalPosts = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalPosts = totalPosts.length;
                console.log(totalPosts);

                if (totalPosts > 0) {

                    let totalPosts = await page.findByCSS('#hashtag-tracking > div > section.route-section > div > div > div > div.key-pagination__info > strong.key-pagination__total');
                    totalPosts = await totalPosts.getText();
                    addContext(this.ctx, 'Total posts before search: '+totalPosts);

                    for (let i = 1; i <= 5; i++) {
                        let keyword = randomWord();
                        let inputRandom = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > form > input');
                        await page.write(inputRandom, keyword);
                        addContext(this.ctx, '#'+i+': Keyword searched: '+keyword);

                        let searchBtn = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > form > button');
                        await searchBtn.click();
                        await driver.sleep(2000);

                        let totalPosts1 = await page.findByCSS('#hashtag-tracking > div > section.route-section > div > div > div > div.key-pagination__info > strong.key-pagination__total');
                        totalPosts1 = await totalPosts1.getText();
                        addContext(this.ctx, '#'+i+': Total posts after search: '+totalPosts1);

                        await page.clearFields('css', '#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > form > input');
                        addContext(this.ctx, '--------------------------------');
                    }                        

                }else assert.fail('Unable to perform test.'+trackerName+' Tracker does not have posts.');

            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
