 /**                                                                                           
 * ### TEST CASE:
 * I-HT-Posts-020
 
 * ### TEST TITLE:
 * Exclude RT
 *
 * ### TEST SUMMARY:
 * User should be able to exclude retweeted content
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Exclude RT', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Exclude RT', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to the Posts';
                await driver.sleep(4000);

                let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                if (totalPost < 2) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Test requires Multiple Posts.'
                        }
                    });
                }
                else{
                    let platforms = ['Instagram', 'News', 'Blogs','Forums'];

                    for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    // await driver.wait(until.elementLocated(By.id('twt_check'), 2000));
                    }
                    
                    let excludeRetweet = await page.findByXPath('//*[@id="excludeRT"]');
                    await excludeRetweet.click();
                    steps += ' ,Clicked Exclude Retweet '
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div'), 6000));

                    let checkRetweetPost = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(2) > td.key-table__column.key-table__column--post > span:nth-child(1) > i');
                    if(!checkRetweetPost) {
                        status = 'Excluded Retweets';
                    } 
                    else{
                        status = 'Retweets are not Excluded, tryagain later';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test Steps': steps,
                            'Test status': status
                        }
                    });                 
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
