/**
 * ### TEST CASE:
 * I-HT-Posts-005
 
 * ### TEST TITLE:
 * Post table popup
 *
 * ### TEST SUMMARY:
 * Clicking on a tweet in the posts table will create a popup displaying all of
 * the tweet information
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Post table popup', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Post table popup', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to posts';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let checkEle = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--user > div > div');

                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. No Posts not found.'
                        }
                    });
                }
                else{

                    let postList =await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div/div');
                    postList = await postList.getAttribute("class");
                    await driver.sleep(2000);
                  
                    let tweet = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]');
                    await tweet.click();
                    await driver.sleep(5000);

                    let popup = await page.checkElementByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active');

                    if (popup) {

                        let viewOnTwitter = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/article/section[2]/div/a');
                        await viewOnTwitter.click();
                        steps += ' ,Clicked View button'
                        await driver.sleep(5000);
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': 'popup loaded.'
                                }
                            });
                        }
                        else {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': ' popup not loaded.'
                                }
                            });
                        }                    
                    }
                });

            it('Checking Post table popup - twitter', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.id('twt_check'), 4000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to posts';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let checkEle = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--user > div > div');

                if(!checkEle){
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Unable to perform test. No Posts not found.'
                        }
                    });
                }
                else{

                    let postList =await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div/div');
                    postList = await postList.getAttribute("class");
                    await driver.sleep(2000);

                    if (postList.indexOf('twitter') > -1) {

                        let tweet = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]');
                        await tweet.click();
                        await driver.sleep(5000);

                        let popup = await page.checkElementByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active');

                        if (popup) {

                            let viewOnTwitter = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/article/section[2]/div/a');
                            await viewOnTwitter.click();
                            steps += ' ,Clicked View on Twitter button'
                            await driver.sleep(5000);

                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test steps': steps,
                                    'Test status': 'popup loaded.'
                                }
                            });
                        }
                        else {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': ' popup not loaded.'
                                }
                            });
                        }

                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Unable to perform test. no Twitter Posts found.'
                            }
                        });
                    }
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
