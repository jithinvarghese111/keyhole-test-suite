/**
 * ### TEST CASE:
 * I-HT-Posts-003
 
 * ### TEST TITLE:
 * Embed Posts Page
 *
 * ### TEST SUMMARY:
 * User should be able to embed Post data from the /streaming endpoint
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const path = require('path');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Embed Posts Page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Embed Posts Page', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to posts';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let embedClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3)');
                await embedClick.click();
                steps += 'Embed button clicked, ';
                await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div.key-modal.key-modal--postEmbed.key-modal--show > div.key-modal__wrapper'), 3000));

                let modalOpen = await page.findElementsByCSS('#hashtag-tracking > div > section.key-modalContainer.key-modalContainer--show > div.key-modal.key-modal--postEmbed.key-modal--show');
                modalOpen = modalOpen.length;

                if (modalOpen > 0)
                    steps += 'Modal opened, ';

                let copySnippet = await page.findByCSS('.key-modal__footerCTA.key-modal__footerCTA--setWidth.key-modal__footerCTA--solidGreen.key-button--copy.copy--left');
                await copySnippet.click();
                steps += 'Copy snippet button clicked, ';
                await driver.wait(until.elementLocated(By.id('post__embed'), 3000));

                let code = await page.findById('post__embed');
                code = await code.getText();
                steps += 'URL highlighted and copied!';

                if (code != "" && modalOpen > 0)
                    test_result = 'Test passed';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Embed code': code,
                        "Test steps": steps,
                        'Test status': test_result
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
