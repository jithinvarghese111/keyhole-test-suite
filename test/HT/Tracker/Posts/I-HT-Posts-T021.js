 /**                                                                                           
 * ### TEST CASE:
 * I-HT-Posts-T021
 
 * ### TEST TITLE:
 * Check post and click view on from modal and check the page exists or not
 *
 * ### TEST SUMMARY:
 * User should be able to check post and click view on from modal and check the page exists or not
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check post and click view on from modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking post and click view on from modal', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));
 
                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to the Posts';
                await driver.sleep(4000);

                let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');
                totalPost = totalPost.length;

                if (totalPost > 0) {
                	if (totalPost > 5) totalPost = 5;
                	
                    for (let i = 1; i <= totalPost; i++) {
                    	let postClick = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child('+i+')');
                    	await postClick.click();
                    	await driver.sleep(4000);

                    	let buttonClick = await page.findByCSS('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > article > section.key-sideviewPost__post.key-sideviewPost__post--noSplit > div > a');
                    	let urlFromButton = await buttonClick.getAttribute("href");
	                	addContext(this.ctx, 'URL from button: '+urlFromButton);
                    	await buttonClick.click();

		                let tab1, tab2;

		                await driver.getAllWindowHandles().then(function(windowHandles) {
		                    tab1 = windowHandles[0];
		                    tab2 = windowHandles[1];                
		                });

		                await driver.switchTo().window(tab2);
		                url = await driver.getCurrentUrl();
		                addContext(this.ctx, 'Current tab URL: '+url);

		                if (url.indexOf(urlFromButton) !== -1) addContext(this.ctx, 'Page exists');
		                else addContext(this.ctx, 'Page not exists');

		                await driver.switchTo().window(tab1);
		                await driver.sleep(4000);

		                let closeModal = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/div/button');
		                await closeModal.click();
                    }
                }
                else {
                    addContext(this.ctx, 'No post available')
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();