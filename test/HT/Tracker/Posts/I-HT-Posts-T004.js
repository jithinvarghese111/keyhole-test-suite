/**
 * ### TEST CASE:
 * I-HT-Posts-004
 
 * ### TEST TITLE:
 * Interacting with the Posts table
 *
 * ### TEST SUMMARY:
 * User should be able to switch between list views and card views
 * for the way their posts are displayed.
 * Users should also be able to filter posts by region, country, engagement, sentiment, and date
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const path = require('path');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interacting with the Posts table', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Interacting with the Posts table', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to posts';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/div/div/div/div[1]/strong[3]');
                await driver.sleep(5000);

                let checkEle = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > div > div > div > div.key-pagination__info > strong.key-pagination__total');

                if(!checkEle){
                    addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Test steps': steps,
                        'Test status': 'Unable to perform test. Tracker does not have posts.'
                        }
                    });
                }
                else{

                    let totalPosts = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/div/div/div/div[1]/strong[3]');
                    totalPosts = await totalPosts.getText();
                    status += 'Total posts:'+totalPosts;
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 3000));

                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/div[1]/div[1]');
                    await driver.sleep(5000);   

                    let cardView = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/div[1]/button[2]');
                    await cardView.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsMasonry"]/div/article[1]/div[1]'), 3000));

                    let cardPost = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[1]/div[1]');                
                    cardPost = await cardPost.getAttribute("class");

                    if (cardPost.indexOf('Card') > -1){
                        status += ' ,Posts displayed as Card View'
                    }
                    else{
                        status += 'Card View changes failed'
                    }

                    let listView = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/div[1]/button[1]');
                    await listView.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 3000));

                    let listPost = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]');                
                    listPost = await listPost.getAttribute("class");

                    if (listPost.indexOf('table') > -1){
                        status += ' ,Posts displayed as List View'
                    }
                    else{
                        status += 'List View changes failed'
                    }

                    let regionSort = await page.findByCSS('#key-postsTable > table > thead > tr > th.key-table__headerRow.key-table__headerRow--user.key-table__headerRow--sortToggle');
                    await regionSort.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 3000));
                
                    let currentURL_Region = await driver.getCurrentUrl();
                    let params_Region = currentURL_Region.substring(currentURL_Region.indexOf("?")+1);
                    let firstParam_Region = params_Region.substr(0, params_Region.indexOf('%'));
                                        
                    if (firstParam_Region.indexOf('sort') > -1 && firstParam_Region.indexOf('state') > -1) {
        
                        steps += ' ,Order of region changed';
                    }

                    let countrySort = await page.findByCSS('#key-postsTable > table > thead > tr > th:nth-child(4)');
                    await countrySort.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 3000));
                    
                    let currentURL_Country = await driver.getCurrentUrl();
                        let params_Country = currentURL_Country.substring(currentURL_Country.indexOf("?")+1);
                        let firstParam_Counrty = params_Country.substr(0, params_Country.indexOf('%'));
                                            
                        if (firstParam_Counrty.indexOf('sort') > -1 && firstParam_Counrty.indexOf('country') > -1) {
                         
                            steps += ' ,Order of country changed';
                        }

                    let sentimentSort = await page.findByXPath('//*[@id="key-postsTable"]/table/thead/tr/th[7]');
                    await sentimentSort.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 3000));
                    
                    let currentURL_Sentiment = await driver.getCurrentUrl();
                    let params_Sentiment = currentURL_Sentiment.substring(currentURL_Sentiment.indexOf("?")+1);
                    let firstParam_Sentiment = params_Sentiment.substr(0, params_Sentiment.indexOf('%'));
                                        
                    if (firstParam_Sentiment.indexOf('sort') > -1 && firstParam_Sentiment.indexOf('sentiment') > -1) {

                        steps += ' ,Order of sentiment changed';
                    }

                    let dateSort = await page.findByXPath('//*[@id="key-postsTable"]/table/thead/tr/th[8]');
                    await dateSort.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]'), 3000));
                    
                    let currentURL_Date = await driver.getCurrentUrl();
                    let params_Date = currentURL_Date.substring(currentURL_Date.indexOf("?")+1);
                    let firstParam_Date = params_Date.substr(0, params_Date.indexOf('%'));
                                        
                    if (firstParam_Date.indexOf('sort') > -1 && firstParam_Date.indexOf('created_at') > -1) {

                        steps += ' ,Order of Date changed. ';
                    }

                    status += ' ,Changed the order of Region, Country, Sentiment or Date.'
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                            
                }
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
