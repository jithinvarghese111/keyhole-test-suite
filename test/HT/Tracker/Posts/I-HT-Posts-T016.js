/**
 * ### TEST CASE:
 * I-HT-Posts-016
 
 * ### TEST TITLE:
 * Sentiment persists to URL
 *
 * ### TEST SUMMARY:
 * When changing sentiment checkboxes, sentiment state should persist to the URL
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sentiment persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Sentiment persists to URL', async () => {
                await page.redirectToDashboard();

                let steps = '', status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to the Posts';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));

                let sentiment = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/button[1]');
                await sentiment.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/button[1]/ul/li[1]/label/div/img'), 5000));

                let totalSentiment = await page.findElementsByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > button.key-trackerSubHeaderAction__button.keyjs-postsActions--sentimentDropdown > ul > li > label > div > img');
                if (totalSentiment<2) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Test requires multiple Sentiment.'
                        }
                    });
                }
                else{
                    for (i= 1; i <= totalSentiment.length; i++) {

                        let sentimentName = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/button[1]/ul/li['+i+']/label/div/img');
                        sentimentName = await sentimentName.getAttribute("alt");
                        sentiment += sentimentName+', ';
                    }

                    if(sentiment.indexOf('positive') === -1){
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Test requires positive sentiment.'
                            }
                        });
                    }
                    else{

                        let positiveSentiment = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > button.key-trackerSubHeaderAction__button.keyjs-postsActions--sentimentDropdown > ul > li:nth-child(1) > label > div > img')
                        await positiveSentiment.click();
                        await driver.wait(until.elementLocated(By.id('filtersSentimentPositive'), 3000));

                        let currentURL = await driver.getCurrentUrl();
                        let params = currentURL.substring(currentURL.indexOf("?")+1);
                        let firstParam = params.substr(0, params.lastIndexOf('='));

                        if (firstParam.indexOf('sentiment') > -1 && firstParam.indexOf('positive') > -1) {

                            let status = '';

                            status += 'Sentiment added to URL., ';

                            await driver.navigate().refresh();
                            await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                            if (firstParam.indexOf('sentiment') > -1 && firstParam.indexOf('positive') > -1) {
                                status += 'Sentiment persisted upon reload.';
                            }

                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Current URL': currentURL,
                                    'Test status': status
                                }
                            });
                        }
                        else {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Sentiment not added to URL.'
                                }
                            });
                        }
                    }    
                }               
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
