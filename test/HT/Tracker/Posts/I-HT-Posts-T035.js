/**
 * ### TEST CASE:
 * I-HT-Posts-T035
 *
 * ### TEST TITLE:
 * Select Subtracker and check Tracker & suTracker name exists in Post content.
 *
 * ### TEST SUMMARY:
 * User should be able to  select Subtracker and check Tracker & suTracker name exists in Post content.
 *
 */


const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Select Subtracker and check Tracker & suTracker name exists in Post content.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Select Subtracker and check Tracker & suTracker name exists in Post content.', async () => {
                await page.redirectToHTDashboard();
                
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let checkSubTracker = await page.checkElementByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                if (checkSubTracker) {
                    let subTrack = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div.key-reactSelect--subTrackerDropdown.css-2b097c-container > div > div.select__value-container.css-1hwfws3 > div.select__placeholder.css-1wa3eu0-placeholder');
                    await subTrack.click();
                    await driver.sleep(1000);

                    let list = await page.findByCSS('#react-select-2-option-0');
                    let subTrackerName = await list.getText();
                    await list.click();
                    addContext(this.ctx, 'clicked on subTracker: '+subTrackerName);
                    await driver.sleep(2000);

                    let posts = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__ht > ul > nav > li:nth-child(2) > a');
                    await posts.click();
                    await driver.sleep(5000); 
                    
                    let totalPosts = await page.findByCSS('#hashtag-tracking > div > section.route-section > div > div > div > div.key-pagination__info > strong.key-pagination__total');
                    totalPosts = await totalPosts.getText();
                    totalPosts = totalPosts.replace (/,/g, "");
                    addContext(this.ctx, 'Total Pots with subTracker: '+totalPosts);

                    if(totalPosts > 10){

                        for (var i = 1; i <= 10;i++) {
                            let postClick = await page.findByCSS('#key-postsTable > table > tbody > tr:nth-child('+i+')');
                            await postClick.click();
                            await driver.sleep(2000);

                            let postContent = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/article/section[2]/div/p');
                            postContent = await postContent.getText();

                            if (postContent.IndexOf(subTrackerName) > - 1 && (postContent.IndexOf(trackerName) > - 1)) addContext(this.ctx, 'subTrackerName Exists in Post Content.'); else addContext(this.ctx, 'No subTracker Found in Post Content.');
                        }
                    }
 
                }else{
                    assert.fail('No subTracker Found');
                }
                               
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();