 /** 
 * ### TEST CASE:
 * I-HT-Posts-019
 
 * ### TEST TITLE:
 * Clear selected posts
 *
 * ### TEST SUMMARY:
 *  User should be able to select multiple posts and clear selection of the same.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Clear selected posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Select multiple posts and clear selection of the same', async () => {
                await page.redirectToHTDashboard();

                let steps = '', status = '';
                
                let firstTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                let trackerName = await firstTrack.getText();
                await firstTrack.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 2000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                steps += 'Navigated to the Posts';
                await driver.sleep(4000);

                let checkEle = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(1)');

                if (!checkEle) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Test requires Posts.'
                        }
                    });
                }
                else {
                    let totalPost = await page.findElementsByCSS('#key-postsTable > table > tbody > tr');

                    if (totalPost.length < 2) {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Test requires Multiple Posts.'
                            }
                        });
                    }
                    else {
                        let sentiEdit = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[7]/i');
                        await sentiEdit.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[7]/div/div[3]/button[1]'), 2000));

                        let sentiImage = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[7]/img');
                        sentiImage = await sentiImage.getAttribute("src");
                        let senti = sentiImage.substr(sentiImage.indexOf('_')+1);
                        let firstSentimentName = senti.substr(0, senti.lastIndexOf('.'));   
                       
                        let firstPostCheck = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div/label/input');
                        await firstPostCheck.click();                       
                        await driver.wait(until.elementLocated(By.id('bulk-manage__modal'), 4000));

                        for (let i = 2; i <= 4; i++) {
                            let secondPostCheck = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr['+i+']/td[1]');
                            await secondPostCheck.click();
                            await driver.sleep(1000);
                        }

                        let clearSelection = await page.findByCSS('#bulk-manage__modal > button.bulk-manage__button.bulk-manage__button--blue.js-bulk-manage__toggle');
                        await clearSelection.click();
                        await driver.sleep(1000);

                        let checkModal = await page.checkElementByCSS('.bulk-manage__modal--active');

                        let lastStatus = '';
                        if (checkModal) lastStatus = 'Clear selection failed';
                        else lastStatus = 'Clear selection success';

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': lastStatus
                            }
                        });
                    }
                }
            });
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
