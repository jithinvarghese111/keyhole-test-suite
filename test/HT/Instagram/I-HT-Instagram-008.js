/**
 * ### TEST CASE:
 * I-HT-Instagram-008
 *
 * ### TEST TITLE:
 * Profile data of instagram accounts posts from Sentiment
 *
 * ### TEST SUMMARY:
 * User is trying to check profile data of instagram accounts posts from Sentiment
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Profile data of instagram accounts posts from Sentiment', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Profile data of instagram accounts posts from Sentiment.', async () => {
                await page.redirectToDashboard();

                let status = '';
                let hash = await page.findByCSS('#items--restructured > a:nth-child(1) > div');
                await hash.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000)); 

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(1000);
                
                let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));
                
                let sentiment = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[5]/a');
                await sentiment.click();
                await driver.sleep(1000);

                let platforms = ['Twitter', 'Facebook'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await driver.sleep(1000);                    
                }
                await driver.navigate().refresh();
                await driver.sleep(8000);

                let checkPost = await page.checkElementByCSS('#trackerSentiment > div > p');
                if (!checkPost) {

                    await page.scrollPage('//*[@id="trackerSentiment"]/div/div/div[2]/h3');
                    await driver.sleep(1000);
                  
                    for (var i = 1; i <= 4; i++) {                    
                        let instaUser = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr['+i+']/td[1]/div/span/span[1]');
                        let instUsers = await instaUser.getText();

                        let instId = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr['+i+']/td[1]/div/span/span[2]');
                        let instaId = await instId.getText();

                        addContext(this.ctx, {
                            title: 'Other Context',
                            value: {
                                'Tracker name': trackerName,
                                'Instagram User Name': instUsers,
                                'Instagram user ID': instaId,
                                'Test status' : 'Instgram profile data displayed on Sentiment posts.'
                            }
                        }); 
                    }
                }
                else{
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'No posts found'
                        }
                    }); 
                }
                          
            });

            it('Checking profile data from Trending Topics with Load more posts.', async () => {
                await page.redirectToDashboard();

                let status = '';
                let hash = await page.findByCSS('#items--restructured > a:nth-child(1) > div');
                await hash.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000)); 

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 20000));
                
                let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let sentiment = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[5]/a');
                await sentiment.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div/h1/strong'), 20000));

                await driver.navigate().refresh();
                await driver.sleep(10000);
                
                let platforms = ['Twitter', 'Facebook', 'News', 'Blogs', 'Forums'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));                    
                }
                await driver.navigate().refresh();
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));
                let checkPost = await page.checkElementByCSS('#trackerSentiment > div > p');
                if (!checkPost) {
                    await page.scrollPage('//*[@id="posts-table"]/button');
                    await driver.sleep(1000);
                  
                    let loadmore = await page.findByXPath('//*[@id="posts-table"]/button');
                    await loadmore.click();
                    await driver.sleep(1000);

                    await page.scrollPage('//*[@id="trackerSentiment"]/div/div/div[2]/h3');
                    await driver.sleep(1000);
                    
                    let instaUser = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[1]/div/span/span[1]');
                    let instUsers = await instaUser.getText();

                    let instId = await page.findByXPath('//*[@id="posts-table"]/div/div/div/table/tbody/tr[1]/td[1]/div/span/span[2]');
                    let instaId = await instId.getText();

                    addContext(this.ctx, {
                        title: 'Test Status',
                        value: {
                            'Tracker name': trackerName,
                            'Instagram User Name': instUsers,
                            'Instagram user ID': instaId,
                            'Test status' : 'Instgram profile data displayed after load more posts.'
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Status',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'No posts found'
                        }
                    });
                }                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



