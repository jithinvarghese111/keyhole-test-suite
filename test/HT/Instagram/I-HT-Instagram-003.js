/**
 * ### TEST CASE:
 * I-HT-Instagram-003
 *
 * ### TEST TITLE:
 * Profile data of instagram post table popup.
 *
 * ### TEST SUMMARY:
 * User is trying to check profile data of instagram post table popup.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking profile data of instagram post table popup', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking profile data of instagram post table popup.', async () => {
                await page.redirectToDashboard();

                let status = '';
                let hash = await page.findByCSS('#items--restructured > a:nth-child(1) > div');
                await hash.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000));

                let pageName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                pageName = await pageName.getText();
                assert.equal(pageName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(1000);
                
                let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let platforms = ['Twitter', 'Facebook', 'News', 'Blogs', 'Forums'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));                   
                }
                await driver.navigate().refresh();
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

              
                let checkInstaPost = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--user.key-table__column--smolWidth > div');
                if (checkInstaPost) {

                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/button[1]/span');
                    await driver.sleep(1000);

                    let postClick = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div/span');
                    await postClick.click();
                    await driver.wait(until.elementLocated(By.css('#hashtag-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > article > section.key-sideviewPost__post.key-sideviewPost__post--horizontalSplit > div.key-sideviewPost__postText > a'), 20000));

                    let instaUser = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/div/div/div/div[2]/span[1]/a');
                    let instUsers = await instaUser.getText();

                    let instId = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/div/div/div/div[2]/span[2]/a');
                    let instaId = await instId.getText();

                    let postDate = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/article/section[1]/div[1]/span/span');
                    let PostsDate = await postDate.getText();

                    addContext(this.ctx, {
                        title: 'Test Status',
                        value: {
                            'Tracker name': trackerName,
                            'Instagram User Name': instUsers,
                            'Instagram user ID': instaId,
                            'Posted Date': PostsDate, 
                            'Test status' : 'Instgram profile data displayed on post popup.'
                        }
                    });
                }
                else{

                    addContext(this.ctx, {
                        title: 'Test Status',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'No Instagram Posts found.'

                        }
                    });
                } 
                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



