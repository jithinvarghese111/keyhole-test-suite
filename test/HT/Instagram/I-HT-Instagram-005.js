/**
 * ### TEST CASE:
 * I-HT-Instagram-005
 *
 * ### TEST TITLE:
 * Instagram accounts posts with no profile data.
 *
 * ### TEST SUMMARY:
 * User is trying to check instagram accounts posts with no profile data.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking instagram accounts posts with no profile data.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking instagram accounts posts with no profile data.', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hash = await page.findByCSS('#items--restructured > a:nth-child(1) > div');
                await hash.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000));

                let pageTitle = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                pageTitle = await pageTitle.getText();
                assert.equal(pageTitle, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(1000);
                
                let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let title = await driver.getTitle();
                expect(title).to.include(trackerName);

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let platforms = ['Twitter', 'Facebook', 'News', 'Blogs', 'Forums'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));                    
                }
                await driver.navigate().refresh();
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                let checkpost = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > div > div > div > div > div.key-chartEmptyState__title');
                
                if (!checkpost) {
                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/div/div/div/div[1]');
                    await driver.sleep(2000);

                    let pagination = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/div/div/div/div[2]/button[4]');
                    await pagination.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div/span/span[1]'), 20000));
                    
                    let instaUser = await page.findByXPath('//*[@id="key-postsTable"]/table/tbody/tr[1]/td[1]/div/span/span[1]');
                    let instUsers = await instaUser.getText();                                   
                    
                    if (instUsers.indexOf('') > -1){
                        status += 'Instagram posts have no user name';
                    }
                    else{
                        status += 'Instagram User Name displayed :'+instUsers;
                    }
                    await driver.sleep(2000);

                    let checkuserId = await page.checkElementByCSS('#key-postsTable > table > tbody > tr:nth-child(1) > td.key-table__column.key-table__column--user.key-table__column--smolWidth > div > span > span.key-table__userInfoUsername');
                    if (!checkuserId) {
                       status += ' ,Instagram posts have no user Id';
                    }
                    else{

                        let instId = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[1]/div[2]/div[1]/div[2]/span[2]');
                        let instaId = await instId.getText();
                        status += ' ,Instagram User Id displayed :'+instaId;                   
                    }

                    addContext(this.ctx, {
                        title: 'Test Status',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': status

                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Status',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'No posts found'

                        }
                    });
                }
                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



