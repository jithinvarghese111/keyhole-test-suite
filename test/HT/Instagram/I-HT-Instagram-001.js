/**
 * ### TEST CASE:
 * I-HT-Instagram-001
 *
 * ### TEST TITLE:
 * Profile data of instagram accounts posts from Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to check profile data of instagram accounts posts from Dashboard
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking profile data of instagram accounts posts from Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking profile data of Tops posts - instagram Accounts.', async () => {
                await page.redirectToDashboard();

                let status = '';
                let hash = await page.findByCSS('#items--restructured > a:nth-child(1) > div');
                await hash.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000));
                
                let pageName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                pageName = await pageName.getText();
                assert.equal(pageName, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(1000);

                let checkinst = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > span > i.fa.fa-instagram');
                if (checkinst) {
                    let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                    let trackerName = await firstTracker.getText();
                    await firstTracker.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                    let platforms = ['Twitter', 'Facebook', 'News', 'Blogs', 'Forums'];

                    for (i = 0; i < platforms.length; i++) {
                        let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                        await platformClick.click();
                        await driver.wait(until.elementLocated(By.id('twt_check'), 20000));
                        
                    }
                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                    checkPosts = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > div > div.key-chartEmptyState__title');
                    if (!checkPosts) {

                        await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[2]/div[2]/div[2]/p');
                        await driver.sleep(3000);
                        
                        let checkInstname = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-postRows__wrapper > div > div.key-postRow__wrapper > div > div:nth-child(1) > p.key-postInfo__username');
                        if (checkInstname) { 

                            let totalinstName = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-postRows__wrapper > div > div.key-postRow__wrapper > div > div:nth-child(1) > p.key-postInfo__username');
                            for (var i = 1; i <= totalinstName.length; i++) {
                                let instName = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[2]/div[1]/div[2]/div[1]/div[1]/div['+i+']/div[2]/div/div[1]/p[1]');
                                let instaText = await instName.getText();

                                let postDate = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[2]/div[1]/div[2]/div[1]/div[1]/div['+i+']/div[2]/div/div[1]/p[2]');
                                let dateName = await postDate.getText();

                                addContext(this.ctx, {
                                    title: 'Other Context',
                                    value: {
                                        'Tracker name': trackerName,
                                        'Displayed Instagram Account Names': instaText,
                                        'Posted Date': dateName
                                    }
                                });
                            }
                        }
                        else{

                            addContext(this.ctx, {
                                title: 'Other Context',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Instagram Account profile details are not provided'
                                }
                            });
                        }
                        await driver.sleep(2000);

                        let mostRecent = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div:nth-child(2)');
                        await mostRecent.click();
                        await driver.sleep(2000);

                        let checkinstprofile = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(2) > div.key-postRows__wrapper > div:nth-child(1) > div.key-profilePicture.key-profilePicture--instagram.key-profilePicture--error');
                        if (checkinstprofile) {

                            let totalinstposts = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--topPosts > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(2) > div.key-postRows__wrapper > div > div.key-postRow__wrapper > div > div:nth-child(1) > p.key-postInfo__username');
                            for (var j = 1; j <= totalinstposts.length; j++) {
                                let instRecentName = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[2]/div[1]/div[2]/div[2]/div[1]/div['+j+']/div[2]/div/div[1]/p[1]');
                                let instaAccount = await instRecentName.getText();

                                let postsDate = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[3]/div[2]/div[1]/div[2]/div[2]/div[1]/div['+j+']/div[2]/div/div[1]/p[2]');
                                let datename = await postsDate.getText();

                                addContext(this.ctx, {
                                    title: 'Other Context',
                                    value: {
                                        'Tracker name': trackerName,
                                        'Displayed Instagram Account Names': instaAccount,
                                        'Posted Date': datename
                                    }
                                });
                            }
                        }
                        else{
                            addContext(this.ctx, {
                                title: 'Other Context',
                                value: {
                                    'Test status': 'No other instgram posts found in Most Recent section.'
                                }
                            });  
                        }                    
                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Other Context',
                            value: {
                                'Test status': 'No posts found.'
                            }
                        });
                    } 
                }
                else{
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Test status': 'No instagram Tracker found'
                        }
                    });
                }
                                             
            });

            it('Checking profile data of Influential Users - instagram Accounts.', async () => {
                await page.redirectToDashboard();

                let hash = await page.findByCSS('#items--restructured > a:nth-child(1) > div');
                await hash.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000)); 

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(2000);

                let checkinst = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > span > i.fa.fa-instagram');
                if (checkinst) {
                    let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                    let trackerName = await firstTracker.getText();
                    await firstTracker.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                    let platforms = ['Twitter', 'Facebook', 'News', 'Blogs', 'Forums'];

                    for (i = 0; i < platforms.length; i++) {
                        let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                        await platformClick.click();
                        await driver.wait(until.elementLocated(By.id('twt_check'), 20000));
                        
                    }
            
                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                    checkPosts = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > div > div.key-chartEmptyState__title');
                    if (!checkPosts) {

                        await page.scrollPage('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[2]/div[2]/div[2]/p');
                        await driver.sleep(2000);

                        let checkInfluential = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartHeading > h3 > span');
                        if (checkInfluential) {

                            let checkInstdetails = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-chart--influencers > div > div:nth-child(1) > div > div > div > p');
                            if (checkInstdetails) {
                                let totalinstName = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-chart--influencers > div > div:nth-child(1) > div > div > div > p');

                                for (var i = 1; i <= totalinstName.length; i++) {
                                    let instinfluName = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[1]/div['+i+']/div/div/p');
                                    let instaInfluencers = await instinfluName.getText();

                                    let instFollowers = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[1]/div['+i+']/div/p[1]');
                                    let followers = await instFollowers.getText();

                                    let totalEng= await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[1]/div['+i+']/div/p[2]');
                                    let engCount = await totalEng.getText();

                                    addContext(this.ctx, {
                                        title: 'Other Context',
                                        value: {
                                            'Tracker name': trackerName,
                                            'Displayed Instagram Influential Users': instaInfluencers,
                                            'Instagram Followers': followers,
                                            'Total Posts': engCount
                                        }
                                    });
                                }

                                let totalsecondinst = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-chart--influencers > div > div:nth-child(2) > div > div > div > p');

                                for (var j = 1; j <= totalsecondinst.length; j++) {
                                    let instinflu = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[2]/div['+j+']/div/div/p');
                                    let instaInfluText = await instinflu.getText();

                                    let instFollow = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[2]/div['+j+']/div/p[1]');
                                    let follow = await instFollow.getText();

                                    let totaleng= await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[2]/div['+j+']/div/p[2]');
                                    let engcount = await totaleng.getText();

                                    addContext(this.ctx, {
                                        title: 'Other Context',
                                        value: {
                                            'Tracker name': trackerName,
                                            'Displayed Instagram Influential Users': instaInfluText,
                                            'Instagram Followers': follow,
                                            'Total Engagements': engcount
                                        }
                                    });
                                }

                                let totalthirdinst = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(1) > div.key-chart--influencers > div > div:nth-child(3) > div > div > div > p');

                                for (var k = 1; k <= totalthirdinst.length; k++) {
                                    let instinfluthird = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[3]/div['+k+']/div/div/p');
                                    let instaInfluthirdText = await instinfluthird.getText();
                                    await driver.sleep(1000);

                                    let instthircFollow = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[3]/div['+k+']/div/p[1]');
                                    let thirdfollowers = await instthircFollow.getText();
                                    await driver.sleep(2000);

                                    let totalthirdeng= await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[3]/div['+k+']/div/p[2]');
                                    let engthirdcount = await totalthirdeng.getText();
                                    await driver.sleep(2000);

                                    addContext(this.ctx, {
                                        title: 'Other Context',
                                        value: {
                                            'Tracker name': trackerName,
                                            'Displayed Instagram Influential Users': instaInfluthirdText,
                                            'Instagram Followers': thirdfollowers,
                                            'Total Posts': engthirdcount
                                        }
                                    });
                                }

                            }
                            else{
                                addContext(this.ctx, {
                                    title: 'Other Context',
                                    value: {
                                        'Tracker name': trackerName,
                                        'Test status': 'Instagram Account details are not provided'
                                    }
                                });
                            }
                            
                            let mostFrq = await page.findByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__header > div > div > div > div:nth-child(2)');
                            await mostFrq.click();
                            await driver.sleep(2000);

                            let checkInstadetail = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(2) > div.key-chart--influencers > div > div:nth-child(1) > div > div > div > p');
                            if (checkInstadetail) {
                                let totalPosts = await page.findElementsByCSS('#hashtag-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--sizeLarge.key-chartBox--influencers > div.key-chartContainer > div.el-tabs > div.el-tabs__content > div:nth-child(2) > div.key-chart--influencers > div > div:nth-child(1) > div > div > div > p');
                                for (var l = 1; l <= totalPosts.length; l++) {
                                    let instfrqName = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[2]/div[1]/div/div[1]/div['+l+']/div/div/p');
                                    let instaUser = await instfrqName.getText();

                                    let instaFollowers = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[2]/div[1]/div/div[1]/div['+l+']/div/p[1]');
                                    let insfollowers = await instaFollowers.getText();

                                    let totalpost = await page.findByXPath('//*[@id="hashtag-tracking"]/div/section[1]/section/div/figure[5]/div[2]/div[1]/div[2]/div[2]/div[1]/div/div[1]/div['+l+']/div/p[2]');
                                    let totalcount = await totalpost.getText();

                                    addContext(this.ctx, {
                                        title: 'Other Context',
                                        value: {
                                            'Tracker name': trackerName,
                                            'Displayed Instagram Influential Users': instaUser,
                                            'Instagram Followers': insfollowers,
                                            'Total Posts': totalcount
                                        }
                                    });
                                }                                     
                            }
                            else{
                                addContext(this.ctx, {
                                    title: 'Other Context',
                                    value: {
                                        'Tracker name': trackerName,
                                        'Test status': 'Instagram Account details are not provided'
                                    }
                                });
                            }
                        }
                        else{

                            addContext(this.ctx, {
                                title: 'Other Context',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Influential Users are not found'
                                }
                            });
                        } 
                        
                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Other Context',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'No posts found'
                            }
                        });
                    }
                }
                else{
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Test status': 'No instagram Tracker found'
                        }
                    });
                } 
                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



