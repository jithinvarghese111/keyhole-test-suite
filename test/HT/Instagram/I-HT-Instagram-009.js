/**
 * ### TEST CASE:
 * I-HT-Instagram-009
 *
 * ### TEST TITLE:
 * Profile data of instagram posts from media wall
 *
 * ### TEST SUMMARY:
 * User is trying to check profile data of instagram posts from media wall
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Profile data of instagram posts from media wall', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Profile data of instagram posts from media wall.', async () => {
                await page.redirectToDashboard();

                let status = '';
                let hash = await page.findByCSS('#items--restructured > a:nth-child(1) > div');
                await hash.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(1000);
                
                let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));
                
                let mediaWall = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[6]/a');
                await mediaWall.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let platforms = ['Twitter', 'Facebook'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));                    
                }

                let checkPost = await page.checkElementByCSS('#media-wall > div.wallContent__wrapper > div > p');
                if (!checkPost) {
                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/button[1]/span');
                    await driver.sleep(1000);
                  
                    for (var i = 1; i <= 3; i++) {                    
                        let instId = await page.findByXPath('//*[@id="media-wall"]/div[2]/div['+i+']/div/div[2]/div/div/div[2]/div[1]');
                        let instaId = await instId.getText();
                        status = 'Displayed profile Id of Instagram posts on media wall:'+instaId;

                        addContext(this.ctx, {
                            title: 'Other Context',
                            value: {
                                'Tracker name': trackerName,
                                'Test status' : status
                            }
                        }); 
                    }
                }
                else{
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Tracker name': trackerName,
                            'Test status' : 'No posts found.'
                        }
                    }); 
                }
                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



