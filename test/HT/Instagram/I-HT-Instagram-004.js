/**
 * ### TEST CASE:
 * I-HT-Instagram-004
 *
 * ### TEST TITLE:
 * Profile data of instagram accounts posts from Posts card view
 *
 * ### TEST SUMMARY:
 * User is trying to check profile data of instagram accounts posts from Posts card view
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking profile data of instagram accounts posts from Posts card view', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking profile data of instagram accounts posts from Posts card view.', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hash = await page.findByCSS('#items--restructured > a:nth-child(1) > div');
                await hash.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000));

                let pageTitle = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                pageTitle = await pageTitle.getText();
                assert.equal(pageTitle, 'Hashtag & Keyword Tracking');

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(1000);
                
                let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTracker.getText();
                await firstTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let posts = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/nav/li[2]/a/span');
                await posts.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 20000));

                let platforms = ['Twitter', 'Facebook', 'News', 'Blogs', 'Forums'];

                for (i = 0; i < platforms.length; i++) {
                    let platformClick = await page.findByCSS('#hashtag-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox[title="'+platforms[i]+'"]');
                    await platformClick.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));                    
                }
                await driver.navigate().refresh();
                await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                let checkPost = await page.checkElementByCSS('#hashtag-tracking > div > section.route-section > div > div > div > div > div.key-chartEmptyState__title');
                if (!checkPost) {

                    let cardView = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[2]/div[1]/button[2]');
                    await cardView.click();
                    await driver.sleep(1000);

                    await page.scrollPage('//*[@id="hashtag-tracking"]/div/div[1]/div[2]/div[1]/button[1]/span');
                    await driver.sleep(1000);
                  
                    for (var i = 1; i <= 3; i++) {                    
                        let instaUser = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article['+i+']/div[1]/div[2]/div[1]/div[2]/span[1]');
                        let instUsers = await instaUser.getText();

                        let instId = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article['+i+']/div[1]/div[2]/div[1]/div[2]/span[2]');
                        let instaId = await instId.getText();

                        let postDate = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article['+i+']/div[2]/span[2]/span');
                        let PostsDate = await postDate.getText();

                        addContext(this.ctx, {
                            title: 'Other Context',
                            value: {
                                'Tracker name': trackerName,
                                'Instagram User Name': instUsers,
                                'Instagram user ID': instaId,
                                'Posted Date': PostsDate,
                                'Test status' : 'Instgram profile data Successfully displayed'
                            }
                        }); 
                    }                

                    let checkInstaPost = await page.checkElementByCSS('#key-postsMasonry > div > article:nth-child(1) > div.key-masonryCard__content.key-masonryCard__content--post > div.key-masonryCard__media');
                    if (checkInstaPost) {

                        let cardPost = await page.findByXPath('//*[@id="key-postsMasonry"]/div/article[1]/div[1]/div[1]/img');
                        await cardPost.click();
                        await driver.sleep();

                        let instUser = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/div/div/div/div[2]/span[1]/a');
                        let intUsers = await instUser.getText();

                        let instaId = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/div/div/div/div[2]/span[2]/a');
                        let intId = await instaId.getText();

                        let postdate = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[3]/article/section[1]/div[1]/span/span');
                        let PostDate = await postdate.getText();

                        addContext(this.ctx, {
                            title: 'Test Status',
                            value: {
                                'Tracker name': trackerName,
                                'Instagram User Name': intUsers,
                                'Instagram user ID': intId,
                                'Posted Date': PostDate, 
                                'Test status' : 'Instgram profile data displayed on card view post popup.'
                            }
                        });
                    }
                    else{

                        addContext(this.ctx, {
                            title: 'Test Status',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'No Instagram Posts found.'

                            }
                        });
                    }
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Status',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'No posts found.'

                        }
                    });
                } 
                          
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();



