/**
 * ### TEST CASE:
 * I-HT-Group-032
 *
 * ### TEST TITLE:
 * Downloading PDF - group Comparison.
 *
 * ### TEST SUMMARY:
 * user should be able to download an pdf file of group comparison information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const util = require('util');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading XLS - group Comparison for HT.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('download an excel file of group comparison information', async () => {
                await page.redirectToHTDashboard();

                let clickGroup = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');

                if (checkGroup) {

                    let test_result = '';
                    
                    let compareGroup = await page.findByCSS('#hashtag-trackers > div.track-info.track-info--groups.track-info--HT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup');
                    await compareGroup.click();
                    await driver.sleep(1000);

                    for (var i = 1; i <= 3; i++) {
                        let select = await page.findByCSS('#hashtag-trackers > div.track-info.track-info--groups.track-info--HT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li:nth-child('+i+')');
                        let groups = await select.getText();
                        await select.click();
                        addContext(this.ctx, 'Group for comparison: '+groups);
                        await driver.sleep(1000);
                    }

                    let goTo = await page.findByCSS('#hashtag-trackers > div.track-info.track-info--groups.track-info--HT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li.dropdown__strong.js-compareGroup.group__dashboardLink ');
                    await goTo.click();
                    await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let pdf = await page.findById('pdf');
                    await pdf.click();
                    await driver.wait(until.elementLocated(By.css('#pdf_confirm'), 20000));

                    let pdfConfirm = await page.findById('pdf_confirm');
                    await pdfConfirm.click();
                    // This process sometimes takes more than 30 seconds
                    await driver.sleep(20000);

                    let checkEle = await page.checkElementByCSS('#pdf_msg > a');

                    if (checkEle) {
                        let viewPdf = await page.findByCSS('#pdf_msg > a');
                        await viewPdf.click();

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });
                        await driver.switchTo().window(tab1);
                        await driver.wait(until.elementLocated(By.css('#pdf_modal > div.modal-body > header > p'), 3000));

                        addContext(this.ctx, {
                            title: 'other Context',
                            value: {
                                'Test status': 'Download file'
                            }
                        });
                    } else assert.fail('Failed Downloading PDF File.');

                } else assert.fail('No groups available');
            
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();