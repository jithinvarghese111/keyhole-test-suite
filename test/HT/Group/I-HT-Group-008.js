/**
 * ### TEST CASE:
 * I-HT-Group-008
 *
 * ### TEST TITLE:
 * Hide Tracker - group Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to Hide Tracker.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Hide Tracker - group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Hide Tracker ', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a'), 2000));

                let groupDashboard = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a');
                let GroupName = await groupDashboard.getText();
                await groupDashboard.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let checkShow = await page.checkElementByCSS('#tracker-group > div > div > div:nth-child(1) > div.key-header__listToggle');
                if (checkShow) {
                    let showMore = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div[2]');
                    await showMore.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div[2]'), 3000));
                }

                let totalGroup = await page.findElementsByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > div');
                console.log(totalGroup.length);
                for (var i = 1; i < totalGroup.length; i++) { 

                    let groupClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/div');
                    await groupClick.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/ul/li[1]'), 3000));

                    let hide = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/ul/li[3]');
                    await hide.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/div/div[3]'), 3000));                                    
                }

                let trackerhide = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/div');
                let checkHide = await trackerhide.getAttribute("class");
                await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > section > div:nth-child(1) > div'), 3000));

                if (checkHide.indexOf('inactive') > -1){

                    status += 'Tracker hidden successfully., ';
                }
                else{
                    status += 'Test failed.'
                }
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Group name': GroupName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();