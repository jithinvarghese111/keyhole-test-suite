/**
 * ### TEST CASE:
 * I-HT-Group-011
 *
 * ### TEST TITLE:
 * Checking total Tracker and posts count - group Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to Check total Tracker and posts count.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking total Tracker and posts count', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking total Tracker and posts count ', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '', totalpostsCount = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupDashboard = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a');
                let GroupName = await groupDashboard.getText();
                await groupDashboard.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let checkShow = await page.checkElementByCSS('#tracker-group > div > div > div:nth-child(1) > div.key-header__listToggle');
                if (checkShow) {
                    let showMore = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div[2]');
                    await showMore.click();
                }

                let totalGroup = await page.findElementsByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > div');
                console.log(totalGroup.length);
                for (var i = 1; i < totalGroup.length; i++) { 

                    let totalTracker = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/div/div[2]/span');
                    let trackerText = await totalTracker.getText();
                
                    let postCount = await page.findByXPath('//*[@id="tracker-group-timeline"]/div[2]/div[1]/div['+i+']/div[2]');
                    let totalpostsCount = await postCount.getText();
                   
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group name': GroupName,
                            'Test steps': steps,
                            'Total Trackers under this Group': trackerText,
                            'Total posts for each tracker under this Group': totalpostsCount                                                
                        }
                    });                  
                
                }                
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();