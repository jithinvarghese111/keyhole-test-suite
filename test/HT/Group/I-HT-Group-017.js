/**
 * ### TEST CASE:
 * I-HT-Group-017
 *
 * ### TEST TITLE:
 * Intract with Top Sources - Group Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to Intract with Top Sources from Group Dashboard.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Intract with Top Sources - Group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Intract with Top Sources', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                let firstGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[2]/td/div[1]/a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                await page.scrollPixel(2200);
                await driver.sleep(3000);
                                
                let totalcheckbox = await page.findElementsByCSS('#top-sources > div.key-chartContainer > div > div.key-chartsLegend.key-chartsLegend--topSources > div > div.key-chartsLegend__item--before');
                for (var i = 1; i <= totalcheckbox.length; i++) {
                    let checktracker = await page.findByXPath('//*[@id="top-sources"]/div[2]/div/div[2]/div['+i+']/div[2]');
                    let hidetrackername = await checktracker.getText();
                    await checktracker.click();                    
                    status = 'Hide data: '+hidetrackername;
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="top-sources"]/div[2]/div/div[2]/div[1]/div[2]'), 2000));
        
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group Name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });

                }

                let totalPosts = await page.findByXPath('//*[@id="top-sources"]/div[1]/div/div[1]/ul/li[1]');
                await totalPosts.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="top-sources"]/div[2]/div/div[2]/div[1]/div[2]'), 2000));

                let totalcheckboxes = await page.findElementsByCSS('#top-sources > div.key-chartContainer > div > div.key-chartsLegend.key-chartsLegend--topSources > div > div.key-chartsLegend__item--before');
                for (var i = 1; i <= totalcheckboxes.length; i++) {
                    let checkTracker = await page.findByXPath('//*[@id="top-sources"]/div[2]/div/div[2]/div['+i+']/div[2]');
                    let hidetrackerName = await checkTracker.getText();
                    await checkTracker.click();
                    status = 'show data: '+hidetrackerName;
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="top-sources"]/div[2]/div/div[2]/div[1]/div[2]'), 2000));
                    
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group Name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });

                }

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();