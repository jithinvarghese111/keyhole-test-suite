/**
 * ### TEST CASE:
 * I-HT-Group-026
 *
 * ### TEST TITLE:
 * Post Timeline Checking  - condensed group dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to interact with Post Timeline from condensed Group Dashboard.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Post Timeline Checking  - condensed group dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Post Timeline checking from condensed Group Dashboard.', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                let firstGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[2]/td/div[1]/a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]');
                await gotoCondensed.click();
                steps += 'Clicked on Condensed group dashboard';
                await driver.wait(until.elementLocated(By.id('twt_check'), 2000));

                await page.scrollPage('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group-timeline"]/div[1]/h3'), 2000));
                
                let checkPosts = await page.findElementsByCSS('#tracker-group-timeline > div.key-chartContainer > div.key-chartsLegend > div > div.key-chartsLegend__item--before');
                for (var i = 1; i <= checkPosts.length; i++) {
                    let checkbox = await page.findByXPath('//*[@id="tracker-group-timeline"]/div[2]/div[1]/div['+i+']/div[1]');
                    await checkbox.click();                    
                }
                let checkele = await page.checkElementByCSS('#chart__tracker-group-timeline > div.chart--noData > div');
                if (checkele) {
                    postcheck = 'Post Unchecked Successfully';
                }
                else{
                    postcheck = 'test failed.';
                }
                addContext(this.ctx, {
                    title: 'Other Context',
                    value: {
                        'Test steps': steps,
                        'Test status': postcheck
                    }
                });

                let checktotal = await page.checkElementByCSS('#tracker-group-timeline > div.key-chartHeading > div > div.key-toggle.keyjs-toggle.keyjs-postsToggle.keyjs-postsToggle--timeline > ul > li.key-postsToggle.null');
                if (checktotal) {   
                    let postTotal = await page.findByXPath('//*[@id="tracker-group-timeline"]/div[1]/div/div[2]/ul/li[2]');
                    await postTotal.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group-timeline"]/div[2]/div[1]/div/div[2]'), 3000));

                    let checkPercent = await page.findElementsByCSS('#tracker-group-timeline > div.key-chartContainer > div.key-chartsLegend > div > div.key-chartsLegend__total');
                    for (var i = 1; i <= checkPercent.length; i++) {
                        let checkboxes = await page.findByXPath(' //*[@id="tracker-group-timeline"]/div[2]/div[1]/div['+i+']/div[1]');
                        await checkboxes.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="chart__tracker-group-timeline"]/div'), 3000));

                        let postpercent = await page.findByXPath(' //*[@id="tracker-group-timeline"]/div[2]/div[1]/div['+i+']/div[2]');
                        let postPercentValue = await postpercent.getText();
                        status = ' ,Post with percentage: '+postPercentValue;
                                            
                        addContext(this.ctx, {
                            title: 'Other Context',
                            value: {
                                'Group name': groupName,
                                'Test steps': steps,
                                'Test status': status
                            }
                        });

                    }
                    
                }      
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();