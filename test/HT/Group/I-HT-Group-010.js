/**
 * ### TEST CASE:
 * I-HT-Group-010
 *
 * ### TEST TITLE:
 * Custom date range persists to URL
 *
 * ### TEST SUMMARY:
 * When selecting a Custom date range in the date dropdown, the Custom date range persists to URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Custom date range persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Custom date range persists to URL', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let firstTrack = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a'), 2000));

                let groupDashboard = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a');
                let GroupName = await groupDashboard.getText();
                await groupDashboard.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let checkShow = await page.checkElementByCSS('#tracker-group > div > div > div:nth-child(1) > div.key-header__listToggle');
                if (checkShow) {
                    let showMore = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div[2]');
                    await showMore.click();
                }
                
                let groupClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/div');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/ul/li[1]'), 2000));

                let filterDate = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/ul/li[1]');
                await filterDate.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/section[2]/div/div[2]/div[1]'), 2000));

                let pickDate = await page.findByXPath('//*[@id="tracker-group"]/div/div/section[2]/div/div[2]/div[2]/span/div/input');
                await pickDate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[12]/div/div/div[2]/div[1]'), 2000));

                let pageClick = await randomNumberFromTo(4,5);
                
                let startdate = await page.findByXPath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[2]/td['+pageClick+']');
                await startdate.click();
               
                let pageClicks = await randomNumberFromTo(1,4);
                
                let enddate = await page.findByXPath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[6]/td['+pageClicks+']');
                await enddate.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/section[2]/div/div[2]/div[2]/p'), 3000));

                let setFilter = await page.findByXPath('//*[@id="tracker-group"]/div/div/section[2]/div/div[2]/div[3]/button[2]');
                await setFilter.click();
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));
                            
                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.indexOf('%'));
                
                if (firstParam.indexOf('custom') > -1) {
                    let status = '';

                    status += 'Custom Date Range added to URL., ';

                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                    if (firstParam.indexOf('custom') > -1) {
                        status += 'Custom Date Range persisted upon reload.';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Group name': GroupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Date not added to URL.'
                        }
                    });
                }                  
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();