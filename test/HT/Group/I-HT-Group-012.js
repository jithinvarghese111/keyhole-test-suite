/**
 * ### TEST CASE:
 * I-HT-Group-012
 *
 * ### TEST TITLE:
 * Navigate to Tracker dashboard from group Dashboard
 *
 * ### TEST SUMMARY:
 * Navigate to Tracker dashboard from group Dashboard.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to Tracker dashboard from group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Navigate to Tracker dashboard from group Dashboard ', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '', trackerName = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupDashboard = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a');
                let GroupName = await groupDashboard.getText();
                await groupDashboard.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let groupClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/div/div[2]/span');
                let trackername = await groupClick.getText();
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/ul/li[4]'), 3000));

                let gotoTracker = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div[1]/ul/li[4]');
                await gotoTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000)); 

                let trackertext = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1');
                let trackerText = await trackertext.getAttribute("title");
                assert.equal(trackerText, 'Real-time Tracker: peace');

                let title = await driver.getTitle();
                expect('Real-time social media analytics for peace, world, olive - Keyhole').to.include('peace');
                status += 'Navigated to tracker Dashboard'; 
                                                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': GroupName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();