/**
 * ### TEST CASE:
 * I-HT-Group-004
 *
 * ### TEST TITLE:
 * Go to group dashboard and check tracker details
 *
 * ### TEST SUMMARY:
 * Go to multiple group dashboard and checking the trcaker details
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Go to Group Dasbhoard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to group dashboard and return to home page', async () => {
                await page.redirectToDashboard();

                let group_name = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[3]/div[2]/button'), 3000));

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                	let totalGroups = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalGroups = totalGroups.length;

                    if (totalGroups > 3)
                    	totalGroups = 3;

                    for (let i = 1; i <= totalGroups; i++) {
		                let goTo = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr['+i+']/td/div[1]/a');
		                await goTo.click();
		                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));

		                let groupName = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong');
		                groupName = await groupName.getText();
		                group_name += groupName+', ';

		                let dashClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a/span');
		                await dashClick.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));
		            }

		            addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': group_name.replace(/,\s*$/, "")
                        }
                    });
	            }
	            else {
	            	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
	            }
            });

            it('Go to group dashboard and check group', async () => {
                await page.redirectToDashboard();

                let group_name = '', total_tracker = '', tracker_name = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[3]/div[2]/button'), 3000));

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                	let totalGroups = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalGroups = totalGroups.length;

                    if (totalGroups > 3)
                    	totalGroups = 3;

                    for (let i = 1; i <= totalGroups; i++) {
		                let goTo = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr['+i+']/td/div[1]/a');
		                await goTo.click();
		                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));

		                let groupName = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong');
		                groupName = await groupName.getText();
		                group_name += groupName+', ';

                        let checkTracker = await page.checkElementByCSS('#tracker-group > div > div > div:nth-child(1) > section > div');

                        if (checkTracker) {
    		                let totalTracker = await page.findElementsByCSS('#tracker-group > div > div > div:nth-child(1) > section > div');
    		                totalTracker = totalTracker.length;
    		                total_tracker += totalTracker+', ';

    		                tracker_name += '[';
    		                for (let j = 1; j <= totalTracker; j++) {
    		                	let trackerName = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div:nth-child('+j+') > div > div.trackerColumn__wrapper > span');
    		                	trackerName = await trackerName.getText();
    		                	tracker_name += trackerName+',';
    		                }
    		                tracker_name += '], ';

    		                let dashClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a/span');
    		                await dashClick.click();
    		                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));
                        }
                        else {
                            total_tracker += 0+', ';
                            tracker_name = 'Tracker not available';

                            let dashClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a/span');
                            await dashClick.click();
                            await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));
                        }
		            }

		            addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': group_name.replace(/,\s*$/, ""),
                            'Total tracker': total_tracker.replace(/,\s*$/, ""),
                            'Tracker name': tracker_name.replace(/,\s*$/, "")
                        }
                    });
	            }
	            else {
	            	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
	            }
            });

            it('Go to group dashboard and check PDF, XLS dowload', async () => {
                await page.redirectToDashboard();

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[3]/div[2]/button'), 3000));

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                	let goTo = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                    let groupLink = await goTo.getAttribute("href");
                    expect(groupLink).to.equal('https://keyhole.co/tracker-groups/wseOT7');
                    await goTo.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));

                	let groupName = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong');
		            groupName = await groupName.getText();
                    assert.equal(groupName, 'uxsscwav');

		            let downloadPDF = await page.findById('pdf');
		            await downloadPDF.click();
		            await driver.wait(until.elementLocated(By.id('pdf_confirm'), 3000));

		            let waitPDF = await page.findById('pdf_confirm');
		            await waitPDF.click();
		            await driver.wait(until.elementLocated(By.xpath('//*[@id="pdf_msg"]/a'), 3000));

		            let finalPDF = await page.findByXPath('//*[@id="pdf_msg"]/a');
		            await finalPDF.click();

                    let tab1, tab2;

                    await driver.getAllWindowHandles().then(function(windowHandles) {
                        tab1 = windowHandles[0];
                        tab2 = windowHandles[1];                
                    });

                    await driver.switchTo().window(tab1);
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));

		            let cancelPDF = await page.findById('pdf_cancel');
		            await cancelPDF.click();
		            await driver.wait(until.elementLocated(By.xpath('//*[@id="update"]/span'), 3000));

		            let downloadXLS = await page.findById('update');
		            await downloadXLS.click();
		            await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 4000));

		            addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'PDF status': 'Download success',
                            'XLS status': 'Dowload success'
                        }
                    });
	            }
	            else {
	            	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
	            }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();