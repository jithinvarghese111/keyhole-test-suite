/**
 * ### TEST CASE:
 * I-HT-Group-021
 *
 * ### TEST TITLE:
 * Preset date persists to URL - condensed group dashboard
 *
 * ### TEST SUMMARY:
 * When selecting a preset date in the date dropdown, the preset should be persisted in the URL.
 (Now it can be test only in qa@wdstech.com )
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Preset date persists to URL - condensed group dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Preset date persists to URL in condensed group dashboard', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';
                
                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                let firstGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[2]/td/div[1]/a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]');
                await gotoCondensed.click();
                steps += 'Clicked on Condensed group dashboard';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let pickDate = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/div/div[2]/span/div');
                await pickDate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[12]/div/div/div[2]'), 3000));
             
                let startdate = await page.findByXPath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[2]/td[4]');
                await startdate.click();
                await driver.wait(until.elementLocated(By.xpath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[2]/td[4]'), 3000));

                let enddate = await page.findByXPath('/html/body/div[12]/div/div/div[2]/div[1]/table/tbody/tr[6]/td[3]');
                await enddate.click();
                               
                let currentURL = await driver.getCurrentUrl();
                let params = currentURL.substring(currentURL.indexOf("?")+1);
                let firstParam = params.substr(0, params.lastIndexOf('='));
                
                if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end') > -1) {
                    let status = '';

                    status += 'Date added to URL., ';

                    await driver.navigate().refresh();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 1000));

                    if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end') > -1) {
                        status += 'Date persisted upon reload.';
                    }
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Test status': 'Date not added to URL.'
                        }
                    });
                }                  
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();