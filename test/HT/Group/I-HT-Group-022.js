/**
 * ### TEST CASE:
 * I-HT-Group-022
 *
 * ### TEST TITLE:
 * Platform Filters - condensed group dashboard
 *
 * ### TEST SUMMARY:
 * Platform checkboxes should filter condensed group dashboard content.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Platform Filters - condensed group dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Platform Filters ', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                let firstGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[2]/td/div[1]/a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]');
                await gotoCondensed.click();
                steps += 'Clicked on Condensed group dashboard';
                await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                let totalPlatform = await page.findElementsByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > div > div.key-platformCheckboxes > form > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox');
            
                for(i= 1; i <= totalPlatform.length; i++) {
                    let selectAll = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/div/div[1]/form/label['+i+']');
                    await selectAll.click();
                    await selectAll.click();
                }
                status += 'Platform checkboxes filtered';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': groupName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();