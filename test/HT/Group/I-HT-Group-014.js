/**
 * ### TEST CASE:
 * I-HT-Group-014
 *
 * ### TEST TITLE:
 * Intract with share of voice  - Group Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to Intract with share of voice from Group Dashboard.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Intract with share of voice - Group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Intract with share of voice', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                let firstGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[2]/td/div[1]/a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                await page.scrollPixel(900);
                await driver.sleep(3000);
                
                let checkPosts = await page.findElementsByCSS('#share-of-voice > div.key-chartContainer > div > div.key-chartsLegend.key-chartsLegend--sov > div> div.key-chartsLegend__item--before');
                for (var i = 1; i <= checkPosts.length; i++) {
                    let checkbox = await page.findByXPath('//*[@id="share-of-voice"]/div[2]/div/div[2]/div['+i+']/div[1]');
                    await checkbox.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="share-of-voice"]/div[2]/div/div[2]/div[2]/div[2]'), 3000));

                    let trackertext = await page.findByXPath('//*[@id="share-of-voice"]/div[2]/div/div[2]/div['+i+']/div[2]');
                    let textname = await trackertext.getText();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="share-of-voice"]/div[2]/div/div[2]/div[1]/div[2]'), 3000));

                    status = ' ,tracker Unchecked Successfully: '+textname;
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group Name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });

                }

                let postTotal = await page.findByXPath('//*[@id="share-of-voice"]/div[1]/div/ul/li[2]');
                await postTotal.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="share-of-voice"]/div[2]/div/div[2]/div[1]/div[2]'), 3000));

                let totalPercent = await page.findElementsByCSS('#share-of-voice > div.key-chartContainer > div > div.key-chartsLegend.key-chartsLegend--sov > div > div.key-chartsLegend__item--before');
                for (var i = 1; i <= totalPercent.length; i++) {
                    let checkboxes = await page.findByXPath('//*[@id="share-of-voice"]/div[2]/div/div[2]/div['+i+']/div[2]');
                    let percentText = await checkboxes.getText();
                    await checkboxes.click();
                    status = ' ,tracker checked Successfully: '+percentText;
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="share-of-voice"]/div[2]/div/div[2]/div[1]/div[2]'), 3000));                                                             
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group Name': groupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();