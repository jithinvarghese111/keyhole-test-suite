/**
 * ### TEST CASE:
 * I-HT-Group-031
 *
 * ### TEST TITLE:
 * Downloading XLS - group Comparison for HT.
 *
 * ### TEST SUMMARY:
 * user should be able to download an excel file of group comparison information locally for HT.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
const util = require('util');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading XLS - group Comparison for HT.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('download an excel file of group comparison information', async () => {
                await page.redirectToHTDashboard();

                let clickGroup = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');

                if (checkGroup) {

                    let test_result = '';
                    
                    let compareGroup = await page.findByCSS('#hashtag-trackers > div.track-info.track-info--groups.track-info--HT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup');
                    await compareGroup.click();
                    await driver.sleep(1000);

                    for (var i = 1; i <= 3; i++) {
                        let select = await page.findByCSS('#hashtag-trackers > div.track-info.track-info--groups.track-info--HT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li:nth-child('+i+')');
                        let groups = await select.getText();
                        await select.click();
                        addContext(this.ctx, 'Group for comparison: '+groups);
                        await driver.sleep(1000);
                    }

                    let goTo = await page.findByCSS('#hashtag-trackers > div.track-info.track-info--groups.track-info--HT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li.dropdown__strong.js-compareGroup.group__dashboardLink ');
                    await goTo.click();
                    await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let dateClick = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > div > div > span > div > input');
                    dateClick.click();
                    await driver.sleep(1000);

                    let titleClick = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong');
                    await titleClick.click();
                
                    let currentURL = await driver.getCurrentUrl();
                    let params = currentURL.substring(currentURL.indexOf("&")+1);
                    let firstParam = params.substr(0, params.indexOf('&'));
                    let dateFrom = firstParam.substring(firstParam.indexOf("=")+1);
                    let dateTo = params.substring(params.lastIndexOf("=")+1);

                    let fromDate = await page.ChangeDate(dateFrom);
                    let toDate = await page.ChangeDate(dateTo);

                    let xlsLink = await page.findById('update');
                    await xlsLink.click();
                    await driver.sleep(5000);

                    const downloadPath = config.downloadPath+'\/'+'Group_Comparison'+'_'+fromDate+'_'+toDate+'.xlsx';
                    const filename = `Group_Comparison_${new Date().getTime()}.xlsx`;
                    const renamePath = config.downloadPath+'\/'+filename;
                    
                    if (fs.existsSync(downloadPath)) {
                        fs.renameSync(downloadPath, renamePath)
                        
                        if (fs.existsSync(renamePath)) {
                            const stats = fs.statSync(renamePath)

                            if (parseFloat(stats.size) > 64) {
                                test_result = "Downloaded file";
                                passed = true
                            } else {
                                test_result = "Downloaded empty file.";
                                passed = true
                            }
                        }
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': test_result
                        }
                    });
                }
                else assert.fail('No groups available');
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();