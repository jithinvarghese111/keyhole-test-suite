/**
 * ### TEST CASE:
 * I-HT-Group-003
 *
 * ### TEST TITLE:
 * Deleting Hashtags & Keywords Groups
 *
 * ### TEST SUMMARY:
 * User is trying to delete group
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Deleting Hashtags & Keywords Groups', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Deleting multiple groups', async () => {
                await page.redirectToDashboard();

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[3]/div[2]/button'), 3000));

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                    let groupNames = '', totalGroups = 0;

                    totalGroups = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalGroups = totalGroups.length;

                    if (totalGroups < 3)
                        totalGroups = 1;
                    else
                        totalGroups = 3;

                    for (let i = 1; i <= totalGroups; i++) {
                        let groupName = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                        groupName = await groupName.getText();
                        groupNames += groupName+', ';

                        let deleteGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[3]/button[2]');
                        await deleteGroup.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Total groups deleted': totalGroups,
                            'Deleted group names': groupNames.replace(/,\s*$/, "")
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();