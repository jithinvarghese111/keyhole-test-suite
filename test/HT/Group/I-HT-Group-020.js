/**
 * ### TEST CASE:
 * I-HT-Group-020
 *
 * ### TEST TITLE:
 * Go to condensed group dashboard and check PDF download
 *
 * ### TEST SUMMARY:
 *User should be allowed to download condensed group data in PDF format.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Go to condensed group dashboard and check PDF download', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to condensed group dashboard and check PDF download ', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name');
                if (checkGroup) {
                
                    let firstGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[2]/td/div[1]/a');
                    let groupName = await firstGroup.getText();
                    await firstGroup.click();
                    steps += 'Clicked on group';
                    await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                    let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]');
                    await gotoCondensed.click();
                    steps += 'Clicked on Condensed group dashboard';
                    await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                    let downloadPDF = await page.findById('pdf');
                    await downloadPDF.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="pdf_confirm"]'), 3000));

                    let waitPDF = await page.findById('pdf_confirm');
                    await waitPDF.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="pdf_msg"]/a'), 7000));

                    let checkEle = await page.checkElementByCSS('#pdf_msg > a');
                    if (checkEle) {

                        let finalPDF = await page.findByXPath('//*[@id="pdf_msg"]/a');
                        await finalPDF.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="pdf_confirm"]'), 3000));

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];                
                        });

                        await driver.switchTo().window(tab1);
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="pdf_cancel"]'), 3000));

                        let cancelPDF = await page.findById('pdf_cancel');
                        await cancelPDF.click();
                        status = 'Download success'
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));                        
                    }
                    else{
                        let error = await page.findByXPath('//*[@id="pdf_msg_status"]');
                        let error_msg = await error.getText();
                        status = 'Error occured: '+error_msg;
                    }
                    
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Test status': status
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
                }
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();