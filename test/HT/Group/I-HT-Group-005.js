/**
 * ### TEST CASE:
 * I-HT-Group-005
 *
 * ### TEST TITLE:
 * Add Tracker to group
 *
 * ### TEST SUMMARY:
 * User is trying to Add Tracker to group.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Tracker to group', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Add Tracker to group', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let addGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[1]');
                await addGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[2]/ul/li[1]'), 2000));

                let groupList = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[2]/ul/li[1]');
                await groupList.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a'), 2000));

                let group = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a');
                let groupLink = await group.getAttribute("href");
                expect(groupLink).to.equal('https://keyhole.co/tracker-groups/wseOT7');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a'), 3000));

                let checkEle = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(2) > td.tracker-info > div.tracker__tags > a:nth-child(1)');
                if (checkEle) {
                    status+= 'Added active tracker to the group'
                }
                else{
                    status += 'No groups available'
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test status': status
                    }
                });
               
            });

            it('Checking Add Tracker to group from Paused Tracker', async () => {
                await page.redirectToDashboard();

                let status = ''

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let pauseTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[2]');
                await pauseTracker.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name.name--paused'), 4000));

                let checkTracker = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name.name--paused');
                if (!checkTracker) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No Tracker Found'
                        }
                    });
                }
                else{
                    let addGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr/td[2]/div[3]/div[1]');
                    await addGroup.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr/td[2]/div[3]/div[2]/ul/li[1]'), 2000));

                    let groupList = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr/td[2]/div[3]/div[2]/ul/li[1]');
                    await groupList.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr/td[2]/div[3]/a[1]'), 3000));

                    let checkEle = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.tracker__tags > a:nth-child(1)');
                    if (checkEle) {
                        status+= 'Added Paused tracker to the group'
                    }
                    else{
                        status += 'No groups available'
                    }
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr/td[2]/div[3]/a[1]'), 4000));
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': status
                        }
                    });
                }
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();