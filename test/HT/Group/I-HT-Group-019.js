/**
 * ### TEST CASE:
 * I-HT-Group-019
 *
 * ### TEST TITLE:
 * Download condensed group data as xlsx
 *
 *
 * ### TEST SUMMARY:
 * User should be allowed to download condensed group data in xlsx format
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Download the condensed group information in xlsx format', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Download the condensed group information in xlsx format ', async () => {
                await page.redirectToDashboard();

                let status = '', steps = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]/div');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 4000));

                let heading = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                let headingName = await heading.getText();
                expect(headingName).to.equal('Hashtag & Keyword Tracking');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]'), 2000));

                let groupClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 3000));

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name');
                if (checkGroup) {
                
                    let firstGroup = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[2]/td/div[1]/a');
                    let groupName = await firstGroup.getText();
                    await firstGroup.click();
                    steps += 'Clicked on group';
                    await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                    let gotoCondensed = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a[2]');
                    await gotoCondensed.click();
                    steps += 'Clicked on Condensed group dashboard';
                    await driver.wait(until.elementLocated(By.id('twt_check'), 3000));

                    let test_result = '';
                    const days = 30;
                    const toDate = new Date();
                    const fromDate = new Date();
                    fromDate.setDate(toDate.getDate() - days);
                    const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                    const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');

                    let downloadXLS = await page.findById('update');
                    await downloadXLS.click();
                    test_result = "Downloaded file";
                    await driver.wait(until.elementLocated(By.id('update'), 4000));

                    const downloadPath = config.downloadPath+'\/'+groupName+'_(condensed)'+fromDateStr+'-'+toDateStr+'.xlsx';
                    const filename = `${groupName}_(condensed)_${new Date().getTime()}.xlsx`;
                    const renamePath = config.downloadPath+'\/'+filename;

                    console.log(downloadPath);
                    console.log(renamePath);

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': test_result
                        }
                    });
                }
                else{
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
                }
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();