/**
 * ### TEST CASE:
 * I-HT-Group-033   
 *
 * ### TEST TITLE:
 * Download Group Condensed data as XLS and PDF File.
 *
 *
 * ### TEST SUMMARY:
 * User should be allowed to download Group Condensed data in XLS and PDF Format.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Download Group Condensed data as XLS and PDF File.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Download the Group Condensed information in XLS format ', async () => {
                await page.redirectToHTDashboard();

                let clickGroup = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                
                    let firstGroup = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                    let groupName = await firstGroup.getText();
                    await firstGroup.click();
                    await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                    let test_result = '';

                    let dateClick = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > div > div > span > div > input');
                    dateClick.click();
                    await driver.sleep(1000);

                    let titleClick = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong');
                    await titleClick.click();
                
                    let currentURL = await driver.getCurrentUrl();
                    let params = currentURL.substring(currentURL.indexOf("&")+1);
                    let firstParam = params.substr(0, params.indexOf('&'));
                    let dateFrom = firstParam.substring(firstParam.indexOf("=")+1);
                    let dateTo = params.substring(params.lastIndexOf("=")+1);

                    let fromDateStr = await page.ChangeDate(dateFrom);
                    let toDateStr = await page.ChangeDate(dateTo);

                    let downloadXLS = await page.findById('update');
                    await downloadXLS.click();
                    await driver.wait(until.elementLocated(By.id('update'), 30000));

                    const downloadPath = config.downloadPath+'\/'+groupName+'_'+fromDateStr+'-'+toDateStr+'.xlsx';
                    const filename = `${groupName}_${new Date().getTime()}.xlsx`;
                    const renamePath = config.downloadPath+'\/'+filename;

                    if (fs.existsSync(downloadPath)) {
                        fs.renameSync(downloadPath, renamePath)
                        
                        if (fs.existsSync(renamePath)) {
                            const stats = fs.statSync(renamePath)

                            if (parseFloat(stats.size) > 64) {
                                test_result = "Downloaded file";
                                passed = true
                            } else {
                                test_result = "Downloaded empty file.";
                                passed = true
                            }
                        }
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': test_result
                        }
                    });
                }
                else assert.fail('No groups available');
               
            });
                
            it('Download the Group Condensed information in PDF format.', async () => {
                await page.redirectToHTDashboard();

                let status = '', steps = '';

                let clickGroup = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                
                    let firstGroup = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                    let groupName = await firstGroup.getText();
                    await firstGroup.click();
                    steps += 'Clicked on group';
                    await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                    let pdf = await page.findById('pdf');
                    await pdf.click();
                    await driver.wait(until.elementLocated(By.css('#pdf_confirm'), 20000));

                    let pdfConfirm = await page.findById('pdf_confirm');
                    await pdfConfirm.click();
                    // This process sometimes takes more than 30 seconds
                    await driver.sleep(20000);

                    let checkEle = await page.checkElementByCSS('#pdf_msg > a');

                    if (checkEle) {
                        let viewPdf = await page.findByCSS('#pdf_msg > a');
                        await viewPdf.click();

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });
                        await driver.switchTo().window(tab1);
                        await driver.wait(until.elementLocated(By.css('#pdf_confirm'), 3000));

                        addContext(this.ctx, {
                            title: 'other Context',
                            value: {
                                'Group name': groupName,
                                'Test status': 'Download file'
                            }
                        });
                    } else assert.fail('Failed Downloading PDF File.');
                }
                else assert.fail('No groups available');
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();