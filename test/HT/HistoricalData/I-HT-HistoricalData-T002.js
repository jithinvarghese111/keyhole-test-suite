/**
 * ### TEST CASE:
 *  I-HT-HistoricalData-002
 
 * ### TEST TITLE:
 * Requesting Historical Data Backfill (Non-Enterprise)
 *
 * ### TEST SUMMARY:
 * Sending a request for a historical data backfill for a specific tracker,
 * while logged in as Non-Enterprise
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Requesting Historical Data Backfill (Non-Enterprise)', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Requesting Historical Data Backfill (Non-Enterprise)', async () => {
                await page.redirectToDashboard();

                let steps = '' , status = '';
                
                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');
                
                let historical_Data = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/div/button');
                await historical_Data.click();
                steps += 'Selected Historical Data';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="request-form-1"]/div[1]/h1'), 3000));

                let fullName = await page.findByXPath('//*[@id="fullname"]');
                await fullName.clear();
                await page.write(fullName, 'Qa 1');
                
                let company = await page.findByXPath('//*[@id="company"]');
                await company.clear();
                await page.write(company, 'Keyhole_one');

                let searchterms = await page.findByXPath('//*[@id="request-form-1"]/div[2]/label[4]/div/div[1]/div[2]/div[1]/input');
                await searchterms.clear();
                await page.write(searchterms, 'cola');
                let add_search_terms = await page.findByXPath('//*[@id="request-form-1"]/div[2]/label[4]/div/div[1]/div[2]/button');
                await add_search_terms.click();

                let search_terms = await page.findByXPath('//*[@id="request-form-1"]/div[2]/label[4]/div/div[1]/div[2]/div[2]/input');
                await search_terms.clear();
                await page.write(search_terms, 'water');            
                
                var fromDate = await page.findByXPath('//*[@id="start_range"]');
                await fromDate.click();
                await page.write(fromDate, '2019-06-15');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="start_range"]'), 3000));
            
                var toDate = await page.findByXPath('//*[@id="end_range"]');
                await toDate.click();
                await toDate.clear();
                await page.write(toDate, '2019-07-24');
                await driver.wait(until.elementLocated(By.id('twtCheck_historical'), 3000));
                
                let totalPlatform = await page.findElementsByCSS('#request-form-1 > div.form-wrapper > div > div > label');              
                for(i= 1; i <= totalPlatform.length; i++) {
                    let selectAll = await page.findByXPath('//*[@id="request-form-1"]/div[2]/div/div/label/i['+i+']');
                    await selectAll.click();
                    await driver.wait(until.elementLocated(By.id('twtCheck_historical'), 2000));
                    await selectAll.click();
                }

                var get_Estimate = await page.findByXPath('//*[@id="sendform"]');
                await get_Estimate.click();
                steps += ', Clicked on Get Estimate';
                await driver.sleep(7000);

                var gotoCheckout = await page.findByCSS('#request-form-summary #sendform');
                await gotoCheckout.click();
                steps += ' ,Clicked on Go to Checkout';
                await driver.sleep(5000);

                var creditcard_Number = await page.findByXPath('//*[@id="request-form-payment"]/div[1]/label[2]/input');
                await page.write(creditcard_Number, '5555555555554444');
              
                var cvc = await page.findByXPath('//*[@id="request-form-payment"]/div[1]/label[3]/input');
                await page.write(cvc, '544');

                var expiration_Month = await page.findByXPath('//*[@id="request-form-payment"]/div[1]/label[4]/span[2]/input[1]');
                await page.write(expiration_Month, '04');

                var expiration_Year = await page.findByXPath('//*[@id="request-form-payment"]/div[1]/label[4]/span[2]/input[2]');
                await page.write(expiration_Year, '2021');

                var submit = await page.findByXPath('//*[@id="request-form-payment"]/div[2]/div[2]/button[2]');
                await submit.click();
                steps += ' ,Clicked on submit payment button';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="request-form-payment"]/div[2]/div[1]/p'), 3000));
                
                var validation = await page.findByXPath('//*[@id="request-form-payment"]/div[2]/div[1]/p');
                text = await validation.getText();
                status = 'Validation for invalid details:' +text;
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps,
                        'Test status': status                        
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();