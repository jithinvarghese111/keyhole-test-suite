/**
 * ### TEST CASE:
 *  I-HT-HistoricalData-001
 
 * ### TEST TITLE:
 * Requesting Historical Data Backfill (Enterprise)
 *
 * ### TEST SUMMARY:
 * Sending a request for a historical data backfill for a specific tracker,
 * while logged in as Enterprise user
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Requesting Historical Data Backfill (Enterprise)', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Requesting Historical Data Backfill (Enterprise)', async () => {
                await page.redirectToDashboard();

                let platform = '' , steps = '' , status = '';
                
                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.sleep(1000);

                let historical_Data = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/div/button');
                await historical_Data.click();
                steps += 'Selected Historical Data';
                await driver.sleep(5000);

                let tracker_dropdown = await page.findByXPath('//*[@id="dd"]');
                await tracker_dropdown.click();
                await driver.wait(until.elementLocated(By.css('#dd'), 3000));

                let tracker_select = await page.findByXPath('//*[@id="dd"]/ul/li[2]/a');
                await tracker_select.click();
                steps += ', Selected Tracker';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_start"]'), 3000));

                var fromDate = await page.findByXPath('//*[@id="backfill_start"]');
                await fromDate.click();
                await page.write(fromDate, '2019-08-01');
                steps += ', Selected start Date';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_start"]'), 3000));

                var toDate = await page.findByXPath('//*[@id="backfill_end"]');
                await toDate.click();
                await toDate.clear();
                await page.write(toDate, '2019-08-07');
                steps += ', Selected End Date';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="backfill_end"]'), 3000));
                
                var continue_Request = await page.findByXPath('//*[@id="get_gnip_volume"]');
                await continue_Request.click();
                steps += ', Clicked on Continue';
                await driver.sleep(5000);
                
                var back = await page.findByXPath('//*[@id="request-form-2"]/div[3]/button[1]');
                await back.click();
                await driver.sleep(5000);

                var close = await page.findByXPath('//*[@id="backfill__modal-step1"]/div/a');
                await close.click();
                steps += ', Closed historical_Data modal';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));
                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test steps': steps                        
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();