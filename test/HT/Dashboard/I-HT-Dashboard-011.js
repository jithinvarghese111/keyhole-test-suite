/**
 * ### TEST CASE:
 * I-HT-Dashboard-011
 *
 * ### TEST TITLE:
 * Check add new tracker button
 *
 * ### TEST SUMMARY:
 * User is trying to check the add new tracker button is enabled or disabled
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check add new tracker button', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User is trying to check the add new tracker button is enabled or disabled', async () => {
                await page.redirectToHTDashboard();

                let addNew = await page.findByCSS('#hashtag-trackers > div.heading > div > a');
                addNew = await addNew.getAttribute("class");

                if (addNew.indexOf('disabled') > -1) {
                    addContext(this.ctx, 'Add New Tracker button is disabled');

                    let getAlert = await page.findByCSS('#hashtag-trackers > div.alert-msg.error.msg-hashtag');
                    getAlert = await getAlert.getText();
                    addContext(this.ctx, getAlert);
                }
                else
                    addContext(this.ctx, 'Add New Tracker button is enabled');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();