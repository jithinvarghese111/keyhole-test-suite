/**
 * ### TEST CASE:
 * I-HT-Dashboard-009
 *
 * ### TEST TITLE:
 * Delete selected paused tracker
 *
 * ### TEST SUMMARY:
 * User is trying to select paused tracker and delete
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Delete selected paused tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User is trying to select paused tracker and delete', async () => {
                await page.redirectToHTDashboard();

                let steps = '';
                
                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let clickPaused = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--paused');
                await clickPaused.click();
                await driver.sleep(5000);

                let checkTracker = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkTracker) {
                	let totalTracker = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalTracker = totalTracker.length;

                    if (totalTracker > 2)
                        totalTracker = 2;

                    for (let i = 1; i <= totalTracker; i++) {
                        let clickCheckbox = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+i+') > td.trackerList__manageCheck > input[type=checkbox]');
                        await clickCheckbox.click();
                        await driver.sleep(2000);
                    }

                    let deleteSelect = await page.findByCSS('#bulk-manage__modal > button.kc-button.kc-button--red.js-deleteTracker');
                    await deleteSelect.click();
                    await driver.sleep(1000);

                    let deleteConfirm = await page.findByCSS('#confirm_delete_btn');
                    await deleteConfirm.click();
                    await driver.sleep(1000);

                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Total paused tracker': totalTracker,
                            'Test status': 'Passed'
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();