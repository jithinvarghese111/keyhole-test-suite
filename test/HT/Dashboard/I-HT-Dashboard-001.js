/**
 * ### TEST CASE:
 * I-HT-Dashboard-001
 *
 * ### TEST TITLE:
 * Checking Trackers in Hashtags & Keywords Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to go to particular tracker and comes back to home page
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Trackers in Hashtags & Keywords Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to tracker dashboard and return to home page', async () => {
                await page.redirectToHTDashboard();

                let steps = '';

                let checkTracker = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkTracker) {
                	let gotoTrack = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                    let trackerName = await gotoTrack.getText();
                	await gotoTrack.click();
                	steps += 'First tracker selected and clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 3000));
                    
                    let secondtrackerName = await page.findByXPath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong');
                	secondtrackerName = await secondtrackerName.getText();
                    assert.equal(secondtrackerName, trackerName);
                    
                    let gotoDash = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[1]/ul/li[1]/a');
                	await gotoDash.click();
                	steps += 'Clicked my tracker link from sidebar menu';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));

                	addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps
                        }
                    });

                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Test status': 'Passed'
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();