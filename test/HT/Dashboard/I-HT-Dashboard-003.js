/**
 * ### TEST CASE:
 * I-HT-Dashboard-003
 *  
 * ### TEST TITLE:
 * Delete Hashtags & Keywords Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to delete tracker from dashboard
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Delete Hashtags & Keywords Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Delete a particular tracker', async () => {
                await page.redirectToHTDashboard();

                let steps = '', status = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let checkTracker = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkTracker) {
                	let getTrackerName = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                    getTrackerName = await getTrackerName.getText();

                    let selectTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[1]/input');
                    await selectTracker.click();
                    await driver.wait(until.elementLocated(By.id('bulk-manage__modal'), 3000));

                    let deleteTracker = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[1]');
                    await deleteTracker.click();
                    steps += 'Delete tracker link clicked, ';
                    await driver.wait(until.elementLocated(By.css('#deletetrack__modal #confirm_delete_btn'), 3000));
                    
                    let confirmDelete = await page.findByCSS('#deletetrack__modal #confirm_delete_btn');
                    await confirmDelete.click();
                    steps += 'Delete button clicked from popup';
                    await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 3000));
                    
                    let afterTracker = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                    let afterTrackerName = await afterTracker.getText();
                    await driver.wait(until.elementLocated(afterTracker),3000);
                    assert.notEqual(getTrackerName, afterTrackerName);
                    status = 'Tracker delete success';
                        
                    addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps
                        }
                    });

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': getTrackerName,
                            'Test status': status
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();