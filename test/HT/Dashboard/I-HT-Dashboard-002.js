/**
 * ### TEST CASE:
 * I-HT-Dashboard-002
 *
 * ### TEST TITLE:
 * Enable and Disable Hashtags & Keywords Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to enable and disable various trackers
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Trackers in Hashtags & Keywords Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Disable/Enable a particular tracker', async () => {
                await page.redirectToHTDashboard();

                let steps = '';

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(5000);

                let checkTracker = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkTracker) {
                	let getTrackerName = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                    getTrackerName = await getTrackerName.getText();

                    let disable = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.action > div > div > label:nth-child(4)');
                    await disable.click();
                    steps += 'First tracker disabled, ';
                    await driver.sleep(3000);

                    let gotoPaused = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--paused');
                    await gotoPaused.click();
                    await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name.name--paused > a'), 20000));

                    let inputTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[4]/div[3]/input');
                    await page.write(inputTracker, getTrackerName);
                    steps += 'Searching disabled tracker, ';

                    let searchTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr/td[2]/div[1]/a');
                    searchTracker = await searchTracker.getText();
                    assert.equal(searchTracker, getTrackerName);

                    let enable = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[3]/div/div/label[1]');
                    await enable.click();
                    steps += 'Disabled tracker enabled';                    
                    await driver.sleep(6000);

                    addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps
                        }
                    });

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': getTrackerName,
                            'Test status': 'Passed'
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            it('Disable/Enable multiple tracker', async () => {
                await page.redirectToDashboard();

                let steps = '', tracker_name = '';

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[4]');
                await driver.sleep(3000); 

                let checkTracker = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkTracker) {
                    let totalTracker = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalTracker = totalTracker.length;

                    if (totalTracker > 3)
                        totalTracker = 3;

                    for (let i = 1; i <= totalTracker; i++) {                        

                        await page.scrollPage('//*[@id="hashtag-trackers"]/div[1]/h1');
                        await driver.sleep(4000);

                        let getTrackerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr['+i+']/td[2]/div[1]/a');
                        getTrackerName = await getTrackerName.getText();
                        tracker_name += getTrackerName+', ';

                        let disable = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+i+') > td.action > div > div > label:nth-child(4)');
                        await disable.click();
                        steps += getTrackerName+' tracker disabled, ';
                        await driver.sleep(2000);

                        let gotoPaused = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[2]');
                        await gotoPaused.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr/td[1]/input'), 20000));

                        let inputTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[4]/div[3]/input');
                        await page.write(inputTracker, getTrackerName);
                        steps += 'Searching '+getTrackerName+' tracker, ';

                        let enable = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.action > div > div > label:nth-child(3)');
                        await enable.click();
                        steps += getTrackerName+' tracker enabled, ';
                        await driver.sleep(2000);
                                                                    
                        let gotoActive = await page.findByXPath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[1]');
                        await gotoActive.click();
                        await driver.sleep(2000);
                        
                        await driver.navigate().refresh();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a'), 3000));
                    }

                    addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps
                        }
                    });

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': tracker_name.replace(/,\s*$/, ""),
                            'Test status': 'Passed'
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();