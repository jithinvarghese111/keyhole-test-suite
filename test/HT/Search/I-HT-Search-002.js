// /**
//  * ### TEST CASE:
//  * I-HT-Search-002
//  *
//  * ### TEST TITLE:
//  * Search Hashtag & Keyword Tracker with Social Platform Select
//  *
//  * ### TEST SUMMARY:
//  * User is trying to search for a particular tracker from the main landing page
//  *
//  * ### TEST STEPS:
//  * 1. Navigate to a user login page
//  * 2. Should be login with a valid email password
//  * 3. Navigate to Hashtag & Keyword Tracking page
//  * 4. Select each platform
//  * 5. Search for a particular tracker
//  *
//  * ### PRE CONDITION:
//  * - Must be logged as a valid user
//  *
//  * ### EXPECTED RESULTS:
//  * - User should be able to login via correct credentials
//  *		and search for a tracker
//  *
//  * ### ACTUAL RESULTS:
//  * - Searching for a tracker
//  */

// const addContext = require('mochawesome/addContext');
// const {Builder, By, until} = require('selenium-webdriver');
// const chai = require('chai');
// const chaiAsPromised = require('chai-as-promised');
// const assert = require('assert');
// const Page = require('../../../lib/basePage');
// const config = require('../../../utils/config');
// const generate = require('../../../utils/generate')();
// const keyhole = require('../../../utils/keyhole')();
// const expect = chai.expect;
// chai.use(chaiAsPromised);

// process.on('unhandledRejection', () => { });

// (async function example() {
//     try {
//         describe('Search Hashtag & Keyword Tracker with Social Platform', async function () {
//             beforeEach(async () => {
//                 page = new Page();
//                 driver = page.driver;

//                 addContext(this.ctx, {
//                     title: 'beforeEach context',
//                     value: 'Test Started'
//                 });

//                 await page.gotoLogin();
//             });

//             it('Search with available tracker and social platform selection', async () => {
//                 await page.redirectToDashboard();

//                 let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
//                 let hashLInk = await hashtag.getAttribute("href");
//                 await hashtag.click();
//                 await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 20000));

//                 let url = await driver.getCurrentUrl();
//                 assert.equal(url, hashLInk);

//                 let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
//                 headerName = await headerName.getText();
//                 assert.equal(headerName, 'Hashtag & Keyword Tracking');

//                 let searchBox = await page.findByCSS('.trackerFilters__search input');
//                 await page.write(searchBox, config.trackerSearch);
//                 await driver.sleep(1000);

//                 let platform = ['Twitter', 'Instagram', 'Facebook', 'News', 'Blogs', 'Forums'];

//                 for (i = 0; i < platform.length; i++) {
//                     let platformClick = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div:nth-child(1) > div.key-platformCheckboxes.trackerFilters__platforms.js-trackerFilters__platforms > label.key-platformCheckboxes__checkbox[title="'+platform[i]+'"]');
//                     await platformClick.click();
//                     await driver.sleep(1000);
//                 }

//                 await driver.sleep(1000);

//                 for (i = 0; i < platform.length; i++) {
//                     let platformClick = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div:nth-child(1) > div.key-platformCheckboxes.trackerFilters__platforms.js-trackerFilters__platforms > label.key-platformCheckboxes__checkbox[title="'+platform[i]+'"]');
//                     await platformClick.click();
//                     await driver.sleep(2000);

//                     let noResult = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

//                     if (noResult) {
//                         let totalTracker = await page.findByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.total');
//                         totalTracker = await totalTracker.getText();

//                         noResult_msg = 'Total tracker found after search: '+totalTracker;
//                     }
//                     else {
//                         noResult_msg = 'Search result empty. No trackers found';
//                     }

//                     addContext(this.ctx, {
//                         title: 'Other context',
//                         value: {
//                             'Search keyword': config.trackerSearch,
//                             'Plaforms selected': platform[i],
//                             'Search result': noResult_msg
//                         }
//                     });

//                     await platformClick.click();
//                     await driver.sleep(2000);
//                 }
//             });

//             afterEach(async () => {
//                 addContext(this.ctx, {
//                     title: 'afterEach context',
//                     value: 'Test Completed'
//                 });

//                 await page.quit();
//             });
//         });
//     } catch (ex) {
//         console.log(new Error(ex.message));
//     } finally {

//     }
// })();