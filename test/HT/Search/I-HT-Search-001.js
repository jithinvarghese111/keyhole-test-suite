/**
 * ### TEST CASE:
 * I-HT-Search-001
 *
 * ### TEST TITLE:
 * Search Active Hashtag & Keyword Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to search for a particular tracker from the main landing page
 *
 * ### TEST STEPS:
 * 1. Navigate to a user login page
 * 2. Should be login with a valid email password
 * 3. Navigate to Hashtag & Keyword Tracking page
 * 3. Search for a particular tracker
 *
 * ### PRE CONDITION:
 * - Must be logged as a valid user
 *
 * ### EXPECTED RESULTS:
 * - User should be able to login via correct credentials
 *		and search for a tracker
 *
 * ### ACTUAL RESULTS:
 * - Searching for a tracker
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search Active Hashtag & Keyword Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Search with available tracker', async () => {
                await page.redirectToDashboard();

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let searchBox = await page.findByCSS('#hashtag-trackers > div.track-info.track-info--trackers > div.trackerFilters__search.trackerFilters--HT > input');
                await page.write(searchBox, config.trackerSearch);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[5]/div[1]/div[1]/button[1]'), 10000));

                let noResult = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (noResult) {
                    let totalTracker = await page.findByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.max');
                    totalTracker = await totalTracker.getText()

                    noResult_msg = 'Total tracker found after search: '+totalTracker;
                }
                else {
                	noResult_msg = 'Search result empty. No trackers found';
                }

                addContext(this.ctx, {
                    title: 'Other context',
                    value: {
                    	'Search keyword': config.trackerSearch,
                        'Search result': noResult_msg
                    }
                });
            });

            it('Search with random keyword', async () => {
                await page.redirectToDashboard();

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                let hashLink = await hashtag.getAttribute("href");
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="items--restructured"]/a[1]'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, hashLink);

                let headerName = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                headerName = await headerName.getText();
                assert.equal(headerName, 'Hashtag & Keyword Tracking');

                for (var i = 1; i <= 3; i++) {
                	var random_name = randomName();

                	await page.clearFields('css', '#hashtag-trackers > div.track-info.track-info--trackers > div.trackerFilters__search.trackerFilters--HT > input');

                	let searchBox = await page.findByCSS('#hashtag-trackers > div.track-info.track-info--trackers > div.trackerFilters__search.trackerFilters--HT > input');
	                await page.write(searchBox, random_name);
	                await driver.sleep(2000);

	                let noResult = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

	                if (noResult) {
                        let totalTracker = await page.findByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.max');
                        totalTracker = await totalTracker.getText();

                        noResult_msg = 'Total tracker found after search: '+totalTracker;
	                }
	                else {
	                	noResult_msg = 'Search result empty. No trackers found';
	                }

	                addContext(this.ctx, {
	                    title: 'Other context',
	                    value: {
	                    	'Search keyword': random_name,
	                        'Search result': noResult_msg
	                    }
	                });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();