// /**
//  * ### TEST CASE:
//  * I-HT-Search-003
//  *
//  * ### TEST TITLE:
//  * Search Hashtag & Keyword Tracker with group, platform filter
//  *
//  * ### TEST SUMMARY:
//  * User is trying to search for a particular tracker from the main landing page
//  *
//  */

// const addContext = require('mochawesome/addContext');
// const chai = require('chai');
// const chaiAsPromised = require('chai-as-promised');
// const assert = require('assert');
// const Page = require('../../../lib/basePage');
// const config = require('../../../utils/config');
// const generate = require('../../../utils/generate')();
// const keyhole = require('../../../utils/keyhole')();
// const expect = chai.expect;
// chai.use(chaiAsPromised);

// process.on('unhandledRejection', () => { });

// (async function example() {
//     try {
//         describe('Search Hashtag & Keyword Tracker with group, platform filter', async function () {
//             beforeEach(async () => {
//                 page = new Page();
//                 driver = page.driver;

//                 addContext(this.ctx, {
//                     title: 'beforeEach context',
//                     value: 'Test Started'
//                 });

//                 await page.gotoLogin();
//             });

//             it('Search with tracker and group filter', async () => {
//                 await page.redirectToDashboard();

//                 await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]/div[1]');
//                 await driver.sleep(1000);

//                 let searchBox = await page.findByCSS('.trackerFilters__search input');
//                 await page.write(searchBox, config.trackerSearch);
//                 await driver.sleep(1000);

//                 let filterToggle = await page.findByXPath('//*[@id="hashtag-trackers"]/div[3]/div[2]/div[2]/div[1]');
//                 await filterToggle.click();
//                 await driver.sleep(1000);

//                 let totalGroup = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li');
//                 totalGroup = await totalGroup.length;
                
//                 if (totalGroup > 5)
//                     totalGroup = 5;

//                 for (var i = 1; i <= totalGroup; i++) {
//                     let mainDrop = await page.findByXPath('//*[@id="hashtag-trackers"]/div[3]/div[2]/div[2]/div[2]/div/div');
//                     await mainDrop.click();
//                     await driver.sleep(1000);

//                     let groupClick = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li:nth-child('+i+')');
//                     let groupText = await groupClick.getText();
//                     await groupClick.click();
//                     await driver.sleep(1000);

//                     let noResult = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

//                     if (noResult) {
//                         let totalTracker = await page.findByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.total');
//                         totalTracker = await totalTracker.getText();

//                         noResult_msg = 'Total tracker found after search: '+totalTracker;
//                     }
//                     else {
//                         noResult_msg = 'Search result empty. No trackers found';
//                     }

//                     addContext(this.ctx, {
//                         title: 'Other context',
//                         value: {
//                             'Search keyword': config.trackerSearch,
//                             'Selected group': groupText,
//                             'Search result': noResult_msg
//                         }
//                     });
//                 }
//             });

//             it('Search with social platform and group filter', async () => {
//                 await page.redirectToDashboard();

//                 let filterToggle = await page.findByXPath('//*[@id="hashtag-trackers"]/div[3]/div[2]/div[2]/div[1]');
//                 await filterToggle.click();
//                 await driver.sleep(1000);

//                 let totalGroup = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li');
//                 totalGroup = await totalGroup.length;

//                 if (totalGroup > 5)
//                     totalGroup = 5;

//                 for (var i = 1; i <= totalGroup; i++) {
//                     let mainDrop = await page.findByXPath('//*[@id="hashtag-trackers"]/div[3]/div[2]/div[2]/div[2]/div/div');
//                     await mainDrop.click();
//                     await driver.sleep(1000);

//                     let groupClick = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li:nth-child('+i+')');
//                     let groupText = await groupClick.getText();
//                     await groupClick.click();
//                     await driver.sleep(1000);

//                     let platform = ['Twitter', 'Instagram', 'Facebook', 'News', 'Blogs', 'Forums'];

//                     for (i = 0; i < platform.length; i++) {
//                         let platformClick = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div:nth-child(1) > div.key-platformCheckboxes.trackerFilters__platforms.js-trackerFilters__platforms > label.key-platformCheckboxes__checkbox[title="'+platform[i]+'"]');
//                         await platformClick.click();
//                         await driver.sleep(1000);
//                     }

//                     await driver.sleep(1000);

//                     for (i = 0; i < platform.length; i++) {
//                         let platformClick = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div:nth-child(1) > div.key-platformCheckboxes.trackerFilters__platforms.js-trackerFilters__platforms > label.key-platformCheckboxes__checkbox[title="'+platform[i]+'"]');
//                         await platformClick.click();
//                         await driver.sleep(2000);

//                         let noResult = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

//                         if (noResult) {
//                             let totalTracker = await page.findByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.total');
//                             totalTracker = await totalTracker.getText();

//                             noResult_msg = 'Total tracker found after search: '+totalTracker;
//                         }
//                         else {
//                             noResult_msg = 'Search result empty. No trackers found';
//                         }

//                         addContext(this.ctx, {
//                             title: 'Other context',
//                             value: {
//                                 'Selected group': groupText,
//                                 'Plaforms selected': platform[i],
//                                 'Search result': noResult_msg
//                             }
//                         });

//                         await platformClick.click();
//                         await driver.sleep(2000);
//                     }
//                 }
//             });

//             it('Search with group filter', async () => {
//                 await page.redirectToDashboard();

//                 await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]/div[1]');
//                 await driver.sleep(1000);

//                 let filterToggle = await page.findByXPath('//*[@id="hashtag-trackers"]/div[3]/div[2]/div[2]/div[1]');
//                 await filterToggle.click();
//                 await driver.sleep(1000);

//                 let totalGroup = await page.findElementsByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li');
//                 totalGroup = await totalGroup.length;

//                 if (totalGroup > 5)
//                     totalGroup = 5;

//                 for (var i = 1; i <= totalGroup; i++) {
//                     let mainDrop = await page.findByXPath('//*[@id="hashtag-trackers"]/div[3]/div[2]/div[2]/div[2]/div/div');
//                     await mainDrop.click();
//                     await driver.sleep(1000);

//                     let groupClick = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackerFilters.trackerFilters--HT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li:nth-child('+i+')');
//                     let groupText = await groupClick.getText();
//                     await groupClick.click();
//                     await driver.sleep(1000);

//                     let noResult = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

//                     if (noResult) {
//                         let totalTracker = await page.findByCSS('#key-pagination > div.key-paginate__summary.keyjs-paginate__summary > span.total');
//                         totalTracker = await totalTracker.getText();

//                         noResult_msg = 'Total tracker found after search: '+totalTracker;
//                     }
//                     else {
//                         noResult_msg = 'Search result empty. No trackers found';
//                     }

//                     addContext(this.ctx, {
//                         title: 'Other context',
//                         value: {
//                             'Selected group': groupText,
//                             'Search result': noResult_msg
//                         }
//                     });
//                 }
//             });

//             afterEach(async () => {
//                 addContext(this.ctx, {
//                     title: 'afterEach context',
//                     value: 'Test Completed'
//                 });

//                 await page.quit();
//             });
//         });
//     } catch (ex) {
//         console.log(new Error(ex.message));
//     } finally {

//     }
// })();