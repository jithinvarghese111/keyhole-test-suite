/**
 * ### TEST CASE:
 * I-HT-New-002
 *
 * ### TEST TITLE:
 * Managing New Keyword or Hashtag Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to go to add new tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord();

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Managing New Keyword or Hashtag Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Add new tracker', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hashtag = await page.findByCSS('#items--restructured > a:nth-child(1)');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 8000));

                let newClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/div/a');
                await newClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/div/input'), 3000));

                let inputKeyword = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/div/input');
                await page.write(inputKeyword, randomKeyword);
                let inputValue = await inputKeyword.getAttribute("value");
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[3]/button'), 3000));

                let nextStep = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[3]/button');
                await nextStep.click();
                await driver.sleep(2000);

                let createTracker = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[3]/button[1]');
                await createTracker.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-tracking"]/div/div[1]/div[1]/h1/strong'), 10000));

                let title = await driver.getTitle();
                expect(title).to.include(inputValue);

                status = 'New Tracker Created'

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': randomKeyword,
                        'Test status': status
                    }
                });
            });
            
            it('Checking created tracker', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hashtag = await page.findByCSS('#items--restructured > a:nth-child(1)');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 8000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[4]/div[2]/table/tbody/tr/td[2]/div[1]/a'), 3000));

                let firstTracker = await page.findByXPath('//*[@id="hashtag-trackers"]/div[4]/div[2]/table/tbody/tr/td[2]/div[1]/a');
                firstTracker = await firstTracker.getText();

                if (firstTracker == randomKeyword)
                    status = 'Tracker created successfully';
                else
                    status = 'Tracker not found.';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': randomKeyword,
                        'Test status': status
                    }
                });
            });

            it('Delete created tracker', async () => {
                await page.redirectToDashboard();

                let status = '';

                let hashtag = await page.findByCSS('#items--restructured > a:nth-child(1)');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.heading > h1'), 8000));

                await page.scrollPage('//*[@id="hashtag-trackers"]/div[3]');
                await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a'), 2000));

                let firstTracker = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.tracker-info > div.name > a');
                firstTracker = await firstTracker.getText();

                if (firstTracker == randomKeyword) {
                    let deleteTracker = await page.findByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td.trackerList__manageCheck > input[type=checkbox]');
                    await deleteTracker.click();
                    await driver.sleep(3000);

                    let deletePopup = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[1]');
                    await deletePopup.click();
                    await driver.sleep(3000);

                    let confirmDelete = await page.findById('confirm_delete_btn');
                    await confirmDelete.click();
                    await driver.wait(until.elementLocated(By.css('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.active.js-my-tracks__filters--running'), 3000));

                    status = 'Tracker deleted successfully';
                }
                else {
                    status = 'Tracker not found.';
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': randomKeyword,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();