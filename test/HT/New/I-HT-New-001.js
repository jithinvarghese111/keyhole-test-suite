/**
 * ### TEST CASE:
 * I-HT-New-001
 *
 * ### TEST TITLE:
 * Checking Add New Tracker Page
 *
 * ### TEST SUMMARY:
 * User is trying to go to add new tracker page
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Add New Tracker Page', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to add new tracker page from my trackers page', async () => {
                await page.redirectToDashboard();

                let hashtag = await page.findByXPath('//*[@id="items--restructured"]/a[1]');
                await hashtag.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="hashtag-trackers"]/div[1]/h1'), 3000));

                let headerName = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/h1');
                headerName = await headerName.getText();
                expect(headerName).to.equal('Hashtag & Keyword Tracking');

                let newClick = await page.findByXPath('//*[@id="hashtag-trackers"]/div[1]/div/a');
                let trackerLink = await newClick.getAttribute("href");
                await newClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[1]/h1'), 3000));

                let url = await driver.getCurrentUrl();
                assert.equal(url, trackerLink);

                let checkEle = await page.checkElementByCSS('#tracker-creation > div > div.key-page--illustrationAndText > div.key-step > div > div.key-step__prompt > h1');

                if (checkEle) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'Page successfully loaded'
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'Page load error. Tryagain'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();