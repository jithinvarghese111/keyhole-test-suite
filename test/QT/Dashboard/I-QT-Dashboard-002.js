/**
 * ### TEST CASE:
 * I-QT-Dashboard-002
 
 * ### TEST TITLE:
 * Search for Quick Trends using Start With an Example
 *
 * ### TEST SUMMARY:
 * User should be able to Search for Quick Trends using Start With an Example
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search for Quick Trends using Start With an Example', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to Search for Quick Trends using Start With an Example', async () => {
                await page.redirectToQTDashboard();

                let totalExample = await page.findElementsByCSS('#marketResearch > div > section > section > div > div > div > button');
                totalExample = totalExample.length;

                let rand = randomNumberFromTo(1, totalExample);

                let clickExample  = await page.findByCSS('#marketResearch > div > section > section > div > div > div > button:nth-child('+rand+')');
                let clickText = await clickExample.getText();
                addContext(this.ctx, 'Clicked example: '+clickText);
                await clickExample.click();
                await driver.sleep(5000);

                let serachKeyword = await page.findByXPath('//*[@id="marketResearch"]/div/section[1]/div[2]/div/div[2]/input');
                serachKeyword = await serachKeyword.getAttribute("value");

                if (clickText == serachKeyword) {
                    addContext(this.ctx, 'Example Quick Trends search success');
                }
                else {
                    addContext (this.ctx, 'Example Quick Trends search failed');
                }

                assert.equal(clickText, serachKeyword);
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
