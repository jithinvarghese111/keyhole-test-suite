    /**
 * ### TEST CASE:
 * I-QT-Dashboard-003
 
 * ### TEST TITLE:
 * Changing Time Range
 *
 * ### TEST SUMMARY:
 * User should be able to change the time range for which they recieve data back
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Changing Time Range', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to change the time range', async () => {
                await page.redirectToQTDashboard();

                let totalExample = await page.findElementsByCSS('#marketResearch > div > section > section > div > div > div > button');
                totalExample = totalExample.length;

                let rand = randomNumberFromTo(1, totalExample);

                let clickExample  = await page.findByCSS('#marketResearch > div > section > section > div > div > div > button:nth-child('+rand+')');
                let clickText = await clickExample.getText();
                addContext(this.ctx, 'Clicked example: '+clickText);
                await clickExample.click();
                await driver.sleep(5000);
            
                const allowedDays = [1, 30, 60, 90]
                const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]
                let changeDate = await setDateRange(days)

                if (changeDate) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Topic name': clickText,
                            'Selected date period': days,
                            'Test status': 'Success'
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Topic name': clickText,
                            'Test status': 'Date selection failed'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
