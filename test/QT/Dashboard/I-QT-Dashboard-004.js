    /**
 * ### TEST CASE:
 * I-QT-Dashboard-004
 
 * ### TEST TITLE:
 * Interact with Key Posts
 *
 * ### TEST SUMMARY:
 * User should be able to Interact with Key Posts
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Key Posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to Interact with Key Posts', async () => {
                await page.redirectToQTDashboard();

                let totalExample = await page.findElementsByCSS('#marketResearch > div > section > section > div > div > div > button');
                totalExample = totalExample.length;

                let rand = randomNumberFromTo(1, totalExample);

                let clickExample  = await page.findByCSS('#marketResearch > div > section > section > div > div > div > button:nth-child('+rand+')');
                let clickText = await clickExample.getText();
                addContext(this.ctx, 'Clicked example: '+clickText);
                await clickExample.click();
                await driver.sleep(5000);
            
                await page.scrollPage('//*[@id="marketResearch"]/div/section[2]/div[3]/div[2]/p');
                await driver.sleep(2000);
                
                let keyPost = await page.findElementsByCSS('#marketResearch > div > section:nth-child(2) > div:nth-child(3) > div:nth-child(2) > div > div > div > article');
                for (var i = 1; i < keyPost.length; i++) {
                    let posts = await page.findByXPath('//*[@id="marketResearch"]/div/section[2]/div[3]/div[2]/div/div/div/article['+i+']/div[2]/p');
                    let postText = await posts.getText();
                    await driver.sleep(1000);
                    if (postText.indexOf(clickText) > -1) {
                        addContext(this.ctx, '-->'+i+'Displayed Post with selected Topic Name');
                        addContext(this.ctx, '------------------------------------------');
                    }
                    await posts.click();

                    let tab1, tab2;

                    await driver.getAllWindowHandles().then(function(windowHandles) {
                        tab1 = windowHandles[0];
                        tab2 = windowHandles[1];                
                    });

                    await driver.switchTo().window(tab1);  
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="marketResearch"]/div/section[2]/div[3]/div[2]/p'), 300000));
                }

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
