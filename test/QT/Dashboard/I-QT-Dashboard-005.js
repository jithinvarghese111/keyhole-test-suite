/**
 * ### TEST CASE:
 * I-QT-Dashboard-005
 
 * ### TEST TITLE:
 * Search for Quick Trends with Multiple Topics 
 *
 * ### TEST SUMMARY:
 * User should be able to Search for Quick Trends with multiple topics
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search for Quick Trends with Multiple Topics', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to Search for Quick Trends with multiple topics and compare it', async () => {
                await page.redirectToQTDashboard();

                let totalExample = await page.findElementsByCSS('#marketResearch > div > section > section > div > div > div > button');
                totalExample = totalExample.length;

                let rand = randomWord();

                let clickExample  = await page.findByCSS('#marketResearch > div > section.key-kkth__section.key-kkth__section--search > div.key-kkth__search > div > div.key-kkth__form > input');
                await page.write(clickExample, rand);
                await page.keyboardClick('#marketResearch > div > section.key-kkth__section.key-kkth__section--search > div.key-kkth__search > div > div.key-kkth__form > input');
                addContext(this.ctx, 'Entered Topic: '+rand);
                await driver.sleep(5000);

                for (var i = 2; i <= 3; i++) {
                    let random = randomWord();
                    let add = await page.findByCSS('#marketResearch > div > section.key-kkth__section.key-kkth__section--search > div.key-kkth__search > button > i');
                    await add.click();

                    let secondTopic = await page.findByXPath('//*[@id="marketResearch"]/div/section[1]/div[2]/div['+i+']/div[2]/input');
                    await page.write(secondTopic, random);
                    addContext(this.ctx, 'Entered Topic: '+random);
                    await page.keyboardClick('#marketResearch > div > section.key-kkth__section.key-kkth__section--search > div.key-kkth__search > div:nth-child('+i+') > div.key-kkth__form > input');
                    await driver.sleep(3000);
                }
                
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
