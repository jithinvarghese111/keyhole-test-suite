/**
 * ### TEST CASE:
 * I-QT-Dashboard-001
 
 * ### TEST TITLE:
 * Navigate to Quick Trends dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to navigate to the 'Quick Trends' dashboard from the topbar
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to Quick Trends dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Navigate to Quick Trends dashboard', async () => {
                await page.redirectToQTDashboard();

                let title = await page.findByCSS('#marketResearch > div > section > div.key-kkth__heading > h1');
                title = await title.getText();

                if (title.indexOf('Social Mentions Over Time') > -1) {
                    addContext(this.ctx, 'Navigate to Quick Trends dashboard success');
                }
                else {
                    addContext(this.ctx, 'Navigate to Quick Trends dashboard failed');
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
