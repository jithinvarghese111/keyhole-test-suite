/**
 * ### TEST CASE:
 * I-QT-Dashboard-007
 
 * ### TEST TITLE:
 * Sorting of Quick Trends Topic Analytics
 *
 * ### TEST SUMMARY:
 * User should be able to Sort for Quick Trends Topic Analytics
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sorting of Quick Trends Topic Analytics', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to Sort for Quick Trends Topic Analytics', async () => {
                await page.redirectToQTDashboard();

                let totalExample = await page.findElementsByCSS('#marketResearch > div > section > section > div > div > div > button');
                totalExample = totalExample.length;

                let rand = randomWord();

                let clickExample  = await page.findByCSS('#marketResearch > div > section.key-kkth__section.key-kkth__section--search > div.key-kkth__search > div > div.key-kkth__form > input');
                await page.write(clickExample, rand);
                await page.keyboardClick('#marketResearch > div > section.key-kkth__section.key-kkth__section--search > div.key-kkth__search > div > div.key-kkth__form > input');
                addContext(this.ctx, 'Entered Topic: '+rand);
                await driver.sleep(5000);

                for (var i = 2; i <= 3; i++) {
                    let random = randomWord();
                    let add = await page.findByCSS('#marketResearch > div > section.key-kkth__section.key-kkth__section--search > div.key-kkth__search > button > i');
                    await add.click();

                    let secondTopic = await page.findByXPath('//*[@id="marketResearch"]/div/section[1]/div[2]/div['+i+']/div[2]/input');
                    await page.write(secondTopic, random);
                    addContext(this.ctx, 'Entered Topic: '+random);
                    await page.keyboardClick('#marketResearch > div > section.key-kkth__section.key-kkth__section--search > div.key-kkth__search > div:nth-child('+i+') > div.key-kkth__form > input');
                    await driver.sleep(3000);
                }
                
                await page.scrollPage('//*[@id="marketResearch"]/div/section[2]/div[2]/div[1]/p');
                await driver.sleep(2000);

                let firstPostValue = await page.findByXPath('//*[@id="marketResearch"]/div/section[2]/div[2]/div[2]/table/tbody/tr[1]/td[2]');
                firstPostValue = await firstPostValue.getText();

                let postFilter = await page.findByXPath('//*[@id="marketResearch"]/div/section[2]/div[2]/div[2]/table/thead/tr/th[2]');
                await postFilter.click();

                let secondPostValue = await page.findByXPath('//*[@id="marketResearch"]/div/section[2]/div[2]/div[2]/table/tbody/tr[1]/td[2]');
                secondPostValue = await secondPostValue.getText();

                assert.notEqual(secondPostValue, firstPostValue);
                addContext(this.ctx, 'Quick Trends Topic analytics sorted Successfully');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
