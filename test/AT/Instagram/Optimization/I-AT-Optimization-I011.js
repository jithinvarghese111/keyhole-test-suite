/**
 * ### TEST CASE:
 * I-AT-Optimization-I011
 *
 * ### TEST TITLE:
 * Checking Likes, Comments counts in the post list is same in second post details modal of best time to post section.
 *
 * ### TEST SUMMARY:
 * User is trying to navigate into Optimization, Checking Likes, Comments counts in the post list is same in second post details modal of best time to post section.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Likes, Comments counts in the post list is same in second post details modal of best time to post section.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            }); 

            it('Checking Likes, Comments counts in the post list is same as in second post details modal of best time to post section.', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1'), 20000));

                    let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(2) > a');
                    await clickPost.click();
                    await driver.sleep(8000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    await page.scrollPage('//*[@id="account-tracking"]/div/section[1]/section/div/figure[3]/div[1]/h3/span');
                    await driver.sleep(2000);

                    let totalCount = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr');    
                    totalCount = totalCount.length;

                    addContext(this.ctx, 'Total count: '+totalCount);

                    for (let i = 1; i <= totalCount; i++) {
                        addContext(this.ctx, '--------------------');

                        let getDay = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr:nth-child('+i+') > td:nth-child(1)');
                        getDay = await getDay.getText();
                        addContext(this.ctx, 'Day '+i+': '+getDay);

                        let clickSingle = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(3000);

                        let checkPost = await page.checkElementByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr');
                        if (checkPost) {
                            let totalPost = await page.findElementsByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr');
                            addContext(this.ctx, 'Total posts in modal: '+totalPost.length);

                            let likeBefore = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr > td:nth-child(2)');
                            likeBefore = await likeBefore.getText();

                            let commentBefore = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr > td:nth-child(3)');
                            commentBefore = await commentBefore.getText();

                            let postClick = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr');
                            await postClick.click();
                            await driver.sleep(2000);

                            let likeAfter = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > article > section.key-sideviewPost__statLine > div > span:nth-child(2) > span:nth-child(2)');
                            likeAfter = await likeAfter.getText();

                            let commentAfter = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > article > section.key-sideviewPost__statLine > div > span:nth-child(3) > span:nth-child(2)');
                            commentAfter = await commentAfter.getText();

                            if (likeAfter == likeBefore) addContext(this.ctx, 'Like Counts are same'); else addContext(this.ctx, 'Like Counts are not same');

                            if (commentAfter == commentBefore) addContext(this.ctx, 'Comments Counts are same'); else addContext(this.ctx, 'Comments Counts are same'); 

                            assert.equal(likeAfter, likeBefore);
                            assert.equal(commentAfter, commentBefore);

                            let closeSec = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > div.key-modalNav > div > button');
                            await closeSec.click();

                            closeModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > div > button');
                            await closeModal.click();

                        }else{
                            assert.fail('No Posts Found for '+getDay+'');
                        }
                    }
                }
                else {
                    assert.fail('No instagram tracker found or added'); 
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();