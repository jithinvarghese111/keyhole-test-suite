/**
 * ### TEST CASE:
 * I-AT-Optimization-I005
 *
 * ### TEST TITLE:
 * Check top hashtag by engagements, frequency graph tab selection
 *
 * ### TEST SUMMARY:
 * User is trying to click tabs in top hashtag by engagements, frequency
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check top hashtag by engagements, frequency graph tab selection', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click tabs in top hashtag by engagements, frequency', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                  	await clickPost.click();
                  	await driver.sleep(5000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    await page.scrollPage('//*[@id="top-hashtags"]');
                    await driver.sleep(2000);

                  	let clickFreq = await page.findByCSS('#top-hashtags > header > div > div > a:nth-child(2)');
                  	await clickFreq.click();
                  	let getTitle = await page.findByCSS('#top-hashtags > header > h1 > span');
                  	getTitle = await getTitle.getText();

                  	if (getTitle == 'Frequency') addContext(this.ctx, 'Clicked top hashtag frequency tab');
                  	else addContext(this.ctx, 'Top hashtag frequency tab selection failed');

                  	let clickEng = await page.findByCSS('#top-hashtags > header > div > div > a:nth-child(1)');
                  	await clickEng.click();
                  	let getTitle1 = await page.findByCSS('#top-hashtags > header > h1 > span');
                  	getTitle1 = await getTitle1.getText();

                  	if (getTitle1 == 'Engagements') addContext(this.ctx, 'Clicked top hashtag engagements tab');
                  	else addContext(this.ctx, 'Top hashtag engagements tab selection failed');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();