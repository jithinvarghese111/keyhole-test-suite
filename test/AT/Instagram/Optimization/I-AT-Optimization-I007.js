/**
 * ### TEST CASE:
 * I-AT-Optimization-I007
 *
 * ### TEST TITLE:
 * Check average engagements by day, time graph tab selection
 *
 * ### TEST SUMMARY:
 * User is trying to click tabs in average engagements by day, time
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check average engagements by day, time graph tab selection', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click tabs in average engagements by day, time', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                  	await clickPost.click();
                  	await driver.sleep(5000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    await page.scrollPage('//*[@id="avg-engagements"]');
                    await driver.sleep(2000);

                  	let clickTime = await page.findByCSS('#avg-engagements > header > div > div > a:nth-child(2)');
                  	await clickTime.click();
                  	let getTitle = await page.findByCSS('#avg-engagements > header > h1 > span');
                  	getTitle = await getTitle.getText();

                  	if (getTitle == 'Time') addContext(this.ctx, 'Clicked Average Engagements by Time tab');
                  	else addContext(this.ctx, 'Average Engagements by Time tab selection failed');

                  	let clickDay = await page.findByCSS('#avg-engagements > header > div > div > a:nth-child(1)');
                  	await clickDay.click();
                  	let getTitle1 = await page.findByCSS('#avg-engagements > header > h1 > span');
                  	getTitle1 = await getTitle1.getText();

                  	if (getTitle1 == 'Day') addContext(this.ctx, 'Clicked Average Engagements by Day tab');
                  	else addContext(this.ctx, 'Average Engagements by Day tab selection failed');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();