/**
 * ### TEST CASE:
 * I-AT-Optimization-I001
 *
 * ### TEST TITLE:
 * Check dashboard counts
 *
 * ### TEST SUMMARY:
 * User is trying to check the dashboard for various count
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check dashboard counts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking dashboard various counts', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                  	await clickPost.click();
                  	await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                  	let totalPosts = await page.findByCSS('#account > div > div.top-bar > div.account-info > div.posts > p');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total posts: '+totalPosts);

                    let postFollow = await page.findByCSS('#account > div > div.top-bar > div.account-info > div.followers > p');
                    postFollow = await postFollow.getText();
                    addContext(this.ctx, 'Total followers: '+postFollow);

                    let totalFollowing = await page.findByCSS('#account > div > div.top-bar > div.account-info > div.following > p');
                    totalFollowing = await totalFollowing.getText();
                    addContext(this.ctx, 'Total following: '+totalFollowing);

                    let postThis = await page.findByCSS('#account > div > div.account-info > div.post-change > p');
                    postThis = await postThis.getText();
                    addContext(this.ctx, 'Post this period: '+postThis);

                    let avgLikes = await page.findByCSS('#account > div > div.account-info > div.avg-likes > p');
                    avgLikes = await avgLikes.getText();
                    addContext(this.ctx, 'Average likes: '+avgLikes);

                    let avgComments = await page.findByCSS('#account > div > div.account-info > div.avg-comments > p');
                    avgComments = await avgComments.getText();
                    addContext(this.ctx, 'Average comments: '+avgComments);
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();