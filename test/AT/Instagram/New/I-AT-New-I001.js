/**
 * ### TEST CASE:
 * I-AT-New-I001
 *
 * ### TEST TITLE:
 * Managing New Instagram Account Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to go to add new tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Managing New Instagram Account Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Add new tracker', async () => {
                await page.redirectToAccDashboard();

                let status = '', error_msg = '';

                let newClick = await page.findByXPath('//*[@id="account-trackers"]/div[1]/div/a');
                await newClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[1]/h1'), 6000));

                let selectInstagram = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/button[2]');
                await selectInstagram.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div/div[1]/h1'), 3000));

                let inputKeyword = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div/div[4]/div[3]/div/div/input');
                await page.write(inputKeyword, randomKeyword);

                let createTracker = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div/div[5]/button[1]');
                await createTracker.click();
                await driver.sleep(5000);

                let error = await page.checkElementByCSS('#error_modal > div > div > div.modal-body > p');
                if (error) {
                    let errorMsg = await page.findByXPath('//*[@id="error_modal"]/div/div/div[1]/p');
                    errorMsg = await errorMsg.getText();
                    error_msg += 'error occured: '+errorMsg;
                    await driver.sleep(3000);

                    addContext(this.ctx, {
                        title: 'other context',
                        value: {
                            'Error msg': error_msg
                        }
                    });
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': randomKeyword,
                        'Test status': status
                    }
                });
            });
            
            it('Checking created tracker', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(1000);

                let firstTracker = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                firstTracker = await firstTracker.getText();
                
                if (firstTracker != randomKeyword)
                    status = 'Tracker not found.';
                
                assert.equal(firstTracker, randomKeyword);
                status = 'Tracker created successfully';
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': randomKeyword,
                        'Test status': status
                    }
                });
            });

            it('Delete created tracker', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(1000);

                let firstTracker = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                firstTracker = await firstTracker.getText();

                if (firstTracker == randomKeyword) {
                    let deleteTracker = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td[2]/div[2]/a[2]');
                    await deleteTracker.click();
                    await driver.sleep(2000);

                    let confirmDelete = await page.findByCSS('#deletetrackacc__modal #confirm_delete_btn');
                    await confirmDelete.click();
                    await driver.sleep(3000);

                    status = 'Tracker deleted successfully';
                }
                else {
                    status = 'Tracker not found.';
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': randomKeyword,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();