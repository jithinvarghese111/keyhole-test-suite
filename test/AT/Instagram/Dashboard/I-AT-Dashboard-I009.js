/**
 * ### TEST CASE:
 * I-AT-Dashboard-I009
 *
 * ### TEST TITLE:
 * Checking tracker profile exists in Posts Engagement Modal.
 *
 * ### TEST SUMMARY:
 * User is trying to click on the tracker profile link and checking the profile page is exists in Posts Engagement Modal.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking tracker profile exists in Instagram', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on the tracker link and checking the page is exists in Instagram', async () => {
                await page.redirectToAccDashboard();

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let trackerlink = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--topStats > div > div.key-topStats--primary > div.key-topStats--primaryProfile > div:nth-child(2) > a > span:nth-child(1)');
                    let trackerText = await trackerlink.getText();

                    await page.scrollPage('//*[@id="account-tracking"]/div/section[1]/section/div/figure[3]/div[1]/h3');
                    await driver.sleep(2000);

                    let totalPosts = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr');

                    for (var i = 1; i < totalPosts.length; i++) {
                            
                        let clickPost = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(2000);

                        let profile = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div > div > div > div.key-userDetails.key-userDetails--fullWidth > span:nth-child(1) > a');
                        let profileText = await profile.getText();
                        addContext(this.ctx, 'Profile Text from Dashboard: '+profileText);
                        // await profile.click();
                        // await driver.sleep(1000);

                        assert.equal(trackerText, profileText);
                        addContext(this.ctx, 'Profile Texts are same');
                        
                        // let tab1, tab2;

                        // await driver.getAllWindowHandles().then(function(windowHandles) {
                        //     tab1 = windowHandles[0];
                        //     tab2 = windowHandles[1];
                        // });

                        // await driver.switchTo().window(tab2);
                        // let url = await driver.getCurrentUrl();
                        // addContext(this.ctx, 'URL after click #: '+url);

                        // assert.equal(url, profileURLText);

                        // if (url === profileURLText) addContext(this.ctx, 'Both links are same');
                        // else addContext(this.ctx, 'Links are not same');
                        // await driver.close();
                        // await driver.switchTo().window(tab1);

                        let closeModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div > button');
                        await closeModal.click();
                        await driver.sleep(1000);

                    }
                }
                else assert.fail('No instagram tracker found or added.');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();