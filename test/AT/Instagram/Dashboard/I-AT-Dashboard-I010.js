/**
 * ### TEST CASE:
 * I-AT-Dashboard-I010
 *
 * ### TEST TITLE:
 * Checking authenticate instagram Tracker.
 *
 * ### TEST SUMMARY:
 * User is trying to Checking authenticate instagram Tracker.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking tracker profile exists in Instagram', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking authenticate instagram Tracker.', async () => {
                await page.redirectToAccDashboard();

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let trackerlink = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--topStats > div > div.key-topStats--primary > div.key-topStats--primaryProfile > div:nth-child(2) > a > span:nth-child(1)');
                    let trackerText = await trackerlink.getText();

                    let checkEle = await page.checkElementByCSS('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div');
                    if (checkEle) {
                        let authenticate = await page.findByCSS('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > div > a');
                        let authenticateURL = await authenticate.getAttribute("href");
                        await authenticate.click();
                        await driver.sleep(1000);

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);
                        let url = await driver.getCurrentUrl();
                        addContext(this.ctx, 'URL after click #: '+url);

                        assert.equal(url, authenticateURL);

                        if (url === profileURLText) addContext(this.ctx, 'Both links are same');
                        else addContext(this.ctx, 'Links are not same');
                        
                        let fbClick = await page.findByCSS('#create_tracker > div > a');
                        await fbClick.click();

                        let email = await page.findByCSS('#email');
                        await page.write(email, 'qa@wdstech.com');

                        let pwd = await page.findByCSS('#pass');
                        await page.write(pwd, 'micr@s@ft123');

                        let login = await page.findByCSS('#loginbutton');
                        await login.click();
                        await driver.sleep(2000);
                    }
                }
                else assert.fail('No instagram tracker found or added.');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();