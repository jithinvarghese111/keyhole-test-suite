/**
 * ### TEST CASE:
 * I-AT-Dashboard-I007
 *
 * ### TEST TITLE:
 * Date Range persists to URL
 *
 * ### TEST SUMMARY:
 * when changing the time range, date range should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Date Range persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Date Range persists to URL', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 4000));

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let secondTrack = await page.checkElementByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--topStats > div > div.key-topStats--secondary > div:nth-child(1) > span.key-topStats__statNumber');

                    if (secondTrack) {
                        const allowedDays = [30, 90, 365]
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                        let changeDate = await setDateRangeAccount(days)

                        if (changeDate) {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Selected date period': days,
                                    'Test status': 'Success'
                                }
                            });
                            let currentURL = await driver.getCurrentUrl();
                            status += 'currentURL :'+currentURL;

                            let params = currentURL.substring(currentURL.indexOf("?")+1);
                            
                            if (params.indexOf('days') > -1 && params.indexOf(days) > -1) {
                                let status = '';

                                status += 'Date added to URL., ';

                                await driver.navigate().refresh();
                                await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                                if (params.indexOf('days') > -1 && params.indexOf(days) > -1) {
                                    status += 'Date persisted upon reload.';
                                }
                                addContext(this.ctx, {
                                    title: 'Test Results',
                                    value: {
                                        'Tracker name': trackerName,
                                        'Test status': status
                                    }
                                });
                            }
                            else assert.fail('Date not added to URL.');
                        }
                        else assert.fail('Date selection failed.');
                    }
                    else assert.fail('We were unable to find any posts from this account.');
                }
                else assert.fail('No instagram tracker found or added.');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();