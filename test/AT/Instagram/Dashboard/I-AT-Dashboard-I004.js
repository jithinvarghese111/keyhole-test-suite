/**
 * ### TEST CASE:
 * I-AT-Dashboard-I004
 *
 * ### TEST TITLE:
 * Checking Top Posts by Engagements modal
 *
 * ### TEST SUMMARY:
 * User is trying to click on the Top Posts by Engagements posts and checking counts from the modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Top Posts by Engagements modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on the Top Posts by Engagements posts and checking counts from the modal', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    addContext(this.ctx, 'Tracker name: '+trackerName);
                    await clickLink.click();
                    await driver.sleep(10000);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                    await driver.sleep(1000);

                    await page.scrollPage('//*[@id="top-posts"]');
                    await driver.sleep(1000);

                    let totalPosts = await page.findElementsByCSS('#top-posts > div > table > tbody > tr');
                    totalPosts = totalPosts.length;
                    addContext(this.ctx, 'Total posts: '+totalPosts);

                    for (let i = 1; i <= totalPosts; i++) {
                        addContext(this.ctx, '--------------- '+i+' -----------------');

                        let likes = await page.findByCSS('#top-posts > div > table > tbody > tr:nth-child('+i+') > td.like-count');
                        likes = await likes.getText();

                        let comments = await page.findByCSS('#top-posts > div > table > tbody > tr:nth-child('+i+') > td.comment-count');
                        comments = await comments.getText();

                        let clickPost = await page.findByCSS('#top-posts > div > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let likeModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-likes > span');
                        likeModal = await likeModal.getText();

                        let commentModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-comments > span');
                        commentModal = await commentModal.getText();

                        if (likes === likeModal) addContext(this.ctx, 'Likes count is same');
                        else addContext(this.ctx, 'Likes count is not same');

                        if (comments === commentModal) addContext(this.ctx, 'Comment count is same');
                        else addContext(this.ctx, 'Comment count is not same');

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();