/**
 * ### TEST CASE:
 * I-AT-Posts-I006
 *
 * ### TEST TITLE:
 * Page persists to URL
 *
 * ### TEST SUMMARY:
 * When changing pagination, page should persist to the URL
 *
 */ 

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Page persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',    
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Page persists to URL', async () => {
                await page.redirectToAccDashboard();

                let status = '', steps = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    steps += 'Tracker name: '+trackerName;
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="account"]/div/div[1]/div[1]/div[2]/div/a/p'), 20000));

                    let instPosts = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                    await instPosts.click();
                    await driver.sleep(5000);
                    
                    await page.scrollPage('//*[@id="all-posts"]/div[2]/div[1]/table/tbody/tr[10]');
                    await driver.sleep(5000);

                    let pageNumber = await page.findByXPath('//*[@id="all-posts"]/div[2]/div[2]/div[2]/div/a[2]');
                    await pageNumber.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="all-posts"]/div[2]/div[1]/table/tbody/tr[10]'), 20000));

                    let currentURL = await driver.getCurrentUrl();
                    let params = currentURL.substring(currentURL.indexOf("?")+1);
                    let firstParam = params.substr(0, params.lastIndexOf('='));
                                        
                    if (firstParam.indexOf('page') > -1){
                        let status = '';

                        status += 'Page added to URL., ';

                        await driver.navigate().refresh();
                        await driver.wait(until.elementLocated(By.id('update'), 6000));

                        if (firstParam.indexOf('page') > -1 && firstParam.indexOf('&') > -1){
                            status += 'Page persisted upon reload.';
                        }
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': status
                            }
                        });
                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Page not added to URL.'
                            }
                        });
                    }     
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();
