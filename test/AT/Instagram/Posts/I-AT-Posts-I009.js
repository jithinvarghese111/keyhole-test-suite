/**
 * ### TEST CASE:
 * I-AT-Posts-I009
 *
 * ### TEST TITLE:
 * Sort persists to URL
 *
 * ### TEST SUMMARY:
 * User is trying to sort the post using LIKES, sort should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sort persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Sort persists to URL', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	await driver.sleep(2000);

                  	let getLikesBefore = await page.findByCSS('#all-posts > div > div.table-container > table > tbody > tr:nth-child(1) > td.like-count');
                  	getLikesBefore = await getLikesBefore.getText();
                  	let clickLikes = await page.findByCSS('#all-posts > div > div.table-container > table > thead > tr > th.like-count.toggle');
                  	await clickLikes.click();
                  	let getLikesAfter = await page.findByCSS('#all-posts > div > div.table-container > table > tbody > tr:nth-child(1) > td.like-count.sorted-by');
                  	getLikesAfter = await getLikesAfter.getText();
                  	if (getLikesBefore != getLikesAfter) addContext(this.ctx, 'Post sorted as per LIKES');

                    let currentURL = await driver.getCurrentUrl();
                    let params = currentURL.substring(currentURL.indexOf("?")+1);
                   
                    if (params.indexOf('sort') > -1 && params.indexOf('desc') > -1){

                        addContext(this.ctx, 'Sorted descending added to URL.,');

                        await driver.navigate().refresh();
                        await driver.wait(until.elementLocated(By.id('update'), 6000));

                        if (params.indexOf('sort') > -1 && params.indexOf('desc') > -1){
                            addContext(this.ctx, 'Sorted descending persisted upon reload.');
                        }
                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Sort not added to URL.'
                            }
                        });
                    }    
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();