/**
 * ### TEST CASE:
 * I-AT-Posts-I016
 *
 * ### TEST TITLE:
 * Checking Post caption in the post list is same in second post details modal.
 *
 * ### TEST SUMMARY:
 * User should be able to click on the post and Checking Post caption in the post list is same in second post details modal.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Post caption in the post list is same in second post details modal.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Post caption in the post list is same in second post details modal.', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(3) > a');
                  	await clickPost.click();
                  	await driver.sleep(8000);

                  	let totalPosts = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr');
	                totalPosts = totalPosts.letength;
	                addContext(this.ctx, 'Total post: '+totalPosts);

                  	if (totalPosts > 5) totalPosts = 5;

                    for (let i = 1; i <= 5; i++) {
                        addContext(this.ctx, '-------------------'+i+'---------------------');

                        let postCaption = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr:nth-child('+i+') > td.key-table__column.key-table__column--caption > span');
                        postCaption = await postCaption.getText();
                        await driver.sleep(2000);

                        let clickPost = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(2000);

                        let captionModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > article > section.key-sideviewPost__post.key-scroll > div.key-sideviewPost__postText > p');
                        captionModal = await captionModal.getText();

                        if (captionModal == postCaption) addContext(this.ctx, '#'+i+': Post captions are equal');
                        else addContext(this.ctx, '#'+i+': Post captions are not equal');

                        let closeModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalHeader > button');
                        await closeModal.click();
                        await driver.sleep(2000);
                    }
                }
                else assert.fail('No instagram tracker found or added');
                    
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();