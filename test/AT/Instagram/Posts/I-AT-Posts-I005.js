/**
 * ### TEST CASE:
 * I-AT-Posts-I005
 *
 * ### TEST TITLE:
 * Checking Likes, Comments counts in the post list is same in post details modal
 *
 * ### TEST SUMMARY:
 * User should be able to click on the post and check Likes, Comments counts in the modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Likes, Comments counts in the post list is same in post details modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on the post and check Likes, Comments counts in the modal', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                  	let totalPosts = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total post: '+totalPosts);

                  	if (totalPosts > 5) totalPosts = 5;

                    for (let i = 1; i <= totalPosts; i++) {
                        addContext(this.ctx, '-----------------------------');

                        let likeBefore = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr:nth-child(1) > td.like-count');
                        likeBefore = await likeBefore.getText();

                        let commentsBefore = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr:nth-child(1) > td.comment-count');
                        commentsBefore = await commentsBefore.getText();

                        let clickPost = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr:nth-child(1)');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let likeModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-likes > span');
                        likeModal = await likeModal.getText();

                        let commentsModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-comments > span');
                        commentsModal = await commentsModal.getText();

                        if (likeBefore == likeModal) addContext(this.ctx, 'Likes count is same');
                        else addContext(this.ctx, 'Likes count is not same');

                        if (commentsBefore == commentsModal) addContext(this.ctx, 'Comments count is same');
                        else addContext(this.ctx, 'Comments count is not same');

                        assert.equal(likeBefore, likeModal);
                        assert.equal(commentsBefore, commentsModal);

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();