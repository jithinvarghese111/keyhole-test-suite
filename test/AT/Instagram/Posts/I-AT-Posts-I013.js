/**
 * ### TEST CASE:
 * I-AT-Posts-I013
 *
 * ### TEST TITLE:
 * Checking view of Instagaram in post details modal
 *
 * ### TEST SUMMARY:
 * User should be able to click on the post and check view in the modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking view of Instagaram in post details modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on the post and check view in the modal', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                  	let totalPosts = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total post: '+totalPosts);

                  	if (totalPosts > 5) totalPosts = 5;

                    for (let i = 1; i <= totalPosts; i++) {
                        addContext(this.ctx, '-----------------------------');

                        let postCaption = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr:nth-child('+i+') > td.post');
                        postCaption = await postCaption.getText();

                        let clickPost = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let view = await page.findByCSS('#post-view > div.content > div > div > a');
                        let selected = await view.getAttribute("href");
                        await view.click();
                        await driver.sleep(1000);
                        addContext(this.ctx, 'URL before click #'+i+': '+selected);
                        addContext(this.ctx, +i+' Post viewed on instagram');
                        addContext(this.ctx, 'Post Caption: '+postCaption+' ---');
                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];                
                        });

                        await driver.switchTo().window(tab2);
                        let url = await driver.getCurrentUrl();
                        addContext(this.ctx, 'URL after click #'+i+': '+url);

                        if (url == selected) addContext(this.ctx, '#'+i+': Both are equal');
                        else addContext(this.ctx, '#'+i+': Both are not equal');
                        await driver.close();
                        await driver.switchTo().window(tab1);
                        await driver.wait(until.elementLocated(By.css('#post-view > div.top-bar > div > div.x-button'), 20000));

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();