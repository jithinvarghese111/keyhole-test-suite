/**
 * ### TEST CASE:
 * I-AT-Posts-T012
 *
 * ### TEST TITLE:
 * Post per page pagination
 *
 * ### TEST SUMMARY:
 * User is trying to select post per page dropdown count and checking the counts are changed or not
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Post per page pagination', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking post per page pagination', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();

                    let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                    await clickPost.click();
                    await driver.sleep(5000);

                    let totalPosts = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.keyhole-pagination.posts-pagination > p > span.total');
                    totalPosts = await totalPosts.getText();
                    totalPosts = totalPosts.replace (/,/g, "");

                    if (totalPosts > 10) {
                        let totalBefore = await page.findElementsByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr');
                        totalBefore = totalBefore.length;
                        addContext(this.ctx, 'Total posts before filter: '+totalBefore);

                        await page.scrollPage('//*[@id="all-posts"]/div/div[1]/table/tbody/tr[10]');
                        await driver.sleep(2000);

                        let clickDrop = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.keyhole-pagination.posts-pagination > div.key-pagination__perPage > select');
                        await clickDrop.click();

                        let clickOption = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.keyhole-pagination.posts-pagination > div.key-pagination__perPage > select > option:nth-child(2)');
                        let clickText = await clickOption.getText();
                        addContext(this.ctx, 'Selected posts per page: '+clickText);
                        await clickOption.click();
                    
                        let totalAfter = await page.findElementsByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr');
                        totalAfter = totalAfter.length;
                        addContext(this.ctx, 'Total posts after filter: '+totalAfter);

                        assert.equal(totalAfter, clickText);

                        if (totalAfter == clickText)
                            addContext(this.ctx, 'Posts per page selection success');
                        else
                            addContext(this.ctx, 'Posts per page selection failed');
                    }
                    else
                        addContext(this.ctx, 'Not enough posts for the selected tracker');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();