/**
 * ### TEST CASE:
 * I-AT-Posts-I014
 *
 * ### TEST TITLE:
 * Checking tracker name is present in the second modal on click the Instagaram Post.
 *
 * ### TEST SUMMARY:
 * User should be able to click on the Instagram post and check tracker name is present in the second modal.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking tracker name is present in the second modal on click the Instagaram Post.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking tracker name is present in the second modal on click the Instagaram Post.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('instagram instagram');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account > div > div.top-bar > div.user-info > div.user-bio > div > a > p'), 20000));

                  	let trackerName = await page.findByCSS('#AT-Dashboard > div > div > div.info > p > span');
                    trackerName = await trackerName.getText();
                    await driver.sleep(2000);

                    let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                  	let totalPosts = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total post: '+totalPosts);

                  	if (totalPosts > 5) totalPosts = 5;

                    for (let i = 1; i <= 5; i++) {
                        addContext(this.ctx, '-----------------------------');

                        let clickPost = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(2000);

                        let nameModal = await page.findByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.screen-name');
                        nameModal = await nameModal.getText();
                        addContext(this.ctx, 'Tracker name from modal: '+nameModal);

                        if (trackerName.indexOf(nameModal) > -1) addContext(this.ctx, 'Tracker names are same');
                        else addContext(this.ctx, 'Tracker names are not same');

                        let getURL = await page.findByCSS('#post-view > div.top-bar > div > div.user-info > a');
                        let selected = await getURL.getAttribute("href");
                        addContext(this.ctx, 'URL before click #'+i+': '+selected);
                        await getURL.click();
                        await driver.sleep(2000);
                        
                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];                
                        });

                        await driver.switchTo().window(tab2);
                        let url = await driver.getCurrentUrl();
                        let urlselect = url.substr(0, url.lastIndexOf('/'));
                        addContext(this.ctx, 'URL after click #'+i+': '+urlselect);

                        if (urlselect == selected) addContext(this.ctx, '#'+i+': Both are equal');
                        else addContext(this.ctx, '#'+i+': Both are not equal');
                        await driver.close();
                        await driver.switchTo().window(tab1);
                        await driver.wait(until.elementLocated(By.css('#post-view > div.top-bar > div > div.x-button'), 20000));

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No instagram tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();