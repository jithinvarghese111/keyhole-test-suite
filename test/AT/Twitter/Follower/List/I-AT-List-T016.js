/**
 * ### TEST CASE:
 * I-AT-List-T016
 *
 * ### TEST TITLE:
 * Navigate to the Follower List page, click on Follower and check Whether it has Photo or Video Post.
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Follower List page, Navigate to the Follower List page, click on Follower and check Whether it has Photo or Video Post.
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('click on Follower and check Whether it has Photo or Video Post.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on Follower and check Whether it has Photo or Video Post.', async () => {
                await page.redirectToAccDashboard();

                let status = '', steps = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    await clickLink.click();
                    await driver.sleep(10000);

                    let trackerName = await page.findByCSS('#AT-Dashboard > div > div > div.info > p > span');
                    trackerName = await trackerName.getText();

                    let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                    await clickList.click();
                    await driver.sleep(2000);

                    let trackerNameHead = await page.findByCSS('#AT-Followers-List > div > div > div.info > p > span');
                    trackerNameHead = await trackerNameHead.getText();
                    addContext(this.ctx, 'Tracker name from header: '+trackerNameHead);

                    let firstFollower = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1)');
                    await firstFollower.click();
                    await driver.sleep(2000);

                    let checkElement = await page.checkElementByCSS('#follower-post-list-view > div.content > table > tbody > tr');
                
                    if (checkElement) {
                        let totalRows = await page.findElementsByCSS('#follower-post-list-view > div.content > table > tbody > tr');
                        totalRows = totalRows.length;

                        if (totalRows > 0) {
                            if (totalRows > 5)
                                totalRows = 5;
                            for (var i = 2; i < totalRows; i++) {
                                addContext(this.ctx, '---------------------'+i+'---------------');
                                
                                let postClick = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+')');
                                await postClick.click();
                                await driver.sleep(2000);

                                let checkImage = await page.checkElementByCSS('#post-view > div.content > div > a > img');
                                if (checkImage){
                                    addContext(this.ctx, 'Displayed Post with Photo');
                                }

                                let checkVideo = await page.checkElementByCSS('#post-view > div.content > div > video')
                                if (checkVideo){
                                    addContext(this.ctx, 'Displayed Post with Video');
                                }

                                let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                                await closeModal.click();
                                await driver.sleep(2000);
                            }
                        }
                        else addContext(this.ctx, 'No post found');
                    }
                    else addContext(this.ctx, 'No post found');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();