/**
 * ### TEST CASE:
 * I-AT-List-T006
 *
 * ### TEST TITLE:
 * View List on Twitter
 *
 * ### TEST SUMMARY:
 * User should be able to click on the follower list and and view on Twitter
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('View List on Twitter', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('View List on Twitter', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let title = await driver.getTitle();
                console.log(title);

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account > div > div.top-bar > div.user-info > div.user-bio > div > a'), 200000));

                  	let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickList.click();
                  	await driver.sleep(3000);

                    let trackerNameHead = await page.findByCSS('#AT-Followers-List > div > div > div.info > p > span');
                    trackerNameHead = await trackerNameHead.getText();
                    addContext(this.ctx, 'Tracker name from header: '+trackerNameHead);

                    let totalList = await page.findElementsByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr');
                    totalList = totalList.length;

                    if (totalList > 4) totalList = 4;

                    for (let i = 1; i <= totalList; i++) {
                        addContext(this.ctx, '--------------------------');

                        let clickEach = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child('+i+') > td.follower__user.user > div > div.user__fullname');
                        let followerName = await clickEach.getText();
                        addContext(this.ctx, 'followerName: '+followerName);
                        await clickEach.click();
                        await driver.sleep(1000);

                        let checkInter = await page.checkElementByCSS('#follower-post-list-view > div.content > table > tbody > tr.follower-post-list__interactions.table-sub-heading');
                        if (checkInter) {
                        let interactionClick= await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                        let postType = await interactionClick.getAttribute("data-posttype");
                        addContext(this.ctx, 'Clicked Post Type from modal: '+postType);
                        await interactionClick.click();
                        await driver.sleep(1000);  

                        let viewOnTwitter = await page.findByCSS('#post-view > div.content > div > div > a');
                        await viewOnTwitter.click();
                        addContext(this.ctx, 'Post Viewed on Twitter');

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];                
                        });

                        await driver.switchTo().window(tab1);

                        let backtoModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                        await backtoModal.click();
                        await driver.sleep(1000);  
                        }

                        let closeModal = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                    }
                   
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();