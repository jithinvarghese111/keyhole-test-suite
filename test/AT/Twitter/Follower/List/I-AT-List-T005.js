/**
 * ### TEST CASE:
 * I-AT-List-T005
 *
 * ### TEST TITLE:
 * Checking tracker name is present in the second modal on click the follower list
 *
 * ### TEST SUMMARY:
 * User should be able to click on the follower list and checking the tracker name in the second modal opened
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking tracker name is present in the second modal on click the follower lists', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Able to click on the follower list and checking the tracker name', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickList.click();
                  	await driver.sleep(3000);

                    let trackerNameHead = await page.findByCSS('#AT-Followers-List > div > div > div.info > p > span');
                    trackerNameHead = await trackerNameHead.getText();
                    addContext(this.ctx, 'Tracker name from header: '+trackerNameHead);

                  	let totalPosts = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.keyhole-pagination.followers-pagination > p > span');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total followers before filter: '+totalPosts);

                    if (totalPosts > 5) totalPosts = 5;

                    for (let i = 1; i <= totalPosts; i++) {
                        addContext(this.ctx, '------------ '+i+' --------------');

                        let clickPost = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let clickFirst = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                        await clickFirst.click();
                        await driver.sleep(1000);

                        let nameModal = await page.findByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.screen-name');
                        nameModal = await nameModal.getText();
                        addContext(this.ctx, 'Tracker name from modal: '+nameModal);

                        if (trackerNameHead.indexOf(nameModal) > -1) addContext(this.ctx, 'Tracker names are same');
                        else addContext(this.ctx, 'Tracker names are not same');

                        let closeSec = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeSec.click();
                        await driver.sleep(1000);

                        let closeFirst = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button');
                        await closeFirst.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();