/**
 * ### TEST CASE:
 * I-AT-List-T008
 *
 * ### TEST TITLE:
 * Sorting followers persists to URL
 *
 * ### TEST SUMMARY:
 * User is trying to sort the followers using INTERACTIONS, Sort should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sorting followers persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking sorting followers persists to URL', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                  	let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickList.click();
                    await driver.sleep(3000);

                  	let getInterBefore = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__interactions.number.sorted-by');
                  	getInterBefore = await getInterBefore.getText();
                  	let clickInter = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > thead > tr > th.follower__interactions.toggle.desc.sorted-by');
                  	await clickInter.click();
                  	let getInterAfter = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__interactions.number.sorted-by');
                  	getInterAfter = await getInterAfter.getText();
                  	if (getInterBefore != getInterAfter) addContext(this.ctx, 'Followers sorted as per INTERACTIONS'); 

                    let currentURL = await driver.getCurrentUrl();
                    let params = currentURL.substring(currentURL.indexOf("?")+1);
                   
                    if (params.indexOf('sort') > -1 && params.indexOf('desc') > -1){

                        addContext(this.ctx, 'Sort added to URL.,');

                        await driver.navigate().refresh();
                        await driver.wait(until.elementLocated(By.id('update'), 6000));

                        if (params.indexOf('sort') > -1 && params.indexOf('desc') > -1){
                            addContext(this.ctx, 'Sort persisted upon reload.');
                        }
                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Sort not added to URL.'
                            }
                        });
                    }    
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();