/**
 * ### TEST CASE:
 * I-AT-List-T013
 *
 * ### TEST TITLE:
 * Navigate to the Follower List page, click on Follower and click on each posts keyword to track the keyword
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Follower List page, click on Follower and click on each posts keyword to track the keyword
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Navigate to the Follower List page, click on Follower and click on each posts keyword to track the keyword', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on posts keyword to track the keyword', async () => {
                await page.redirectToAccDashboard();

                let status = '', steps = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickList.click();
                  	await driver.sleep(3000);

                    let trackerNameHead = await page.findByCSS('#AT-Followers-List > div > div > div.info > p > span');
                    trackerNameHead = await trackerNameHead.getText();
                    addContext(this.ctx, 'Tracker name from header: '+trackerNameHead);

                  	let firstFollower = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1)');
                    await firstFollower.click();
                    await driver.sleep(2000);

                    let checkElement = await page.checkElementByCSS('#follower-post-list-view > div.content > table > tbody > tr');
                
                    if (checkElement) {
                        let totalRows = await page.findElementsByCSS('#follower-post-list-view > div.content > table > tbody > tr');
                        totalRows = totalRows.length;

                        if (totalRows > 0) {
                            if (totalRows > 5)
                                totalRows = 5;

                            let checkht = await page.checkElementByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2) > td.post > a.search-hashtag');
                            if (checkht) {
                                let hashtag = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2) > td.post > a.search-hashtag');
                                let hashtagname = await hashtag.getText();
                                let htName = hashtagname.substring(hashtagname.indexOf("#")+1);
                                await hashtag.click();
                                await driver.sleep(2000);

                                let hashtagClick = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                                steps += 'Clicked Hashtag tracking keyword link';
                                await hashtagClick.click();

                                let tab1, tab2;

                                await driver.getAllWindowHandles().then(function(windowHandles) {
                                    tab1 = windowHandles[0];
                                    tab2 = windowHandles[1];
                                });

                                await driver.switchTo().window(tab2);

                                let url = await driver.getCurrentUrl();
                                let params = url.substring(url.lastIndexOf("/")+1);

                                let status = '';

                                if (params.indexOf(htName) > -1) status = 'Passed';
                                else status = 'failed';

                                addContext(this.ctx, {
                                    title: 'Test Results',
                                    value: {
                                        'Selected Hashtag tracker name': hashtagname,
                                        'Start tracker URL': url,
                                        'Test steps': steps,
                                        'Test status': status
                                    }
                                });
                            }
                            else{
                                let trackerName1 = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2) > td.post > a.search-account');
                                trackerName1 = await trackerName1.getText();
                                let AtName = trackerName1.substring(trackerName1.indexOf("@")+1);

                                let startTracking = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2) > td.post > a.search-account');
                                steps += 'Clicked tracking keyword link';
                                await startTracking.click();
                                await driver.sleep(2000);

                                let tab1, tab2;

                                await driver.getAllWindowHandles().then(function(windowHandles) {
                                    tab1 = windowHandles[0];
                                    tab2 = windowHandles[1];
                                });

                                await driver.switchTo().window(tab2);

                                let url = await driver.getCurrentUrl();
                                let params = url.substring(url.lastIndexOf("/")+1);

                                let status = '';

                                if (params.indexOf(AtName) > -1) status = 'Passed';
                                else status = 'failed';

                                addContext(this.ctx, {
                                    title: 'Test Results',
                                    value: {
                                        'Selected tracker name': trackerName1,
                                        'Start tracker URL': url,
                                        'Test steps': steps,
                                        'Test status': status
                                    }
                                });
                            }
                        }
                        else addContext(this.ctx, 'No post found');
                    }
                    else addContext(this.ctx, 'No post found');
                    }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();