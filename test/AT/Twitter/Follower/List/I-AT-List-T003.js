/**
 * ### TEST CASE:
 * I-AT-List-T003
 *
 * ### TEST TITLE:
 * Changing Time Range
 *
 * ### TEST SUMMARY:
 * User should be able to change the time range for which they recieve data back
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Changing Time Range', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Change the time range of followers', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter'); 
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickPost.click();
                    await driver.sleep(5000);
            
                  	let totalPosts = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.keyhole-pagination.followers-pagination > p > span');
	                totalPosts = await totalPosts.getText();

                  	const allowedDays = [30, 90, 365]
                    const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                    let changeDate = await setDateRangeAccount(days)
                    
                    if (changeDate) {
                        
                        await driver.switchTo().alert().accept();

                    	let totalPostsAfter = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.keyhole-pagination.followers-pagination > p > span');
	                	totalPostsAfter = await totalPostsAfter.getText();

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                            	'Total followers before': totalPosts,
                                'Selected date period': days,
                                'Total followers after': totalPostsAfter,
                                'Test status': 'Success'
                            }
                        });
                
                        let currentURL = await driver.getCurrentUrl();
                        status += 'currentURL :'+currentURL;

                        let params = currentURL.substring(currentURL.indexOf("?")+1);
                        let firstParam = params.substr(0, params.indexOf('='));
                        
                        if (firstParam.indexOf('days') > -1) {
                            let status = '';

                            status += 'Date added to URL., ';

                            await driver.navigate().refresh();
                            await driver.wait(until.elementLocated(By.xpath('//*[@id="followers-list"]/div[1]/div[1]/table/tbody/tr[1]/td[1]'), 3000));

                            if (firstParam.indexOf('days') > -1) {
                                status += 'Date persisted upon reload.';
                            }
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': status
                                }
                            });
                        }
                        else {
                                
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Date not added to URL.'
                                }
                            });
                        }         
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Test status': 'Date selection failed'
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();