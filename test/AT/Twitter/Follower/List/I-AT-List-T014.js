/**
 * ### TEST CASE:
 * I-AT-List-T014
 *
 * ### TEST TITLE:
 * Navigate to the Follower List page, click on Follower and check each post is retweeted and retweet & like counts are same as in the post details modal.
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Follower List page, Navigate to the Follower List page, click on Follower and check each post is retweeted and retweet & like counts are same as in the post details modal.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('click on Follower and check each post is retweeted and retweet & like counts are same as in the post details modal.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on Follower and check each post is retweeted and retweet & like counts are same as in the post details modal.', async () => {
                await page.redirectToAccDashboard();

                let status = '', steps = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickList.click();
                  	await driver.sleep(2000);

                    let trackerNameHead = await page.findByCSS('#AT-Followers-List > div > div > div.info > p > span');
                    trackerNameHead = await trackerNameHead.getText();
                    addContext(this.ctx, 'Tracker name from header: '+trackerNameHead);

                  	let firstFollower = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1)');
                    await firstFollower.click();
                    await driver.sleep(2000);

                    let checkElement = await page.checkElementByCSS('#follower-post-list-view > div.content > table > tbody > tr');
                
                    if (checkElement) {
                        let totalRows = await page.findElementsByCSS('#follower-post-list-view > div.content > table > tbody > tr');
                        totalRows = totalRows.length;

                        if (totalRows > 0) {
                            if (totalRows > 5)
                                totalRows = 5;
                            for (var i = 2; i <= totalRows; i++) {
                                addContext(this.ctx, '---------------------'+i+'---------------');
                                let checkRT = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+') > td.avatar > span');
                                if (checkRT) {
                                    addContext(this.ctx, 'Retweeted Post');
                                }

                                let retweetBefore = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+') > td.comment-count');
                                retweetBefore = await retweetBefore.getText();

                                let likeBefore = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+') > td.like-count');
                                likeBefore = await likeBefore.getText();

                                let postClick = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+')');
                                await postClick.click();
                                addContext(this.ctx, 'Clicked Post: '+i+'');
                                await driver.sleep(3000);

                                let retweetAfter = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-comments > span'); 
                                retweetAfter = await retweetAfter.getText();

                                let likeAfter = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-likes > span');
                                likeAfter = await likeAfter.getText();

                                if (retweetAfter == retweetBefore) addContext(this.ctx, 'Retweet Counts are same'); else addContext(this.ctx, 'Retweet Counts are not same'); 
                                assert.equal(retweetAfter, retweetBefore);

                                if (likeAfter == likeBefore) addContext(this.ctx, 'Like Counts are same'); else addContext(this.ctx, 'Like Counts are not same');
                                assert.equal(likeAfter, likeBefore);

                                let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                                await closeModal.click();
                                await driver.sleep(2000);
                            }
                        }
                        else addContext(this.ctx, 'No post found');
                    }
                    else addContext(this.ctx, 'No post found');
                    }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();