/**
 * ### TEST CASE:
 * I-AT-List-T010
 *
 * ### TEST TITLE:
 * Followers per page pagination
 *
 * ### TEST SUMMARY:
 * User is trying to select followers per page dropdown count and checking the counts are changed or not
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Followers per page pagination', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking followers per page pagination', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                    let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                    await clickList.click();
                    await driver.sleep(5000);

                    let totalPosts = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.keyhole-pagination.followers-pagination > p > span');
                    totalPosts = await totalPosts.getText();
                    totalPosts = totalPosts.replace (/,/g, "");

                    if (totalPosts > 10) {
                        let totalBefore = await page.findElementsByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr');
                        totalBefore = totalBefore.length;
                        addContext(this.ctx, 'Total followers list before filter: '+totalBefore);

                        await page.scrollPage('//*[@id="followers-list"]/div[2]/div[1]/table/tbody/tr[10]');
                        await driver.sleep(2000);

                        let clickDrop = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.keyhole-pagination.followers-pagination > div.key-pagination__perPage > select');
                        await clickDrop.click();
                        await driver.sleep(1000);

                        let clickOption = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.keyhole-pagination.followers-pagination > div.key-pagination__perPage > select > option:nth-child(2)');
                        let clickText = await clickOption.getText();
                        addContext(this.ctx, 'Selected followers list per page: '+clickText);
                        await clickOption.click();
                        await driver.sleep(1000);

                        let totalAfter = await page.findElementsByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr');
                        totalAfter = totalAfter.length;
                        addContext(this.ctx, 'Total followers list after filter: '+totalAfter);

                        assert.equal(totalAfter, clickText);

                        if (totalAfter == clickText)
                            addContext(this.ctx, 'Followers per page selection success');
                        else
                            addContext(this.ctx, 'Followers per page selection failed');
                    }
                    else
                        addContext(this.ctx, 'Not enough followers for the selected tracker');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();