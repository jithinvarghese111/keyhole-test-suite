/**
 * ### TEST CASE:
 * I-AT-List-T019
 *
 * ### TEST TITLE:
 * Checking Next & Prev Post Button in Post modal on click the follower list
 *
 * ### TEST SUMMARY:
 * User should be able to click on the follower list and Checking Next & Prev Post Button in Post modal opened.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Next & Prev Post Button in Post modal on click the follower list.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to click on the follower list and Checking Next & Prev Post Button in Post modal opened.', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));      

                  	let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(5) > a');
                  	await clickList.click();
                  	await driver.sleep(3000);

                    let totalFollower = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr');
	                totalFollower = await totalFollower.length;
	                addContext(this.ctx, 'Total follower: '+totalFollower);

                    if (totalFollower > 5) totalFollower = 5;

                    for (let i = 1; i <= totalFollower; i++) {
                        addContext(this.ctx, '------------ '+i+' --------------');
                       
                        let clickFollower = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr:nth-child('+i+')');
                        await clickFollower.click();
                        await driver.sleep(1000);

                        let checkModal = await page.checkElementByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active');
                        if (checkModal) {

                            let totalPosts = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > table > tbody > tr');

                            totalPosts = await totalPosts.length;

                            if (totalPosts > 3) totalPosts = 3;
                            
                            let clickPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > table > tbody > tr:nth-child(2)');
                            await clickPost.click();
                            await driver.sleep(1000);

                            for (var j = 1; j <= totalPosts; j++) {
                                                             
                            let nextPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > div.key-modalNav > div > div > button.key-button__postNav.key-button__postNav--next.key-link.link--blue');
                            await nextPost.click();
                            await driver.sleep(1000);

                            addContext(this.ctx, '------------ '+j+'--------------');

                            let likeCount = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > article > section.key-sideviewPost__statLine > div > span:nth-child(2) > span:nth-child(2)');
                            likeCount = await likeCount.getText();
                            addContext(this.ctx, 'Like Count of Post: '+j+': '+likeCount+'');

                            let rTCount = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > article > section.key-sideviewPost__statLine > div > span:nth-child(3) > span:nth-child(2)');
                            rTCount = await rTCount.getText();
                            addContext(this.ctx, 'Retweet Count of Post: '+j+': '+rTCount+'');
                            }

                            for (var k = j; k <= totalPosts; k++) {
                                                             
                            let prevPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > div.key-modalNav > div > div > button.key-button__postNav.key-button__postNav--prev.key-link.link--blue');
                            await prevPost.click();
                            await driver.sleep(1000);

                            addContext(this.ctx, '------------ '+k+'--------------');

                            addContext(this.ctx, 'Clicked on Prev Post: '+k+': ');

                            }                            
                            let closeSec = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > div.key-modalHeader > button');
                            await closeSec.click();
                            await driver.sleep(1000); 

                        } else assert.fail('Error Occured after Clicks on Follower: '+i+'');

                    } 
                                           
                }
                else assert.fail('No twitter tracker found or added');
                   
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();