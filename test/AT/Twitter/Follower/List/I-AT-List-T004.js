/**
 * ### TEST CASE:
 * I-AT-List-T004
 *
 * ### TEST TITLE:
 * Filter followers with advanced filters option
 *
 * ### TEST SUMMARY:
 * User should be able to filter the followers using advanced filter options
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Filter followers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Filter followers using advanced filter', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                  	let totalPosts = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.keyhole-pagination.followers-pagination > p > span');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total followers before filter: '+totalPosts);

                    let firstName = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__user.user > div > div.user__fullname');
                    firstName = await firstName.getText();
                    addContext(this.ctx, 'Name before filter: '+firstName);

                    let maxInteraction = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__interactions.number.sorted-by');
                    maxInteraction = await maxInteraction.getText();
                    addContext(this.ctx, 'Maximum interactions: '+maxInteraction);

                    let clickEng = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > thead > tr > th.follower__engagement.toggle');
                    await clickEng.click();
                    await driver.sleep(2000);

                    let maxEng = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__engagement.number.sorted-by');
                    maxEng = await maxEng.getText();
                    addContext(this.ctx, 'Maximum engagement: '+maxEng);

                    let clickFollowers = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > thead > tr > th.follower__followers.toggle');
                    await clickFollowers.click();
                    await driver.sleep(2000);

                    let maxFollowers = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__followers.number.sorted-by');
                    maxFollowers = await maxFollowers.getText();
                    addContext(this.ctx, 'Maximum followers: '+maxFollowers);

                    let clickFollowing = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > thead > tr > th.follower__following.toggle');
                    await clickFollowing.click();
                    await driver.sleep(2000);

                    let maxFollowing = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__following.number.sorted-by');
                    maxFollowing = await maxFollowing.getText();
                    addContext(this.ctx, 'Maximum following: '+maxFollowing);

                    let clickAdv = await page.findByCSS('#filters-trigger');
                    await clickAdv.click();
                    await driver.sleep(2000);

                    let clickFilter = await page.findByCSS('#filter-followers-button');
                    await clickFilter.click();
                    await driver.sleep(2000);

                    let afterName = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__user.user > div > div.user__fullname');
                    afterName = await afterName.getText();
                    addContext(this.ctx, 'Name after filter: '+afterName);

                    if (firstName !== afterName) addContext(this.ctx, 'Followers filter success');
                    else  addContext(this.ctx, 'Followers filter failed');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();