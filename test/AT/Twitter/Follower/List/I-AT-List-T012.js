/**
 * ### TEST CASE:
 * I-AT-List-T012
 *
 * ### TEST TITLE:
 * Checking date, likes and Retweets posts of the follower are same in second modal on click the follower list
 *
 * ### TEST SUMMARY:
 * User should be able to click on the follower list and checking date, likes and Retweets posts of the follower are same in second modal opened
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking date, likes and Retweets are same in second modal on click the follower list', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to click on the follower list and checking date, likes and Retweets are same in second modal opened', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickList.click();
                  	await driver.sleep(3000);

                    let trackerNameHead = await page.findByCSS('#AT-Followers-List > div > div > div.info > p > span');
                    trackerNameHead = await trackerNameHead.getText();
                    addContext(this.ctx, 'Tracker name from header: '+trackerNameHead);

                  	let firstFollower = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1)');
                    await firstFollower.click();
                    await driver.sleep(2000);

                    let totalPost = await page.findElementsByCSS('#follower-post-list-view > div.content > table > tbody > tr');
	                let totalPosts = await totalPost.length;
	                addContext(this.ctx, 'Total Posts of the follower: '+totalPosts);

                    if (totalPosts > 5) totalPosts = 5;

                    for (let i = 2; i <= totalPosts; i++) {
                        addContext(this.ctx, '------------ '+i+' --------------');
                        
                        let likeBefore = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+') > td.like-count');
                        likeBefore = await likeBefore.getText();

                        let retwtBefore = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+') > td.comment-count');
                        retwtBefore = await retwtBefore.getText();

                        let dateBefore = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+') > td.date.sorted-by');
                        dateBefore = await dateBefore.getText();

                        let clickPost = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let likeAfter = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-likes > span');
                        likeAfter = await likeAfter.getText();
                        let statusInt = '';
                        if (likeAfter === likeBefore) statuslike = 'Same'; else statuslike = 'Not same';
                        addContext(this.ctx, 'Likes from first Modal: '+likeBefore+', Likes from second modal: '+likeAfter+' --> '+statuslike);
                        assert.equal(likeAfter, likeBefore);

                        let retwtAfter = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-comments > span');
                        retwtAfter = await retwtAfter.getText();
                        let statusAvg = '';
                        if (retwtAfter === retwtBefore) statusretwt = 'Same'; else statusretwt = 'Not same';
                        addContext(this.ctx, 'Retweets from first Modal: '+retwtBefore+', Retweets from second modal: '+retwtAfter+' --> '+statusretwt);
                        assert.equal(retwtAfter, retwtBefore);

                        let dateAfter = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-date');
                        dateAfter = await dateAfter.getText();
                        let statusFoll = '';
                        if (dateAfter === dateBefore) statusdate = 'Same'; else statusdate = 'Not same';
                        addContext(this.ctx, 'Date from first modal: '+dateBefore+', Date from second modal: '+dateAfter+' --> '+statusdate);
                        assert.equal(dateAfter, dateBefore);

                        let closeSec = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeSec.click();
                        await driver.sleep(1000);

                    }

                    let closeFirst = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button');
                    await closeFirst.click();
                    await driver.sleep(1000);
                        
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();