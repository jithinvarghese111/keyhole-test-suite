/**
 * ### TEST CASE:
 * I-AT-List-T018
 *
 * ### TEST TITLE:
 * Checking Bio is same as in modal on click the follower list
 *
 * ### TEST SUMMARY:
 * User should be able to click on the follower list and checking BIO of the follower is same as in modal opened
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Bio is same as in modal on click the follower list.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should be able to click on the follower list and Checking Bio is same as in modal opened', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));      

                  	let clickList = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(5) > a');
                  	await clickList.click();
                  	await driver.sleep(3000);

                    let totalFollower = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr');
	                totalFollower = await totalFollower.length;
	                addContext(this.ctx, 'Total follower: '+totalFollower);

                    if (totalFollower > 5) totalFollower = 5;

                    for (let i = 1; i <= totalFollower; i++) {
                        addContext(this.ctx, '------------ '+i+' --------------');
                        
                        let bioBefore = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr:nth-child('+i+') > td.key-table__column.key-table__column--bio > div');
                        bioBefore = await bioBefore.getText();

                        if (bioBefore !== '') {
                            let clickPost = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr:nth-child('+i+')');
                            await clickPost.click();
                            await driver.sleep(1000);

                            let bioAfter = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)');
                            bioAfter = await bioAfter.getText();
                            let statusBio = '';
                            if (bioAfter === bioBefore) statusBio = 'Same'; else statusBio = 'Not same';
                            addContext(this.ctx, 'Bio from Follower List: '+bioBefore+', Bio from modal: '+bioAfter+' --> '+statusBio);
                            assert.equal(bioAfter, bioBefore);

                            let closeSec = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--followerView.key-sideview--active > div > button');
                            await closeSec.click();
                            await driver.sleep(1000);

                        } else addContext(this.ctx, 'Follower: '+i+' has no Bio');

                    }
                        
                }
                else assert.fail('No twitter tracker found or added');
                   
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();