/**
 * ### TEST CASE:
 * I-AT-List-T002
 *
 * ### TEST TITLE:
 * Sorting followers
 *
 * ### TEST SUMMARY:
 * User is trying to sort the followers using INTERACTIONS, AVG ENGAGEMENT, FOLLOWERS and FOLLOWING
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sorting followers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Sorting the followers using INTERACTIONS, AVG ENGAGEMENT, FOLLOWERS and FOLLOWING', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(6) > a');
                  	await clickPost.click();
                  	await driver.sleep(2000);

                  	let getInterBefore = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__interactions.number');
                  	getInterBefore = await getInterBefore.getText();
                  	let clickInter = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > thead > tr > th.follower__interactions.toggle');
                  	await clickInter.click();
                  	let getInterAfter = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__interactions.number');
                  	getInterAfter = await getInterAfter.getText();
                  	if (getInterBefore != getInterAfter) addContext(this.ctx, 'Followers sorted as per INTERACTIONS');
                  	await driver.sleep(2000);

                  	let getEngBefore = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__engagement.number');
                  	getEngBefore = await getEngBefore.getText();
                  	let clickEng = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > thead > tr > th.follower__engagement.toggle');
                  	await clickEng.click();
                  	let getEngAfter = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__engagement.number');
                  	getEngAfter = await getEngAfter.getText();
                  	if (getEngBefore != getEngAfter) addContext(this.ctx, 'Followers sorted as per AVG ENGAGEMENT');
                  	await driver.sleep(2000);

                  	let getFollBefore = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__followers.number');
                  	getFollBefore = await getFollBefore.getText();
                  	let clickFoll = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > thead > tr > th.follower__followers.toggle');
                  	await clickFoll.click();
                  	let getFollAfter = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__followers.number');
                  	getFollAfter = await getFollAfter.getText();
                  	if (getFollBefore != getFollAfter) addContext(this.ctx, 'Followers sorted as per FOLLOWERS');
                  	await driver.sleep(2000);

                    let getFollsBefore = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__following.number');
                    getFollsBefore = await getFollsBefore.getText();
                    let clickFolls = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > thead > tr > th.follower__following.toggle');
                    await clickFolls.click();
                    let getFollsAfter = await page.findByCSS('#followers-list > div.keyhole-table.followers-table > div.table-container > table > tbody > tr:nth-child(1) > td.follower__following.number');
                    getFollsAfter = await getFollsAfter.getText();
                    if (getFollsBefore != getFollsAfter) addContext(this.ctx, 'Followers sorted as per FOLLOWING');
                    await driver.sleep(2000);
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();