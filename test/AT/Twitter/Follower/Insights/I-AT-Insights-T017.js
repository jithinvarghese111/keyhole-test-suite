/**
 * ### TEST CASE:
 * I-AT-Insights-T017
 *
 * ### TEST TITLE:
 * Check Whether Post is Video or Image from top countries, top states.
 *
 * ### TEST SUMMARY:
 * User is trying to click on each Follower posts and check whether it is a video Post or Image Post from top countries, top states next to follower location section.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check Whether Post is Video or Image.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on each Follower posts and check whether it is a video or Image Post from Top Countries.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(2000);
                    await clickLink.click();
                    await driver.sleep(10000);
                    
                    let trackerName = await page.findByCSS('#AT-Dashboard > div > div > div.info > p > span');
                    trackerName = await trackerName.getText();

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(3000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#top-region > header > h1');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(2000);

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    if (count > 0) {
                        if (count > 4)
                            count = 4;
                        for (let i = 1; i <= count; i++) {
                            addContext(this.ctx, '----------------');

                            let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                            text = await text.getText();
                            addContext(this.ctx, 'Country '+i+': '+text);

                            let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                            await clickSingle.click();
                            await driver.sleep(2000);

                            let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                            countModal = countModal.length;
                            addContext(this.ctx, 'Total post: '+countModal);

                            let clickUser = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child('+i+')');
                            await clickUser.click();
                            await driver.sleep(2000);

                            let postClick = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                            await postClick.click();
                            await driver.sleep(2000);

                            let checkVideo = await page.checkElementByCSS('#post-view > div.content > div > video');
                            if (checkVideo) {
                                addContext(this.ctx, 'Video exists in a Post.');
                            }

                            let checkImage = await page.checkElementByCSS('#post-view > div.content > div > a > img');
                            if (checkImage) {
                                addContext(this.ctx, 'Image exists in a Post.');
                            }

                            let closethird = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                            await closethird.click();
                            await driver.sleep(2000);

                            let closeSec = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button > i');
                            await closeSec.click();
                            await driver.sleep(2000);

                            let closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                            await closeModal.click();
                            await driver.sleep(2000);
                        }
                    }                    
                }
                else assert.fail('No twitter tracker found or added');
            });

            it('click on each Follower posts and check whether it its a video or Image Post from Top States.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let trackerName = await page.findByCSS('#AT-Dashboard > div > div > div.info > p > span');
                    trackerName = await trackerName.getText();

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(8000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    let scrollTo = await page.findByCSS('#top-region > header > h1');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(2000);   

                    let secondTab = await page.findByCSS('#follower-location > header > div > div > a:nth-child(2)');
                    await secondTab.click();
                    let secondText = await page.findByCSS('#top-region > header > h1');
                    secondText = await secondText.getText();

                    if (secondText == 'Top States') addContext(this.ctx, 'Second tab clicked');
                    else addContext(this.ctx, 'Statesecond tab not clicked');

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    if (count > 0) {
                        if (count > 4)
                            count = 4;
                        for (let i = 1; i <= count; i++) {
                            addContext(this.ctx, '----------------');

                            let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                            text = await text.getText();
                            addContext(this.ctx, 'Country '+i+': '+text);

                            let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                            await clickSingle.click();
                            await driver.sleep(2000);

                            let clickUser = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child('+i+')');
                            await clickUser.click();
                            await driver.sleep(2000);

                            let postClick = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                            await postClick.click();
                            await driver.sleep(2000);

                            let checkVideo = await page.checkElementByCSS('#post-view > div.content > div > video');
                            if (checkVideo) {
                                addContext(this.ctx, 'Video exists in a Post: '+i+'');
                            }

                            let checkImage = await page.checkElementByCSS('#post-view > div.content > div > a > img');
                            if (checkImage) {
                                addContext(this.ctx, 'Image exists in a Post: '+i+'');
                            }

                            let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                            closeModal.click();
                            await driver.sleep(1000);

                            closeModal = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button > i');
                            closeModal.click();
                            await driver.sleep(1000);

                            closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                            closeModal.click();
                            await driver.sleep(1000);
                        }
                    }
                }
                else assert.fail('No twitter tracker found or added');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {  
        console.log(new Error(ex.message));
    } finally {

    }
})();