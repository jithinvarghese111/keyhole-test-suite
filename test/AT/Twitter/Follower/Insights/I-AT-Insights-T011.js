/**
 * ### TEST CASE:
 * I-AT-Insights-T011
 *
 * ### TEST TITLE:
 * Check User posts contains video or Image from Follower Bios
 *
 * ### TEST SUMMARY:
 * User is trying to check User posts contains video or Image in Top Keywords from Follower Bios section
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check User posts contains video or Image from Follower Bios', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check User posts contains video or Image from Follower Bios', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.    click();
                    await driver.wait(until.elementLocated(By.css('#AT-Dashboard > div > div > div.info > p > span'), 2000));

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(8000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                   
                    await page.scrollPage('//*[@id="top-keywords"]/header/h1');
                    await driver.sleep(3000);

                    let totalKeyword = await page.findElementsByCSS('#chart-top-keywords > table > tbody > tr');
                    totalKeyword = totalKeyword.length;

                    if (totalKeyword > 5) totalKeyword = 5;

                    for (let i = 1; i <= totalKeyword; i++) {
                        addContext(this.ctx, '------------- '+i+' --------------');

                        let text = await page.findByCSS('#chart-top-keywords > table > tbody > tr:nth-child('+i+') > td.keyword');
                        text = await text.getText();
                        addContext(this.ctx, 'Keyword '+i+': '+text);

                        let clickSingle = await page.findByCSS('#chart-top-keywords > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(2000);

                        let clickModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1)');
                        await clickModal.click();
                        await driver.sleep(1000);

                        let postClick = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                        await postClick.click();
                        await driver.sleep(1000);

                        let checkImage = await page.checkElementByCSS('#post-view > div.content > div > a > img');
                        if (checkImage) {
                            addContext(this.ctx, 'Post Contains Image');
                        }

                        let checkVideo = await page.checkElementByCSS('#post-view > div.content > div > video');

                        if (checkVideo) {
                            addContext(this.ctx, 'Post Contains Video');
                        };

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);

                        closeModal = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);

                        closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();