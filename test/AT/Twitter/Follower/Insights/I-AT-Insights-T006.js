/**
 * ### TEST CASE:
 * I-AT-Insights-T006
 *
 * ### TEST TITLE:
 * Check Interactions, Avg engagements, followers, following count in Top Countries and Top States
 *
 * ### TEST SUMMARY:
 * User is trying to check the Interactions, Avg engagements, followers, following count in Top Countries and Top States next to follower location section
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check Interactions, Avg engagements, followers, following count in Top Countries and Top States', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check Interactions, Avg engagements, followers, following count in top countries section', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    let scrollTo = await page.findByCSS('#follower-location');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    for (let i = 1; i <= count; i++) {
                        addContext(this.ctx, '----------------');

                        let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                        text = await text.getText();
                        addContext(this.ctx, 'Country '+i+': '+text);

                        let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(2000);

                        let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                        countModal = countModal.length;
                        addContext(this.ctx, 'Total post: '+countModal);

                        let getInt = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--interactions.number');
                        getInt = await getInt.getText();

                        let getAvg = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--engagement.number');
                        getAvg = await getAvg.getText();

                        let getFoll = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--followers.number');
                        getFoll = await getFoll.getText();

                        let getFollo = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--following.number');
                        getFollo = await getFollo.getText();

                        let clickModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1)');
                        await clickModal.click();
                        await driver.sleep(1000);

                        let intAfter = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--interactions > div.stat--number');
                        intAfter = await intAfter.getText();

                        let avgAfter = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--engagements > div.stat--number');
                        avgAfter = await avgAfter.getText();

                        let follAfter = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--followers > div.stat--number');
                        follAfter = await follAfter.getText();

                        let folloAfter = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--following > div.stat--number');
                        folloAfter = await folloAfter.getText();

                        let intStatus = '';
                        if (getInt === intAfter) intStatus = 'Same'; else intStatus = 'Not same';
                        addContext(this.ctx, 'Interactions from first modal: '+getInt+', Interactions from 2nd modal header: '+intAfter+' --> '+intStatus);

                        let avgStatus = '';
                        if (getAvg === avgAfter) avgStatus = 'Same'; else avgStatus = 'Not same';
                        addContext(this.ctx, 'Avg Engagement from first modal: '+getAvg+', Avg Engagement from 2nd modal header: '+avgAfter+' --> '+avgStatus);

                        let follStatus = '';
                        if (getFoll === follAfter) follStatus = 'Same'; else follStatus = 'Not same';
                        addContext(this.ctx, 'Followers from first modal: '+getFoll+', Followers from 2nd modal header: '+follAfter+' --> '+follStatus);

                        let follwStatus = '';
                        if (getFollo === folloAfter) folloStatus = 'Same'; else folloStatus = 'Not same';
                        addContext(this.ctx, 'Following from first modal: '+getFollo+', Following from 2nd modal header: '+folloAfter+' --> '+folloStatus);

                        let closeModal = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);

                        closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            it('Check Interactions, Avg engagements, followers, following count in top states section', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#follower-location');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let secondTab = await page.findByCSS('#follower-location > header > div > div > a:nth-child(2)');
                    await secondTab.click();

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    for (let i = 1; i <= count; i++) {
                        addContext(this.ctx, '----------------');

                        let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                        text = await text.getText();
                        addContext(this.ctx, 'Country '+i+': '+text);

                        let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(2000);

                        let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                        countModal = countModal.length;
                        addContext(this.ctx, 'Total post: '+countModal);

                        let getInt = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--interactions.number');
                        getInt = await getInt.getText();

                        let getAvg = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--engagement.number');
                        getAvg = await getAvg.getText();

                        let getFoll = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--followers.number');
                        getFoll = await getFoll.getText();

                        let getFollo = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--following.number');
                        getFollo = await getFollo.getText();

                        let clickModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1)');
                        await clickModal.click();
                        await driver.sleep(1000);

                        let intAfter = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--interactions > div.stat--number');
                        intAfter = await intAfter.getText();

                        let avgAfter = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--engagements > div.stat--number');
                        avgAfter = await avgAfter.getText();

                        let follAfter = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--followers > div.stat--number');
                        follAfter = await follAfter.getText();

                        let folloAfter = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--following > div.stat--number');
                        folloAfter = await folloAfter.getText();

                        let intStatus = '';
                        if (getInt === intAfter) intStatus = 'Same'; else intStatus = 'Not same';
                        addContext(this.ctx, 'Interactions from first modal: '+getInt+', Interactions from 2nd modal header: '+intAfter+' --> '+intStatus);

                        let avgStatus = '';
                        if (getAvg === avgAfter) avgStatus = 'Same'; else avgStatus = 'Not same';
                        addContext(this.ctx, 'Avg Engagement from first modal: '+getAvg+', Avg Engagement from 2nd modal header: '+avgAfter+' --> '+avgStatus);

                        let follStatus = '';
                        if (getFoll === follAfter) follStatus = 'Same'; else follStatus = 'Not same';
                        addContext(this.ctx, 'Followers from first modal: '+getFoll+', Followers from 2nd modal header: '+follAfter+' --> '+follStatus);

                        let follwStatus = '';
                        if (getFollo === folloAfter) folloStatus = 'Same'; else folloStatus = 'Not same';
                        addContext(this.ctx, 'Following from first modal: '+getFollo+', Following from 2nd modal header: '+folloAfter+' --> '+folloStatus);

                        let closeModal = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);

                        closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();