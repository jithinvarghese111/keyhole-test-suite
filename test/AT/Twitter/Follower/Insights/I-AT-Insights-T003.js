/**
 * ### TEST CASE:
 * I-AT-Insights-T003
 *
 * ### TEST TITLE:
 * Check follower location
 *
 * ### TEST SUMMARY:
 * User is trying to check the follower location tab and changes in top countries, top states next to follower location section
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check follower location', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User is trying to check the follower location tab', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                  	await clickInsights.click();
                  	await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                  	let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#follower-location');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let secondTab = await page.findByCSS('#follower-location > header > div > div > a:nth-child(2)');
                    await secondTab.click();
                    let secondText = await page.findByCSS('#top-region > header > h1');
                    secondText = await secondText.getText();

                    if (secondText == 'Top States') addContext(this.ctx, 'Second tab clicked');
                    else addContext(this.ctx, 'Statesecond tab not clicked');

                    let firstTab = await page.findByCSS('#follower-location > header > div > div > a:nth-child(1)');
                    await firstTab.click();
                    let firstText =  await page.findByCSS('#top-region > header > h1');
                    firstText = await firstText.getText();

                    if (firstText == 'Top Countries') addContext(this.ctx, 'First tab clicked');
                    else addContext(this.ctx, 'First tab not clicked');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            it('Check top countries section', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    let scrollTo = await page.findByCSS('#follower-location');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    for (let i = 1; i <= count; i++) {
                        addContext(this.ctx, '----------------');

                        let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                        text = await text.getText();
                        addContext(this.ctx, 'Country '+i+': '+text);

                        let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(2000);

                        let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                        countModal = countModal.length;
                        addContext(this.ctx, 'Total post: '+countModal);

                        let closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            it('Check top states section', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#follower-location');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let secondTab = await page.findByCSS('#follower-location > header > div > div > a:nth-child(2)');
                    await secondTab.click();

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    for (let i = 1; i <= count; i++) {
                        addContext(this.ctx, '----------------');

                        let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                        text = await text.getText();
                        addContext(this.ctx, 'Country '+i+': '+text);

                        let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(2000);

                        let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                        countModal = countModal.length;
                        addContext(this.ctx, 'Total post: '+countModal);

                        let closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();