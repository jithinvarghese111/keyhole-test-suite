/**
 * ### TEST CASE:
 * I-AT-Insights-T001
 *
 * ### TEST TITLE:
 * Changing Time Range
 *
 * ### TEST SUMMARY:
 * User should be able to change the time range for which they recieve data back
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Changing Time Range', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Change the time range of follower insights', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account > div > div.top-bar > div.user-info > div.user-bio > div > a > p'), 20000));

                  	let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                  	await clickInsights.click();
                  	await driver.sleep(2000);

                  	const allowedDays = [30, 90, 365]
                    const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                    let changeDate = await setDateRangeAccount(days)

                    if (changeDate) {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Selected date period': days,
                                'Test status': 'Success'
                            }
                        });
                        let checkEle = await page.checkElementByCSS('#account > div > div.top-bar > div.account-info > div.posts > p');
                            if (!checkEle) {
                                addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Test status': 'Posts no found in this date range'
                                }
                            });
                        }
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Test status': 'Date selection failed'
                            }
                        });
                    } 
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();