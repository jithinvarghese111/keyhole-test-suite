/**
 * ### TEST CASE:
 * I-AT-Insights-T005
 *
 * ### TEST TITLE:
 * Check top states, top countries and Top Keywords from Follower Bios
 *
 * ### TEST SUMMARY:
 * User is trying to check the view more link
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check top states, top countries and Top Keywords from Follower Bios', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking VIEW MORE link for Top Countries', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                  	await clickInsights.click();
                  	await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                  	let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#follower-location');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let checkElement = await page.checkElementByCSS('#chart-top-region > a');

                    if (checkElement) {
                        let viewMore = await page.findByCSS('#chart-top-region > a');
                        await viewMore.click();
                        await driver.sleep(1000);

                        addContext(this.ctx, 'VIEW MORE button clicked');
                    }
                    else addContext(this.ctx, 'VIEW MORE button not available');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            it('Checking VIEW MORE link for Top States', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#follower-location');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let secondTab = await page.findByCSS('#follower-location > header > div > div > a:nth-child(2)');
                    await secondTab.click();

                    let checkElement = await page.checkElementByCSS('#chart-top-region > a');

                    if (checkElement) {
                        let viewMore = await page.findByCSS('#chart-top-region > a');
                        await viewMore.click();
                        await driver.sleep(1000);

                        addContext(this.ctx, 'VIEW MORE button clicked');
                    }
                    else addContext(this.ctx, 'VIEW MORE button not available');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            it('Checking VIEW MORE link for Top Keywords from Follower Bios', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#top-keywords');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let checkElement = await page.checkElementByCSS('#chart-top-keywords > a');

                    if (checkElement) {
                        let viewMore = await page.findByCSS('#chart-top-keywords > a');
                        await viewMore.click();
                        await driver.sleep(1000);

                        addContext(this.ctx, 'VIEW MORE button clicked');
                    }
                    else addContext(this.ctx, 'VIEW MORE button not available');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();