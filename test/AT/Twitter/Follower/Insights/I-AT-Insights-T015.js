/**
 * ### TEST CASE:
 * I-AT-Insights-T015
 *
 * ### TEST TITLE:
 * Navigate to the Follower Insights page, click on each Follower posts HT or AT Tracking button to track the keyword from top countries, top states next to follower location section.
 *
 * ### TEST SUMMARY:
 * User is trying to check the follower location tab and click on each Follower posts HT or AT Tracking button to track the keyword from top countries, top states next to follower location section.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check follower location - top countries, top states', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on each Follower posts HT or AT Tracking button to track the keyword from top countries.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(2000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#AT-Dashboard > div > div > div.info > p > span'), 20000));

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(5000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#top-region > header > h1');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(2000);

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    for (let i = 1; i <= count; i++) {
                        addContext(this.ctx, '----------------');

                        let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                        text = await text.getText();
                        addContext(this.ctx, 'Country '+i+': '+text);

                        let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(2000);

                        let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                        countModal = countModal.length;
                        addContext(this.ctx, 'Total post: '+countModal);

                        let clickUser = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child('+i+')');
                        await clickUser.click();
                        await driver.sleep(2000);

                        let postClick = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                        await postClick.click();
                        await driver.sleep(3000);

                        let checkHT = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                        if (checkHT) {
                            let hTTracker = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                            await hTTracker.click();
                            addContext(this.ctx, 'Clicked on hashtag Tracking Link.');

                            let tab1, tab2;

                            await driver.getAllWindowHandles().then(function(windowHandles) {
                                tab1 = windowHandles[0];
                                tab2 = windowHandles[1];
                            });

                            await driver.switchTo().window(tab2);

                            let url = await driver.getCurrentUrl();

                            if (url == 'https://keyhole.co/preview_new') addContext(this.ctx, 'Passed');
                            else addContext(this.ctx, 'Failed');
                            await driver.close();
                            await driver.switchTo().window(tab1);
                        }else{
                            addContext(this.ctx, 'No Found Hashtag Tracking Link.');
                        }

                        let checkAT = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a');
                        if (checkAT) {
                            let aTTracker = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a');
                            await aTTracker.click();
                            addContext(this.ctx, 'Clicked on Account Tracking Link.');

                            let tab1, tab2;

                            await driver.getAllWindowHandles().then(function(windowHandles) {
                                tab1 = windowHandles[0];
                                tab2 = windowHandles[1];
                            });

                            await driver.switchTo().window(tab2);

                            let url = await driver.getCurrentUrl();

                            if (url == 'https://keyhole.co/account-tracking/preview') addContext(this.ctx, 'Passed');
                            else addContext(this.ctx, 'Failed');
                            await driver.close();
                            await driver.switchTo().window(tab1);
                        }else{
                            addContext(this.ctx, 'No Found Account Tracking Link.');
                        }

                        let closethird = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                        await closethird.click();
                        await driver.sleep(2000);

                        let closeSec = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button > i');
                        await closeSec.click();
                        await driver.sleep(1000);

                        let closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }                    
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            it('click on each Follower posts HT or AT Tracking button to track the keyword from top States.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(3000);

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    let scrollTo = await page.findByCSS('#top-region > header > h1');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(2000);

                    let secondTab = await page.findByCSS('#follower-location > header > div > div > a:nth-child(2)');
                    await secondTab.click();
                    let secondText = await page.findByCSS('#top-region > header > h1');
                    secondText = await secondText.getText();

                    if (secondText == 'Top States') addContext(this.ctx, 'Second tab clicked');
                    else addContext(this.ctx, 'Statesecond tab not clicked');

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    for (let i = 1; i <= count; i++) {
                        addContext(this.ctx, '----------------');

                        let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                        text = await text.getText();
                        addContext(this.ctx, 'Country '+i+': '+text);

                        let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(2000);

                        let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                        countModal = countModal.length;
                        addContext(this.ctx, 'Total Users: '+countModal);

                        let clickUser = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child('+i+')');
                        await clickUser.click();
                        await driver.sleep(2000);

                        let clickPost = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                        await clickPost.click();
                        await driver.sleep(2000);

                        let checkHT = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                        if (checkHT) {
                            let hTTracker = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                            await hTTracker.click();
                            addContext(this.ctx, 'Clicked on hashtag Tracking Link.');

                            let tab1, tab2;

                            await driver.getAllWindowHandles().then(function(windowHandles) {
                                tab1 = windowHandles[0];
                                tab2 = windowHandles[1];
                            });

                            await driver.switchTo().window(tab2);

                            let url = await driver.getCurrentUrl();

                            if (url == 'https://keyhole.co/preview_new') addContext(this.ctx, 'Passed');
                            else addContext(this.ctx, 'Failed');
                            await driver.close();
                            await driver.switchTo().window(tab1);
                        }

                        let checkAT = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a');
                        if (checkAT) {
                            let aTTracker = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a');
                            await aTTracker.click();
                            addContext(this.ctx, 'Clicked on Account Tracking Link.');

                            let tab1, tab2;

                            await driver.getAllWindowHandles().then(function(windowHandles) {
                                tab1 = windowHandles[0];
                                tab2 = windowHandles[1];
                            });

                            await driver.switchTo().window(tab2);

                            let url = await driver.getCurrentUrl();

                            let status = '';

                            if (url == 'https://keyhole.co/account-tracking/preview') addContext(this.ctx, 'Passed');
                            else addContext(this.ctx, 'Failed');
                            await driver.close();
                            await driver.switchTo().window(tab1);
                        }

                        let closethird = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                        await closethird.click();
                        await driver.sleep(1000);

                        let closeSec = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button > i');
                        await closeSec.click();

                        let closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();