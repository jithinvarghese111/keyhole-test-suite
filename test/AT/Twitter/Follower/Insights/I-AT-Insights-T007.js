/**
 * ### TEST CASE:
 * I-AT-Insights-T007
 *
 * ### TEST TITLE:
 * Check Interactions, Avg engagements, followers, following count in Top Keywords from Follower Bios
 *
 * ### TEST SUMMARY:
 * User is trying to check the Interactions, Avg engagements, followers, following count in Top Keywords from Follower Bios section
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check Interactions, Avg engagements, followers, following count in Top Keywords from Follower Bios', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Interactions, Avg engagements, followers, following count in Top Keywords from Follower Bios', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollTo = await page.findByCSS('#top-keywords');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(1000);

                    let totalKeyword = await page.findElementsByCSS('#chart-top-keywords > table > tbody > tr');
                    totalKeyword = totalKeyword.length;

                    if (totalKeyword > 5) totalKeyword = 5;

                    for (let i = 1; i <= totalKeyword; i++) {
                        addContext(this.ctx, '------------- '+i+' --------------');

                        let keywordName = await page.findByCSS('#chart-top-keywords > table > tbody > tr:nth-child('+i+') > td.keyword');
                        keywordName = await keywordName.getText();
                        addContext(this.ctx, 'Keyword: '+keywordName);

                        let clickKeyword = await page.findByCSS('#chart-top-keywords > table > tbody > tr:nth-child('+i+')');
                        await clickKeyword.click();
                        await driver.sleep(1000);

                        let intModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--interactions.number');
                        intModal = await intModal.getText();

                        let avgModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--engagement.number');
                        avgModal = await avgModal.getText();

                        let folModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--followers.number');
                        folModal = await folModal.getText();

                        let follModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1) > td.follower--following.number');
                        follModal = await follModal.getText();

                        let clickModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1)');
                        await clickModal.click();
                        await driver.sleep(1000);

                        let intHead = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--interactions > div.stat--number');
                        intHead = await intHead.getText();

                        let avgHead = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--engagements > div.stat--number');
                        avgHead = await avgHead.getText();

                        let folHead = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--followers > div.stat--number');
                        folHead = await folHead.getText();

                        let follHead = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.follower-stats > div.stat.stat--following > div.stat--number');
                        follHead = await follHead.getText();

                        let intStatus = '';
                        if (intModal === intHead) intStatus = 'Same'; else intStatus = 'Not same';
                        addContext(this.ctx, 'Interactions from first modal: '+intHead+', Interactions from 2nd modal header: '+intHead+' --> '+intStatus);

                        let avgStatus = '';
                        if (avgModal === avgHead) avgStatus = 'Same'; else avgStatus = 'Not same';
                        addContext(this.ctx, 'Avg Engagement from first modal: '+avgModal+', Avg Engagement from 2nd modal header: '+avgHead+' --> '+avgStatus);

                        let follStatus = '';
                        if (folModal === folHead) follStatus = 'Same'; else follStatus = 'Not same';
                        addContext(this.ctx, 'Followers from first modal: '+folModal+', Followers from 2nd modal header: '+folHead+' --> '+follStatus);

                        let follwStatus = '';
                        if (follModal === follHead) folloStatus = 'Same'; else folloStatus = 'Not same';
                        addContext(this.ctx, 'Following from first modal: '+follModal+', Following from 2nd modal header: '+follHead+' --> '+folloStatus);

                        let closeSec = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button');
                        await closeSec.click();
                        await driver.sleep(1000);

                        let closeFirst = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeFirst.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();