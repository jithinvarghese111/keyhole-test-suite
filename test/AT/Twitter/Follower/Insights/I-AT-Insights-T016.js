/**
 * ### TEST CASE:
 * I-AT-Insights-T016
 *
 * ### TEST TITLE:
 * Navigate to the Follower Insights page, click on each Follower posts and check account Tracker Name exists in Post details Modal from top countries, top states.
 * 
 *
 * ### TEST SUMMARY:
 * User is trying to check the follower location tab and click on each Follower posts and check account Tracker Name exists in Post details Modal.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check account Tracker Name exists in Post details Modal from top countries, top states.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on each Follower posts and check account Tracker Name exists in Post details Modal from top Countries.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(2000);
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));
                    
                    let trackerName = await page.findByCSS('#AT-Dashboard > div > div > div.info > p > span');
                    trackerName = await trackerName.getText();

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(4) > a');
                    await clickInsights.click();
                    await driver.sleep(8000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    var scrollIntoViewTo = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--followersInsightsList > div.key-chartHeading > h3 > span');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(2000);

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    if (count > 0) {
                        if (count > 4)
                            count = 4;
                        for (let i = 1; i <= count; i++) {
                            addContext(this.ctx, '-------'+i+'---------');

                            let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                            text = await text.getText();
                            addContext(this.ctx, 'Country '+i+': '+text);

                            let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                            await clickSingle.click();
                            await driver.sleep(2000);

                            let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                            countModal = countModal.length;
                            addContext(this.ctx, 'Total post: '+countModal);

                            let clickUser = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child('+i+')');
                            await clickUser.click();
                            await driver.sleep(2000);

                            let postClick = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                            await postClick.click();
                            await driver.sleep(2000);

                            let secondTrackerName = await page.findByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.screen-name');
                            secondTrackerName = await secondTrackerName.getText();

                            assert.equal(secondTrackerName, trackerName);
                            addContext(this.ctx, 'Account Tracker Name exists in Post details Modal.');

                            let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                            closeModal.click();
                            await driver.sleep(2000);

                            closeModal = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button > i');
                            closeModal.click();
                            await driver.sleep(2000);

                            closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                            closeModal.click();
                            await driver.sleep(2000);
                        }
                    }                    
                }
                else assert.fail('No twitter tracker found or added');
            });

            it('click on each Follower posts and check account Tracker Name exists in Post details Modal from top states.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let trackerName = await page.findByCSS('#AT-Dashboard > div > div > div.info > p > span');
                    trackerName = await trackerName.getText();

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(4) > a');
                    await clickInsights.click();
                    await driver.sleep(8000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    let scrollTo = await page.findByCSS('#top-region > header > h1');
                    await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                    await driver.sleep(3000);

                    let secondTab = await page.findByCSS('#follower-location > header > div > div > a:nth-child(2)');
                    await secondTab.click();
                    let secondText = await page.findByCSS('#top-region > header > h1');
                    secondText = await secondText.getText();

                    if (secondText == 'Top States') addContext(this.ctx, 'Second tab clicked');
                    else addContext(this.ctx, 'State tab not clicked');

                    let count = await page.findElementsByCSS('#chart-top-region > table > tbody > tr');
                    count = count.length;

                    if (count > 0) {
                        if (count > 4)
                            count = 4;
                        for (let i = 1; i <= count; i++) {
                            addContext(this.ctx, '----------------');

                            let text = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+') > td.region');
                            text = await text.getText();
                            addContext(this.ctx, 'Country '+i+': '+text);

                            let clickSingle = await page.findByCSS('#chart-top-region > table > tbody > tr:nth-child('+i+')');
                            await clickSingle.click();
                            await driver.sleep(2000);

                            let countModal = await page.findElementsByCSS('#follower-list-view > div.content > table > tbody > tr');
                            countModal = countModal.length;
                            addContext(this.ctx, 'Total Users: '+countModal);

                            let clickUser = await page.findByXPath('//*[@id="follower-list-view"]/div[2]/table/tbody/tr['+i+']');
                            await clickUser.click();
                            await driver.sleep(2000);

                            let clickPost = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                            await clickPost.click();
                            await driver.sleep(2000);

                            let secondTrackerName = await page.findByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.screen-name');
                            secondTrackerName = await secondTrackerName.getText();

                            assert.equal(secondTrackerName, trackerName);
                            addContext(this.ctx, 'Account Tracker Name exist in Post Modal');

                            let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                            closeModal.click();
                            await driver.sleep(1000);

                            closeModal = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button > i');
                            closeModal.click();
                            await driver.sleep(1000);

                            closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                            closeModal.click();
                            await driver.sleep(1000);
                        }
                    }
                }
                else assert.fail('No twitter tracker found or added');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();