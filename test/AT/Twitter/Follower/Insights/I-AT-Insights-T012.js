/**
 * ### TEST CASE:
 * I-AT-Insights-T012
 *
 * ### TEST TITLE:
 * Click on each posts Hashtag or Account Tracking Link to track the keyword from Follower Bios.
 *
 * ### TEST SUMMARY: 
 * User should be able to Navigate to the follower Insights page, Click on each posts Hashtag or Account Tracking Link to track the keyword from Follower Bios section.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../../lib/basePage');
const config = require('../../../../../utils/config');
const generate = require('../../../../../utils/generate')();
const keyhole = require('../../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Click on each posts Hashtag or Account Tracking Link to track the keyword from Follower Bios.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on each posts Hashtag or Account Tracking Link to track the keyword from Follower Bios.', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#AT-Dashboard > div > div > div.info > p > span'), 2000));

                    let clickInsights = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(5) > a');
                    await clickInsights.click();
                    addContext(this.ctx, 'Tracker name: '+trackerName);
                    await driver.sleep(8000);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    await page.scrollPage('//*[@id="top-keywords"]/header/h1');
                    await driver.sleep(3000);

                    let totalKeyword = await page.findElementsByCSS('#chart-top-keywords > table > tbody > tr');
                    totalKeyword = totalKeyword.length;

                    if (totalKeyword > 5) totalKeyword = 5;

                    for (let i = 1; i <= totalKeyword; i++) {
                        addContext(this.ctx, '------------- '+i+' --------------');

                        let keywordName = await page.findByCSS('#chart-top-keywords > table > tbody > tr:nth-child('+i+') > td.keyword');
                        keywordName = await keywordName.getText();
                        addContext(this.ctx, 'Keyword: '+keywordName);

                        let clickKeyword = await page.findByCSS('#chart-top-keywords > table > tbody > tr:nth-child('+i+')');
                        await clickKeyword.click();
                        await driver.sleep(2000);

                        let clickModal = await page.findByCSS('#follower-list-view > div.content > table > tbody > tr:nth-child(1)');
                        await clickModal.click();
                        await driver.sleep(1000);

                        let postClick = await page.findByCSS('#follower-post-list-view > div.content > table > tbody > tr:nth-child(2)');
                        await postClick.click();
                        await driver.sleep(2000);

                        let checkTracker = await page.checkElementByCSS('#post-view > div.content > div > div > div.post-caption > a');
                        if (checkTracker) {
                            let hTLink = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a');
                            addContext(this.ctx, 'Clicked hashtag tracking link');
                            await hTLink.click();
                            await driver.sleep(2000);

                            let tab1, tab2;

                            await driver.getAllWindowHandles().then(function(windowHandles) {
                                tab1 = windowHandles[0];
                                tab2 = windowHandles[1];
                            });

                            await driver.switchTo().window(tab2);

                            let url = await driver.getCurrentUrl();
                            addContext(this.ctx, 'URL after Clicked: '+url);

                            if (url == 'https://keyhole.co/preview_new') {
                                addContext(this.ctx, 'Passed');
                            } else if (url == 'https://keyhole.co/account-tracking/preview') {
                                addContext(this.ctx, 'Passed');
                            } else {
                                addContext(this.ctx, 'failed');
                            }
                            await driver.close();                           
                            await driver.switchTo().window(tab1);
                        }

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                        await closeModal.click();
                        await driver.sleep(1000);
        
                        closeModal = await page.findByCSS('#follower-post-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                        
                        closeModal = await page.findByCSS('#follower-list-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);

                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();