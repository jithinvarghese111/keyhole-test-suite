/**
 * ### TEST CASE:
 * I-AT-Posts-T001
 *
 * ### TEST TITLE:
 * Search posts
 *
 * ### TEST SUMMARY:
 * User is trying to input random search keywords and recording the search results
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking search for posts using keywords', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	await driver.sleep(2000);

                  	let totalPosts = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total posts before search: '+totalPosts);

                  	for (let i = 1; i <= 5; i++) {
	                  	let keyword = randomWord();
	                  	let inputRandom = await page.findByCSS('#AT-Posts > div > div > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > form > input');
	                  	await page.write(inputRandom, keyword);
	                  	await driver.sleep(2000);
	                  	addContext(this.ctx, '#'+i+': Keyword searched: '+keyword);

                        let searchBtn = await page.findByCSS('#AT-Posts > div > div > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > form > button');
	                  	await searchBtn.click();
	                  	await driver.sleep(2000);

	                  	let totalPosts1 = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                  	totalPosts1 = await totalPosts1.getText();
	                  	addContext(this.ctx, '#'+i+': Total posts after search: '+totalPosts1);

	                  	await page.clearFields('css', '#AT-Posts > div > div > div.key-trackerSubHeader > div.key-trackerSubHeader__filters > form > input');
                		addContext(this.ctx, '--------------------------------');
	            	}
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No facebook tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();