/**
 * ### TEST CASE:
 * I-AT-Posts-T013
 *
 * ### TEST TITLE:
 * View post on Twitter from post details modal
 *
 * ### TEST SUMMARY:
 * User should be able to click on the follower list and view on Twitter from the post details modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('View post on Twitter from post details modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('View post on Twitter from post details modal', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    let clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                    let trackerNameHead = await page.findByCSS('#AT-Posts > div > div > div.info > p > span');
                    trackerNameHead = await trackerNameHead.getText();
                    addContext(this.ctx, 'Tracker name from header: '+trackerNameHead);

                    let totalPost = await page.findElementsByCSS('#all-posts > div > div.table-container > table > tbody > tr');
                    totalPost = totalPost.length;

                    if (totalPost > 5) totalPost = 5;

                    for (let i = 1; i <= totalPost; i++) {
                        addContext(this.ctx, '--------------------------');

                        let clickEach = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr:nth-child('+i+') > td.post');
                        let postCaption = await clickEach.getText();
                        addContext(this.ctx, 'Post Caption: '+postCaption);
                        await clickEach.click();
                        await driver.sleep(1000);
  
                        let viewOnTwitter = await page.findByCSS('#post-view > div.content > div > div > a');
                        await viewOnTwitter.click();
                        addContext(this.ctx, 'Post Viewed on Twitter');

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];                
                        });

                        await driver.switchTo().window(tab1);

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
 
                    }
    
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();