/**
 * ### TEST CASE:
 * I-AT-Posts-T014
 *
 * ### TEST TITLE:
 * Create Hashtag tracker from Twitter Post details.
 *
 * ### TEST SUMMARY:
 * User is trying to Create Hashtag tracker from Twitter Post details.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Create Hashtag tracker from Twitter Post details.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Create Hashtag tracker from Twitter Post details.', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
    
                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);

                    let trackerName = await clickLink.getText();
                    await clickLink.click();

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	
                  	let firstPost = await page.findByCSS('#all-posts > div.keyhole-table.all-posts-table > div.table-container > table > tbody > tr:nth-child(1)');
                    await firstPost.click();
                    await driver.sleep(2000);

                    let checkHT = await page.checkElementByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                    if (checkHT) {
                      let ht = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                      await ht.click(); 

                      addContext(this.ctx, 'Hashtag Tracker Created.');

                    }else{
                      addContext(this.ctx, 'No Hashtag found.');
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No facebook tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();