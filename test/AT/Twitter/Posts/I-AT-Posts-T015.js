/**
 * ### TEST CASE:
 * I-AT-Posts-I015
 *
 * ### TEST TITLE:
 * Checking tracker name is present in the modal on click the Twitter Post.
 *
 * ### TEST SUMMARY:
 * User should be able to click on the Twitter post and check tracker name is present in the modal opened.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking tracker name is present in the modal on click the Twitter Post.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking tracker name is present in the modal on click the Twitter Post.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
    
                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    await clickLink.click();

                    let trackerName = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--topStats > div > div.key-topStats--primary > div.key-topStats--primaryProfile > div:nth-child(2) > a > span:nth-child(1)');
                    trackerName = await trackerName.getText();

                    let postClick = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(3) > a');
                    await postClick.click();
                    await driver.sleep(3000);

                    let totalPosts = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-paginationContainer > div.key-pagination__info > strong.key-pagination__total');
                    totalPosts = await totalPosts.getText();
                    totalPosts = totalPosts.replace (/,/g, "");
                    addContext(this.ctx, 'Total post: '+totalPosts);

                    if (totalPosts > 5) totalPosts = 5;

                    for (let i = 1; i <= 5; i++) {
                        addContext(this.ctx, '-----------------------------');

                        let clickPost = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(2000);

                        let nameModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalHeader > div > div > div.key-userDetails.key-userDetails--fullWidth > span:nth-child(1) > a');
                        let selected = await nameModal.getAttribute("href");
                        let trackernameModal = await nameModal.getText();
                        addContext(this.ctx, 'Tracker name from modal: '+trackernameModal);
                        addContext(this.ctx, 'URL before click #'+i+': '+selected);
                        await nameModal.click();
                        await driver.sleep(2000);

                        if (trackerName.indexOf(trackernameModal) > -1) addContext(this.ctx, 'Tracker names are same');
                        else addContext(this.ctx, 'Tracker names are not same');
                                                
                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];                
                        });

                        await driver.switchTo().window(tab2);
                        let url = await driver.getCurrentUrl();
                        addContext(this.ctx, 'URL after click #'+i+': '+url);

                        if (url == selected) addContext(this.ctx, '#'+i+': Both are equal');
                        else addContext(this.ctx, '#'+i+': Both are not equal');
                        await driver.close();
                        await driver.switchTo().window(tab1);
                        await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalHeader > button'), 20000));

                        let closeModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalHeader > button');
                        await closeModal.click();
                        
                    }
                }
                else  assert.fail('No Twitter tracker found or added.');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();