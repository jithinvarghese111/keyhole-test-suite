/**
 * ### TEST CASE:
 * I-AT-Posts-T005
 *
 * ### TEST TITLE:
 * Sort posts by LIKES, RETWEETS, DATE
 *
 * ### TEST SUMMARY:
 * User should be able to sort the post using LIKES, RETWEETS, DATE
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Sort posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Sort the post using LIKES, RETWEETS, DATE', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                  	let totalPosts = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total post before filter: '+totalPosts);

                  	let clickLikes = await page.findByCSS('#all-posts > div > div.table-container > table > thead > tr > th.like-count.toggle');
                    await clickLikes.click();
                    await driver.sleep(1000);
                    let checkLikes = await page.checkElementByCSS('#all-posts > div > div.table-container > table > thead > tr > th.like-count.toggle.sorted-by');
                    if (checkLikes) addContext(this.ctx, 'Sort post by LIKES success');
                    else addContext(this.ctx, 'Sort post by LIKES failed');

                    let clickRetweet = await page.findByCSS('#all-posts > div > div.table-container > table > thead > tr > th.comment-count.toggle');
                    await clickRetweet.click();
                    await driver.sleep(1000);
                    let checkRetweet = await page.checkElementByCSS('#all-posts > div > div.table-container > table > thead > tr > th.comment-count.toggle.sorted-by');
                    if (checkRetweet) addContext(this.ctx, 'Sort post by RETWEETS success');
                    else addContext(this.ctx, 'Sort post by RETWEETS failed');

                    let clickDate = await page.findByCSS('#all-posts > div > div.table-container > table > thead > tr > th.date.toggle');
                    await clickDate.click();
                    await driver.sleep(1000);
                    let checkDate = await page.checkElementByCSS('#all-posts > div > div.table-container > table > thead > tr > th.date.toggle.sorted-by');
                    if (checkDate) addContext(this.ctx, 'Sort post by DATE success');
                    else addContext(this.ctx, 'Sort post by DATE failed');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No facebook tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();