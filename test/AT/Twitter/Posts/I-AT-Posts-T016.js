/**
 * ### TEST CASE:
 * I-AT-Posts-T016
 *
 * ### TEST TITLE:
 * Checking Next & Prev Post Button in Post modal on click the Twitter Posts list
 *
 * ### TEST SUMMARY:
 * User should be able to click on the Twitter Post and Checking Next & Prev Post Button in Post modal opened.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });
 
(async function example() {
    try {
        describe('Checking Next & Prev Post Button in Post modal on click the Twitter Posts list', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });
   
                await page.gotoLogin();
            });

            it('User should be able to click on the Twitter Post and Checking Next & Prev Post Button in Post modal opened.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
    
                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    await clickLink.click();

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(3) > a');
                    await clickPost.click();
                    await driver.sleep(10000);

                    let totalPosts = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-paginationContainer > div.key-pagination__info > strong.key-pagination__total');
                    totalPosts = await totalPosts.getText();
                    totalPosts = totalPosts.replace (/,/g, "");
                    addContext(this.ctx, 'Total Posts: '+totalPosts);

                    if (totalPosts > 5) totalPosts = 5;
                   
                    for (var i = 1; i < totalPosts; i++) {
                    addContext(this.ctx, '--------Clicked on Post: '+i+'-------------');
                    let clickPosts = await page.findByCSS('#account-tracking > div > section.route-section > section > div > div.key-tableWrapper > table > tbody > tr:nth-child('+i+')');
                    await clickPosts.click();
                    await driver.sleep(1000);
                    
                    let checkModal = await page.checkElementByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active');
                    if (checkModal) {

                        for (var j = 1; j <= 4; j++) {
                                                         
                        let nextPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalNav > div > div:nth-child(2) > button.key-button__postNav.key-button__postNav--next.key-link.link--blue');
                        await nextPost.click();
                        await driver.sleep(1000);

                        let postNumber = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalNav > div > div:nth-child(1) > span > strong:nth-child(1)');
                        postNumber = await postNumber.getText();
                        addContext(this.ctx, 'Clicked Next Post: '+postNumber+'');
                        }

                        for (var k = 1; k <= 4; k++) {
                                                                                 
                        let prevPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalNav > div > div:nth-child(2) > button.key-button__postNav.key-button__postNav--prev.key-link.link--blue');
                        await prevPost.click();
                        await driver.sleep(1000);

                        let postsNumber = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalNav > div > div:nth-child(1) > span > strong:nth-child(1)');
                        postsNumber = await postsNumber.getText();
                        addContext(this.ctx, 'Clicked Prev Post: '+postsNumber+'');
                        }

                        let closeSec = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div.key-modalHeader > button');
                        await closeSec.click();
                        await driver.sleep(2000); 

                    } else assert.fail('Error Occured after Clicks on post: '+i+'');

                    }                                           
                }
                else assert.fail('No twitter tracker found or added');
                   
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();