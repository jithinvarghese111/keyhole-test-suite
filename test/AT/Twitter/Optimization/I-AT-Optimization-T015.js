/**
 * ### TEST CASE:
 * I-AT-Optimization-T015
 *
 * ### TEST TITLE:
 * Checking Next & Prev Post Button in Post modal on click the day of Best Times to Post section.
 *
 * ### TEST SUMMARY:
 * User should be able to click on the Tday and Checking Next & Prev Post Button in Post modal opened.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });
 
(async function example() {
    try {
        describe('Checking Next & Prev Post Button in Post modal on click the day of Best Times to Post section.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test111111 Started'
                });
   
                await page.gotoLogin();
            });

            it('Checking Next & Prev Post Button in Post modal on click the day of Best Times to Post section.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                  	let clickOpti = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(2) > a');
                    await clickOpti.click();
                    await driver.sleep(10000);

                    await page.scrollPage('//*[@id="account-tracking"]/div/section[1]/section/div/figure[3]/div[1]/h3');
                    await driver.sleep(2000);

                    let totalDays = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr');
                
                    for (var i = 1; i <= totalDays.length; i++) {
                        addContext(this.ctx, '--------------------');

                        let getDay = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr:nth-child('+i+') > td:nth-child(1)');
                        getDay = await getDay.getText();
                        addContext(this.ctx, 'Day '+i+': '+getDay);

                        let clickSingle = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr:nth-child('+i+')');
                        await clickSingle.click();
                        await driver.sleep(2000);          
                        
                        let checkModal = await page.checkElementByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active');
                        if (checkModal) {

                            let totalPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > div > div > p');
                            totalPost = await totalPost.getText();
                            let firstParam = totalPost.substr(0, totalPost.indexOf(' '));
                            addContext(this.ctx, 'Total posts in modal: '+totalPost);
                        
                            let clickPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr');
                            await clickPost.click();
                            await driver.sleep(1000);

                            if (firstParam > 3) {
                                for (var j = 1; j <= 3; j++) {
                                    let nextPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > div.key-modalNav > div > div > button.key-button__postNav.key-button__postNav--next.key-link.link--blue');
                                    await nextPost.click();
                                    addContext(this.ctx, 'Clicked Next Post: '+j+'');
                                    await driver.sleep(1000);                            
                                }

                                for (var k = 1; k <= 3; k++) {
                                    let prevPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > div.key-modalNav > div > div > button.key-button__postNav.key-button__postNav--prev.key-link.link--blue');
                                    await prevPost.click();
                                    addContext(this.ctx, 'Clicked Prev Post '+k+'');
                                    await driver.sleep(1000);
                                } 
                            }else addContext(this.ctx, 'No enough Posts for checking prev & next Button.');

                            let closeSec = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > div.key-modalHeader > button');
                            await closeSec.click();
                            await driver.sleep(2000); 
                            
                        } else assert.fail('Error Occured after Clicks on post: '+i+'');
                    }                                           
                }
                else assert.fail('No twitter tracker found or added'); 
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();