/**
 * ### TEST CASE:
 * I-AT-Optimization-T003
 *
 * ### TEST TITLE:
 * Check best time to post section
 *
 * ### TEST SUMMARY:
 * User is trying to click each row in best time to post section
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check best time to post section', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click each row in best time to post section', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                    await clickPost.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    await page.scrollPage('//*[@id="optimal-post-time-by-day"]');
                  await driver.sleep(2000);

                    let totalCount = await page.findElementsByCSS('#chart-best-post-time > table > tbody > tr');
                    totalCount = totalCount.length;

                    addContext(this.ctx, 'Total count: '+totalCount);

                    for (let i = 1; i <= totalCount; i++) {
                      addContext(this.ctx, '--------------------');

                      let getDay = await page.findByCSS('#chart-best-post-time > table > tbody > tr:nth-child('+i+') > td.day');
                      getDay = await getDay.getText();
                      addContext(this.ctx, 'Day '+i+': '+getDay);
                      await driver.sleep(2000);

                      let clickSingle = await page.findByCSS('#chart-best-post-time > table > tbody > tr:nth-child('+i+')');
                      await clickSingle.click();
                      await driver.sleep(2000);

                      let totalPost = await page.findElementsByCSS('#post-list-view > div.content > table > tbody > tr');
                      addContext(this.ctx, 'Total posts in modal: '+totalPost.length);

                      let closeModal = await page.findByCSS('#post-list-view > div.top-bar > div > div.x-button');
                      await closeModal.click();
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();