/**
 * ### TEST CASE:
 * I-AT-Optimization-T012
 *
 * ### TEST TITLE:
 * click each post and from the modal click on VIEW ON TWITTER button of Best Times to Post section.
 *
 * ### TEST SUMMARY:
 * User is trying to click each post and from the modal click on VIEW ON TWITTER button of Best Times to Post and check page exists or not.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Likes, Comments, Shares counts in the post list is same in second post details modal.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Likes, Comments, Shares counts in the post list is same in second post details modal.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(2000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#AT-Dashboard > div > div > div.info > p > span'), 3000));

                    let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                    await clickPost.click();
                    await driver.sleep(10000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    await page.scrollPage('//*[@id="optimal-post-time-by-day"]');
                    await driver.sleep(2000);

                    let totalCount = await page.findElementsByCSS('#chart-best-post-time > table > tbody > tr');    
                    totalCount = totalCount.length;

                    addContext(this.ctx, 'Total count: '+totalCount);

                    for (let i = 1; i <= totalCount; i++) {
                      addContext(this.ctx, '--------------------');

                      let getDay = await page.findByCSS('#chart-best-post-time > table > tbody > tr:nth-child('+i+') > td.day');
                      getDay = await getDay.getText();
                      addContext(this.ctx, 'Day '+i+': '+getDay);
                      await driver.sleep(2000);

                      let clickSingle = await page.findByCSS('#chart-best-post-time > table > tbody > tr:nth-child('+i+')');
                      await clickSingle.click();
                      await driver.sleep(2000);

                      let totalPost = await page.findElementsByCSS('#post-list-view > div.content > table > tbody > tr');
                      addContext(this.ctx, 'Total posts in modal: '+totalPost.length);

                      let clickPost = await page.findByCSS('#post-list-view > div.content > table > tbody > tr');
                      await clickPost.click();
                      await driver.sleep(2000);

                      let checkImage = await page.checkElementByCSS('#post-view > div.content > div >  a > img');
                      if(checkImage) addContext(this.ctx, 'Displayed Post with Image');

                      let checkVideo = await page.checkElementByCSS('#post-view > div.content > div > video');
                      if (checkVideo) addContext(this.ctx, 'Displayed Post with Video');

                      let getURL = await page.findByCSS('#post-view > div.content > div > div > a');
                      let selected = await getURL.getAttribute("href");
                      addContext(this.ctx, 'URL before click #'+i+': '+selected);
                      await getURL.click();

                      let tab1, tab2;

                      await driver.getAllWindowHandles().then(function(windowHandles) {
                          tab1 = windowHandles[0];
                          tab2 = windowHandles[1];
                      });

                      await driver.switchTo().window(tab2);
                      let url = await driver.getCurrentUrl();
                      addContext(this.ctx, 'URL after click #'+i+': '+url);

                      if (url == selected) addContext(this.ctx, '#'+i+': Both are equal');
                      else addContext(this.ctx, '#'+i+': Both are not equal');
                      await driver.close();
                      await driver.switchTo().window(tab1);
                      
                      let closeSec = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                      await closeSec.click();
                      await driver.sleep(2000); 

                      let closeModal = await page.findByCSS('#post-list-view > div.top-bar > div > div.x-button');
                      await closeModal.click();
                    }
                }
                else assert.fail('No twitter tracker found or added'); 
                    
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();