/**
 * ### TEST CASE:
 * I-AT-Optimization-T013
 *
 * ### TEST TITLE:
 * Navigate to the Twitter Optimization page, click on each posts HT or AT Tracking link to track the keyword of Best Times to Post section.
 *
 * ### TEST SUMMARY:
 * User should be able to Navigate to the Twitter Optimization page, click on each posts HT or AT Tracking link to track the keyword of Best Times to Post section.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Click on each posts HT or AT Tracking link to track the keyword of Best Times to Post section.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on each posts HT or AT Tracking link to track the keyword of Best Times to Post section.', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(2000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#AT-Dashboard > div > div > div.info > p > span'), 3000))

                    let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                    await clickPost.click();
                    await driver.sleep(10000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    await page.scrollPage('//*[@id="post-length"]/header/h1');
                    await driver.sleep(5000);

                    let totalCount = await page.findElementsByCSS('#chart-best-post-time > table > tbody > tr');    
                    totalCount = totalCount.length;

                    for (let i = 1; i <= totalCount; i++) {
                      addContext(this.ctx, '--------------------');

                      let getDay = await page.findByCSS('#chart-best-post-time > table > tbody > tr:nth-child('+i+') > td.day');
                      getDay = await getDay.getText();
                      addContext(this.ctx, 'Day '+i+': '+getDay);

                      let clickSingle = await page.findByCSS('#chart-best-post-time > table > tbody > tr:nth-child('+i+')');
                      await clickSingle.click();
                      await driver.sleep(3000);

                      let totalPost = await page.findElementsByCSS('#post-list-view > div.content > table > tbody > tr');
                      addContext(this.ctx, 'Total posts in modal: '+totalPost.length);

                      let clickPost = await page.findByCSS('#post-list-view > div.content > table > tbody > tr');
                      await clickPost.click();
                      await driver.sleep(2000);

                      let checkHT = await page.checkElementByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag');
                      if (checkHT) {
                        let startTracking = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-hashtag'); 
                        let htTrackerName = await startTracking.getText();    
                        addContext(this.ctx, 'Clicked HT tracking link '+htTrackerName);
                        await startTracking.click();
                        await driver.sleep(2000);

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);

                        let url = await driver.getCurrentUrl();

                        let status = '';

                        if (url == 'https://keyhole.co/preview') addContext(this.ctx, 'Passed');
                        else addContext(this.ctx, 'Failed');
                        await driver.close();
                        await driver.switchTo().window(tab1);

                      }else{
                        addContext(this.ctx, 'No HT tacker Link Found');
                      }
                      
                      let checkAT = await page.checkElementByCSS('#post-view > div.content > div > div > div.post-caption > a');
                      if (checkAT) {
                        let startTracking = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a'); 
                        let atTrackerName = await startTracking.getText();    
                        addContext(this.ctx, 'Clicked AT tracking link '+atTrackerName);
                        await startTracking.click();
                        await driver.sleep(2000);

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);

                        let url = await driver.getCurrentUrl();

                        let status = '';

                        if (url == 'https://keyhole.co/account-tracking/preview') addContext(this.ctx, 'Passed');
                        else addContext(this.ctx, 'Failed');

                        await driver.close();
                        await driver.switchTo().window(tab1);
                      }else{
                        addContext(this.ctx, 'No AT tacker Link Found');
                      }

                      let closeSec = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                      await closeSec.click();
                      await driver.sleep(2000);

                      let closeModal = await page.findByCSS('#post-list-view > div.top-bar > div > div.x-button');
                      await closeModal.click();
                  }
              }
              else assert.fail('No twitter tracker found or added'); 
                 
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();