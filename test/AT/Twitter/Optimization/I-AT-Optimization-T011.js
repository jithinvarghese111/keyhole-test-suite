/**
 * ### TEST CASE:
 * I-AT-Optimization-T011
 *
 * ### TEST TITLE:
 * Checking Likes, Comments, Shares counts in the post list is same in second post details modal of best time to post section
 *
 * ### TEST SUMMARY:
 * User is trying to click each row in best time to post section and Checking Likes, Comments, Shares counts in the post list is same in second post details modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Likes, Comments, Shares counts in the post list is same in second post details modal.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Likes, Comments, Shares counts in the post list is same in second post details modal.', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(2000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#AT-Dashboard > div > div > div.info > p > span'), 2000));

                    let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                    await clickPost.click();
                    await driver.sleep(10000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    await page.scrollPage('//*[@id="optimal-post-time-by-day"]');
                    await driver.sleep(2000);

                    let totalCount = await page.findElementsByCSS('#chart-best-post-time > table > tbody > tr');    
                    totalCount = totalCount.length;

                    addContext(this.ctx, 'Total count: '+totalCount);

                    for (let i = 1; i <= totalCount; i++) {
                      addContext(this.ctx, '--------------------');

                      let getDay = await page.findByCSS('#chart-best-post-time > table > tbody > tr:nth-child('+i+') > td.day');
                      getDay = await getDay.getText();
                      addContext(this.ctx, 'Day '+i+': '+getDay);
                      await driver.sleep(2000);

                      let clickSingle = await page.findByCSS('#chart-best-post-time > table > tbody > tr:nth-child('+i+')');
                      await clickSingle.click();
                      await driver.sleep(2000);

                      let totalPost = await page.findElementsByCSS('#post-list-view > div.content > table > tbody > tr');
                      addContext(this.ctx, 'Total posts in modal: '+totalPost.length);

                      let likeBefore = await page.findByCSS('#post-list-view > div.content > table > tbody > tr > td.like-count.twitter');
                      likeBefore = await likeBefore.getText();

                      let retweetBefore = await page.findByCSS('#post-list-view > div.content > table > tbody > tr > td.comment-count.twitter');
                      retweetBefore = await retweetBefore.getText();

                      let dateBefore = await page.findByCSS('#post-list-view > div.content > table > tbody > tr > td.date.sorted-by');
                      dateBefore = await dateBefore.getText();

                      let clickPost = await page.findByCSS('#post-list-view > div.content > table > tbody > tr');
                      await clickPost.click();
                      await driver.sleep(2000);

                      let likeAfter = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-likes > span');
                      likeAfter = await likeAfter.getText();

                      let retweetAfter = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-comments > span');
                      retweetAfter = await retweetAfter.getText();

                      let dateAfter = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-date');
                      dateAfter = await dateAfter.getText();

                      if (likeAfter == likeBefore) addContext(this.ctx, 'Likes Counts are same'); else addContext(this.ctx, 'Likes Counts are not same');
                      if (retweetAfter == retweetBefore) addContext(this.ctx, 'RT Counts are same'); else addContext(this.ctx, 'RT Counts are not same');                     
                      if (dateAfter == dateBefore) addContext(this.ctx, 'Date is same'); else addContext(this.ctx, 'Date is not same');

                      assert.equal(likeAfter, likeBefore);
                      assert.equal(retweetAfter, retweetBefore);
                      assert.equal(dateAfter, dateBefore);

                      let closeSec = await page.findByCSS('#post-view > div.top-bar > div > div.x-button > i');
                      await closeSec.click();
                      await driver.sleep(2000); 

                      let closeModal = await page.findByCSS('#post-list-view > div.top-bar > div > div.x-button');
                      await closeModal.click();
                    }
                }
                else assert.fail('No twitter tracker found or added');                     
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();