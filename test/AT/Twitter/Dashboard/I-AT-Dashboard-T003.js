/**
 * ### TEST CASE:
 * I-AT-Dashboard-T003
 *
 * ### TEST TITLE:
 * Managing top posts by engagements
 *
 * ### TEST SUMMARY:
 * User is trying to click each post and from the modal click on VIEW ON TWITTER button and check page exists or not
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Managing top posts by engagements', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User is trying to click each post and from the modal click on VIEW ON button', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let secondTrack = await page.checkElementByCSS('#top-posts > header > h1');

                    if (secondTrack) {
                        let scrollTo = await page.findByCSS('#top-posts > header > h1');
                        await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                        await driver.sleep(5000);

                        let totalCount = await page.findElementsByCSS('#top-posts > div > table > tbody > tr');
                        totalCount = totalCount.length;

                        if (totalCount > 5) totalCount = 5;

                        for (let i = 1; i <= totalCount; i++) {
                            let clickPost = await page.findByCSS('#top-posts > div > table > tbody > tr:nth-child('+i+')');
                            await clickPost.click();
                            await driver.sleep(2000);

                            let getURL = await page.findByCSS('#post-view > div.content > div > div > a');
                            let selected = await getURL.getAttribute("href");
                            addContext(this.ctx, 'URL before click #'+i+': '+selected);
                            await getURL.click();

                            let tab1, tab2;

                            await driver.getAllWindowHandles().then(function(windowHandles) {
                                tab1 = windowHandles[0];
                                tab2 = windowHandles[1];
                            });

                            await driver.switchTo().window(tab2);
                            let url = await driver.getCurrentUrl();
                            addContext(this.ctx, 'URL after click #'+i+': '+url);

                            if (url == selected) addContext(this.ctx, '#'+i+': Both are equal');
                            else addContext(this.ctx, '#'+i+': Both are not equal');

                            await driver.switchTo().window(tab1);

                            let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                            await closeModal.click();
                            await driver.sleep(2000);
                        }
                    }
                    else {
                        status = 'We were unable to find any posts from this account.';

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': status
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No facebook tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();