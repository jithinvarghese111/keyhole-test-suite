/**
 * ### TEST CASE:
 * I-AT-Dashboard-T002
 *
 * ### TEST TITLE:
 * Changing Time Range
 *
 * ### TEST SUMMARY:
 * User should be able to change the time range for which they recieve data back
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Changing Time Range', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking tracker time range', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 4000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let secondTrack = await page.checkElementByCSS('#AT-Dashboard > div > div > div.info > p > span');

                    if (secondTrack) {
                        const allowedDays = [30, 90, 365]
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                        let changeDate = await setDateRangeAccount(days)

                        if (changeDate) {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Selected date period': days,
                                    'Test status': 'Success'
                                }
                            });

                            let checkEle = await page.checkElementByCSS('#account > div > div.top-bar > div.account-info > div.posts > p');
                            if (!checkEle) {
                                    addContext(this.ctx, {
                                    title: 'Test Results',
                                    value: {
                                        'Test status': 'Posts no found in this date range'
                                    }
                                });
                            }
                        }
                        else {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Date selection failed'
                                }
                            });
                        }

                        let currentURL = await driver.getCurrentUrl();
                        status += 'currentURL :'+currentURL;

                        let params = currentURL.substring(currentURL.indexOf("?")+1);
                        let firstParam = params.substr(0, params.indexOf('='));
                        
                        if (firstParam.indexOf('days') > -1) {
                            let status = '';

                            status += 'Date added to URL., ';

                            await driver.navigate().refresh();
                            await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 3000));

                            if (firstParam.indexOf('days') > -1) {
                                status += 'Date persisted upon reload.';
                            }
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': status
                                }
                            });
                        }
                        else {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Tracker name': trackerName,
                                    'Test status': 'Date not added to URL.'
                                }
                            });
                        }         
                    }
                    else {
                        status = 'We were unable to find any posts from this account.';

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': status
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No twitter tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();