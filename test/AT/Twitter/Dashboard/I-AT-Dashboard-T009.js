/**
 * ### TEST CASE:
 * I-AT-Dashboard-T009
 *
 * ### TEST TITLE:
 * Check the official website URL of the tracker
 *
 * ### TEST SUMMARY:
 * User should be able to get the official website URL of the tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check the official website URL of the tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Get the official website URL of the tracker', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 4000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);

                    await clickLink.click();
                    await driver.sleep(10000);

                    let totalItems = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--topStats > div > div.key-topStats--primary > div.key-topStats--primaryProfile > div:nth-child(2) > div > div > p');

                    for (let i = 1; i <= totalItems.length; i++) {
                        let checkEle = await page.checkElementByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--topStats > div > div.key-topStats--primary > div.key-topStats--primaryProfile > div:nth-child(2) > div > div > p:nth-child('+i+') > i.fa-link');

                        if (checkEle) {
                            let getURL = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--topStats > div > div.key-topStats--primary > div.key-topStats--primaryProfile > div:nth-child(2) > div > div > p:nth-child('+i+') > a');
                            getURL = await getURL.getText();

                            addContext(this.ctx, 'Twitter Website URL: '+getURL);
                        }
                    }
                }
                else
                    assert.fail('No twitter tracker found or added.');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();