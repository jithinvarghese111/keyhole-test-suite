/**
 * ### TEST CASE:
 * I-AT-Dashboard-T008
 *
 * ### TEST TITLE:
 * Checking tracker profile exists in Top posts By Engagements Modal
 *
 * ### TEST SUMMARY:
 * User is trying to click on the tracker profile link and checking the profile page is exists in Top posts By Engagements Modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking tracker profile exists in Top posts By Engagements Modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on the tracker link and checking the page is exists in Top posts By Engagements Modal', async () => {
                await page.redirectToAccDashboard();

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 4000));

                let trackerPos = await accountTrackerSelect('twitter');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    await page.scrollPage('//*[@id="account-tracking"]/div/section[1]/section/div/figure[3]/div[1]/h3/span');
                    await driver.sleep(3000);

                    let totalPosts = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr');
                    for (var i = 1; i <= 5; i++) {
                        addContext(this.ctx, '--------------------'+i+'---------------------');

                        let clickPost = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let profileURL = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div > div > div > div.key-userDetails.key-userDetails--fullWidth > span.key-influencers__linkWrapper.key-influencers__linkWrapper--small > a');
                        let profileURLText = await profileURL.getAttribute("href");
                        addContext(this.ctx, 'Profile URL from Dashboard: '+profileURLText);
                        await profileURL.click();
                        await driver.sleep(1000);

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);
                        let url = await driver.getCurrentUrl();

                        await driver.close();
                        await driver.switchTo().window(tab1);
                        addContext(this.ctx, 'URL after click #: '+url);

                        assert.equal(url, profileURLText);

                        if (url === profileURLText) addContext(this.ctx, 'Both links are same');
                        else addContext(this.ctx, 'Links are not same');
                        
                        let closeModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div > button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else assert.fail('No twitter tracker found or added.');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();