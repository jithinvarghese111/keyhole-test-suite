/**
 * ### TEST CASE:
 * I-AT-Group-004
 *
 * ### TEST TITLE:
 * Go to group dashboard and check tracker details
 *
 * ### TEST SUMMARY:
 * Go to multiple group dashboard and checking the tracker details
 *
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Go to Group Dasbhoard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to group dashboard and return to home page', async () => {
                await page.redirectToAccDashboard();

                let group_name = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                	let totalGroups = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalGroups = totalGroups.length;

                    if (totalGroups > 3)
                    	totalGroups = 3;

                    for (let i = 1; i <= totalGroups; i++) {
		                let goTo = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr['+i+']/td/div[1]/a');
		                await goTo.click();
		                await driver.sleep(5000);

		                let groupName = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong');
		                groupName = await groupName.getText();
		                group_name += groupName+', ';

		                let dashClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a');
		                await dashClick.click();
		                await driver.sleep(1000);
		            }

		            addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': group_name.replace(/,\s*$/, "")
                        }
                    });
	            }
	            else {
	            	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
	            }
            });

            it('Go to group dashboard and check group', async () => {
                await page.redirectToAccDashboard();

                let group_name = '', total_tracker = '', tracker_name = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.sleep(2000);

                await page.scrollPage('//*[@id="account-trackers"]/div[1]');
                await driver.sleep(1000);

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                	let totalGroups = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalGroups = totalGroups.length;

                    if (totalGroups > 3)
                    	totalGroups = 3;

                    for (let i = 1; i <= totalGroups; i++) {
		                let goTo = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr['+i+']/td/div[1]/a');
		                await goTo.click();
		                await driver.sleep(5000);

		                let groupName = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong');
		                groupName = await groupName.getText();
		                group_name += groupName+', ';

                        let showMore = await page.checkElementByCSS('#tracker-group > div > div > div:nth-child(1) > div.key-header__listToggle.keyjs-header__listToggle--showMore');
                        
                        if (showMore) {
                            let showClick = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div.key-header__listToggle.keyjs-header__listToggle--showMore');
                            await showClick.click();
                        }

                        let checkTracker = await page.checkElementByCSS('#tracker-group > div > div > div:nth-child(1) > section > div');

                        if (checkTracker) {
    		                let totalTracker = await page.findElementsByCSS('#tracker-group > div > div > div:nth-child(1) > section > div');
    		                totalTracker = totalTracker.length;
    		                total_tracker += totalTracker+', ';

    		                tracker_name += '[';
    		                for (let j = 1; j <= totalTracker; j++) {
    		                	let trackerName = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div:nth-child('+j+') > div > div.trackerColumn__wrapper > span');
    		                	trackerName = await trackerName.getText();
    		                	tracker_name += trackerName+',';
    		                }
    		                tracker_name += '], ';
                        }
                        else {
                            total_tracker = 'No tracker in this group';
                            tracker_name = 'No tracker in this group';
                        }

		                let dashClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]/div[1]/a');
		                await dashClick.click();
		                await driver.sleep(1000);
		            }

		            addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': group_name.replace(/,\s*$/, ""),
                            'Total tracker': total_tracker.replace(/,\s*$/, ""),
                            'Tracker name': tracker_name.replace(/,\s*$/, "")
                        }
                    });
	            }
	            else {
	            	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
	            }
            });

            it('Go to group dashboard and check PDF, XLS dowload', async () => {
                await page.redirectToAccDashboard();

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                	let goTo = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                	await goTo.click();
                	await driver.sleep(5000);

                	let groupName = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong');
		            groupName = await groupName.getText();

		            let downloadPDF = await page.findById('pdf');
		            await downloadPDF.click();
		            await driver.sleep(1000);

		            let waitPDF = await page.findById('pdf_confirm');
		            await waitPDF.click();
		            await driver.sleep(15000);

		            let finalPDF = await page.findByXPath('//*[@id="pdf_msg"]/a');
		            await finalPDF.click();
		            await driver.sleep(1000);

		            let cancelPDF = await page.findById('pdf_cancel');
		            await cancelPDF.click();
		            await driver.sleep(1000);

		            let downloadXLS = await page.findById('update');
		            await downloadXLS.click();
		            await driver.sleep(5000);

		            addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Group name': groupName,
                            'PDF status': 'Download success',
                            'XLS status': 'Dowload success'
                        }
                    });
	            }
	            else {
	            	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
	            }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();