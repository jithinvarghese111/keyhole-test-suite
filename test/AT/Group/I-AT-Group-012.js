/**
 * ### TEST CASE:
 * I-AT-Group-012
 *
 * ### TEST TITLE:
 * Show and Hide Tracker - group Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to Filter Dates.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Hide and Show Tracker - group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Hide and show Tracker', async () => {
                await page.redirectToAccDashboard();

                let status = '', steps = '';

                let groupClick = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a'), 20000));

                let groupDashboard = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                let GroupName = await groupDashboard.getText();
                await groupDashboard.click();
                steps += 'Clicked on group';
                await driver.sleep(2000);

                let totalTracker = await page.findElementsByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > div');

                for (var i = 1; i <= totalTracker.length; i++) {

                    let clickOptions = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/div/div[2]');
                    let trackerName = await clickOptions.getText();
                    await clickOptions.click();
                    await driver.sleep(2000);

                    let hideTracker = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/ul/li[3]');
                    await hideTracker.click();
                    status = 'Hide Tracker Successfully :'+trackerName;
                    await driver.sleep(2000);

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Group name': GroupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }

                for (var i = 1; i <= totalTracker.length; i++) {

                    let clickOptions = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/div/div[2]');
                    let trackerName = await clickOptions.getText();
                    await clickOptions.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/ul/li[3]'), 20000));

                    let showTracker = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/ul/li[3]');
                    await showTracker.click();
                    status = 'show Tracker Successfully :'+trackerName;
                    await driver.sleep(2000);

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Group name': GroupName,
                            'Test steps': steps,
                            'Test status': status
                        }
                    });
                }
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();