/**
 * ### TEST CASE:
 * I-AT-Group-013
 *
 * ### TEST TITLE:
 * Downloading XLS - group Dashboard
 *
 * ### TEST SUMMARY:
 * ser should be able to download an excel file of group information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const fs = require('fs');
util = require('util');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading XLS - group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Downloading XLS', async () => {
                await page.redirectToAccDashboard();

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {

                    let test_result = '';

                    const days = 309;
                    const toDate = new Date();
                    const fromDate = new Date();
                    const toDateStr= (toDate.getMonth() + 1) + '-' + toDate.getDate() + '-' +  toDate.getFullYear();
                    fromDate.setDate(toDate.getDate() - days);
                    fromDate.setMonth(toDate.getMonth() - 10);
                    const fromDateStr= (fromDate.getMonth() + 1) + '-' + fromDate.getDate() + '-' +  toDate.getFullYear();
                    
                    let firstGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                    let groupName = await firstGroup.getText();
                    await firstGroup.click();
                    await driver.wait(until.elementLocated(By.id('update'), 200000));

                    let xlsLink = await page.findByCSS('#update');
                    await xlsLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));

                    const downloadPath = config.downloadPath+'\/'+groupName+'_'+fromDateStr+'_'+toDateStr+'.xlsx';
                    const filename = `${groupName}_${new Date().getTime()}.xlsx`;
                    const renamePath = config.downloadPath+'\/'+filename;
                    
                    if (fs.existsSync(downloadPath)) {
                        fs.renameSync(downloadPath, renamePath)
                        
                        if (fs.existsSync(renamePath)) {
                            const stats = fs.statSync(renamePath)

                            if (parseFloat(stats.size) > 64) {
                                test_result = "Downloaded file";
                                passed = true
                            } else {
                                test_result = "Downloaded empty file... but that's what happens on env other than prod";
                                passed = true
                            }
                        }
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': groupName,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': test_result
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
                }
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();