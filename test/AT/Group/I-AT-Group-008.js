/**
 * ### TEST CASE:
 * I-AT-Group-008
 *
 * ### TEST TITLE:
 * Posts Timeline Checking - Group Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to interact with the Posts Timeline Checking from Group Dashboard.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Posts Timeline Checking - Group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Posts Timeline Checking - Group Dashboard', async () => {
                await page.redirectToAccDashboard();

                let status = '', steps = '', postcheck ='';

                let firstGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a[1]  ');
                let firstGroupName = await firstGroup.getText();
                await firstGroup.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                await page.scrollPage('//*[@id="tracker-group"]/div/div/div[1]/div/section[2]');
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group-timeline"]/div[1]/h3'), 3000));
                    
                let checkTracker = await page.findElementsByCSS('#tracker-group-timeline > div.key-chartContainer > div.key-chartsLegend > div > div.key-chartsLegend__item--before');
                
                for (var i = 1; i <= checkTracker.length; i++) {
                    let checkbox = await page.findByXPath('//*[@id="tracker-group-timeline"]/div[2]/div[1]/div['+i+']/div[1]');
                    await checkbox.click();                               
                }

                let checkele = await page.checkElementByCSS('#chart__tracker-group-timeline--account > div.chart--noData > div');
                if (checkele) {
                    postcheck = 'Tracker Unchecked Successfully';
                }
                else{
                    postcheck = 'test failed.';
                }
                addContext(this.ctx, {
                    title: 'Other Context',
                    value: {
                        'Test steps': steps,
                        'Test status': postcheck
                    }
                });

                for (var i = 1; i <= checkTracker.length; i++) {
                    let checkbox = await page.findByXPath('//*[@id="tracker-group-timeline"]/div[2]/div[1]/div['+i+']/div[1]');
                    await checkbox.click();                               
                }

                for (var i = 1; i <= 3; i++) {

                    let dropdown = await page.findByXPath('//*[@id="tracker-group-timeline"]/div[1]/div/div/div/div/span');
                    await dropdown.click();
                    await driver.sleep(1000);
                    let options = await page.findByXPath('//*[@id="tracker-group-timeline"]/div[1]/div/div/div/ul/li['+i+']');
                    let optionText = await options.getText();
                    await options.click();
                    await driver.sleep(1000);
                    status = 'Clicked on '+optionText;

                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Test status': status
                            
                        }
                    });
                }
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();