/**
 * ### TEST CASE:
 * I-AT-Group-015
 *
 * ### TEST TITLE:
 * Checking Tracker Status from Group details
 *
 * ### TEST SUMMARY:
 * User is trying to check tracker status, go to the tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Tracker Status from Group details', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });
                await page.gotoLogin();
                await driver.manage().setTimeouts({implicit:50000});
            });

            it('Checking Tracker Status', async () => {
                await page.redirectToAccDashboard();

                let clickGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1)'), 4000));

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                    
                    let firstGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                    let groupNames = await firstGroup.getText();
                    await firstGroup.click();
                    await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let totalTracker = await page.findElementsByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > div > div.trackerColumn__wrapper > span');

                    for (var i = 1; i <= totalTracker.length; i++) {
                        let status = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/div/div[3]');
                        let statusText = await status.getText();

                        let trackerClick = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/div/div[2]/span');
                        let trackername = await trackerClick.getText();
                        await trackerClick.click();
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/ul/li[4]'), 20000));

                        let gotoTracker = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div['+i+']/ul/li[4]');
                        await gotoTracker.click();            
                        await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                        await driver.navigate().back();
                        await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 20000));

                        addContext(this.ctx, {
                            title: 'Other context',
                            value: {
                                'Group names': groupNames,
                                'Tracker name' : trackername,
                                'Tracker status': statusText
                            }
                        });

                    }

                }
                else {
                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();