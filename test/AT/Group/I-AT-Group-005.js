/**
 * ### TEST CASE:
 * I-AT-Group-005
 *
 * ### TEST TITLE:
 * Add Tracker to group
 *
 * ### TEST SUMMARY:
 * User is trying to Add Tracker to group.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Add Tracker to group', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Add Tracker to group', async () => {
               await page.redirectToAccDashboard();

                let group_name = '', status = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[1]');
                await clickGroup.click();
                await driver.sleep(3000);

                let number = await randomNumberFromTo(1, 3);

                let addGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[2]/ul/li['+number+']');
                await addGroup.click();
                await driver.sleep(2000);

                let checkEle = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > a');
                if (checkEle) {
                    status+= 'Added active tracker to the group'
                }
                else{
                    status += 'No groups available'
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Test status': status
                    }
                });
               
            });

            it('Checking Add Tracker to group from Paused Tracker', async () => {
                await page.redirectToAccDashboard();

                let status = ''

                let pauseTracker = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[2]');
                await pauseTracker.click();
                await driver.sleep(3000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name.name--paused');
                if (!checkTracker) {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No Tracker Found'
                        }
                    });
                }
                else{
                    let addGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[1]');
                    await addGroup.click();
                    await driver.sleep(2000);

                    let number = await randomNumberFromTo(1, 3);

                    let groupList = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[2]/ul/li['+number+']');
                    await groupList.click();
                    await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > a')));
                
                    let checkEle = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > a');
                    if (checkEle) {
                        status+= 'Added Paused tracker to the group'
                    }
                    else{
                        status += 'No groups available'
                    }
                    
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': status
                        }
                    });
                }
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();