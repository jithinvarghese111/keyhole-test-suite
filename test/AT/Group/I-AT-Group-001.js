/**
 * ### TEST CASE:
 * I-AT-Group-001
 *
 * ### TEST TITLE:
 * Checking Account Tracker Groups
 *
 * ### TEST SUMMARY:
 * User is trying to manage, search groups
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Account Tracker Groups', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking all groups', async () => {
                await page.redirectToAccDashboard();

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(1000);

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr/td/div[1]/a'), 4000));

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                    let groupNames = '', totalGroups = 0;

                    totalGroups = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalGroups = totalGroups.length;

                    for (var i = 1; i <= totalGroups; i++) {
                        let groupName = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+i+') > td > div.name > a');
                        groupName = await groupName.getText();
                        groupNames += groupName+', ';
                    }

                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Total groups': totalGroups,
                            'Group names': groupNames.replace(/,\s*$/, "")
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
                }
            });

            it('Manage any group', async () => {
                await page.redirectToAccDashboard();

                let steps = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[3]');
                await driver.sleep(1000);

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr/td/div[1]/a'), 3000));

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                    let manageClick = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.created-date.created-date--groups > button.edit.js-manageGroup');
                    await manageClick.click();
                    await driver.wait(until.elementLocated(By.css('#tracker-groups__modal-step1 > div > div > span > span'), 6000));

                    let groupName = await page.findByCSS('#tracker-groups__modal-step1 > div > div > span > span');
                    groupName = await groupName.getText();

                    let totalTracker = await page.findByCSS('#tracker-groups__modal-step1 > div > div > div.footer > p > span.tracker-groups__trackerCount');
                    totalTracker = await totalTracker.getText();

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Selected group': groupName,
                            'Total tracker already selected': totalTracker
                        }
                    });

                    let selectAll = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[2]/div/a[1]');
                    await selectAll.click();
                    steps += 'Select all tracker button clicked, ';
                    await driver.sleep(2000);

                    let selectNone = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[2]/div/a[2]');
                    await selectNone.click();
                    steps += 'Select none button clicked, ';
                    await driver.sleep(2000);

                    let selectFirst =await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[2]/table/tbody/tr[1]/td[1]/input');
                    await selectFirst.click();
                    steps += 'Select first tracker checkbox clicked, ';
                    await driver.sleep(2000);

                    let updateClick = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[3]/div/a[2]');
                    await updateClick.click();
                    steps += 'Update group button clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr/td/div[1]/a'), 3000));

                    addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps.replace(/,\s*$/, "")
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();