/**
 * ### TEST CASE:
 * I-AT-Group-016
 *
 * ### TEST TITLE:
 * Comparison of groups
 *
 * ### TEST SUMMARY:
 * User should able to navigate into group, checking comparison of groups.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Comparison of groups', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User should able to navigate into group, checking comparison of groups.', async () => {
                await page.redirectToAccDashboard();

                let randomGroupName = randomName(), steps = '';

                let clickGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-createGroup'), 10000));

                let totalGroup = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                totalGroup = totalGroup.length;
                addContext(this.ctx, 'Total Groups: '+totalGroup+'');

                if (totalGroup > 2) {
                    let compareGroup = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup');
                    await compareGroup.click();
                    await driver.sleep(1000);

                    for (var i = 1; i <= 3; i++) {
                        let select = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li:nth-child('+i+')');
                        let groups = await select.getText();
                        await select.click();
                        addContext(this.ctx, 'Group for comparison: '+groups);
                        await driver.sleep(1000);
                    }

                    let goTo = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li.dropdown__strong.js-compareGroup.group__dashboardLink');
                    await goTo.click();
                    await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 20000));
                    addContext(this.ctx, 'Compared Groups successfully.');

                } else assert.fail('No enough group available for comparison.')
                
            });
            
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();