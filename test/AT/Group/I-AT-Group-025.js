/**
 * ### TEST CASE:
 * I-AT-Group-025
 *
 * ### TEST TITLE:
 * Hide and Show Group from condensed group dashboard for AT
 *
 * ### TEST SUMMARY:
 * User should able to Hide and Show Group from condensed Group Dashboard.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on( 'unhandledRejection', () => { });

(async function example() {
    try {
        describe('Hide and Show Group from condensed group dashboard for account-trackers', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Hide and Show Group from condensed group dashboard', async () => {
                await page.redirectToAccDashboard();

                let groupClick = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-createGroup'), 30000));

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td > div.name > a');
                if (checkGroup) {

                let firstGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                addContext(this.ctx, 'Clicked on group '+groupName+'');
                await driver.wait(until.elementLocated(By.id('pdf'), 3000));

                let gotoCondensed = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.key-trackerSubHeader > div.key-trackerSubHeader__filters > a:nth-child(3)');
                await gotoCondensed.click();
                addContext(this.ctx, 'Clicked on Condensed group dashboard');
                await driver.sleep(5000);

                let firstClick = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div:nth-child(1) > div > div.trackerColumn__wrapper');
                await firstClick.click();
                await driver.sleep(1000);

                let hideGroup = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > ul > li.hide-tracker');
                await hideGroup.click();
                await driver.sleep(3000);

                let groupText = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > div > div.key-chartsLegend__item--before.keyjs-chartsLegend__item--before.show-tracker');
                groupText = await groupText.getAttribute("data-tip");

                if (groupText.indexOf('Show')) addContext(this.ctx, 'Condensed Group Hidden'); else addContext(this.ctx, 'Failed');

                let secondClick = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > div > div.trackerColumn__wrapper');
                await secondClick.click();
                await driver.sleep(1000);

                let showGroup = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > ul > li.show-tracker');
                await showGroup.click();
                await driver.sleep(2000);

                let grouptext = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div > div > div.key-chartsLegend__item--before.keyjs-chartsLegend__item--before.hide-tracker');
                grouptext = await grouptext.getAttribute("data-tip");

                if (groupText.indexOf('Hide')) addContext(this.ctx, 'Show condensed hidden Group.'); else addContext(this.ctx, 'Failed to show hidden group');

                } else assert.fail('No groups available');    
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();