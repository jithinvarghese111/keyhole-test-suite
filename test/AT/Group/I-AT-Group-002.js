/**
 * ### TEST CASE:
 * I-AT-Group-002
 *
 * ### TEST TITLE:
 * Creating New Account Tracker Group
 *
 * ### TEST SUMMARY:
 * User is trying to add new group
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Creating New Account Tracker Group', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Creating group with name', async () => {
                await page.redirectToAccDashboard();

                let randomGroupName = randomName(), steps = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[2]/div[2]/button'), 10000));

                let createButton = await page.findByXPath('//*[@id="account-trackers"]/div[2]/div[2]/button');
                await createButton.click();
                steps += 'Create a new group button clicked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-groups__modal-step1"]/div/div/div[2]/table/tbody/tr[1]/td[1]/input'), 10000));

                var editText = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span');
                await editText.click();
                await driver.sleep(2000);

                var inputGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/input');
                await page.write(inputGroup, randomGroupName);
                steps += 'Group name entered, ';

                var saveName = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/i');
                await saveName.click();
                steps += 'Group name saved, ';

                var saveGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[3]/div/a[2]');
                await saveGroup.click();
                steps += 'Group saved, ';
                await driver.sleep(1000);

                var checkGroupName = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                checkGroupName = await checkGroupName.getText();

                let msg = '';
                assert.equal(randomGroupName, checkGroupName);
                msg = 'Group creation success';
                if (randomGroupName != checkGroupName)
                    msg = 'Group creation failed';
                    

                addContext(this.ctx, {
                    title: 'Test Steps',
                    value: {
                        'Steps': steps.replace(/,\s*$/, "")
                    }
                });

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': randomGroupName,
                        'Status': msg
                    }
                });
            });

            it('Creating group with name, color', async () => {
                await page.redirectToAccDashboard();

                let randomGroupName = randomName(), steps = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[2]/div[2]/button'), 10000));

                let createButton = await page.findByXPath('//*[@id="account-trackers"]/div[2]/div[2]/button');
                await createButton.click();
                steps += 'Create a new group button clicked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-groups__modal-step1"]/div/div/div[2]/table/tbody/tr[1]/td[1]/input'), 10000));

                var editText = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span');
                await editText.click();
                await driver.sleep(2000);

                var inputGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/input');
                await page.write(inputGroup, randomGroupName);
                steps += 'Group name entered, ';

                var saveName = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/i');
                await saveName.click();
                steps += 'Group name saved, ';

                var colorDrop = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]');
                await colorDrop.click();
                await driver.sleep(1000); 

                let randomNum = randomNumberFromTo(1, 9);

                var selectColor = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/ul/li['+randomNum+']');
                await selectColor.click();
                steps += 'Color picked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/div'), 8000));

                let colorSelected = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/ul/li['+randomNum+']');
                let colorName = await colorSelected.getAttribute("data-name");
                let colorCode = await colorSelected.getAttribute("data-color");

                var saveGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[3]/div/a[2]');
                await saveGroup.click();
                steps += 'Group saved, ';
                await driver.sleep(1000);

                var checkGroupName = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                checkGroupName = await checkGroupName.getText();

                let msg = '';
                assert.equal(randomGroupName, checkGroupName);
                msg = 'Group creation success';
                if (randomGroupName != checkGroupName)
                    msg = 'Group creation failed';

                addContext(this.ctx, {
                    title: 'Test Steps',
                    value: {
                        'Steps': steps.replace(/,\s*$/, "")
                    }
                });

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': randomGroupName,
                        'Color selected': colorName+' ('+colorCode+')',
                        'Status': msg
                    }
                });
            });

            it('Creating group with name, trackers', async () => {
                await page.redirectToAccDashboard();

                let randomGroupName = randomName(), steps = '', trackerMSG = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[2]/div[2]/button'), 10000));

                let createButton = await page.findByXPath('//*[@id="account-trackers"]/div[2]/div[2]/button');
                await createButton.click();
                steps += 'Create a new group button clicked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-groups__modal-step1"]/div/div/div[2]/table/tbody/tr[1]/td[1]/input'), 10000));

                var editText = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span');
                await editText.click();
                await driver.sleep(2000);

                var inputGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/input');
                await page.write(inputGroup, randomGroupName);
                steps += 'Group name entered, ';

                var saveName = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/i');
                await saveName.click();
                steps += 'Group name saved, ';

                let checkTracker = await page.checkElementByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr:nth-child(1) > td.inGroup > input[type=checkbox]');

                if (checkTracker) {
                    let totalTracker = await page.findElementsByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr');
                    totalTracker = totalTracker.length;

                    if (totalTracker > 5)
                        totalTracker = 5;

                    for (let i = 1; i <= totalTracker; i++) {
                        let checkTracker = await page.findByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr:nth-child('+i+') > td.inGroup > input[type=checkbox]');
                        await checkTracker.click();
                    }

                    trackerMSG = totalTracker+' tracker added';
                }
                else
                    trackerMSG = 'No tracker found. Tracker no added';

                var saveGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[3]/div/a[2]');
                await saveGroup.click();
                steps += 'Group saved, ';
                await driver.sleep(1000);

                var checkGroupName = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                checkGroupName = await checkGroupName.getText();

                let msg = '';
                assert.equal(randomGroupName, checkGroupName);
                msg = 'Group creation success';
                if (randomGroupName != checkGroupName)
                    msg = 'Group creation failed';
                    
                addContext(this.ctx, {
                    title: 'Test Steps',
                    value: {
                        'Steps': steps.replace(/,\s*$/, "")
                    }
                });

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': randomGroupName,
                        'Tracker select': trackerMSG,
                        'Status': msg
                    }
                });
            });

            it('Creating group with name, color and trackers', async () => {
                await page.redirectToAccDashboard();

                let randomGroupName = randomName(), steps = '', trackerMSG = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[2]/div[2]/button'), 10000));

                let createButton = await page.findByXPath('//*[@id="account-trackers"]/div[2]/div[2]/button');
                await createButton.click();
                steps += 'Create a new group button clicked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-groups__modal-step1"]/div/div/div[2]/table/tbody/tr[1]/td[1]/input'), 10000));

                var editText = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span');
                await editText.click();
                await driver.sleep(1000);

                var inputGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/input');
                await page.write(inputGroup, randomGroupName);
                steps += 'Group name entered, ';
                await driver.sleep(1000);

                var saveName = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/i');
                await saveName.click();
                steps += 'Group name saved, ';
                await driver.sleep(1000);

                var colorDrop = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]');
                await colorDrop.click();
                await driver.sleep(1000);

                let randomNum = randomNumberFromTo(1, 9);

                var selectColor = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/ul/li['+randomNum+']');
                await selectColor.click();
                steps += 'Color picked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/div'), 8000));

                let colorSelected = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/ul/li['+randomNum+']');
                let colorName = await colorSelected.getAttribute("data-name");
                let colorCode = await colorSelected.getAttribute("data-color");

                let checkTracker = await page.checkElementByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr:nth-child(1) > td.inGroup > input[type=checkbox]');

                if (checkTracker) {
                    let totalTracker = await page.findElementsByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr');
                    totalTracker = totalTracker.length;

                    if (totalTracker > 5)
                        totalTracker = 5;

                    for (let i = 1; i <= totalTracker; i++) {
                        let checkTracker = await page.findByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr:nth-child('+i+') > td.inGroup > input[type=checkbox]');
                        await checkTracker.click();
                        // await driver.sleep(1000);
                    }

                    trackerMSG = totalTracker+' tracker added';
                }
                else
                    trackerMSG = 'No tracker found. Tracker no added';

                var saveGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[3]/div/a[2]');
                await saveGroup.click();
                steps += 'Group saved, ';
                await driver.sleep(1000);

                var checkGroupName = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                checkGroupName = await checkGroupName.getText();

                let msg = '';
                assert.equal(randomGroupName, checkGroupName);
                msg = 'Group creation success';
                if (randomGroupName != checkGroupName)
                    msg = 'Group creation failed';
                    
                addContext(this.ctx, {
                    title: 'Test Steps',
                    value: {
                        'Steps': steps.replace(/,\s*$/, "")
                    }
                });

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': randomGroupName,
                        'Color selected': colorName+' ('+colorCode+')',
                        'Tracker select': trackerMSG,
                        'Status': msg
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();