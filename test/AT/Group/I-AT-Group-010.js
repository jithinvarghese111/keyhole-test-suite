/**
 * ### TEST CASE:
 * I-AT-Group-010
 *
 * ### TEST TITLE:
 * Interact with Top posts - Group Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to Intract with Top posts from Group Dashboard.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Top posts - Group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with Top posts', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let firstGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a[1]');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));
                
                await page.scrollPixel(1200);
                await driver.sleep(2000);
                                
                let trackerPost = await page.findByXPath('//*[@id="posts-table"]/div[2]/div/div/table/tbody/tr[1]/td[2]/div[1]');
                let postText = await trackerPost.getText();
                await trackerPost.click();
                status = 'Redirected to Tracker Post '+postText;
                await driver.sleep(1000);

                let tab1, tab2;

                await driver.getAllWindowHandles().then(function(windowHandles) {
                    tab1 = windowHandles[0];
                    tab2 = windowHandles[1];                
                });

                await driver.switchTo().window(tab1);  
                await driver.wait(until.elementLocated(By.xpath('//*[@id="posts-table"]/div[1]/h3'), 20000));

                addContext(this.ctx, {
                    title: 'Other Context',
                    value: {
                        'Group Name': groupName,
                        'Test status': status
                    }
                });
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();