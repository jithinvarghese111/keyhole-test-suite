/**
 * ### TEST CASE:
 * I-AT-Group-003
 *
 * ### TEST TITLE:
 * Deleting Account Tracker Groups
 *
 * ### TEST SUMMARY:
 * User is trying to delete group
 *
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Deleting Account Tracker Groups', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Deleting multiple groups', async () => {
                await page.redirectToAccDashboard();

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.sleep(2000);

                await page.scrollPage('//*[@id="account-trackers"]/div[1]');
                await driver.sleep(1000);

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {
                    let groupNames = '', totalGroups = 0;

                    totalGroups = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalGroups = totalGroups.length;

                    if (totalGroups < 3)
                        totalGroups = 1;
                    else
                        totalGroups = 3;

                    for (let i = 1; i <= totalGroups; i++) {
                        let groupName = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                        groupName = await groupName.getText();
                        groupNames += groupName+', ';

                        let deleteGroup = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td/div[3]/button[2]');
                        await deleteGroup.click();
                        await driver.sleep(1000);
                    }

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Total groups deleted': totalGroups,
                            'Deleted group names': groupNames.replace(/,\s*$/, "")
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Result': 'No groups available'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();