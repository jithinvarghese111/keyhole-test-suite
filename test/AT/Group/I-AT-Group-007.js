/**
 * ### TEST CASE:
 * I-AT-Group-010
 *
 * ### TEST TITLE:
 * Interact with Followers growth - Group Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to Intract with Followers growth from Group Dashboard.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Intract with Followers growth - Group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Intract with Followers growth', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let firstGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a[1]');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));

                await page.scrollPixel(1500);
                await driver.sleep(2000);
                                
                for (var i = 1; i <= 2; i++) {

                    let dropdown = await page.findByXPath('//*[@id="tracker-group-followers"]/div[1]/div/div/div/div/span');
                    await dropdown.click();
                    await driver.wait(until.elementLocated(By.css('#tracker-group-followers > div.key-chartHeading > div > div > div > ul > li:nth-child(1)'), 20000));

                    let followers = await page.findByXPath('//*[@id="tracker-group-followers"]/div[1]/div/div/div/ul/li['+i+']');
                    let followertype = await followers.getText();
                    await followers.click();
                    await driver.sleep(1000);                        

                     addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                        
                            'Test status': 'Clicked on'+ followertype
                        }
                    });

                }

                let totalcheckbox = await page.findElementsByCSS('#tracker-group-followers > div.key-chartContainer > div.key-chartsLegend > div > div.key-chartsLegend__keyword.key-chartsLegend__keyword--account > span.chartsLegend__label');
                for (var i = 1; i <= totalcheckbox.length; i++) {
                    let checktracker = await page.findByXPath('//*[@id="tracker-group-followers"]/div[2]/div[1]/div['+i+']/div[2]/span[1]');
                    let hidetrackername = await checktracker.getText();
                    await checktracker.click();                    
                    status = 'Hide tracker: '+hidetrackername;
                    await driver.sleep(1000);
        
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group Name': groupName,
                            'Test status': status
                        }
                    });
                }

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();