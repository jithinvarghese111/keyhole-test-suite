/**
 * ### TEST CASE:
 * I-AT-Group-009
 *
 * ### TEST TITLE:
 * Interact with Post Type - Group Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to Intract with Post Type from Group Dashboard.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Intract with Post Type - Group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Intract with Post Type', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let firstGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a[1]');
                let groupName = await firstGroup.getText();
                await firstGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-group"]/div/div/div[1]/div/section[1]/h1/strong'), 3000));
                
                await page.scrollPixel(1800);
                await driver.sleep(2000);
                                
                let totalcheckbox = await page.findElementsByCSS('#post-type > div.key-chartContainer > div > div.key-chartsLegend.key-chartsLegend--postType > div > div.key-chartsLegend__item--before');
                for (var i = 1; i <= totalcheckbox.length; i++) {
                    let checkPost = await page.findByXPath('//*[@id="post-type"]/div[2]/div/div[2]/div['+i+']/div[2]');
                    let postType = await checkPost.getText();
                    await checkPost.click();                    
                    status = 'Hide Post type: '+postType;
                    await driver.sleep(1000);

                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Test status': status
                        }
                    });

                }

                let total = await page.findByXPath('//*[@id="post-type"]/div[1]/div/ul/li[2]');
                await total.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="post-type"]/div[2]/div/div[2]/div[2]/div[2]'), 3000));

                let totalcheckboxes = await page.findElementsByCSS('#post-type > div.key-chartContainer > div > div.key-chartsLegend.key-chartsLegend--postType > div:nth-child(1) > div.key-chartsLegend__keyword.key-chartsLegend__keyword--postType');
                for (var i = 1; i <= totalcheckboxes.length; i++) {
                    let checkPosttype = await page.findByXPath('//*[@id="post-type"]/div[2]/div/div[2]/div['+i+']/div[2]');
                    let contents = await checkPosttype.getText();
                    await checkPosttype.click();
                    status = 'Show Post type: '+contents;
                    await driver.sleep(1000);
                                        
                    addContext(this.ctx, {
                        title: 'Other Context',
                        value: {
                            'Group Name': groupName,
                            'Test status': status
                        }
                    });
                }

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();