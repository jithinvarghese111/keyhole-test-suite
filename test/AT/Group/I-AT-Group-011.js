/**
 * ### TEST CASE:
 * I-AT-Group-011
 *
 * ### TEST TITLE:
 * Filter Dates - group Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to Filter Dates.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Filter Dates - group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Filter Dates', async () => {
                await page.redirectToAccDashboard();

                let status = '', steps = '';

                let firstTrack = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                let trackerName = await firstTrack.getText();

                let groupDashboard = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[2]/div[3]/a[1]');
                let GroupName = await groupDashboard.getText();
                await groupDashboard.click();
                steps += 'Clicked on group';
                await driver.sleep(2000);

                let clickOptions = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div[2]/div/div[2]');
                await clickOptions.click();
                await driver.sleep(3000);

                let dateFilter = await page.findByXPath('//*[@id="tracker-group"]/div/div/div[1]/section/div[2]/ul/li[1]');
                await dateFilter.click();
                await driver.sleep(2000);

                let dateClick = await page.findByCSS('#tracker-group > div > div > section.key-modalContainer.key-modalContainer--show > div > div.key-modal__wrapper > div.key-modal__body > span > div > input');
                await dateClick.click();
                await driver.sleep(5000);
                
                let startdate = await page.findByXPath('/html/body/div[15]/div/div/div[2]/div[1]/table/tbody/tr[3]/td[1]');
                await startdate.click();
                await driver.sleep(2000);
                
                let enddate = await page.findByXPath('/html/body/div[15]/div/div/div[2]/div[1]/table/tbody/tr[6]/td[7]');
                await enddate.click();
                await driver.sleep(2000);   

                let setFilter = await page.findByXPath('//*[@id="tracker-group"]/div/div/section[2]/div/div[2]/div[3]/button[2]');
                await setFilter.click();
                await driver.wait(until.elementLocated(By.id('pdf'), 20000));

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': trackerName,
                        'Group name': GroupName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();