/**
 * ### TEST CASE:
 * I-AT-Group-014
 *
 * ### TEST TITLE:
 * Edit New Account Tracker Group
 *
 * ### TEST SUMMARY:
 * User is trying to edit new group
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Edit New Account Tracker Group', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Editing Group with Name', async () => {
                await page.redirectToAccDashboard();

                let randomGroupName = randomName(), steps = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 10000));

                let groupName = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                groupName = await groupName.getText();

                let editButton = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[3]/button[1]');
                await editButton.click();
                steps += 'Edit group button clicked, ';
                await driver.sleep(3000);

                let editText = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span');
                await editText.click();
                await driver.sleep(2000);

                let inputGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/input');
                await inputGroup.clear();
                await page.write(inputGroup, randomGroupName);
                steps += 'Group name entered, ';

                let saveName = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/i');
                await saveName.click();
                steps += 'Group name saved, ';

                let saveGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[3]/div/a[2]');
                await saveGroup.click();
                steps += 'Group saved, ';
                await driver.sleep(2000);

                let checkGroupName = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                checkGroupName = await checkGroupName.getText();
        
                let msg = '';
                assert.notEqual(checkGroupName, groupName);
                msg = 'Group Name edited successfully';
                if (checkGroupName == groupName)
                    msg = 'Group Name editing failed';
                    
                addContext(this.ctx, {
                    title: 'Test Steps',
                    value: {
                        'Steps': steps.replace(/,\s*$/, "")
                    }
                });

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': randomGroupName,
                        'Status': msg
                    }
                });
            });

            it('Editing group with color', async () => {
                await page.redirectToAccDashboard();

                let randomGroupName = randomName(), steps = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 10000));

                let groupName = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                groupName = await groupName.getText();

                let editButton = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[3]/button[1]');
                await editButton.click();
                steps += 'Edit group button clicked, ';
                await driver.sleep(3000);

                let colorDrop = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/div');
                let firstcolor = await colorDrop.getAttribute("data-color");
                await colorDrop.click();
                await driver.sleep(1000); 

                let randomNum = randomNumberFromTo(1, 9);

                let selectColor = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/ul/li['+randomNum+']');
                await selectColor.click();
                steps += 'Color picked, ';
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/div'), 8000));
                
                let colorSelected = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[1]/ul/li['+randomNum+']');
                let colorName = await colorSelected.getAttribute("data-name");
                let colorCode = await colorSelected.getAttribute("data-color");

                let saveGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[3]/div/a[2]');
                await saveGroup.click();
                steps += 'Group saved, ';
                await driver.sleep(1000);

                let checkGroupName = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                let secondcolor = await checkGroupName.getAttribute("data-color");

                let msg = '';
                assert.notEqual(secondcolor, firstcolor);
                msg = 'Edited Group with color';
                if (secondcolor == firstcolor)
                    msg = 'Group Editing failed';

                addContext(this.ctx, {
                    title: 'Test Steps',
                    value: {
                        'Steps': steps.replace(/,\s*$/, "")
                    }
                });

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': randomGroupName,
                        'Color selected': colorName+' ('+colorCode+')',
                        'Status': msg
                    }
                });
            });

            it('Creating group with name, trackers', async () => {
                await page.redirectToAccDashboard();

                let randomGroupName = randomName(), steps = '', trackerMSG = '';

                let clickGroup = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[3]');
                await clickGroup.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a'), 10000));

                let groupName = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                groupName = await groupName.getText();

                let trackerCount = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[2]/p/strong');
                trackerCount = await trackerCount.getText();

                let editButton = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[3]/button[1]');
                await editButton.click();
                steps += 'Edit group button clicked, ';
                await driver.sleep(3000);

                let editText = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span');
                await editText.click();
                await driver.sleep(2000);

                let inputGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/input');
                await inputGroup.clear();
                await page.write(inputGroup, randomGroupName);
                steps += 'Group name entered, ';

                let saveName = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/span/span/i');
                await saveName.click();
                steps += 'Group name saved, ';

                let checkTracker = await page.checkElementByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr:nth-child(1) > td.inGroup > input[type=checkbox]');

                if (checkTracker) {
                    let totalTracker = await page.findElementsByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr');
                    totalTracker = totalTracker.length;

                    if (totalTracker > 5)
                        totalTracker = 5;

                    for (let i = 1; i <= totalTracker; i++) {
                        let checkTracker = await page.findByCSS('#tracker-groups__modal-step1 > div > div > div.table-container > table > tbody > tr:nth-child('+i+') > td.inGroup > input[type=checkbox]');
                        await checkTracker.click();
                    }

                    trackerMSG = totalTracker+' tracker added';
                }
                else
                    trackerMSG = 'No tracker found. Tracker no added';

                let saveGroup = await page.findByXPath('//*[@id="tracker-groups__modal-step1"]/div/div/div[3]/div/a[2]');
                await saveGroup.click();
                steps += 'Group saved, ';
                await driver.sleep(1000);

                let secondtrackerCount = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[2]/p/strong');
                secondtrackerCount = await secondtrackerCount.getText();

                let checkGroupName = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td/div[1]/a');
                checkGroupName = await checkGroupName.getText();

                let msg = '';
                assert.notEqual(secondtrackerCount, trackerCount);
                assert.notEqual(checkGroupName, groupName);
                msg = 'Edited Group with name, trackers';
                if (checkGroupName == groupName)
                    msg = 'Group editing failed';
                    
                addContext(this.ctx, {
                    title: 'Test Steps',
                    value: {
                        'Steps': steps.replace(/,\s*$/, "")
                    }
                });

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': randomGroupName,
                        'Tracker select': trackerMSG,
                        'Status': msg
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();