/**
 * ### TEST CASE:
 * I-AT-Group-021
 *
 * ### TEST TITLE:
 * Go to Condensed Dashboard from group Dashboard for AT
 *
 * ### TEST SUMMARY:
 * User should allow to Condensed Dashboard from group Dashboard.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Go to Condensed Dashboard from group Dashboard for AT', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Navigate to Condensed Dashboard from group Dashboard ', async () => {
                await page.redirectToAccDashboard();

                let steps = '', status = '';

               let clickGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#hashtag-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                if (checkGroup) {

                let groupname = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                groupName = await groupname.getText();
                await groupname.click();
                steps += 'Clicked on group';
                await driver.wait(until.elementLocated(By.id('pdf'), 30000));

                let gotoCondensed = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div.key-trackerHeader.key-trackerHeader--noSidebarRestructured > section.key-trackerSubHeader > div.key-trackerSubHeader__filters > a:nth-child(3)');
                await gotoCondensed.click();
                await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 30000));

                let title = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong');
                title = await title.getText();

                if (title.indexOf('condensed') > -1) {

                    status += 'Navigated into Condensed Tracker: '+title+'';
                }                   
                else{
                    status += 'page not found.';
                }
                                
                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Group name': groupName,
                        'Test steps': steps,
                        'Test status': status
                    }
                });
            } else assert.fail('No groups available');
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();