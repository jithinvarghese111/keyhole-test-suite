/**
 * ### TEST CASE:
 * I-AT-Group-024
 *
 * ### TEST TITLE:
 * Filter Date of each group after group comparison.
 *
 * ### TEST SUMMARY:
 * User should able to Filter Date of each group after group comparison.
 *(Now it can be test only in qa@wdstech.com )
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Filter Date of each group after group comparison for AT', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Filter Date of each group after group comparison.', async () => {
                await page.redirectToAccDashboard();

                let groupClick = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await groupClick.click();
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-createGroup'), 30000));

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr > td > div.name > a');
                if (checkGroup) {

                    let compareGroup = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup');
                    await compareGroup.click();
                    await driver.sleep(2000);

                    for (var i = 1; i <= 3; i++) {
                        let select = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li:nth-child('+i+')'); 
                        await select.click();
                        await driver.sleep(1000);
                    }

                    let goTo = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li.dropdown__strong.js-compareGroup.group__dashboardLink');
                    await goTo.click();
                    await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let totalGroups = await page.findElementsByCSS('#tracker-group > div > div > div:nth-child(1) > section > div');
                    for (var i = 1; i <= totalGroups.length; i++) {
                        addContext(this.ctx, '--------------------------------------');
                    
                        let clickGroup = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div:nth-child('+i+') > div > div.trackerColumn__wrapper > span');
                        let groupNames = await clickGroup.getText();
                        await clickGroup.click();
                        groupNames += groupNames+', ';
                        await driver.sleep(1000);

                        let filter = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > section > div:nth-child('+i+') > ul > li:nth-child(1)');
                        await filter.click();
                        await driver.sleep(2000);

                        const allowedDays = [1, 7, 30, 90, 365]
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                        let changeDate = await setDateRange(days)

                        if (changeDate) {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Group names for Comparison': groupNames.replace(/,\s*$/, ""),
                                    'Selected date period': days,
                                    'Test status': 'Success'
                                }
                            });
                        } else assert.fail('Date selection failed');

                        let cancel = await page.findByCSS('#tracker-group > div > div > section.key-modalContainer.key-modalContainer--show > div > div.key-modal__wrapper > div.key-modal__heading > button');
                        await cancel.click();
                        await driver.sleep(1000);
                    }
                
                } else assert.fail('No groups available');    
               
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();