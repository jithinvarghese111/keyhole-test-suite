/**
 * ### TEST CASE:
 * I-AT-Group-019
 *
 * ### TEST TITLE:
 * Interact with Summary - Group Dashboard
 *
 * ### TEST SUMMARY:
 * User should be able to Intract with Summary from Group Dashboard.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Interact with Summary - Group Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Interact with Summary', async () => {
                await page.redirectToAccDashboard();

               let clickGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.sleep(5000);

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkGroup) {

                let firstGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');
                let groupName = await firstGroup.getText();
                addContext(this.ctx, 'Group Name: '+groupName); 
                await firstGroup.click();
                await driver.sleep(5000);

                await page.scrollPixel(1000);
                await driver.sleep(2000);
                                
                let totalTracker = await page.findElementsByCSS('#summary-table > div.key-chartContainer.key-chartContainer--table > div > div > div > div.tbody > a > div.key-metric.key-metric__tracker > span');
                totalTracker = totalTracker.length;
                if (totalTracker > 2) {

                    let firstTrackerName = await page.findByCSS('#summary-table > div.key-chartContainer.key-chartContainer--table > div > div > div > div.tbody > a:nth-child(1) > div.key-metric.key-metric__tracker > span:nth-child(1)');
                    firstTrackerName = await firstTrackerName.getText();

                    let filterTracker = await page.findByCSS('#summary-table > div.key-chartContainer.key-chartContainer--table > div > div > div > div.thead > div > div.key-thead.key-metric--head.keyjs-metric__posts.key-metric__posts.key-table__sortToggle.keyjs-table__sortToggle.key-table__column--sortedBy.key-table__column--desc.sorted-by');
                    await filterTracker.click();
                    await driver.sleep(2000);

                    let secondTrackerName = await page.findByCSS('#summary-table > div.key-chartContainer.key-chartContainer--table > div > div > div > div.tbody > a:nth-child(1) > div.key-metric.key-metric__tracker > span:nth-child(1)');
                    secondTrackerName = await secondTrackerName.getText();

                    if (secondTrackerName != firstTrackerName) addContext(this.ctx, 'sorted Group Summary'); else addContext(this.ctx, 'Failed');

                    assert.notEqual(secondTrackerName, firstTrackerName);
                } else assert.fail('Not enough Tracker for sorting Summary.');

            } else addContext(this.ctx, 'No Group available.');

            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();