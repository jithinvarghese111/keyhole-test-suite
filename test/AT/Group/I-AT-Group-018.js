/**
 * ### TEST CASE:
 * I-AT-Group-018
 *
 * ### TEST TITLE:
 * Date persists to URL - Group Comparison
 *
 * ### TEST SUMMARY:
 * When selecting a date in the date dropdown, the date should be persisted in the URL after group comparison.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');   
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Date persists to URL after group comparison.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Date persists to URL after group comparison.', async () => {
                await page.redirectToAccDashboard();

                let clickGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--groups');
                await clickGroup.click();
                await driver.sleep(2000);

                let checkGroup = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td > div.name > a');

                if (checkGroup) {

                    let test_result = '';
                    
                    let compareGroup = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup');
                    await compareGroup.click();
                    await driver.sleep(2000);

                    for (var i = 1; i <= 3; i++) {
                        let select = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li:nth-child('+i+')');
                        let groups = await select.getText();
                        await select.click();
                        addContext(this.ctx, 'Group for comparison: '+groups);
                        await driver.sleep(1000);
                    }

                    let goTo = await page.findByCSS('#account-trackers > div.track-info.track-info--groups.track-info--AT > div.track-info__groupAction > button.key-button.js-toggleDropdown--compareGroup > ul > li.dropdown__strong.js-compareGroup.group__dashboardLink');
                    await goTo.click();
                    await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 20000));


                    const allowedDays = [30, 90, 365]
                    const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                    let changeDate = await setDateRangeAccount(days)

                    if (changeDate) {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Selected date period': days,
                                'Test status': 'Success'
                            }
                        });

                        let currentURL = await driver.getCurrentUrl();
                        let params = currentURL.substring(currentURL.indexOf("?")+1);
                        let firstParam = params.substr(0, params.lastIndexOf('='));
                        
                        if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end') > -1) {
                            let status = '';

                            status += 'Date added to URL., ';

                            await driver.navigate().refresh();
                            await driver.wait(until.elementLocated(By.id('pdf'), 30000));

                            if (firstParam.indexOf('start') > -1 && firstParam.indexOf('end') > -1) {
                                status += 'Date persisted upon reload.';
                            }
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Test status': status
                                }
                            });
                        }
                        else assert.fail('Date not added to URL.');
                    }
                    else assert.fail('Date selection failed');
                                    
                } else assert.fail('No groups available');                
            });
        
            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();