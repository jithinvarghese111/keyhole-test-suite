/**
 * ### TEST CASE:
 * I-AT-Dashboard-001
 *
 * ### TEST TITLE:
 * Checking Trackers in Account Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to go to particular tracker and comes back to home page
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Trackers in Account Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to tracker dashboard and return to home page', async () => {
                await page.redirectToAccDashboard();

                let steps = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(3000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkTracker) {
                	let gotoTrack = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                	await gotoTrack.click();
                	steps += 'First tracker selected and clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 10000));

                    let trackerNameCheck = await page.checkElementByCSS('#AT-Dashboard > div > div > div.info > p > span');

                    if (trackerNameCheck) {
                    	let trackerName = await page.findByXPath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span');
                    	trackerName = await trackerName.getText();

                    	let gotoDash = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[1]/a');
                    	await gotoDash.click();
                    	steps += 'Clicked my tracker link from sidebar menu';
                    	await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a'), 10000));

                    	addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'Test status': 'Passed'
                            }
                        });
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Result': 'No post available. Redirecting to dashboard.'
                            }
                        });
                    }
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();