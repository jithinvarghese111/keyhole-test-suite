/**
 * ### TEST CASE:
 * I-AT-Dashboard-007
 *
 * ### TEST TITLE:
 * Delete Account Tracker from paused Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to delete tracker from paused Tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Delete Account Tracker from paused Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Delete a particular tracker from paused Tracker', async () => {
                await page.redirectToAccDashboard();

                let steps = '', status = '';

                let pauseTracker = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[2]');
                await pauseTracker.click();
                await driver.sleep(3000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                	let getTrackerName = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                    getTrackerName = await getTrackerName.getText();
                    
                    let deleteTracker = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[2]/table/tbody/tr[1]/td[1]/input');
                    await deleteTracker.click();
                    steps += 'Checkbox checked, ';
                    await driver.sleep(2000);

                    let deletePop = await page.findByXPath('//*[@id="bulk-manage__modal"]/button[1]');
                    await deletePop.click();
                    steps += 'Delete button clicked from bottom popup, ';
                    await driver.sleep(2000);

                    let confirmDelete = await page.findByCSS('#deletetrackacc__modal #confirm_delete_btn');
                    await confirmDelete.click();
                    steps += 'Delete button clicked from popup';
                    await driver.sleep(10000);

                    let afterTrackerName = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                    afterTrackerName = await afterTrackerName.getText();

                    if (getTrackerName == afterTrackerName)
                        status = 'Tracker not deleted, tryagain later';
                    else
                        status = 'Tracker delete success';
                       
                    addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps
                        }
                    });

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': getTrackerName,
                            'Test status': status
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();