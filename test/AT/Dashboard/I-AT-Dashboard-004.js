/**
 * ### TEST CASE:
 * I-AT-Dashboard-004
 *
 * ### TEST TITLE:
 * Adding Group to Account Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to add a group to particular tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Adding Group to Account Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Add single group to a particular tracker', async () => {
                await page.redirectToAccDashboard();

                let status = '', groupName = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.heading > h1'), 3000));

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(3000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                	let getTrackerName = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                    getTrackerName = await getTrackerName.getText();

                    let addGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > div.tracker__tag.tracker__tag--addNew.js-tracker__toggleTagDropdown');
                    await addGroup.click();
                    await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > div.tracker__tag--dropdownWrapper > ul > li:nth-child(1)'), 20000));

                    let totalGroups = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > div.tracker__tag--dropdownWrapper > ul > li.js-tracker__addToGroup');
                    totalGroups = totalGroups.length;

                    if (totalGroups > 0) {
                        let addFirst = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[2]/ul/li[1]');
                        groupName = await addFirst.getText();
                        await addFirst.click();
                        await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > a'), 20000));

                        status = 'Passed';
                    }
                    else
                        status = 'No groups found';

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': getTrackerName,
                            'Group name': groupName,
                            'Test status': status
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            it('Add multiple group to a particular tracker', async () => {
                await page.redirectToAccDashboard();

                let status = '', groupName = '', group_name = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.heading > h1'), 4000));

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(5000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                    let getTrackerName = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                    getTrackerName = await getTrackerName.getText();

                    let addGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > div.tracker__tag.tracker__tag--addNew.js-tracker__toggleTagDropdown');
                    await addGroup.click();
                    await driver.sleep(3000);
                    
                    let totalGroups = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > div.tracker__tag--dropdownWrapper > ul > li.js-tracker__addToGroup');
                    totalGroups = totalGroups.length;
                    await addGroup.click();
                    if (totalGroups > 0) {
                        if (totalGroups > 3)
                            totalGroups = 3;

                        for (let i = 1; i <= 3; i++) {
                            let addGroup = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.tracker__tags > div.tracker__tag.tracker__tag--addNew.js-tracker__toggleTagDropdown');
                            await addGroup.click();
                            await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[2]/ul/li['+i+']'), 20000));

                            let addFirst = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr[1]/td[2]/div[3]/div[2]/ul/li['+i+']');
                            groupName = await addFirst.getText();
                            group_name += groupName+', ';
                            await addFirst.click();
                            await driver.sleep(10000);
                        }
                        
                        status = 'Passed';  
                    }
                    else
                        status = 'No groups found';

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': getTrackerName,
                            'Group name': groupName.replace(/,\s*$/, ""),
                            'Test status': status
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();