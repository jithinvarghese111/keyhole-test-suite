/**
 * ### TEST CASE:
 * I-AT-Dashboard-008
 *
 * ### TEST TITLE:
 * Select one or more paused tracker and cancel the selection from the popup
 *
 * ### TEST SUMMARY:
 * User is trying to go to select one or more paused tracker and cancel selection
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Select one or more paused tracker and cancel the selection from the popup', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to paused tracker, select one or more and cancel the selection', async () => {
                await page.redirectToAccDashboard();

                let steps = '', status = '';

                let pauseTracker = await page.findByXPath('//*[@id="account-trackers"]/div[6]/div[1]/div[1]/button[2]');
                await pauseTracker.click();
                await driver.sleep(3000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                	let getTrackerName = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                    getTrackerName = await getTrackerName.getText();
                    
                    let totalTracker = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalTracker = totalTracker.length;

                    if (totalTracker > 2)
                        totalTracker = 2;

                    for (let i = 1; i <= totalTracker; i++) {
                        let clickCheckbox = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+i+') > td.trackerList__manageCheck > input[type=checkbox]');
                        await clickCheckbox.click();
                        await driver.sleep(2000);
                    }

                    let cancelSelect = await page.findByCSS('#bulk-manage__modal > button.kc-button.kc-button--white.js-bulk-manage__toggle.js-bulk-manage__close');
                    await cancelSelect.click();
                    await driver.sleep(1000);

                    let checkCancel = await page.checkElementByCSS('#bulk-manage__modal > button.kc-button.kc-button--white.js-bulk-manage__toggle.js-bulk-manage__close');
                    status = checkCancel ? "Not passed" : "Passed";

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': getTrackerName,
                            'Test status': status
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();