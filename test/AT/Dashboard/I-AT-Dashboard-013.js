/**
 * ### TEST CASE:
 * I-AT-Dashboard-013
 *
 * ### TEST TITLE:
 * Create Account Trackers From the tracker Post
 *
 * ### TEST SUMMARY:
 * User is trying to Create Account Trackers From Tracker post modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Trackers in Account Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('User is trying to Create Account Trackers From Tracker post modal', async () => {
                await page.redirectToAccDashboard();

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkTracker) {
                	let gotoTrack = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                	await gotoTrack.click();
                    addContext(this.ctx, 'First tracker selected and clicked,');
                    await driver.sleep(10000);                   

                    let clickPost = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr:nth-child(1)');
                    await clickPost.click();
                    await driver.sleep(5000);

                    let checkAT = await page.checkElementByCSS('#post-view > div.content > div > div > div.post-caption > a.search-account');
                    if (checkAT) {
                      let at = await page.findByCSS('#post-view > div.content > div > div > div.post-caption > a.search-account');
                      let firstAT = await at.getText();
                      await at.click();
                      await driver.sleep(8000);      

                      addContext(this.ctx, 'New Account Tracker Created.');

                    }else assert.fail('No Account Tracker link found.');
                      
                }
                else assert.fail('No trackers available. Try to add new tracker.');

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();