/**
 * ### TEST CASE:
 * I-AT-Dashboard-002
 *
 * ### TEST TITLE:
 * Enable and Disable Account Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to enable and disable various trackers
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Trackers in Account Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Disable/Enable a particular tracker', async () => {
                await page.redirectToAccDashboard();

                let steps = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(2000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                	let getTrackerName = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                    getTrackerName = await getTrackerName.getText();
                    getTrackerName = getTrackerName.substr(0, getTrackerName.indexOf(' '));

                    let disable = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.action > div > div > label:nth-child(4)');
                    await disable.click();
                    steps += 'First tracker disabled, ';
                    await driver.sleep(2000);

                    let pausedTrack = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--paused');
                    await pausedTrack.click();
                    await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                    let inputTracker = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[1]/input');
                    await page.write(inputTracker, getTrackerName);
                    steps += 'Searching disabled tracker, ';
                    await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name.name--paused > a'), 20000));

                    let enable = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.action > div > div > label:nth-child(3)');
                    await enable.click();
                    steps += 'Disabled tracker enabled';
                    await driver.sleep(1000);

                    addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps
                        }
                    });

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': getTrackerName,
                            'Test status': 'Passed'
                        }
                    });
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            it('Disable/Enable multiple tracker', async () => {
                await page.redirectToAccDashboard();

                let steps = '', tracker_name = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 4000));

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(2000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                    let totalTracker = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');
                    totalTracker = totalTracker.length;

                    if (totalTracker > 3)
                        totalTracker = 3;

                    for (let i = 1; i <= totalTracker; i++) {
                        let getTrackerName = await page.findByXPath('//*[@id="account-trackers"]/div[5]/div[2]/table/tbody/tr['+i+']/td[2]/div[1]/a');
                        getTrackerName = await getTrackerName.getText();
                        tracker_name += getTrackerName+', ';
                        getTrackerName = getTrackerName.substr(0, getTrackerName.indexOf(' '));

                        let disable = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+i+') > td.action > div > div > label:nth-child(4)');
                        await disable.click();
                        steps += getTrackerName+' tracker disabled, ';
                        await driver.sleep(2000);

                        let pausedClick = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--paused');
                        await pausedClick.click();
                        await driver.wait(until.elementLocated(By.id('twt_check'), 20000));

                        let inputTracker = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[3]/div[1]/input');
                        await page.write(inputTracker, getTrackerName);
                        steps += 'Searching '+getTrackerName+' tracker, ';
                        await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name.name--paused > a'), 20000));

                        let enable = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.action > div > div > label:nth-child(3)');
                        await enable.click();
                        steps += getTrackerName+' tracker enabled, ';
                        await driver.sleep(1000);

                        let clearSearch = await page.clearFields('css', '#account-trackers > div.track-info.track-info--trackers > div.trackerFilters__wrapper.trackerFilters--AT > div.trackerFilters__search.trackerSearchbar--AT > input');
                        await driver.actions({bridge: true}).sendKeys(webdriver.Key.ARROW_DOWN).click(inputTracker).sendKeys(webdriver.Key.ARROW_UP).perform();
                        await driver.sleep(1000);

                        let activeTab = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--running');
                        await activeTab.click();
                        await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a'), 60000));
                    }

                    addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps
                        }
                    });

                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': tracker_name.replace(/,\s*$/, ""),
                            'Test status': 'Passed'
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();