/**
 * ### TEST CASE:
 * I-AT-Dashboard-005
 *
 * ### TEST TITLE:
 * Compare multiple tracker
 *
 * ### TEST SUMMARY:
 * User is trying to select multiple tracker and compare the same
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Compare multiple tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Select multiple tracker and compare the same', async () => {
                await page.redirectToAccDashboard();

                let status = '', groupName = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.heading > h1'), 3000));

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(3000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                	let totalCount = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalCount = totalCount.length;

                    if (totalCount >= 2) {
                        for (let i = 1; i <= 2; i++) {
                            let clickCheck = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+i+') > td.trackerList__manageCheck > input[type=checkbox]');
                            await clickCheck.click();
                        }

                        let clickCompare = await page.findByCSS('#bulk-manage__modal > a');
                        let hashLink = await clickCompare.getAttribute("href");
                        await clickCompare.click();
                        await driver.wait(until.elementLocated(By.css('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong'), 2000));

                        let url = await driver.getCurrentUrl();

                        assert.equal(url, hashLink);

                        let trackers = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong');
                        trackers = await trackers.getText();

                        if (url === hashLink)
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'URL before click': hashLink,
                                    'URL after click': url,
                                    'Trackers for compare': trackers,
                                    'Result': 'Tracker compare success'
                                }
                            });
                        else
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'URL before click': hashLink,
                                    'URL after click': url,
                                    'Trackers for compare': trackers,
                                    'Result': 'Tracker compare failed'
                                }
                            });
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Result': 'Multiple trackers not available.'
                            }
                        });
                    }
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            it('Select multiple tracker, goto compare page and return to dashboard', async () => {
                await page.redirectToAccDashboard();

                let status = '', groupName = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.heading > h1'), 3000));

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(3000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                    let totalCount = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalCount = totalCount.length;

                    if (totalCount >= 2) {
                        for (let i = 1; i <= 2; i++) {
                            let clickCheck = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+i+') > td.trackerList__manageCheck > input[type=checkbox]');
                            await clickCheck.click();
                            await driver.sleep(2000);
                        }

                        let url = await driver.getCurrentUrl();

                        let clickCompare = await page.findByCSS('#bulk-manage__modal > a');
                        await clickCompare.click();
                        await driver.sleep(2000);

                        let trackers = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.keyjs-tracker-group__header.key-trackerHeader__wrapper > h1 > strong');
                        trackers = await trackers.getText();

                        let backTo = await page.findByCSS('#tracker-group > div > div > div:nth-child(1) > div > section.key-trackerSubHeader > div.key-trackerSubHeader__filters > a');
                        let clickLink = await backTo.getAttribute("href");
                        await backTo.click();

                        assert.equal(url, clickLink);

                        if (url === clickLink)
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Trackers for compare': trackers,
                                    'Result': 'Tracker compare and back to dashboard success'
                                }
                            });
                        else
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Trackers for compare': trackers,
                                    'Result': 'Tracker compare and back to dashboard failed'
                                }
                            });
                    }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Result': 'Multiple trackers not available.'
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();