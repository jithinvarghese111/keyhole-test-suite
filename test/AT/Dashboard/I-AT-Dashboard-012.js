/**
 * ### TEST CASE:
 * I-AT-Dashboard-012
 *
 * ### TEST TITLE:
 * Checking Data Limit Reached
 *
 * ### TEST SUMMARY:
 * User is checking data limit reached and trying to purhase to Add More Data to your Keyhole Account
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Data Limit Reached', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Data Limit Reached', async () => {
                await page.redirectToDashboard();

                let steps = '';
                let checkLimit = await page.checkElementByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__heading > h3');

                if (checkLimit) {
                    let getData = await page.findByCSS('body > section.key-modalContainer.key-modalContainer--show.keyjs-modalContainer--usage-exceeded > div > div.key-modal__wrapper > div.key-modal__footer > button.kc-button.kc-button--caps.kc-button--medium.kc-button--orange.keyjs-getMoreButton');
                    await getData.click();
                    await driver.sleep(2000);

                    let gotoCheckout = await page.findByCSS('#overrideCta-1');
                    await gotoCheckout.click();
                    await driver.sleep(2000);

                    let stripeNumber = await page.findByCSS('#user-override-payment-form > div:nth-child(2) > div > input');
                    await page.write(stripeNumber, '4242424242424242');

                    let stripeCvc = await page.findByCSS('#user-override-payment-form > div:nth-child(3) > div > input');
                    await page.write(stripeCvc, '111');

                    let stripeExpMonth = await page.findByCSS('#user-override-payment-form > div.form-group.lastFormGroup > div > input:nth-child(1)');
                    await page.write(stripeExpMonth, '01');

                    const stripeExpYear = new Date().getFullYear() + 2

                    let stripeYear = await page.findByCSS('#user-override-payment-form > div.form-group.lastFormGroup > div > input:nth-child(3)');
                    await page.write(stripeYear, stripeExpYear);

                    let purchase = await page.findById('overrideCta-2');
                    await purchase.click();
                    await driver.sleep(2000);

                    await page.screenshot('C:/wamp64/www/keyhole-test-suite/mochawesome-report/report/out1.png');

                    let checkerr = await page.checkElementByCSS('#user-override__modal-2 > div > div.trackerDataContent > div > div > p');
                    if (checkerr) {

                        let errorText = await page.findByCSS('#user-override__modal-2 > div > div.trackerDataContent > div > div > p');
                        errorText = await errorText.getText();
                        addContext(this.ctx, 'error occured'+errorText);
                    
                    }

                }else{
                    addContext(this.ctx, 'Data Limit Reached Popup No found');
                }

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();