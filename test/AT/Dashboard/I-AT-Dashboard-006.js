    /**
 * ### TEST CASE:
 * I-AT-Dashboard-006
 *
 * ### TEST TITLE:
 * Activity Log of Account Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to check Activities that performed on Account Tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
var webdriver = require('selenium-webdriver');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Activity Log of Account Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Activity Log', async () => {
                await page.redirectToAccDashboard();

                let status = '', steps = '', groupName = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.css('#account-trackers > div.heading > h1'), 3000));

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(1000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

                if (checkTracker) {
                    let getTrackerName = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a');
                    getTrackerName = await getTrackerName.getText();

                    let disable = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.action > div > div > label:nth-child(4)');
                    await disable.click();
                    steps += 'First tracker disabled, ';
                    await driver.sleep(2000);

                    let pausedTrack = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--paused');
                    await pausedTrack.click();
                    await driver.sleep(3000);

                    let inputTracker = await page.findByCSS('#account-trackers > div.track-info.track-info--trackers > div.trackerFilters__wrapper.trackerFilters--AT > div.trackerFilters__search.trackerSearchbar--AT.tracker__search-wrapper.tracker__search-wrapper--AT > input');
                    await inputTracker.click();
                    await page.write(inputTracker, getTrackerName);
                    steps += 'Searching disabled tracker, ';
                    await driver.sleep(2000);

                    let enable = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.action > div > div > label:nth-child(3)');
                    await enable.click();
                    steps += 'Disabled tracker enabled';
                    await driver.sleep(2000);

                    let activeTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--running');
                    await activeTracker.click();
                    await driver.sleep(2000);

                    addContext(this.ctx, {
                        title: 'Test Steps',
                        value: {
                            'Steps': steps
                        }
                    });

                    let activityLog = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.created-date > a.show-tracker-logs.account');
                    await activityLog.click();
                    await driver.sleep(2000);

                    let totalActions = await page.findElementsByCSS('#tracker-logs__modal-step1 > div > div > div > table > tbody > tr > td.log__user');
                    for (var i = 1; i <= totalActions.length; i++) {
                        let actions = await page.findByXPath(' //*[@id="tracker-logs__modal-step1"]/div/div/div/table/tbody/tr['+i+']/td[4]/p');
                        actions = await actions.getText();
                        status = 'Tracker Activity log :'+actions;
                        addContext(this.ctx, 'Tracker Activity log :'+actions);
                    }
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();   