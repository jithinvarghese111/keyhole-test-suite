/**
 * ### TEST CASE:
 * I-AT-Dashboard-011
 *
 * ### TEST TITLE:
 * Checking Trackers from Paused Account Dashboard
 *
 * ### TEST SUMMARY:
 * User is trying to go to particular paused tracker and comes back to home page
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Trackers in Account Dashboard', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Go to tracker dashboard and return to home page', async () => {
                await page.redirectToAccDashboard();

                let steps = '';

                let pausedTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--paused');
                await pausedTracker.click();
                await driver.sleep(2000);

                let checkTracker = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (checkTracker) {
                	let gotoTrack = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name.name--paused > a');
                	await gotoTrack.click();
                	steps += 'First paused tracker selected and clicked, ';
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="account"]/div/div[1]/div[1]/div[2]/div/a/p'), 10000));

                    let trackerNameCheck = await page.checkElementByCSS('#AT-Dashboard > div > div > div.info > p > span');

                    if (trackerNameCheck) {
                    	let trackerName = await page.findByXPath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span');
                    	trackerName = await trackerName.getText();

                        let alertmsg = await page.findByXPath('//*[@id="dashboard"]/div[2]');
                        alertmsg = await alertmsg.getText();

                    	let gotoDash = await page.findByXPath('//*[@id="keyhole-global-sidebar"]/div/div[2]/ul/li[1]/a');
                    	await gotoDash.click();
                    	steps += 'Clicked my tracker link from sidebar menu';
                    	await driver.wait(until.elementLocated(By.css('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(1) > td.tracker-info > div.name > a'), 10000));

                    	addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test steps': steps,
                                'alert message': alertmsg,
                                'Test status': 'Passed'
                            }
                        });
                        }
                    else {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Result': 'No post available. Redirecting to dashboard.'
                            }
                        });
                    }
                }
                else {
                	addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Result': 'No trackers available. Try to add new tracker.'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();