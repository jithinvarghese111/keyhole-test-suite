/**
 * ### TEST CASE:
 * I-AT-Optimization-F012
 *
 * ### TEST TITLE:
 * click each post and from the modal click on VIEW ON FACEBOOK button
 *
 * ### TEST SUMMARY:
 * User is trying to click each post and from the modal click on VIEW ON FACEBOOK button and check page exists or not
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('click each post and from the modal click on VIEW ON FACEBOOK button', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click each post and from the modal click on VIEW ON FACEBOOK button', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('facebook');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let clickOpti = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(2) > a');
                    await clickOpti.click();
                    await driver.sleep(8000);

                    addContext(this.ctx, 'Tracker name: '+trackerName);

                    await page.scrollPage('//*[@id="account-tracking"]/div/section[1]/section/div/figure[3]/div[1]/h3/span');
                	await driver.sleep(2000);

                  	let totalCount = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr');
                  	totalCount = totalCount.length;

                  	addContext(this.ctx, 'Total count: '+totalCount);

                  	for (let i = 1; i <= totalCount; i++) {

                        addContext(this.ctx, '--------------------');

                  		let getDay = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr:nth-child('+i+') > td:nth-child(1)');
                        getDay = await getDay.getText();
                        addContext(this.ctx, 'Day '+i+': '+getDay);
                        await driver.sleep(2000);

                        let clickSingle = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--optimalPostTime > div.key-chartContainer.key-scroll.key-scroll--thin > table > tbody > tr:nth-child('+i+')');
                  		await clickSingle.click();
                  		await driver.sleep(2000);

                        let checkPosts = await page.checkElementByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr');
                        if (checkPosts) {

                        let totalPost = await page.findElementsByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr');
                        totalPost = totalPost.length;
                        addContext(this.ctx, 'Total posts in modal: '+totalPost);

                        let checkOne = await page.checkElementByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr');
                        if (checkOne) {
                            let clickFirst = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr');
                            await clickFirst.click();
                            await driver.sleep(1000);
                        }

                        let checkMore = await page.checkElementByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr:nth-child('+i+')');
                        if (checkMore) {

                            let clickPost = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > table > tbody > tr:nth-child('+i+')');
                            await clickPost.click();
                            await driver.sleep(1000);
                        }

                        let getURL = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > article > section.key-sideviewPost__statLine > a');
                        let selected = await getURL.getAttribute("href");
                        addContext(this.ctx, 'URL before click #'+i+': '+selected);
                        addContext(this.ctx, 'Clicked View On Facebook for Post'+i+'of :'+getDay+'');
                        await getURL.click();

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });

                        await driver.switchTo().window(tab2);
                        let url = await driver.getCurrentUrl();
                        addContext(this.ctx, 'URL after click #'+i+': '+url);

                        if (url == selected) addContext(this.ctx, '#'+i+': Both are equal');
                        else addContext(this.ctx, '#'+i+': Both are not equal');

                        await driver.close();
                        await driver.switchTo().window(tab1);

                        let closeModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postListView.key-sideview--active > div > button');
                		await closeModal.click();
                        await driver.sleep(1000);

                    }else
                        addContext(this.ctx, 'No Posts Found');
                  	}
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No facebook tracker found or added'
                        }
                    });
                   }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();