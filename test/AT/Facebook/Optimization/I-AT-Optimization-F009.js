/**
 * ### TEST CASE:
 * I-AT-Optimization-F009
 *
 * ### TEST TITLE:
 * Downloading XLS
 *
 * ### TEST SUMMARY:
 * User should be able to download an excel file of Optimization of Facebook account tracker information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const fs = require('fs');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading XLS', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking of downloading XLS', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('facebook');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                
                    let hash = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.trackerList__manageCheck > input[type=checkbox]');
                    hash = await hash.getAttribute("value");
                    
                    let trackerName = await clickLink.getText();
                    let params = trackerName.substring(trackerName.indexOf("@")+1);
                    let firstParam = params.substr(0, params.indexOf(')'));
                    await clickLink.click();

                    let optimization = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(2) > a');
                    await optimization.click();
                    await driver.sleep(8000);
                    
                    let test_result = '';
                    var toDate = new Date(); 
                    var lastDay =  new Date(toDate.getFullYear(), toDate.getMonth() + 1, 1);           
                    const toDateStr = lastDay.toJSON().slice(0, 10).replace(/-/g, '');

                    let convertdate = await page.findByCSS('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > div > div > span > div > input');
                    convertdate = await convertdate.getAttribute("value");
                    let firstsub = convertdate.substr(0, convertdate.indexOf('-'));
                    let sub = firstsub.substr(0, firstsub.lastIndexOf(' '));
                    let dateNew = await page.changeformat(sub);
                    dateNew = dateNew.replace(/-/g,'');

                    let xlsLink = await page.findByCSS('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerSubHeader > div.key-trackerSubHeader__actions > button:nth-child(3)');
                    await xlsLink.click();  
                    await driver.sleep(10000);
                    
                    const downloadPath = config.downloadPath+'\/'+'Keyhole_'+firstParam+'_Optimization_'+'('+hash+')_'+dateNew+'-'+toDateStr+'.xlsx';
                    const filename = `Keyhole_${firstParam}_(${hash})_${new Date().getTime()}.xls`;
                    const renamePath = config.downloadPath+'\/'+filename;  

                    if (fs.existsSync(downloadPath)) {
                        fs.renameSync(downloadPath, renamePath)
                        
                        if (fs.existsSync(renamePath)) {
                            const stats = fs.statSync(renamePath)

                            if (parseFloat(stats.size) > 64) {
                                test_result = "Downloaded file";
                                passed = true
                            } else {
                                test_result = "Downloaded empty file.";
                                passed = true
                            }
                        }
                    }     
                    
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': test_result
                        }
                    });
                }
                else assert.fail('No facebook tracker found or added');

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();