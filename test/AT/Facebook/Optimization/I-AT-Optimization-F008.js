    /**
 * ### TEST CASE:
 * I-AT-Optimization-F008
 *
 * ### TEST TITLE:
 * Downloading PDF
 *
 * ### TEST SUMMARY:
 * User should be able to download a PDF file of social media account tracker information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading PDF', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking of downloading PDF', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('facebook');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));
                
                    let optimization = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > nav > li:nth-child(2) > a');
                    await optimization.click();
                    await driver.sleep(5000);

                    let pdfLink = await page.findById('pdf');
                    await pdfLink.click();
                    // This process sometimes takes more than 30 seconds
                    await driver.sleep(10000);  

                    let checkEle = await page.checkElementByCSS('#download_btn');

                    if (checkEle) {
                        let viewPdf = await page.findByCSS('#download_btn');
                        await viewPdf.click();

                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];
                        });
                        await driver.switchTo().window(tab1);
                        await driver.wait(until.elementLocated(By.id('pdf'), 20000));

                        addContext(this.ctx, {
                            title: 'other Context',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Download file'
                            }
                        });
                    }
                    else assert.fail('Error with download pdf file for '+trackerName+'');
                }
                else assert.fail('No facebook tracker found or added');

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();