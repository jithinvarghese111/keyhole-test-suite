/**
 * ### TEST CASE:
 * I-AT-Posts-F005
 *
 * ### TEST TITLE:
 * Checking Likes, Comments, Shares counts in the post list is same in post details modal
 *
 * ### TEST SUMMARY:
 * User should be able to click on the post and check Likes, Comments, Shares counts in the modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Likes, Comments, Shares counts in the post list is same in post details modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on the post and check Likes, Comments, Shares counts in the modal', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('facebook');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(4) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                  	let totalPosts = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total post: '+totalPosts);

                  	if (totalPosts > 5) totalPosts = 5;

                    for (let i = 1; i <= totalPosts; i++) {
                        addContext(this.ctx, '---------------------------');

                        let likesList = await page.findByCSS('#all-posts > div > div.table-container > table > tbody > tr:nth-child('+i+') > td.like-count');
                        likesList = await likesList.getText();

                        let commentList = await page.findByCSS('#all-posts > div > div.table-container > table > tbody > tr:nth-child('+i+') > td.comment-count');
                        commentList = await commentList.getText();

                        let shareList = await page.findByCSS('#all-posts > div > div.table-container > table > tbody > tr:nth-child('+i+') > td.share-count');
                        shareList = await shareList.getText();

                        let clickPost = await page.findByCSS('#all-posts > div > div.table-container > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let likeModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-likes > span');
                        likeModal = await likeModal.getText();

                        let commentModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-comments > span');
                        commentModal = await commentModal.getText();

                        let shareModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-shares > span');
                        shareModal = await shareModal.getText();

                        if (likesList == likeModal) addContext(this.ctx, 'Likes count is same');
                        else addContext(this.ctx, 'Likes count is not same');

                        if (commentList == commentModal) addContext(this.ctx, 'Comment count is same');
                        else addContext(this.ctx, 'Comment count is not same');

                        if (shareList == shareModal) addContext(this.ctx, 'Shares count is same');
                        else addContext(this.ctx, 'Shares count is not same');

                        assert.equal(likesList, likeModal);
                        assert.equal(commentList, commentModal);
                        assert.equal(shareList, shareModal);

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No facebook tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();