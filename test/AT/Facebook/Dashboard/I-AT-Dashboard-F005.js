/**
 * ### TEST CASE:
 * I-AT-Dashboard-F005
 *
 * ### TEST TITLE:
 * Checking Top Posts by Engagements modal
 *
 * ### TEST SUMMARY:
 * User is trying to click on the Top Posts by Engagements posts and checking counts from the modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Top Posts by Engagements modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on the Top Posts by Engagements posts and checking counts from the modal', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('facebook');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    addContext(this.ctx, 'Tracker name: '+trackerName);
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.css('#account-tracking > div > div.key-trackerHeader.key-trackerHeader--restructured > div.key-trackerHeader__wrapper > h1 > strong'), 20000));

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                    await driver.sleep(1000);

                    await page.scrollPage('//*[@id="account-tracking"]/div/section[1]/section/div/figure[3]/div[1]/h3');
                    await driver.sleep(1000);

                    let totalPosts = await page.findElementsByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr');
                    totalPosts = totalPosts.length;
                    addContext(this.ctx, 'Total posts: '+totalPosts);

                    for (let i = 1; i <= totalPosts; i++) {
                        addContext(this.ctx, '--------------- '+i+' -----------------');

                        let likes = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr:nth-child('+i+') > td:nth-child(2)');
                        likes = await likes.getText();

                        let comments = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr:nth-child('+i+') > td:nth-child(3)');
                        comments = await comments.getText();

                        let shares = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr:nth-child('+i+') > td:nth-child(4)');
                        shares = await shares.getText();

                        let clickPost = await page.findByCSS('#account-tracking > div > section.route-section > section > div > figure.key-chartBox.key-chartBox--noLogo.key-chartBox--titleOutsideBox.key-chartBox--table.key-chartBox--topPosts > div.key-chartContainer > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(2000);

                        let likeModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > article > section.key-sideviewPost__statLine > div > span:nth-child(2) > span:nth-child(2)');
                        likeModal = await likeModal.getText();

                        let commentModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > article > section.key-sideviewPost__statLine > div > span:nth-child(3) > span:nth-child(2)');
                        commentModal = await commentModal.getText();

                        let shareModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > article > section.key-sideviewPost__statLine > div > span:nth-child(4) > span:nth-child(2)');
                        shareModal = await shareModal.getText();

                        if (likes === likeModal) addContext(this.ctx, 'Likes count is same');
                        else addContext(this.ctx, 'Likes count is not same');

                        if (comments === commentModal) addContext(this.ctx, 'Comment count is same');
                        else addContext(this.ctx, 'Comment count is not same');

                        if (shares === shareModal) addContext(this.ctx, 'Shares count is same');
                        else addContext(this.ctx, 'Shares count is not same');

                        let closeModal = await page.findByCSS('#account-tracking > div > div.key-sideview.key-sideview--postView.key-sideview--active > div > button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else assert.fail('No facebook tracker found or added');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();