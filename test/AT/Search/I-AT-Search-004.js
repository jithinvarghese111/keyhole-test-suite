/**
 * ### TEST CASE:
 * I-AT-Search-004
 *
 * ### TEST TITLE:
 * Search Paused Account Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to search for a particular paused tracker.
 *       
 *
 */

const addContext = require('mochawesome/addContext');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../lib/basePage');
const config = require('../../../utils/config');
const generate = require('../../../utils/generate')();
const keyhole = require('../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Search Paused Account Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Search Paused Account Tracker', async () => {
                await page.redirectToAccDashboard();

                let pausedTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--paused');
                await pausedTracker.click();
                await driver.sleep(3000);

                let searchBox = await page.findByCSS('#account-trackers > div.track-info.track-info--trackers > div.trackerFilters__wrapper.trackerFilters--AT > div.trackerFilters__search.trackerSearchbar--AT > input');
                await page.write(searchBox, config.atTrackerSearch);
                await driver.sleep(5000);

                let noResult = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                if (noResult) {
                    let totalTracker = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                    totalTracker = totalTracker.length;

                    noResult_msg = 'Total paused tracker found after search: '+totalTracker;
                }
                else {
                    noResult_msg = 'Search result empty. No paused trackers found';
                }

                addContext(this.ctx, {
                    title: 'Other context',
                    value: {
                        'Search keyword': config.atTrackerSearch,
                        'Search result': noResult_msg
                    }
                });
            });

            it('Search with random keyword', async () => {
                await page.redirectToAccDashboard();

                let pausedTracker = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.my-tracks__filters > div.my-tracks__filters--section.my-tracks__filters--sectionTop > button.key-button.key-button--mytracksFilters.js-my-tracks__filters--paused');
                await pausedTracker.click();
                await driver.sleep(3000);

                for (var i = 1; i <= 3; i++) {
                    var random_name = randomName();

                    await page.clearFields('css', '#account-trackers > div.track-info.track-info--trackers > div.trackerFilters__wrapper.trackerFilters--AT > div.trackerFilters__search.trackerSearchbar--AT > input');
                    await driver.sleep(2000);

                    let searchBox = await page.findByCSS('#account-trackers > div.track-info.track-info--trackers > div.trackerFilters__wrapper.trackerFilters--AT > div.trackerFilters__search.trackerSearchbar--AT > input');
                    await page.write(searchBox, random_name);
                    await driver.sleep(2000);

                    let noResult = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');

                    if (noResult) {
                        let totalTracker = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr');
                        totalTracker = totalTracker.length;

                        noResult_msg = 'Total paused tracker found after search: '+totalTracker;
                    }
                    else {
                        noResult_msg = 'Search result empty. No paused trackers found';
                    }

                    addContext(this.ctx, {
                        title: 'Other context',
                        value: {
                            'Search keyword': random_name,
                            'Search result': noResult_msg
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();