// /**
//  * ### TEST CASE:
//  * I-AT-Search-002
//  *
//  * ### TEST TITLE:
//  * Search Account Tracker with Social Platform Select
//  *
//  * ### TEST SUMMARY:
//  * User is trying to search for a particular tracker
//  *       from the main account tracker landing page
//  *
//  */

// const addContext = require('mochawesome/addContext');
// const chai = require('chai');
// const chaiAsPromised = require('chai-as-promised');
// const assert = require('assert');
// const Page = require('../../../lib/basePage');
// const config = require('../../../utils/config');
// const generate = require('../../../utils/generate')();
// const keyhole = require('../../../utils/keyhole')();
// const expect = chai.expect;
// chai.use(chaiAsPromised);

// process.on('unhandledRejection', () => { });

// (async function example() {
//     try {
//         describe('Search Account Tracker with Social Platform', async function () {
//             beforeEach(async () => {
//                 page = new Page();
//                 driver = page.driver;

//                 addContext(this.ctx, {
//                     title: 'beforeEach context',
//                     value: 'Test Started'
//                 });

//                 await page.gotoLogin();
//             });

//             it('Search with available tracker and social platform selection', async () => {
//                 await page.redirectToAccDashboard();

//                 await page.scrollPage('//*[@id="account-trackers"]/div[3]');
//                 await driver.sleep(1000);

//                 let searchBox = await page.findByCSS('#account-trackers > div.trackerFilters.trackerFilters--AT > div:nth-child(1) > div.trackerFilters__search > input');
//                 await page.write(searchBox, config.atTrackerSearch);
//                 await driver.sleep(1000);

//                 let platform = ['twitter', 'instagram', 'facebook', 'youtube'];

//                 for (i = 0; i < platform.length; i++) {
//                     let platformClick = await page.findByCSS('#account-trackers > div.trackerFilters.trackerFilters--AT > div:nth-child(1) > div.key-platformCheckboxes.trackerFilters__platforms.js-trackerFilters__platforms > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox--'+platform[i]);
//                     await platformClick.click();
//                     await driver.sleep(1000);
//                 }

//                 await driver.sleep(1000);

//                 for (i = 0; i < platform.length; i++) {
//                     let platformClick = await page.findByCSS('#account-trackers > div.trackerFilters.trackerFilters--AT > div:nth-child(1) > div.key-platformCheckboxes.trackerFilters__platforms.js-trackerFilters__platforms > label.key-platformCheckboxes__checkbox.key-platformCheckboxes__checkbox--'+platform[i]);
//                     await platformClick.click();
//                     await driver.sleep(2000);

//                     let noResult = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

//                     if (noResult) {
//                         let totalTracker = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');
//                         totalTracker = totalTracker.length;

//                         noResult_msg = 'Total tracker found after search: '+totalTracker;
//                     }
//                     else {
//                         noResult_msg = 'Search result empty. No trackers found';
//                     }

//                     addContext(this.ctx, {
//                         title: 'Other context',
//                         value: {
//                             'Search keyword': config.atTrackerSearch,
//                             'Plaforms selected': platform[i],
//                             'Search result': noResult_msg
//                         }
//                     });

//                     await platformClick.click();
//                     await driver.sleep(2000);
//                 }
//             });

//             afterEach(async () => {
//                 addContext(this.ctx, {
//                     title: 'afterEach context',
//                     value: 'Test Completed'
//                 });

//                 await page.quit();
//             });
//         });
//     } catch (ex) {
//         console.log(new Error(ex.message));
//     } finally {

//     }
// })();