// /**
//  * ### TEST CASE:
//  * I-AT-Search-003
//  *
//  * ### TEST TITLE:
//  * Search Account Tracker with group, platform filter
//  *
//  * ### TEST SUMMARY:
//  * User is trying to search for a particular tracker from 
//  *       the main account tracker landing page
//  *
//  */

// const addContext = require('mochawesome/addContext');
// const chai = require('chai');
// const chaiAsPromised = require('chai-as-promised');
// const assert = require('assert');
// const Page = require('../../../lib/basePage');
// const config = require('../../../utils/config');
// const generate = require('../../../utils/generate')();
// const keyhole = require('../../../utils/keyhole')();
// const expect = chai.expect;
// chai.use(chaiAsPromised);

// process.on('unhandledRejection', () => { });

// (async function example() {
//     try {
//         describe('Search Account Tracker with group, platform filter', async function () {
//             beforeEach(async () => {
//                 page = new Page();
//                 driver = page.driver;

//                 addContext(this.ctx, {
//                     title: 'beforeEach context',
//                     value: 'Test Started'
//                 });

//                 await page.gotoLogin();
//             });

//             it('Search with tracker and group filter', async () => {
//                 await page.redirectToAccDashboard();

//                 await page.scrollPage('//*[@id="account-trackers"]/div[3]');
//                 await driver.sleep(1000);

//                 let searchBox = await page.findByCSS('#account-trackers > div.trackerFilters.trackerFilters--AT > div:nth-child(1) > div.trackerFilters__search > input');
//                 await page.write(searchBox, config.atTrackerSearch);
//                 await driver.sleep(1000);

//                 let filterToggle = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/div[1]');
//                 await filterToggle.click();
//                 await driver.sleep(1000);

//                 let totalGroup = await page.findElementsByCSS('#account-trackers > div.trackerFilters.trackerFilters--AT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li');
//                 totalGroup = await totalGroup.length;

//                 if (totalGroup > 5)
//                     totalGroup = 5;

//                 for (var i = 1; i <= totalGroup; i++) {
//                     let mainDrop = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/div[2]/div/div/div');
//                     await mainDrop.click();
//                     await driver.sleep(1000);

//                     let groupClick = await page.findByCSS('#account-trackers > div.trackerFilters.trackerFilters--AT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li:nth-child('+i+')');
//                     let groupText = await groupClick.getText();
//                     await groupClick.click();
//                     await driver.sleep(1000);

//                     let noResult = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

//                     if (noResult) {
//                         let totalTracker = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');
//                         totalTracker = totalTracker.length;

//                         noResult_msg = 'Total tracker found after search: '+totalTracker;
//                     }
//                     else {
//                         noResult_msg = 'Search result empty. No trackers found';
//                     }

//                     addContext(this.ctx, {
//                         title: 'Other context',
//                         value: {
//                             'Search keyword': config.atTrackerSearch,
//                             'Selected group': groupText,
//                             'Search result': noResult_msg
//                         }
//                     });
//                 }
//             });
            
//             it('Search with group filter', async () => {
//                 await page.redirectToAccDashboard();

//                 await page.scrollPage('//*[@id="account-trackers"]/div[3]');
//                 await driver.sleep(1000);

//                 let filterToggle = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/div[1]');
//                 await filterToggle.click();
//                 await driver.sleep(1000);

//                 let totalGroup = await page.findElementsByCSS('#account-trackers > div.trackerFilters.trackerFilters--AT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li');
//                 totalGroup = await totalGroup.length;

//                 if (totalGroup > 5)
//                     totalGroup = 5;

//                 for (var i = 1; i <= totalGroup; i++) {
//                     let mainDrop = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/div[2]/div/div/div');
//                     await mainDrop.click();
//                     await driver.sleep(1000);

//                     let groupClick = await page.findByCSS('#account-trackers > div.trackerFilters.trackerFilters--AT > div.trackerFilters__advanced.trackerFilters__wrapper > div.trackerFilters__advancedContent.active > div > div > ul > li:nth-child('+i+')');
//                     let groupText = await groupClick.getText();
//                     await groupClick.click();
//                     await driver.sleep(1000);

//                     let noResult = await page.checkElementByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');

//                     if (noResult) {
//                         let totalTracker = await page.findElementsByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div > table > tbody > tr');
//                         totalTracker = totalTracker.length;

//                         noResult_msg = 'Total tracker found after search: '+totalTracker;
//                     }
//                     else {
//                         noResult_msg = 'Search result empty. No trackers found';
//                     }

//                     addContext(this.ctx, {
//                         title: 'Other context',
//                         value: {
//                             'Selected group': groupText,
//                             'Search result': noResult_msg
//                         }
//                     });
//                 }
//             });

//             afterEach(async () => {
//                 addContext(this.ctx, {
//                     title: 'afterEach context',
//                     value: 'Test Completed'
//                 });

//                 await page.quit();
//             });
//         });
//     } catch (ex) {
//         console.log(new Error(ex.message));
//     } finally {

//     }
// })();