/**
 * ### TEST CASE:
 * I-AT-New-T001
 *
 * ### TEST TITLE:
 * Managing New Youtube Account Tracker
 *
 * ### TEST SUMMARY:
 * User is trying to go to add new tracker
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Managing New Youtube Account Tracker', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Add new tracker', async () => {
                await page.redirectToAccDashboard();

                let newClick = await page.findByXPath('//*[@id="account-trackers"]/div[1]/div/a');
                await newClick.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[1]/h1'), 3000));

                let selectYoutube = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/button[4]');
                await selectYoutube.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/input'), 4000));

                let inputKeyword = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/input');
                await page.write(inputKeyword, randomKeyword);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/div/div[1]'), 4000));

                let selectFirst = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/div/div[1]');
                await selectFirst.click();
                await driver.wait(until.elementLocated(By.xpath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[3]/button[1]'), 3000));

                let getSelected = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[2]/div/input');
                selected = await getSelected.getAttribute("value");

                let createTracker = await page.findByXPath('//*[@id="tracker-creation"]/div/div[1]/div[2]/div/div[3]/button[1]');
                await createTracker.click();
                await driver.sleep(5000);

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': randomKeyword,
                        'Selected tracker': selected,
                        'Test status': 'Tracker creation in progress'
                    }
                });
            });
            
            it('Checking created tracker', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(1000);

                let firstTracker = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                firstTracker = await firstTracker.getText();

                if (firstTracker.indexOf(selected) > -1)
                    status = 'Tracker created successfully';
                else
                    status = 'Tracker not found.';

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': selected,
                        'Test status': status
                    }
                });
            });

            it('Delete created tracker', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                await page.scrollPage('//*[@id="account-trackers"]/div[4]');
                await driver.sleep(1000);

                let firstTracker = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td[2]/div[1]/a');
                firstTracker = await firstTracker.getText();

                if (firstTracker.indexOf(selected) > -1) {
                    let deleteTracker = await page.findByXPath('//*[@id="account-trackers"]/div[4]/div[2]/table/tbody/tr[1]/td[2]/div[2]/a[2]');
                    await deleteTracker.click();
                    await driver.sleep(1000);

                    let confirmDelete = await page.findByCSS('#deletetrackacc__modal #confirm_delete_btn');
                    await confirmDelete.click();
                    await driver.sleep(5000);

                    status = 'Tracker deleted successfully';
                }
                else {
                    status = 'Tracker not found.';
                }

                addContext(this.ctx, {
                    title: 'Test Results',
                    value: {
                        'Tracker name': selected,
                        'Test status': status
                    }
                });
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();