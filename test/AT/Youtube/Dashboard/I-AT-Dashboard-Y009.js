/**
 * ### TEST CASE:
 * I-AT-Dashboard-Y009
 *
 * ### TEST TITLE:
 * Checking Likes, Comments, views counts in the top video list is same in video post details modal
 *
 * ### TEST SUMMARY:
 * User should be able to click on the video post and check Likes, Comments, view counts in the modal
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking Likes, Comments, views counts in the top video list is same in video post details modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on the video post and check Likes, Comments, view counts in the modal', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');
        
                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(5000);
                    
                  	let secondTrack = await page.checkElementByCSS('#top-posts > header > h1');

                    if (secondTrack) {
                        let scrollTo = await page.findByCSS('#top-posts > header > h1');
                        await driver.executeScript("arguments[0].scrollIntoView();", scrollTo);
                        await driver.sleep(5000);

                        let totalCount = await page.findElementsByCSS('#top-posts > div > table > tbody > tr');
                        totalCount = totalCount.length;

                        if (totalCount > 5) totalCount = 5;

                        for (let i = 1; i <= totalCount; i++) {

                        let likesList = await page.findByCSS('#top-posts > div > table > tbody > tr:nth-child('+i+') > td.like-count.youtube');
                        likesList = await likesList.getText();

                        let commentList = await page.findByCSS('#top-posts > div > table > tbody > tr:nth-child('+i+') > td.comment-count.youtube');
                        commentList = await commentList.getText();

                        let viewList = await page.findByCSS('#top-posts > div > table > tbody > tr:nth-child('+i+') > td.view-count.youtube');
                        viewList = await viewList.getText();

                        let clickPost = await page.findByXPath('//*[@id="top-posts"]/div/table/tbody/tr['+i+']');
                        await clickPost.click();
                        await driver.sleep(1000);

                        let likeModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-likes > span');
                        likeModal = await likeModal.getText();

                        let commentModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-comments > span');
                        commentModal = await commentModal.getText();

                        let viewModal = await page.findByCSS('#post-view > div.content > div > div > div.post-meta > div.post-stats > div.post-views > span');
                        viewModal = await viewModal.getText();

                        if (likesList == likeModal) addContext(this.ctx, 'Likes count is same');
                        else addContext(this.ctx, 'Likes count is not same');

                        if (commentList == commentModal) addContext(this.ctx, 'Comment count is same');
                        else addContext(this.ctx, 'Comment count is not same');

                        if (viewList == viewModal) addContext(this.ctx, 'Views count is same');
                        else addContext(this.ctx, 'Views count is not same');

                        assert.equal(likesList, likeModal);
                        assert.equal(commentList, commentModal);
                        assert.equal(viewList, viewModal);

                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                        }
                    }
                    else {
                        status = 'We were unable to find any video posts from this account.';

                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': status
                            }
                        });
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No facebook tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();