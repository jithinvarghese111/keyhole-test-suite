/**
 * ### TEST CASE:
 * I-AT-Dashboard-Y010
 *
 * ### TEST TITLE:
 * Checking tracker name is present in the modal on click Top Videos by Views.
 *
 * ### TEST SUMMARY:
 * User should be able to click on the Top Videos and check tracker name is present in the modal opened.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking tracker name is present in the modal on click Top Videos by Views.', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking tracker name is present in the modal on click Top Videos by Views.', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');
        
                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    await page.scrollPage('//*[@id="top-posts"]/header/h1');
                    await driver.sleep(1000);

                    let totalVideos = await page.findElementsByCSS('#top-posts > div > table > tbody > tr');
                    totalVideos = totalVideos.length;
                    
                    addContext(this.ctx, 'Total Video: '+totalVideos);

                    if (totalVideos > 5) totalVideos = 5;

                    for (let i = 1; i <= totalVideos; i++) {
                        addContext(this.ctx, '-----------------------------');

                        let clickPost = await page.findByCSS('#top-posts > div > table > tbody > tr:nth-child('+i+')');
                        await clickPost.click();
                        await driver.sleep(2000);

                        let nameModal = await page.findByCSS('#post-view > div.top-bar > div > div.user-info > a > div > div.screen-name');
                        let trackernameModal = await nameModal.getText();
                        addContext(this.ctx, 'Tracker name from modal: '+trackernameModal);
                        await nameModal.click();
                        await driver.sleep(1000);

                        let getURL = await page.findByCSS('#post-view > div.top-bar > div > div.user-info > a');
                        let selected = await getURL.getAttribute("href");
                        addContext(this.ctx, 'URL before click #'+i+': '+selected);

                        if (trackerName.indexOf(trackernameModal) > -1) addContext(this.ctx, 'Tracker names are same');
                        else addContext(this.ctx, 'Tracker names are not same');
                                                
                        let tab1, tab2;

                        await driver.getAllWindowHandles().then(function(windowHandles) {
                            tab1 = windowHandles[0];
                            tab2 = windowHandles[1];                
                        });

                        await driver.switchTo().window(tab2);
                        let url = await driver.getCurrentUrl();
                        addContext(this.ctx, 'URL after click #'+i+': '+url);

                        if (url == selected) addContext(this.ctx, '#'+i+': Both are equal');
                        else addContext(this.ctx, '#'+i+': Both are not equal');
                        await driver.close();
                        await driver.switchTo().window(tab1);
                        // await driver.wait(until.elementLocated(By.css('#post-view > div.top-bar > div  > div.x-button'), 20000));
                        await driver.sleep(5000);
                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                    }
                }
                else assert.fail('No Youtube tracker found or added.');
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();