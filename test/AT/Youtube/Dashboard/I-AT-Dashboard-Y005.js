/**
 * ### TEST CASE:
 * I-AT-Dashboard-Y005
 *
 * ### TEST TITLE:
 * Checking all the section tab selection
 *
 * ### TEST SUMMARY:
 * User is trying to click on different tabs in all sections
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking all the section tab selection', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Click on different tabs in Account Statistics', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    await page.scrollPage('//*[@id="account-stats"]');
                    await driver.sleep(3000);

                    let clickLikes = await page.findByCSS('#account-stats > header > div.button-container > div > a:nth-child(2)');
                    await clickLikes.click();
                    let checkLikes = await page.checkElementByCSS('#account-stats > header > div.button-container > div > a:nth-child(2).active');
                    if (checkLikes) addContext(this.ctx, 'Likes tab click success');
                    else addContext(this.ctx, 'Likes tab click failed');

                    let clickComments = await page.findByCSS('#account-stats > header > div.button-container > div > a:nth-child(3)');
                    await clickComments.click();
                    let checkComments = await page.checkElementByCSS('#account-stats > header > div.button-container > div > a:nth-child(3).active');
                    if (checkComments) addContext(this.ctx, 'Comments tab click success');
                    else addContext(this.ctx, 'Comments tab click failed');

                    let clickViews = await page.findByCSS('#account-stats > header > div.button-container > div > a:nth-child(1)');
                    await clickViews.click();
                    let checkViews = await page.checkElementByCSS('#account-stats > header > div.button-container > div > a:nth-child(1).active');
                    if (checkViews) addContext(this.ctx, 'Views tab click success');
                    else addContext(this.ctx, 'Views tab click failed');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No youtube tracker found or added'
                        }
                    });
                }
            });

            it('Click on different tabs in Subscriber Growth', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let hideHeader = await page.findById('header-container');
                    await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);

                    await page.scrollPage('//*[@id="follower-growth"]');
                    await driver.sleep(3000);

                    let clickChange = await page.findByCSS('#follower-growth > header > div > div > a:nth-child(2)');
                    await clickChange.click();
                    let checkChange = await page.checkElementByCSS('#follower-growth > header > div > div > a:nth-child(2).active');
                    if (checkChange) addContext(this.ctx, 'Subscriber Change tab click success');
                    else addContext(this.ctx, 'Subscriber Change tab click failed');

                    let clickCount = await page.findByCSS('#follower-growth > header > div > div > a:nth-child(1)');
                    await clickCount.click();
                    let checkCount = await page.checkElementByCSS('#follower-growth > header > div > div > a:nth-child(1).active');
                    if (checkCount) addContext(this.ctx, 'Subscriber Count tab click success');
                    else addContext(this.ctx, 'Subscriber Count tab click failed');
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No youtube tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();