/**
 * ### TEST CASE:
 * I-AT-Videos-Y010
 *
 * ### TEST TITLE:
 * Date Range persists to URL
 *
 * ### TEST SUMMARY:
 * when changing the time range, date range should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Date Range persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Date Range persists to URL', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                  	let totalPosts = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts = await totalPosts.getText();

                    let checkEle = await page.checkElementByCSS('#AT-Posts > div > div > div.info > div > div.date-dropdown--disabled');

                    if (checkEle) {
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Test status': 'We can only provide up to 30 days worth of data for any unauthenticated YouTube account.'
                            }
                        });
                    }
                    else {
                      	const allowedDays = [30, 90, 365]
                        const days = allowedDays[Math.floor(Math.random() * allowedDays.length)]

                        let changeDate = await setDateRangeAccount(days)

                        if (changeDate) {
                        	let totalPostsAfter = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
    	                	totalPostsAfter = await totalPostsAfter.getText();

                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                	'Total posts before': totalPosts,
                                    'Selected date period': days,
                                    'Total posts after': totalPostsAfter,
                                    'Test status': 'Success'
                                }
                            });

                            let currentURL = await driver.getCurrentUrl();
                            let params = currentURL.substring(currentURL.indexOf("?")+1);
                           
                            if (params.indexOf('days') > -1){

                                addContext(this.ctx, 'Date range added to URL.,');

                                await driver.navigate().refresh();
                                await driver.wait(until.elementLocated(By.id('update'), 6000));

                                if (params.indexOf('days') > -1){
                                    addContext(this.ctx, 'Date range persisted upon reload.');
                                }
                            }
                            else{
                                addContext(this.ctx, {
                                    title: 'Test Results',
                                    value: {
                                        'Tracker name': trackerName,
                                        'Test status': 'Date range not added to URL.'
                                    }
                                });
                            }   
                        }
                        else {
                            addContext(this.ctx, {
                                title: 'Test Results',
                                value: {
                                    'Test status': 'Date selection failed'
                                }
                            });
                        }
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No youtube tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();