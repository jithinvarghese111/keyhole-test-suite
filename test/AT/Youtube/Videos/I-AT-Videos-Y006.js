/**
 * ### TEST CASE:
 * I-AT-Videos-T006
 *
 * ### TEST TITLE:
 * Downloading XLS
 *
 * ### TEST SUMMARY:
 * User should be able to download an excel file of social media account tracker information locally
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Downloading XLS', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking of downloading XLS', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);

                    let hash = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child(14) > td.trackerList__manageCheck > input[type=checkbox]');
                    hash = await hash.getAttribute("value");
                    
                    let test_result = '';
                    const days = 30;
                    const toDate = new Date();
                    const fromDate = new Date();
                    fromDate.setDate(toDate.getDate() - days);
                    const toDateStr = toDate.toJSON().slice(0, 10).replace(/-/g, '');
                    const fromDateStr = fromDate.toJSON().slice(0, 10).replace(/-/g, '');
                    
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                    let tracker = await page.findByCSS('#AT-Dashboard > div > div > div.info > p > span');
                    tracker = await tracker.getText();
                    
                    let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                    await clickPost.click();
                    await driver.wait(until.elementLocated(By.css('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total'), 20000));

                    let xlsLink = await page.findById('update');
                    await xlsLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Posts"]/div/div/div[3]/p/span'), 3000));
                    
                    const downloadPath = config.downloadPath+'\/'+'Keyhole_'+tracker+'_'+'('+hash+')_'+fromDateStr+'-'+toDateStr+'.xlsx';
                    const filename = `Keyhole_${tracker}_(${hash})_${new Date().getTime()}.xls`;
                    const renamePath = config.downloadPath+'\/'+filename;
                    
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Tracker name': trackerName,
                            'Downloade path': downloadPath,
                            'Renamed path': renamePath,
                            'Test status': 'Downloaded file'
                        }
                    });
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No youtube tracker found or added'
                        }
                    });
                }

            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();