/**
 * ### TEST CASE:
 * I-AT-Videos-Y013
 *
 * ### TEST TITLE:
 * Checking links in the video post details modal
 *
 * ### TEST SUMMARY:
 * User should be able to click on the video post and check the links inside the modal are working or not.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Checking links in the video post details modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('click on the video post and check the links inside the modal are working or not.', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                    let clickVideo = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                    await clickVideo.click();
                    await driver.wait(until.elementLocated(By.css('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total'), 30000));

                  	let totalVideos = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalVideos = await totalVideos.getText();
	                addContext(this.ctx, 'Total post: '+totalVideos);

                  	if (totalVideos > 5) totalVideos = 5;

                    for (let i = 1; i <= totalVideos; i++) {
                        addContext(this.ctx, '-----------------------------');

                        let clickVideo = await page.findByCSS('#all-posts > div > div.table-container > table > tbody > tr:nth-child('+i+')');
                        await clickVideo.click();
                        await driver.sleep(1000);

                        let checkLink = await page.checkElementByCSS('#post-view > div.content > div > div > div.post-description > a:nth-child(1)');
                        if (checkLink) {
                            let linkClick = await page.findByCSS('#post-view > div.content > div > div > div.post-description > a:nth-child(1)');
                            let linkselected = await linkClick.getAttribute("href");
                            addContext(this.ctx, 'URL before click #'+i+': '+linkselected);
                            await linkClick.click();
                            addContext(this.ctx, 'Clicked on link');

                            let tab1, tab2;

                            await driver.getAllWindowHandles().then(function(windowHandles) {
                                tab1 = windowHandles[0];
                                tab2 = windowHandles[1];                
                            });

                            await driver.switchTo().window(tab2);
                            let url = await driver.getCurrentUrl();
                            addContext(this.ctx, 'URL after click #'+i+': '+url);

                            if (url == selected) addContext(this.ctx, '#'+i+': Both are equal');
                            else addContext(this.ctx, '#'+i+': Both are not equal');

                            await driver.switchTo().window(tab1);
                            await driver.wait(until.elementLocated(By.css('#post-view > div.top-bar > div > div.x-button'), 20000));
                        }
                        
                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No youtube tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();