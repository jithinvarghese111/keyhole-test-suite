/**
 * ### TEST CASE:
 * I-AT-Videos-Y004
 *
 * ### TEST TITLE:
 * Filter posts with advanced filters option
 *
 * ### TEST SUMMARY:
 * User should be able to filter the post using advanced filter options
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Filter posts', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Filter posts using advanced filter', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');
                await driver.sleep(1000);

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.sleep(10000);

                  	let clickPost = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                  	await clickPost.click();
                  	await driver.sleep(3000);

                  	let totalPosts = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts = await totalPosts.getText();
	                addContext(this.ctx, 'Total post before filter: '+totalPosts);

                  	let clickAdv = await page.findByCSS('#filters-trigger');
                  	await clickAdv.click();
                  	await driver.sleep(2000);

                  	let randomFirst = Math.floor(Math.random() * 8) + 1;
                  	let clickFirst = await page.findByCSS('#day_week');
                  	await clickFirst.click();
                  	let selectAny = await page.findByCSS('#day_week > option:nth-child('+randomFirst+')');
                  	await selectAny.click();
                  	await driver.sleep(2000);

                  	let filterPost = await page.findByCSS('#filter-posts-button');
                  	await filterPost.click();
                  	await driver.sleep(2000);

                  	let totalPosts1 = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
                    totalPosts1 = await totalPosts1.getText();
                    addContext(this.ctx, 'Total post after day of post filter: '+totalPosts1);

                  	await clickAdv.click();
                  	await driver.sleep(2000);

                  	let resetFilter = await page.findByCSS('#reset-filters');
                  	await resetFilter.click();
                  	await driver.sleep(2000);

                  	await clickAdv.click();
                  	await driver.sleep(2000);

                  	let randomSecond = Math.floor(Math.random() * 13) + 1;
                  	let timeOfPost = await page.findByCSS('#hour');
                  	await timeOfPost.click();
                  	let selectAny1 = await page.findByCSS('#hour > option:nth-child('+randomSecond+')');
                  	await selectAny1.click();
                  	await driver.sleep(2000);

                  	let filterPost1 = await page.findByCSS('#filter-posts-button');
                  	await filterPost1.click();
                  	await driver.sleep(2000);

                  	let totalPosts2 = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalPosts2 = await totalPosts2.getText();
	                addContext(this.ctx, 'Total post after time of post filter: '+totalPosts2);

	                await clickAdv.click();
                  	await driver.sleep(2000);

                  	await resetFilter.click();
                  	await driver.sleep(2000);
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No youtube tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();