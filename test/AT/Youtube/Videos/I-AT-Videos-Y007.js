/**
 * ### TEST CASE:
 * I-AT-Videos-Y007
 *
 * ### TEST TITLE:
 * Pagination persists to URL
 *
 * ### TEST SUMMARY:
 * When trying with pagination , Page should persist to the URL
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Page persists to URL', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Checking Page persists to URL', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                    let clickVideos = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                    await clickVideos.click();
                    await driver.sleep(5000);

                    await page.scrollPage('//*[@id="all-posts"]/div/div[1]/table/tbody/tr[10]');
                    await driver.sleep(5000);

                    let pagination = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > div.pagination > div > a.page-2');
                    await pagination.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="all-posts"]/div/div[1]/table/tbody/tr[10]'), 20000)); 

                    let currentURL = await driver.getCurrentUrl();
                    let params = currentURL.substring(currentURL.indexOf("?")+1);
                    let firstParam = params.substr(0, params.lastIndexOf('='));
                    
                    if (firstParam.indexOf('page') > -1){

                        addContext(this.ctx, 'Page added to URL.,');

                        await driver.navigate().refresh();
                        await driver.wait(until.elementLocated(By.id('update'), 6000));

                        if (firstParam.indexOf('page') > -1){
                            addContext(this.ctx, 'Page persisted upon reload.');
                        }
                    }
                    else{
                        addContext(this.ctx, {
                            title: 'Test Results',
                            value: {
                                'Tracker name': trackerName,
                                'Test status': 'Page not added to URL.'
                            }
                        });     
                    }    
                }
                else {
                    addContext(this.ctx, {
                        title: 'Test Results',
                        value: {
                            'Test status': 'No Youtube tracker found or added'
                        }
                    });
                }
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();