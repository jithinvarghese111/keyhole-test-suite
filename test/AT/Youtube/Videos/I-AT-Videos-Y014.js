/**
 * ### TEST CASE:
 * I-AT-Videos-Y014
 *
 * ### TEST TITLE:
 * Check Video played is played inside video Post Modal
 *
 * ### TEST SUMMARY:
 * User should be able to click on the video post and Check Video played inside video Post Modal.
 *
 */

const addContext = require('mochawesome/addContext');
const {Builder, By, until} = require('selenium-webdriver');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = require('assert');
const Page = require('../../../../lib/basePage');
const config = require('../../../../utils/config');
const generate = require('../../../../utils/generate')();
const keyhole = require('../../../../utils/keyhole')();
const expect = chai.expect;
chai.use(chaiAsPromised);

let randomKeyword = randomWord(), selected = '';

process.on('unhandledRejection', () => { });

(async function example() {
    try {
        describe('Check Video played is played inside video Post Modal', async function () {
            beforeEach(async () => {
                page = new Page();
                driver = page.driver;

                addContext(this.ctx, {
                    title: 'beforeEach context',
                    value: 'Test Started'
                });

                await page.gotoLogin();
            });

            it('Check Video played is played inside video Post Modal', async () => {
                await page.redirectToAccDashboard();

                let status = '';

                let hideHeader = await page.findById('header-container');
                await driver.executeScript("arguments[0].setAttribute('style','display:none')", hideHeader);
                await driver.wait(until.elementLocated(By.xpath('//*[@id="account-trackers"]/div[1]/h1'), 3000));

                let trackerPos = await accountTrackerSelect('youtube');

                if (trackerPos) {
                    var clickLink = await page.findByCSS('#account-trackers > div.my-tracks__trackersAndFilters > div.trackers > table > tbody > tr:nth-child('+trackerPos+') > td.tracker-info > div.name > a');
                    await driver.executeScript("arguments[0].scrollIntoView();", clickLink);
                    await driver.sleep(1000);
                    let trackerName = await clickLink.getText();
                    await clickLink.click();
                    await driver.wait(until.elementLocated(By.xpath('//*[@id="AT-Dashboard"]/div/div/div[3]/p/span'), 20000));

                    let clickVideo = await page.findByCSS('#keyhole-global-sidebar > div > div.sidebar__item.item__at > ul > li:nth-child(3) > a');
                    await clickVideo.click();
                    await driver.wait(until.elementLocated(By.css('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total'), 30000));

                  	let totalVideos = await page.findByCSS('#all-posts > div > div.keyhole-pagination.posts-pagination > p > span.total');
	                totalVideos = await totalVideos.getText();
                    totalVideos = totalVideos.replace (/,/g, "");
	                addContext(this.ctx, 'Total post: '+totalVideos);

                  	if (totalVideos > 5) totalVideos = 5;

                    for (let i = 1; i <= 5; i++) {
                        addContext(this.ctx, '-----------------------------');

                        let clickVideo = await page.findByCSS('#all-posts > div > div.table-container > table > tbody > tr:nth-child('+i+')');
                        await clickVideo.click();
                        await driver.sleep(1000);

                        await page.movetoEle('#post-view > div.content > div > a > iframe');
                        let videoClick = await page.findByCSS('#post-view > div.content > div > a > iframe');
                        await videoClick.click();
                        addContext(this.ctx, 'Video Played for post '+i+''); 
                        
                        let closeModal = await page.findByCSS('#post-view > div.top-bar > div > div.x-button');
                        await closeModal.click();
                        await driver.sleep(1000);
                    }
                }
                else assert.fail('No youtube tracker found or added');
                    
            });

            afterEach(async () => {
                addContext(this.ctx, {
                    title: 'afterEach context',
                    value: 'Test Completed'
                });

                await page.quit();
            });
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }
})();